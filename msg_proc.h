/*****************************************************************************
 * C-Plan Concentrator
 *
 * (c) Copyright 2006-2007, Shenzhen Kaifa Technology Co.,Ltd.
 * All Rights Reserved
 *
 *****************************************************************************/

#ifndef _MSG_PROC_H
#define _MSG_PROC_H

#include "typedef.h"

typedef struct st_msg
{
	BYTE code; /*link layer function code*//*Control Field*/
	BYTE addr[5]; // address
	BYTE afn; // application layer function code
	BYTE seq; // frame sequence
	WORD size; // size of data
	BYTE *data; // pointer of the data
	BYTE aux_flag; // exist flag, bit 0 for pw, bit 1 for ec, bit 2 for tp
	BYTE aux_pw[2]; /* additional field for password *//*注意:规约中PW是16byte*/
	BYTE aux_ec[2]; /* additional field for event count */
	BYTE aux_tp[6]; // additional field for time stamp
} ST_MSG;

int check_address(const BYTE *buf, int len);

int check_packet(const BYTE *buf, int buf_len);

int packet_any(BYTE *buf, int len, BYTE code, BYTE afn, BYTE seq_no,
		const BYTE *data, int data_len);

int packet(BYTE *buf, int len, BYTE afn, BYTE seq_no, BYTE fn);

void fep_response(BYTE seq_no);

void msg_proc(int in_idx, int out_idx, int max_out_len);

#endif /* _MSG_PROC_H */
