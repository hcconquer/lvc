/*
 *  C-Plan Concentrator
 *
 *  Copyright (C) 2006-2007, Shenzhen Kaifa Technology Co.,Ltd.
 *
 *  Author: dajiang wan
 *  Created on: 2008-01-08
 */
#include "afn.h"
#include "msg_proc.h"
#include "f_param.h"
#include "f_alm.h"
#include "err_code.h"
#include "common.h"
#include "misc.h"
#include "dwl_con.h"
#include "device.h"
#include "global.h"
#include "threads.h"
#include "mt_sync.h"
#include "t_mt.h"
#include "t_485.h"
#include "mt_485.h"

#define MAX_USED_HOUR		(380 * 200 * 2 / 10)
#define MAX_USED_DAY		(24 * MAX_USED_HOUR)
#define MAX_USED_MONTH		(31 * MAX_USED_DAY)
#define MAX_ENERGY			99999999

static int fill_read_date_time(BYTE *buf, int len)
{
	struct tm tm;

	if (len >= 5) {
		my_time(&tm);
		buf[0] = bin_to_bcd(tm.tm_min);
		buf[1] = bin_to_bcd(tm.tm_hour);
		buf[2] = bin_to_bcd(tm.tm_mday);
		buf[3] = bin_to_bcd(tm.tm_mon + 1);
		buf[4] = bin_to_bcd(tm.tm_year % 100);
		return 5;
	}
	else {
		return 0;
	}
}

static int fill_read_date_time_tariff(BYTE *buf, int len, int tariff)
{
	struct tm tm;

	if (len >= 6) {
		my_time(&tm);
		buf[0] = bin_to_bcd(tm.tm_min);
		buf[1] = bin_to_bcd(tm.tm_hour);
		buf[2] = bin_to_bcd(tm.tm_mday);
		buf[3] = bin_to_bcd(tm.tm_mon + 1);
		buf[4] = bin_to_bcd(tm.tm_year % 100);
		buf[5] = tariff;
		return 6;
	}
	else {
		return 0;
	}
}

static void time_mt_to_1(const BYTE *in_buf, int in_len, BYTE *out_buf,
	int max_len)
{
	memcpy(out_buf, in_buf, 3);
	out_buf[3] = in_buf[4];
	out_buf[4] = (in_buf[3] << 5) | in_buf[5];
	out_buf[5] = in_buf[6];
}

static void create_time_stamp(time_t tt, BYTE *time_stamp)
{
	struct tm tm;

	localtime_r(&tt, &tm);
	time_stamp[0] = bin_to_bcd(tm.tm_year % 100);
	time_stamp[1] = bin_to_bcd(tm.tm_mon + 1);
	time_stamp[2] = bin_to_bcd(tm.tm_mday);
	time_stamp[3] = bin_to_bcd(tm.tm_hour);
	time_stamp[4] = bin_to_bcd(tm.tm_min);
}

static int check_today(const BYTE *time)
{
	struct tm tm;

	my_time(&tm);
	return (bin_to_bcd(tm.tm_year % 100) == time[0])
		&& (bin_to_bcd(tm.tm_mon + 1) == time[1])
		&& (bin_to_bcd(tm.tm_mday) == time[2]);
}

static int used_energy(int val1, int val2, int max_used)
{
	int wrap = MAX_ENERGY + 1;

	if (val1 - val2 >= 0) // not wraped
		return val1 - val2;
	if (val1 - val2 + wrap < max_used) // wraped
		return val1 - val2 + wrap;
	return val1 - val2; // bad value
}

static int mt485_sub1(BYTE pn, BYTE *out_buf, int max_len, int di1, int di2,
	int di3, int di4)
{
	BYTE data[128], *ptr = out_buf, *ptr2;
	int tmp, cnt, len = max_len;
	MTID mtid;

	t485_lock();
	t485_get_mtid(pn - 1, mtid);
	t485_unlock();
	tmp = mt485_read(mtid, data, sizeof(data), di1);
	if (tmp > 0) {
		cnt = tmp / 4;
		if ((tmp = fill_read_date_time_tariff(ptr, len, cnt - 1)) > 0) {
			ptr += tmp; len -= tmp;
			if (len >= cnt * 5) {
				ptr2 = data;
				for (tmp = 0; tmp < cnt; tmp ++) { // from format 14 to 11
					ptr[0] = 0;
					memcpy(ptr + 1, ptr2, 4);
					ptr2 += 4;
					ptr += 5;
					len -= 5;
				}
				tmp = mt485_read_arg(mtid, ptr, len, 3, di2, di3, di4);
				if (tmp > 0) {
					ptr += tmp;
					return ptr - out_buf;
				}
			}
		}
	}
	return 0;
}

static int mt485_sub2(BYTE pn, BYTE *out_buf, int max_len, int di1_len,
	int di1, int di2, int di3, int di4)
{
	BYTE *ptr = out_buf, *tariff_cnt;
	int tmp, len = max_len;
	MTID mtid;

	t485_lock();
	t485_get_mtid(pn - 1, mtid);
	t485_unlock();
	if ((tmp = fill_read_date_time_tariff(ptr, len, 0)) > 0) {
		tariff_cnt = ptr + 5;
		ptr += tmp; len -= tmp;
		if ((tmp = mt485_read(mtid, ptr, len, di1)) > 0) {
			ptr += tmp; len -= tmp;
			*tariff_cnt = (tmp / di1_len) - 1;
			if ((tmp = mt485_read_arg(mtid, ptr, len, 3, di2, di3, di4)) > 0) {
				ptr += tmp;
				return ptr - out_buf;
			}
		}
	}
	return 0;
}

static int mt485_sub3(BYTE pn, BYTE *out_buf, int max_len, WORD di,
	int offset, int max_used)
{
	int idx, tmp, cnt, val, val1, val2, ret1, ret2, di_len = 4;
	BYTE buf[128], *ptr, *ptr1, *ptr2;
	MTID mtid;
	T485_DATA day_data;

	t485_lock();
	t485_get_mtid(pn - 1, mtid);
	t485_get_day_data(pn - 1, &day_data);
	t485_unlock();
	if (day_data.flag && check_today(day_data.time)) {
		if ((tmp = mt485_read(mtid, buf, sizeof(buf), di)) > 0) {
			cnt = tmp / di_len;
			ptr = out_buf, ptr1 = buf, ptr2 = ((BYTE *)&day_data) + offset;
			if (max_len >= 1 + cnt * di_len) {
				*ptr ++ = cnt - 1;
				for (idx = 0; idx < cnt; idx ++) {
					ret1 = bcd_ctol(ptr1, &val1);
					ret2 = bcd_ctol(ptr2, &val2);
					val = used_energy(val1, val2, max_used) * 100;
					if (ret1 == 0 || ret2 == 0 || val < 0)
						memset(ptr, INVALID_VALUE, di_len);
					else
						bcd_ltoc(ptr, val);
					ptr += di_len, ptr1 += di_len, ptr2 += di_len;
				}
				return ptr - out_buf;
			}
		}
	}
	return 0;
}

static int mt485_sub4(BYTE pn, BYTE *out_buf, int max_len, WORD di1, WORD di2,
	int max_used)
{
	int idx, tmp, cnt, val, val1, val2, ret1, ret2, di_len = 4;
	BYTE buf[128], *ptr, *ptr1, *ptr2;
	MTID mtid;

	t485_lock();
	t485_get_mtid(pn - 1, mtid);
	t485_unlock();
	if ((tmp = mt485_read_arg(mtid, buf, sizeof(buf), 2, di1, di2)) > 0) {
		cnt = tmp / (2 * di_len);
		ptr = out_buf, ptr1 = buf, ptr2 = buf + (tmp / 2);
		if (max_len >= 1 + cnt * di_len) {
			*ptr ++ = cnt - 1;
			for (idx = 0; idx < cnt; idx ++) {
				ret1 = bcd_ctol(ptr1, &val1);
				ret2 = bcd_ctol(ptr2, &val2);
				val = used_energy(val1, val2, max_used) * 100;
				if (ret1 == 0 || ret2 == 0 || val < 0)
					memset(ptr, INVALID_VALUE, di_len);
				else
					bcd_ltoc(ptr, val);
				ptr += di_len, ptr1 += di_len, ptr2 += di_len;
			}
			return ptr - out_buf;
		}
	}
	return 0;
}

static int mt485_sub5(BYTE pn, BYTE fn, BYTE *out_buf, int max_len,
	int di_len, int fill_len, int offset)
{
	BYTE frzn, time_stamp[5], *ptr = out_buf;
	int i, cnt, interval, one_hour = 60 * 60;
	time_t tt;

	if (max_len > 1 + 4 * (di_len + fill_len)) { // maximum length
		t485_lock();
		frzn = t485_find_frzn(pn - 1, fn);
		t485_unlock();
		if (frzn >= 1 && frzn <= 3) {
			if (frzn == 1)
				cnt = 4;
			else if (frzn == 2)
				cnt = 2;
			else
				cnt = 1;
			interval = one_hour / cnt;
			time(&tt);
			tt = tt - (tt % 3600) - one_hour; // previous hour start
			create_time_stamp(tt, time_stamp);
			*ptr ++ = (frzn << 6) + time_stamp[3];
			for (i = 0; i < cnt; i ++) {
				create_time_stamp(tt + i * interval, time_stamp);
				PRINTF("frzn:%d, fill_len:%d, i:%d, interval:%d\n",
					frzn, fill_len, i, interval);
				PRINTB("time_stamp", time_stamp, 5);
				if (!t485_fill_frzn_data(pn - 1, time_stamp, di_len, offset,
					ptr + fill_len)) {
					memset(ptr, INVALID_VALUE, fill_len + di_len);
				}
				else {
					memset(ptr, 0x00, fill_len);
				}
				ptr += (fill_len + di_len);
			}
		}
	}
	return ptr - out_buf;
}

static int mt485_sub6(BYTE pn, BYTE fn, BYTE *out_buf, int max_len,
	int offset, int max_used)
{
	BYTE frzn, time_stamp[5], tmp1[4], tmp2[4], *ptr = out_buf;
	int i, cnt, interval, val, val1, val2, one_hour = 60 * 60, di_len = 4;
	time_t tt;

	if (max_len > 1 + 4 * di_len) { // maximum length
		t485_lock();
		frzn = t485_find_frzn(pn - 1, fn);
		t485_unlock();
		if (frzn >= 1 && frzn <= 3) {
			if (frzn == 1)
				cnt = 4;
			else if (frzn == 2)
				cnt = 2;
			else
				cnt = 1;
			interval = one_hour / cnt;
			time(&tt);
			tt = tt - (tt % 3600) - one_hour; // previous hour start
			create_time_stamp(tt, time_stamp);
			if (!t485_fill_frzn_data(pn - 1, time_stamp, di_len, offset, tmp1)
				|| !bcd_ctol(tmp1, &val1))
				return 0;
			*ptr ++ = (frzn << 6) + time_stamp[3];
			for (i = 0; i < cnt; i ++) {
				create_time_stamp(tt + i * interval, time_stamp);
				PRINTF("frzn:%d, i:%d, interval:%d\n", frzn, i, interval);
				PRINTB("time_stamp", time_stamp, 5);
				if (!t485_fill_frzn_data(pn - 1, time_stamp, di_len, offset,
					tmp2) || !bcd_ctol(tmp2, &val2)) {
					memset(ptr, 0xEE, di_len);
				}
				else if ((val = used_energy(val2, val1, max_used) * 100) < 0) {
					memset(ptr, 0xEE, di_len);
				}
				else {
					bcd_ltoc(ptr, val);
				}
				ptr += di_len;
			}
		}
	}
	return ptr - out_buf;
}

static int mt485_sub7(BYTE pn, BYTE fn, BYTE *out_buf, int max_len,
	int di_len, int offset)
{
	BYTE frzn, time_stamp[5], *ptr = out_buf;
	int i, cnt, interval, val, one_hour = 60 * 60;
	time_t tt;

	if (max_len > 1 + 4 * di_len) { // maximum length
		t485_lock();
		frzn = t485_find_frzn(pn - 1, fn);
		t485_unlock();
		if (frzn >= 1 && frzn <= 3) {
			if (frzn == 1)
				cnt = 4;
			else if (frzn == 2)
				cnt = 2;
			else
				cnt = 1;
			interval = one_hour / cnt;
			time(&tt);
			tt = tt - (tt % 3600) - one_hour; // previous hour start
			create_time_stamp(tt, time_stamp);
			*ptr ++ = (frzn << 6) + time_stamp[3];
			for (i = 0; i < cnt; i ++) {
				PRINTF("frzn:%d, i:%d, interval:%d\n", frzn, i, interval);
				PRINTB("time_stamp", time_stamp, 5);
				create_time_stamp(tt + i * interval, time_stamp);
				if (!t485_fill_frzn_data(pn - 1, time_stamp, di_len, offset,
					ptr)) {
					memset(ptr, 0xEE, di_len);
				}
				else {
					// meter format to format 7
					bcd_ctos(ptr, &val); bcd_stoc(ptr, val * 10);
				}
				ptr += di_len;
			}
		}
	}
	return ptr - out_buf;
}

static int afn0c_f1(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	int tmp;

	*param_len = 0;
	fparam_lock();
	tmp = fparam_read(FPARAM_AFN0C_F1, out_buf, max_len);
	fparam_unlock();
	return tmp <= 0 ? 0 : tmp;
}

static int afn0c_f2(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	int tmp;

	*param_len = 0;
	tmp = get_date_time(out_buf, max_len);
	return tmp <= 0 ? 0 : tmp;
}

static int afn0c_f3(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	int tmp;

	*param_len = 0;
	fparam_lock();
	tmp = fparam_read(FPARAM_AFN0C_F3, out_buf, max_len);
	fparam_unlock();
	return tmp <= 0 ? 0 : tmp;
}

static int afn0c_f4(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	BYTE data[128];
	int tmp, tmp2;

	*param_len = 0;
	fparam_lock();
	tmp = fparam_read(FPARAM_ALLOW_REMOTE, data, 1);
	tmp2 = fparam_read(FPARAM_ALLOW_SPONT, data + 1, 1);
	fparam_unlock();
	if (max_len < 1 || tmp <= 0 || tmp2 <= 0)
		return 0;
	tmp = (data[0] == 0) ? (2 << 2) : (1 << 2);
	tmp2 = (data[1] == 0) ? 2 : 1;
	out_buf[0] = tmp | tmp2;
	return 1;
}

static int afn0c_f5(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{ // TODO 0
	*param_len = 0;
	return 0;
}

static int afn0c_f6(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{ // TODO 0
	*param_len = 0;
	return 0;
}

static int afn0c_f7(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	*param_len = 0;
	if (max_len >= 2) {
		falm_lock();
		falm_get_ec(0x0e, out_buf);
		falm_unlock();
		return 2;
	}
	return 0;
}

static int afn0c_f8(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	BYTE val[8];
	int tmp;

	*param_len = 0;
	fparam_lock();
	fparam_get_value(FPARAM_AFN0C_F8, val, 8);
	memset(val, 0, 8);
	fparam_set_value(FPARAM_AFN0C_F8, val, 8, &tmp);
	fparam_unlock();
	return 0;
}

static int afn0c_f9(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	BYTE val[2];
	int tmp;

	*param_len = 0;
	fparam_lock();
	fparam_get_value(FPARAM_AFN0C_F9, val, 2);
	val[1] = 0;
	fparam_set_value(FPARAM_AFN0C_F9, val, 2, &tmp);
	fparam_unlock();
	return 0;
}

static int afn0c_f17(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{ // TODO
	*param_len = 0;
	return 0;
}

static int afn0c_f18(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{ // TODO
	*param_len = 0;
	return 0;
}

static int afn0c_f19(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{ // TODO
	*param_len = 0;
	return 0;
}

static int afn0c_f20(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{ // TODO
	*param_len = 0;
	return 0;
}

static int afn0c_f21(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{ // TODO
	*param_len = 0;
	return 0;
}

static int afn0c_f22(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{ // TODO
	*param_len = 0;
	return 0;
}

static int afn0c_f23(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{ // TODO
	*param_len = 0;
	return 0;
}

static int afn0c_f24(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{ // TODO
	*param_len = 0;
	return 0;
}

static int afn0c_f25(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	BYTE buf[128], *ptr1 = buf, *ptr2 = out_buf;
	int i, tmp, val, len = max_len;
	MTID mtid;

	*param_len = 0;
	t485_get_mtid(pn - 1, mtid);
	if ((tmp = fill_read_date_time(ptr2, len)) > 0) {
		ptr2 += tmp; len -= tmp;
		tmp = mt485_read_arg(mtid, buf, sizeof(buf), 18, 0xb630, 0xb631,
			0xb632, 0xb633, 0xb640, 0xb641, 0xb642, 0xb643, 0xb650, 0xb651,
			0xb652, 0xb653, 0xb611, 0xb612, 0xb613, 0xb621, 0xb622, 0xb623);
		if (tmp == 12 + 8 + 20 && len >= tmp + 2 + 4) {
			memcpy(ptr2, ptr1, 12); // 0xb630--0xb633
			ptr2 += 12; ptr1 += 12;
			for (i = 0; i < 4; i ++) {
				// 0xb640--0xb643, from format XX.XX to XX.XXXX
				*ptr2 ++ = 0x00;
				memcpy(ptr2, ptr1, 2);
				ptr2 += 2; ptr1 += 2;
			}
			for (i = 0; i < 4; i ++) {
				// 0xb650--0xb653, from format X.XXX to XXX.X
				bcd_ctos(ptr1, &val);
				val = (int)((val / 1000.0 + 0.05) * 10.0);
				bcd_stoc(ptr2, val);
				ptr2 += 2; ptr1 += 2;
			}
			for (i = 0; i < 3; i ++) {
				// 0xb611--0xb613, from format 0XXX to XXX.X
				bcd_ctos(ptr1, &val);
				val = val * 10;
				bcd_stoc(ptr2, val);
				ptr2 += 2; ptr1 += 2;
			}
			memcpy(ptr2, ptr1, 6); ptr2 += 6; ptr1 += 6; // 0xb621--0xb623
			*ptr2 ++ = 0; *ptr2 ++ = 0; // null phase current
			return ptr2 - out_buf;
		}
	}
	return 0;
}

static int afn0c_f26(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	BYTE *ptr = out_buf;
	int tmp, len = max_len;
	MTID mtid;

	*param_len = 0;
	t485_get_mtid(pn - 1, mtid);
	if ((tmp = fill_read_date_time(ptr, len)) > 0) {
		ptr += tmp; len -= tmp;
		tmp = mt485_read_arg(mtid, ptr, len, 16, 0xb310, 0xb311,
			0xb312, 0xb313, 0xb320, 0xb321, 0xb322, 0xb323, 0xb330, 0xb331,
			0xb332, 0xb333, 0xb340, 0xb341, 0xb342, 0xb343);
		if (tmp > 0) {
			ptr += tmp;
			return ptr - out_buf;
		}
	}
	return 0;
}

static int afn0c_f27(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	BYTE date_time[7], *ptr = out_buf;
	int tmp, len = max_len;
	MTID mtid;

	*param_len = 0;
	t485_get_mtid(pn - 1, mtid);
	if ((tmp = fill_read_date_time(ptr, len)) > 0) {
		ptr += tmp; len -= tmp;
		tmp = mt485_read_arg(mtid, date_time, 7, 2, 0xc011, 0xc010);
		if (tmp > 0 && len >= 6) {
			time_mt_to_1(date_time, 7, ptr, len);
			ptr += 6; len -= 6;
			tmp = mt485_read_arg(mtid, ptr, len, 7, 0xc020, 0xc021, 0xb210,
				0xb211, 0xb212, 0xb213, 0xb214);
			if (tmp > 0) {
				ptr += tmp;
				return ptr - out_buf;
			}
		}
	}
	return 0;
}

static int afn0c_f33(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	*param_len = 0;
	return mt485_sub1(pn, out_buf, max_len, 0x901f, 0x911f, 0x913f, 0x914f);
}

static int afn0c_f34(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	*param_len = 0;
	return mt485_sub1(pn, out_buf, max_len, 0x902f, 0x912f, 0x915f, 0x916f);
}

static int afn0c_f35(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	*param_len = 0;
	return mt485_sub2(pn, out_buf, max_len, 3, 0xa01f, 0xb01f, 0xa11f, 0xb11f);
}

static int afn0c_f36(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	*param_len = 0;
	return mt485_sub2(pn, out_buf, max_len, 3, 0xa02f, 0xb02f, 0xa12f, 0xb12f);
}

static int afn0c_f37(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	*param_len = 0;
	return mt485_sub1(pn, out_buf, max_len, 0x941f, 0x951f, 0x953f, 0x954f);
}

static int afn0c_f38(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	*param_len = 0;
	return mt485_sub1(pn, out_buf, max_len, 0x942f, 0x952f, 0x955f, 0x956f);
}

static int afn0c_f39(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	*param_len = 0;
	return mt485_sub2(pn, out_buf, max_len, 3, 0xa41f, 0xb41f, 0xa51f, 0xb51f);
}

static int afn0c_f40(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	*param_len = 0;
	return mt485_sub2(pn, out_buf, max_len, 3, 0xa42f, 0xb42f, 0xa52f, 0xb52f);
}

static int afn0c_f41(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	*param_len = 0;
	return mt485_sub3(pn, out_buf, max_len, 0x901f, offsetof(T485_DATA,
		di_901f), MAX_USED_DAY);
}

static int afn0c_f42(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	*param_len = 0;
	return mt485_sub3(pn, out_buf, max_len, 0x911f, offsetof(T485_DATA,
		di_911f), MAX_USED_DAY);
}

static int afn0c_f43(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	*param_len = 0;
	return mt485_sub3(pn, out_buf, max_len, 0x902f, offsetof(T485_DATA,
		di_902f), MAX_USED_DAY);
}

static int afn0c_f44(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	*param_len = 0;
	return mt485_sub3(pn, out_buf, max_len, 0x912f, offsetof(T485_DATA,
		di_912f), MAX_USED_DAY);
}

static int afn0c_f45(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	*param_len = 0;
	return mt485_sub4(pn, out_buf, max_len, 0x901f, 0x941f, MAX_USED_MONTH);
}

static int afn0c_f46(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	*param_len = 0;
	return mt485_sub4(pn, out_buf, max_len, 0x911f, 0x951f, MAX_USED_MONTH);
}

static int afn0c_f47(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	*param_len = 0;
	return mt485_sub4(pn, out_buf, max_len, 0x902f, 0x942f, MAX_USED_MONTH);
}

static int afn0c_f48(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	*param_len = 0;
	return mt485_sub4(pn, out_buf, max_len, 0x912f, 0x952f, MAX_USED_MONTH);
}

static int afn0c_f49(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{ // TODO
	*param_len = 0;
	return 0;
}

static int afn0c_f57(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{ // TODO
	*param_len = 0;
	return 0;
}

static int afn0c_f58(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{ // TODO
	*param_len = 0;
	return 0;
}

static int afn0c_f65(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{ // TODO
	*param_len = 0;
	return 0;
}

static int afn0c_f66(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{ // TODO
	*param_len = 0;
	return 0;
}

static int afn0c_f67(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{ // TODO
	*param_len = 0;
	return 0;
}

static int afn0c_f73(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{ // TODO
	*param_len = 0;
	return 0;
}

static int afn0c_f81(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{ // TODO
	*param_len = 0;
	return 0;
}

static int afn0c_f82(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{ // TODO
	*param_len = 0;
	return 0;
}

static int afn0c_f83(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{ // TODO
	*param_len = 0;
	return 0;
}

static int afn0c_f84(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{ // TODO
	*param_len = 0;
	return 0;
}

static int afn0c_f89(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	*param_len = 0;
	return mt485_sub5(pn, 89, out_buf, max_len, 3, 0, offsetof(T485_DATA,
		di_b630));
}

static int afn0c_f90(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	*param_len = 0;
	return mt485_sub5(pn, 90, out_buf, max_len, 3, 0, offsetof(T485_DATA,
		di_b631));
}

static int afn0c_f91(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	*param_len = 0;
	return mt485_sub5(pn, 91, out_buf, max_len, 3, 0, offsetof(T485_DATA,
		di_b632));
}

static int afn0c_f92(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	*param_len = 0;
	return mt485_sub5(pn, 92, out_buf, max_len, 3, 0, offsetof(T485_DATA,
		di_b633));
}

static int afn0c_f93(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	*param_len = 0;
	return mt485_sub5(pn, 93, out_buf, max_len, 2, 1, offsetof(T485_DATA,
		di_b640));
}

static int afn0c_f94(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	*param_len = 0;
	return mt485_sub5(pn, 94, out_buf, max_len, 2, 1, offsetof(T485_DATA,
		di_b641));
}

static int afn0c_f95(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	*param_len = 0;
	return mt485_sub5(pn, 95, out_buf, max_len, 2, 1, offsetof(T485_DATA,
		di_b642));
}

static int afn0c_f96(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	*param_len = 0;
	return mt485_sub5(pn, 96, out_buf, max_len, 2, 1, offsetof(T485_DATA,
		di_b643));
}

static int afn0c_f97(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	*param_len = 0;
	return mt485_sub7(pn, 97, out_buf, max_len, 2, offsetof(T485_DATA,
		di_b611));
}

static int afn0c_f98(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	*param_len = 0;
	return mt485_sub7(pn, 98, out_buf, max_len, 2, offsetof(T485_DATA,
		di_b612));
}

static int afn0c_f99(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	*param_len = 0;
	return mt485_sub7(pn, 99, out_buf, max_len, 2, offsetof(T485_DATA,
		di_b613));
}

static int afn0c_f100(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	*param_len = 0;
	return mt485_sub5(pn, 100, out_buf, max_len, 2, 0, offsetof(T485_DATA,
		di_b621));
}

static int afn0c_f101(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	*param_len = 0;
	return mt485_sub5(pn, 101, out_buf, max_len, 2, 0, offsetof(T485_DATA,
		di_b622));
}

static int afn0c_f102(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	*param_len = 0;
	return mt485_sub5(pn, 102, out_buf, max_len, 2, 0, offsetof(T485_DATA,
		di_b623));
}

static int afn0c_f103(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{ // TODO
	*param_len = 0;
	return 0;
}

static int afn0c_f105(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	*param_len = 0;
	return mt485_sub6(pn, 105, out_buf, max_len, offsetof(T485_DATA, di_901f),
		MAX_USED_HOUR);
}

static int afn0c_f106(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	*param_len = 0;
	return mt485_sub6(pn, 106, out_buf, max_len, offsetof(T485_DATA, di_911f),
		MAX_USED_HOUR);
}

static int afn0c_f107(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	*param_len = 0;
	return mt485_sub6(pn, 107, out_buf, max_len, offsetof(T485_DATA, di_902f),
		MAX_USED_HOUR);
}

static int afn0c_f108(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	*param_len = 0;
	return mt485_sub6(pn, 108, out_buf, max_len, offsetof(T485_DATA, di_912f),
		MAX_USED_HOUR);
}

static int afn0c_f109(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	*param_len = 0;
	return mt485_sub5(pn, 109, out_buf, max_len, 4, 0, offsetof(T485_DATA,
		di_901f));
}

static int afn0c_f110(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	*param_len = 0;
	return mt485_sub5(pn, 110, out_buf, max_len, 4, 0, offsetof(T485_DATA,
		di_911f));
}

static int afn0c_f111(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	*param_len = 0;
	return mt485_sub5(pn, 111, out_buf, max_len, 4, 0, offsetof(T485_DATA,
		di_902f));
}

static int afn0c_f112(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	*param_len = 0;
	return mt485_sub5(pn, 112, out_buf, max_len, 4, 0, offsetof(T485_DATA,
		di_912f));
}

static int afn0c_f113(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	*param_len = 0;
	return mt485_sub5(pn, 113, out_buf, max_len, 2, 0, offsetof(T485_DATA,
		di_b650));
}

static int afn0c_f114(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	*param_len = 0;
	return mt485_sub5(pn, 114, out_buf, max_len, 2, 0, offsetof(T485_DATA,
		di_b651));
}

static int afn0c_f115(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	*param_len = 0;
	return mt485_sub5(pn, 115, out_buf, max_len, 2, 0, offsetof(T485_DATA,
		di_b652));
}

static int afn0c_f116(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	*param_len = 0;
	return mt485_sub5(pn, 116, out_buf, max_len, 2, 0, offsetof(T485_DATA,
		di_b653));
}

static int afn0c_f121(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{ // TODO
	return 0;
}

static int afn0c_f247(BYTE pn, BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	WORD di;
	MTID mtid;

	if (*param_len < 2)
		return 0;
	t485_get_mtid(pn - 1, mtid);
	di = ctos(param);
	return mt485_read_no_check(mtid, out_buf, max_len, di);
}

static struct st_fn {
	BYTE fn;
	int (*get_fn)(BYTE pn, BYTE *buf, int len, const BYTE *param,
		int *param_len);
} fn_arr[] = {
	{1, afn0c_f1},     {2, afn0c_f2},     {3, afn0c_f3},     {4, afn0c_f4},
	{5, afn0c_f5},     {6, afn0c_f6},     {7, afn0c_f7},     {8, afn0c_f8},
	{9, afn0c_f9},     {17, afn0c_f17},   {18, afn0c_f18},   {19, afn0c_f19},
	{20, afn0c_f20},   {21, afn0c_f21},   {22, afn0c_f22},   {23, afn0c_f23},
	{24, afn0c_f24},   {25, afn0c_f25},   {26, afn0c_f26},   {27, afn0c_f27},
	{33, afn0c_f33},   {34, afn0c_f34},   {35, afn0c_f35},   {36, afn0c_f36},
	{37, afn0c_f37},   {38, afn0c_f38},   {39, afn0c_f39},   {40, afn0c_f40},
	{41, afn0c_f41},   {42, afn0c_f42},   {43, afn0c_f43},   {44, afn0c_f44},
	{45, afn0c_f45},   {46, afn0c_f46},   {47, afn0c_f47},   {48, afn0c_f48},
	{49, afn0c_f49},   {57, afn0c_f57},   {58, afn0c_f58},   {65, afn0c_f65},
	{66, afn0c_f66},   {67, afn0c_f67},   {73, afn0c_f73},   {81, afn0c_f81},
	{82, afn0c_f82},   {83, afn0c_f83},   {84, afn0c_f84},   {89, afn0c_f89},
	{90, afn0c_f90},   {91, afn0c_f91},   {92, afn0c_f92},   {93, afn0c_f93},
	{94, afn0c_f94},   {95, afn0c_f95},   {96, afn0c_f96},   {97, afn0c_f97},
	{98, afn0c_f98},   {99, afn0c_f99},   {100, afn0c_f100}, {101, afn0c_f101},
	{102, afn0c_f102}, {103, afn0c_f103}, {105, afn0c_f105}, {106, afn0c_f106},
	{107, afn0c_f107}, {108, afn0c_f108}, {109, afn0c_f109}, {110, afn0c_f110},
	{111, afn0c_f111}, {112, afn0c_f112}, {113, afn0c_f113}, {114, afn0c_f114},
	{115, afn0c_f115}, {116, afn0c_f116}, {121, afn0c_f121}, {247, afn0c_f247},
};

int afn0c_get_fn(BYTE pn, BYTE fn, BYTE *out_buf, int max_len,
	const BYTE *param, int *param_len)
{
	int i;

	PRINTF("AFN=0CH, Pn=%d, Fn=%d\n", pn, fn);
	for (i = 0; i < sizeof(fn_arr) / sizeof(struct st_fn); i ++) {
		if (fn == fn_arr[i].fn)
			return (fn_arr[i].get_fn)(pn, out_buf, max_len, param, param_len);
	}
	return 0;
}
