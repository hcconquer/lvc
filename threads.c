#include "threads.h"
#include "global.h"
#include "common.h"
#include "lvc_test.h"

#define SIGWATCHDOG		SIGRTMIN
#define THREADS_COUNT	7

typedef void * (*FUNC) (void *arg);

static int pid_watchdog = 0;

static pthread_t th[THREADS_COUNT];

static int reboot_flag[THREADS_COUNT];

static int sched_disable_flag[THREADS_COUNT];

static int thread_idle_flag[THREADS_COUNT];

static FUNC func[THREADS_COUNT] = { 
	fep_thread,
	cascade_thread,
	hhu_thread,
	plc_thread,
	rs485_thread,
	iosys_thread,
	poll_thread,
};

void threads_create(void)
{
	int i;

	for (i = 0; i < THREADS_COUNT; i ++)
		pthread_create(&th[i], NULL, func[i], NULL);
	nice(-20);
}

void threads_join(void)
{
	int i;

	for (i = 0; i < THREADS_COUNT; i ++)
		pthread_join(th[THREADS_COUNT - 1 - i], NULL);
}

static int which_thread(void)
{
	int i;
	pthread_t self;

	self = pthread_self();
	for (i = 0; i < THREADS_COUNT; i ++) {
		if (pthread_equal(self, th[i]))
			return i;
	}
	return 0;
}

void set_reboot_flag(int flag)
{
	int i;

	if (flag) { // only for the same thread
		reboot_flag[which_thread()] = 1;
	}
	else { // all threads
		for (i = 0; i < THREADS_COUNT; i ++)
			reboot_flag[i] = 1;
	}
}

int get_reboot_flag(int flag)
{
	int i;

	if (flag) { // only for the same thread
		if (reboot_flag[which_thread()])
			return 1;
	}
	else { // all threads
		for (i = 0; i < THREADS_COUNT; i ++)
			if (reboot_flag[i])
				return 1;
	}
	return 0;
}

void init_watchdog(void)
{
	pid_watchdog = find_pid("watchdog");
	PRINTF("watchdog pid = %d\n", pid_watchdog);
}

int kill_watchdog(void)
{
	if (pid_watchdog > 0) {
		kill(pid_watchdog, SIGTERM);
		return 1;
	}
	return 0;
}

void notify_watchdog(void)
{
	int i;
	pthread_t self;

	if (pid_watchdog > 0) {
		self = pthread_self();
		for (i = 0; i < THREADS_COUNT; i ++) {
			if (pthread_equal(self, th[i]))
				kill(pid_watchdog, SIGWATCHDOG + i);
		}
	}
}

static char *thread_name[] = {
	"fep_thread",
	"cascade_thread",
	"hhu_thread",
	"plc_thread",
	"rs485_thread",
	"iosys_thread",
	"poll_thread",
};

int disable_thread_sched(int flag)
{
	long idle_time;
	int i;
	
	for (i = 0; i < THREADS_COUNT; i ++) {
		if (i != 2 && i != 5)
			sched_disable_flag[i] = flag;
	}
	if (!flag)
		return 1;
	for (i = 0; i < THREADS_COUNT; i ++) {
		if (i != 2 && i != 5) {
			idle_time = uptime();
			PRINTF("Wait for %s idle\n", thread_name[i]);
			while (!g_terminated && !thread_idle_flag[i]) {
				wait_delay(500);
				notify_watchdog();
				if (uptime() - idle_time >= 30) {
					PRINTF("Wait for %s idle timeout\n", thread_name[i]);
					return 0;
				}
			}
		}
	}
	return 1;
}

void check_sched(void)
{
	int which, flag = 0;

	which = which_thread();
	thread_idle_flag[which] = 1;
	while (!g_terminated && sched_disable_flag[which]) {
		if (flag == 0) {
			PRINTF("%s is idle\n", thread_name[which]);
			flag = 1;
		}
		wait_delay(500);
		notify_watchdog();
	}
	thread_idle_flag[which] = 0;
}
