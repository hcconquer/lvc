/*****************************************************************************
*	C-Plan Concentrator 
*
*	(c) Copyright 2006-2007, Shenzhen Kaifa Technology Co.,Ltd. 
*	All Rights Reserved 
*
*	to manage the meter information 
*
*	Author: fanhua zeng
*	Created on: 2006-07-31
*
*	modify: jianping zhang
*****************************************************************************/

#include "common.h"
#include "misc.h"
#include "t_mt.h"
#include "t_stat.h"
#include "t_rt.h"

static T_MT t_mt[T_MT_ROWS_CNT];
static T_MT	*p_t_mt = t_mt;
static sem_t sem_t_mt;
static int fd_t_mt;

void tmt_lock(void)
{
	sem_wait(&sem_t_mt);
}

void tmt_unlock(void)
{
	sem_post(&sem_t_mt);
}

static void tmt_init_row(int rowno) // initial one line meter record
{
	T_MT *p_mt = p_t_mt + rowno - 1;

	memcpy(p_mt->mtid, INVALID_MTID, MTID_LEN);
	p_mt->mt_type = 0xff;
	p_mt->connect_type = 0;
	p_mt->tariff = 0xff;
	p_mt->mt_state = 0;
	p_mt->line_no = 0;
	p_mt->grp_no = 0;
	memset(p_mt->collect, 0, 6);
}

void tmt_get_mtid(short mtidx, MTID mtid) // find meter ID via meter index
{
	memcpy(mtid, p_t_mt[mtidx].mtid, MTID_LEN);	
}

short tmt_get_mtidx(const MTID mtid) // find meter index via meter ID
{
	int idx;

	for (idx = 0; idx < T_MT_ROWS_CNT; idx ++) {
		if (!tmt_is_empty(idx) && !memcmp(mtid, p_t_mt[idx].mtid, MTID_LEN))
			return idx;
	}
	return -1;
}

int tmt_get_phase(short mtidx) // get phase of meter index
{
	return p_t_mt[mtidx].mt_state & 0x03;
}

void tmt_set_phase(short mtidx, int phase) // set phase of meter index
{
	int state;
	state = p_t_mt[mtidx].mt_state;
	if ((state & 0x03) != (phase & 0x03)) {
		state = (state & (~0x03)) | (phase & 0x03);
		p_t_mt[mtidx].mt_state = state;
		tmt_update(mtidx, 1);
	}
}

/* get tariff count, return 0..4, 0: single tariff, others: tariff count */
int tmt_get_tariff(short mtidx)
{
	BYTE tariff;

	tariff = p_t_mt[mtidx].tariff;
	if (((tariff >> 4) & 0x0f) == 0)
		return 0;
	return tariff & 0x0f;
}

int tmt_is_focus(short mtidx)
{
	return (p_t_mt[mtidx].mt_state & 0x08) == 0x08;
}

int tmt_get_type(short mtidx)
{
	return p_t_mt[mtidx].mt_type;
}

int tmt_set_allow_poweroff(short mtidx, int allow)
{
	BYTE bit = 1 << 5;
	BYTE *mt_state = &p_t_mt[mtidx].mt_state;
	if ((*mt_state & 0x04) != 0x04) // no relay
		return 0;
	if (allow)
		*mt_state = *mt_state & ~bit;
	else
		*mt_state = *mt_state | bit;
	tmt_update(mtidx, 1);
	return 1;
}

int tmt_get_allow_poweroff(short mtidx)
{
	BYTE bit = 1 << 5;
	BYTE mt_state = p_t_mt[mtidx].mt_state;

	if ((mt_state & 0x04) != 0x04) // no relay
		return 0;
	return !(mt_state & bit);
}

int tmt_is_empty(short mtidx)
{
	return (mtidx < 0 || mtidx >= T_MT_ROWS_CNT
		|| memcmp(p_t_mt[mtidx].mtid, INVALID_MTID, MTID_LEN) == 0);
}

int tmt_all_is_empty(void)
{
	int i;
	for (i = 0; i < T_MT_ROWS_CNT; i++) {
		if (!tmt_is_empty(i))
			return 0;
	}
	return 1;
}

int tmt_is_selected(short mtidx)
{
	return (p_t_mt[mtidx].mt_state & 0x10) == 0x10;
}

int tmt_get_read_nbr(void)
{
	int i, nbr = 0;

	for (i = 0; i < T_MT_ROWS_CNT; i ++) {
		if (!tmt_is_empty(i) && tmt_is_selected(i))
			nbr ++;
	}
	return nbr;
}

int tmt_get_select(BYTE *buf, int len, BYTE type)
{
	BYTE *ptr = buf;
	int idx;
	WORD nbr = 0;

	if (len < 3)
		return 0;
	*ptr ++ = type;
	ptr += 2; len -= 2; // skip 2 bytes for number of read meter
	for (idx = 0; idx < T_MT_ROWS_CNT; idx ++) {
		if (tmt_is_empty(idx) || tmt_is_selected(idx) != type)
			continue;
		if (len < 2)
			return 0;
		stoc(ptr, METER_ORDER(idx)); ptr += 2; len -= 2;
		nbr ++;
	}
	stoc(buf + 1, nbr);
	return ptr - buf;
}

void tmt_set_select(short mtidx, BYTE select)
{
	if (select == 0x01)
		p_t_mt[mtidx].mt_state |= 0x10;
	else if (select == 0x00)
		p_t_mt[mtidx].mt_state &= ~0x10;
	tmt_update(mtidx, 1);
}

int tmt_get(short mtidx, BYTE *buf, int skip)
{
	BYTE *ptr = buf;
	T_MT *p_mt = p_t_mt + mtidx;
	if (skip && tmt_is_empty(mtidx))
		return 0;
	stoc(ptr, METER_ORDER(mtidx)); ptr += 2;
	memcpy(ptr, p_mt->mtid, 6); ptr += 6;
	*ptr ++ = p_mt->mt_type;
	*ptr ++ = p_mt->connect_type;
	*ptr ++ = p_mt->tariff;
	*ptr ++ = p_mt->mt_state;
	stoc(ptr, p_mt->line_no); ptr += 2;
	stoc(ptr, p_mt->grp_no); ptr += 2;
	memcpy(ptr, p_mt->collect, 6); ptr += 6;
	return ptr - buf;
}

int tmt_set(short mtidx, const BYTE *buf, BYTE option)
{
	const BYTE *ptr = buf;
	T_MT *p_mt = p_t_mt + mtidx;

	if (option)	{
		tstat_init_row(mtidx + 1);
		trt_init_row(mtidx + 1);
	}
	memcpy(p_mt->mtid, ptr, MTID_LEN); ptr += MTID_LEN;
	p_mt->mt_type = *ptr ++;
	p_mt->connect_type = *ptr ++;
	p_mt->tariff = *ptr ++;
	p_mt->mt_state = *ptr ++;
	p_mt->line_no = ctos(ptr); ptr += 2;
	p_mt->grp_no = ctos(ptr); ptr += 2;
	memcpy(p_mt->collect, ptr, 6); ptr += 6;
	tmt_update(mtidx, 1);
	return ptr - buf;
}

int tmt_first(void)
{
	int i;
	for (i = 0; i < T_MT_ROWS_CNT; i++) {
		if (!tmt_is_empty(i) && is_valid_phase(tmt_get_phase(i)))
			return i;
	}
	return -1;
}

/* browse the meter idx in the meter seq no., do not care the phase */
int tmt_next(short mtidx)
{
	int i;
	for (i = mtidx + 1; i < T_MT_ROWS_CNT; i++) {
		if (!tmt_is_empty(i) && is_valid_phase(tmt_get_phase(i)))
			return i;
	}
	return -1;
}

/* browse the meter idx in the order of PHASE A, B, C */
int tmt_next2(short mtidx)
{
	int i, phase, first_idx;

	if (mtidx < 0) {	// find first 
		phase = PHASE_A;
		while (1) {
			for (i = 0; i < T_MT_ROWS_CNT; i++) {
				if (!tmt_is_empty(i) && tmt_get_phase(i) == phase)
					return i;
			}
			if (phase == PHASE_A)
				phase = PHASE_B;
			else if (phase == PHASE_B)
				phase = PHASE_C;
			else 
				return -1;
		}
	}

	phase = tmt_get_phase(mtidx);
	first_idx = mtidx + 1;
	while (1) {
		for (i = first_idx; i < T_MT_ROWS_CNT; i++) {
			if (!tmt_is_empty(i) && tmt_get_phase(i) == phase)
				return i;
		}
		if (phase == PHASE_A)
			phase = PHASE_B;
		else if (phase == PHASE_B)
			phase = PHASE_C;
		else 
			break;
		first_idx = 0;
	}
	
	return -1;
}

void tmt_set_meter_focus(short mtidx, int focus)
{
	int update = 0;

	if (focus && (p_t_mt[mtidx].mt_state & 0x08) != 0x08) {
		p_t_mt[mtidx].mt_state |= 0x08;
		update = 1;
	}
	if (!focus && (p_t_mt[mtidx].mt_state & 0x08) == 0x08) {
		p_t_mt[mtidx].mt_state &= (~0x08);
		update = 1;
	}
	if (update) tmt_update(mtidx, 1);
}

int tmt_get_all_focus_order(BYTE *buf, int len)
{
	BYTE *ptr = buf;
	short idx;
	BYTE focus_nbr = 0;

	if (len < 1)
		return 0;
	ptr ++; len --; // skip 1 byte for number of focus meter
	for (idx = 0; idx < T_MT_ROWS_CNT; idx ++) {
		if (tmt_is_empty(idx) || !tmt_is_focus(idx))
			continue;
		if (len < 2) {
			PRINTF("length of buffer is not enough\n");
			return 0;
		}
		stoc(ptr, METER_ORDER(idx)); ptr += 2; len -= 2;
		focus_nbr ++;
	}
	*buf = focus_nbr;
	return ptr - buf;
}

int tmt_get_sort_nbr(BYTE *buf, int len)
{
	int normal, multi, focus, carr, plc485, total;
	int i, type, state;
	BYTE *ptr = buf;
	if (len < 11)
		return 0;
	normal = multi = focus = carr = plc485 = total = 0;
	for (i = 0; i < T_MT_ROWS_CNT; i++) {
		if (tmt_is_empty(i))
			continue;
		type = (t_mt[i].mt_type >> 4) & 0x07;
		state = t_mt[i].mt_state;
		if (state & 0x08)
			focus ++;
		else
			normal ++;
		switch (type) {
		case 0:
			plc485 ++;
			break;
		case 1:
			carr ++;
			break;
		case 2:
			multi ++;
			break;
		}
		total ++;
	}
	stoc(ptr, normal); ptr += 2;
	stoc(ptr, multi); ptr += 2;
	*ptr ++ = focus;
	stoc(ptr, carr); ptr += 2;
	stoc(ptr, plc485); ptr += 2;
	stoc(ptr, total); ptr += 2;
	return ptr - buf;
}

void tmt_get_lines(short mtidx, BYTE *line, BYTE *line1, BYTE *line2,
	BYTE *line3)
{
	WORD line_no;
	line_no = p_t_mt[mtidx].line_no;
	*line = (line_no >> 12) & 0x0f;
	*line1 = (line_no >> 8) & 0x0f;
	*line2 = (line_no >> 4) & 0x0f;
	*line3 = (line_no) & 0x0f;
}

WORD tmt_get_grp_no(short mtidx)
{
	return (p_t_mt + mtidx)->grp_no;
}

void tmt_get_ms_no(short mtidx, BYTE *ms_no)
{
	memcpy(ms_no, p_t_mt[mtidx].collect, MS_NO_LEN);
}

void path_idx_to_path(RPT_PATH_IDX *path_idx, RPT_PATH *path)
{
	int i;

	for (i = 0; i < path_idx->count; i ++)
		tmt_get_mtid(path_idx->rpts[i], path->rpts[i]);
	path->count = path_idx->count;
}

int add_to_path_idx(short mtidx, RPT_PATH_IDX *path)
{
	if (path->count < NBR_RPT) {
		path->rpts[path->count ++] = mtidx;
		return 1;
	}
	return 0;
}

int tmt_insert_meter(short idx, const BYTE *buf)
{
//	if (idx >= T_MT_ROWS_CNT || !tmt_is_empty(idx))
//		return 0;
	if (idx >= T_MT_ROWS_CNT)
		return 0;
	PRINTF("tmt_insert_meter, mtidx:%d\n", idx);
	return tmt_set(idx, buf, 1);
}

int tmt_modify_meter(short idx, const BYTE *buf)
{
	//if (idx >= T_MT_ROWS_CNT || tmt_is_empty(idx))
	//	return 0;
	if (idx >= T_MT_ROWS_CNT)
		return 0;
	PRINTF("tmt_modify_meter, mtidx:%d\n", idx);
	return tmt_set(idx, buf, 0);
}

int tmt_del_meter(short idx, BYTE flag)
{
	PRINTF("tmt_del_meter, mtidx:%d\n", idx);
	if (flag)
		tmt_init_row(idx + 1);
	else
		memcpy(p_t_mt[idx].mtid, INVALID_MTID, MTID_LEN);
	tmt_update(idx, 1);
	return 1;
}

int tmt_del_all_meters(BYTE flag)
{
	int idx;
	PRINTF("tmt_del_all_meters\n");
	for (idx = 0; idx < T_MT_ROWS_CNT; idx ++) {
		if (flag)
			tmt_init_row(idx + 1);
		else
			memcpy(p_t_mt[idx].mtid, INVALID_MTID, MTID_LEN);
	}
	tmt_update(idx, 1);
	return 1;
}

void tmt_open(void)
{
	int mtidx, size;
	const char *name = T_MT_NAME;

	size = sizeof(T_MT) * T_MT_ROWS_CNT;
	sem_init(&sem_t_mt, 0, 1);
	if (!check_file(name, size)) {
		/* File doesnot exist or file length is invalid. */
		PRINTF("File %s is created, size:%d\n", name, size);
		fd_t_mt = open(name, O_CREAT | O_RDWR | O_TRUNC, 0600);
		for (mtidx = 0; mtidx < T_MT_ROWS_CNT; mtidx ++)
			tmt_init_row(mtidx + 1);
		safe_write(fd_t_mt, (char *)p_t_mt, size);
		fdatasync(fd_t_mt);
		close(fd_t_mt);
	}
	fd_t_mt = open(name, O_RDWR);
	safe_read(fd_t_mt, (char *)p_t_mt, size);
}

void tmt_update(short mtidx, int flush_flag)
{
	lseek(fd_t_mt, mtidx * sizeof(T_MT), SEEK_SET);
	safe_write(fd_t_mt, (char *)(p_t_mt + mtidx), sizeof(T_MT));
	if (flush_flag) tmt_flush();
}

void tmt_flush(void)
{
	fdatasync(fd_t_mt);
}

void tmt_close(void)
{
	fdatasync(fd_t_mt);
	close(fd_t_mt);
	sem_destroy(&sem_t_mt);
}
