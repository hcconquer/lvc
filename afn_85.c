/*
 *  C-Plan Concentrator
 *
 *  Copyright (C) 2006-2007, Shenzhen Kaifa Technology Co.,Ltd.
 *
 *  Author: dajiang wan
 *  Created on: 2006-09-05
 *	modify: jianping zhang
 */
#include "afn.h"
#include "f_param.h"
#include "misc.h"
#include "err_code.h"
#include "common.h"
#include "mt_access.h"
#include "t_mt.h"
#include "t_stat.h"
#include "mt_sync.h"
#include "msg_proc.h"
#include "msg_code.h"

static int afn85_f1(const BYTE *in_buf, int in_len, int *used_len)
{
	const BYTE *ptr = in_buf, *rpt_ptr;
	BYTE method, key[4];
	WORD di;
	int mtidx, allow;
	MTID mtid;
		
	if (in_len < 11)
		return ERR_BAD_VALUE;
	if (in_buf[9]) { // have repeaters
		if (in_len < 11 + 2 * NBR_RPT)
			return ERR_BAD_VALUE;
		*used_len = 11 + 2 * NBR_RPT;
	}
	else { // no repeater
		*used_len = 11;
	}
	ptr += 3; // skip meter administrator password
	mtidx = METER_INDEX(ctos(ptr)); ptr += 2;
	memcpy(key, ptr, 4); ptr += 4;
	method = *ptr ++;
	if (method) {
		rpt_ptr = ptr;
		ptr += 2 * NBR_RPT;
	}
	else {
		rpt_ptr = NULL;
	}
	if (ptr[0] != 0xaa && ptr[0] != 0x55)
		return ERR_BAD_VALUE;
	tmt_lock();
	allow = tmt_get_allow_poweroff(mtidx);
	tmt_unlock();
	if (!allow && ptr[0] == 0xaa)
		return ERR_BAD_VALUE;
	di = (ptr[0] == 0xaa) ? 0xc03c : 0xc03d;
	tmt_get_mtid(mtidx, mtid);
	return meter_write(mtidx, mtid, get_nbr_retry(), key, 1, di, NULL, 0);
}

static int afn85_f2(const BYTE *in_buf, int in_len, int *used_len)
{
	const BYTE *ptr = in_buf;
	BYTE key[4], req_code;
	int mtidx, ret;

	if (in_len < 7)
		return ERR_BAD_VALUE;
	*used_len = 7;
	mtidx = METER_INDEX(ctos(ptr)); ptr += 2;
	memcpy(key, ptr, 4); ptr += 4;
	req_code = *ptr ++;
	if (req_code != 0xaa && req_code != 0x55)
		return ERR_BAD_VALUE;
	tmt_lock();
	ret = tmt_set_allow_poweroff(mtidx, req_code == 0xaa);
	tmt_unlock();
	return ret == 0;
}

static int afn85_f3(const BYTE *in_buf, int in_len, int *used_len)
{
	*used_len = 7;
	return ERR_BAD_VALUE; // TODO, not need at present
}

static int afn85_f4(const BYTE *in_buf, int in_len, int *used_len)
{
	return ERR_BAD_VALUE; // TODO, not need at present
}

static int afn85_f5(const BYTE *in_buf, int in_len, int *used_len)
{
	const BYTE *ptr = in_buf, *rpt_ptr;
	BYTE method, key[4];
	WORD di;
	int mtidx, allow;
	MTID mtid;

	if (in_len < 8)
		return ERR_BAD_VALUE;
	if (in_buf[6]) { // have repeaters
		if (in_len < 8 + 2 * NBR_RPT)
			return ERR_BAD_VALUE;
		*used_len = 8 + 2 * NBR_RPT;
	}
	else { // no repeater
		*used_len = 8;
	}
	mtidx = METER_INDEX(ctos(ptr)); ptr += 2;
	memcpy(key, ptr, 4); ptr += 4;
	method = *ptr ++;
	if (method) {
		rpt_ptr = ptr;
		ptr += 2 * NBR_RPT;
	}
	else {
		rpt_ptr = NULL;
	}
	if (ptr[0] != 0xaa && ptr[0] != 0x55)
		return ERR_BAD_VALUE;
	tmt_lock();
	allow = tmt_get_allow_poweroff(mtidx);
	tmt_unlock();
	if (!allow)
		return ERR_BAD_VALUE;
	di = (ptr[0] == 0xaa) ? 0xc03a : 0xc03b;
	tmt_get_mtid(mtidx, mtid);
	return meter_write(mtidx, mtid, get_nbr_retry(), key, 1, di, NULL, 0);
}

static int afn85_f9(const BYTE *in_buf, int in_len, int *used_len)
{
	const BYTE *ptr = in_buf;
	WORD state;
	BYTE code;
	int param_len;

	*used_len = 2;
	if (in_len < 2)
		return ERR_BAD_VALUE;	
	state = ctos(ptr); ptr += 2;
	if (state == 0x2255) // start auto read meter daily data
		code = 0x00;
	else if (state == 0x3399) // stop auto read meter daily data
		code = 0xff;
	else
		return ERR_BAD_VALUE;
	fparam_lock();
	fparam_write(FPARAM_AUTO_UNREAD, &code, 1, &param_len);
	fparam_unlock();
	return ERR_OK;
}

static int afn85_f10(const BYTE *in_buf, int in_len, int *used_len)
{
	const BYTE *ptr = in_buf;
	BYTE sync_flag[3];
	int tmp;

	if (in_len < 1)
		return ERR_BAD_VALUE;
	if (ptr[0] == 0x00) { // donot sync meter clock auto
		*used_len = 1;
		sync_flag[0] = *ptr ++;
		sync_flag[1] = sync_flag[2] = 0;
	}
	else { // sync meter clock auto
		if (in_len < 3)
			return ERR_BAD_VALUE;
		*used_len = 3;
		memcpy(sync_flag, in_buf, 3);
	}
	fparam_lock();
	fparam_write(FPARAM_SYNC_CLOCK_FLAG, sync_flag, 3, &tmp);
	sync_event(EVENT_DAY_CHANGE);
	fparam_unlock();
	return ERR_OK;
}

static void fill_current_clock(BYTE *buf)
{
	time_t tt;
	struct tm tm;
	BYTE *ptr = buf;
	BYTE wmon;
	
	time(&tt);
	localtime_r(&tt, &tm);
	*ptr ++ = bin_to_bcd(tm.tm_sec);
	*ptr ++ = bin_to_bcd(tm.tm_min); 
	*ptr ++ = bin_to_bcd(tm.tm_hour); 
	if (tm.tm_wday == 0)
		wmon = 7;
	else
		wmon = tm.tm_wday;
	*ptr ++ = bin_to_bcd(wmon); 
	*ptr ++ = bin_to_bcd(tm.tm_mday); 
	*ptr ++ = bin_to_bcd(tm.tm_mon + 1);
	*ptr ++ = bin_to_bcd(tm.tm_year % 100);
}

static int afn85_f11(const BYTE *in_buf, int in_len, int *used_len)
{
	const BYTE *ptr = in_buf, *rpt_ptr;
	BYTE method, key[4], data[7];
	int mt_idx;
	MTID mtid;

	if (in_len < 7)
		return ERR_BAD_VALUE;
	if (in_buf[6]) {
		if (in_len < 7 + 2 * NBR_RPT)
			return ERR_BAD_VALUE;
		*used_len = 7 + 2 * NBR_RPT; 
	}
	else {
		*used_len = 7;
	}
	mt_idx = METER_INDEX(ctos(ptr)); ptr += 2;
	memcpy(key, ptr, 4); ptr += 4;
	method = *ptr ++;
	if (method) {
		rpt_ptr = ptr;
		ptr += 2 * NBR_RPT;
	}
	else {
		rpt_ptr = NULL;
	}
	fill_current_clock(data);
	tmt_get_mtid(mt_idx, mtid);
	return meter_write(mt_idx, mtid, get_nbr_retry(), key, 2, 0xC011,
		data, 3, 0xC010, data + 3, 4);
}

static int afn85_f20(const BYTE *in_buf, int in_len, int *used_len)
{
	*used_len = 0;
	return ERR_BAD_VALUE; // TODO, not need at present
}

static int afn85_f21(const BYTE *in_buf, int in_len, int *used_len)
{
	*used_len = 0;
	return ERR_BAD_VALUE; // TODO, not need at present
}

static struct st85_fn {
	BYTE fn;
	int (*afn85_fn)(const BYTE *in_buf, int in_len, int *used_len);
} fn_arr[] = {
	{1, afn85_f1}, {2, afn85_f2}, {3, afn85_f3}, {4, afn85_f4},
	{5, afn85_f5}, {9, afn85_f9}, {10, afn85_f10}, {11, afn85_f11},
	{20, afn85_f20}, {21, afn85_f21}
};

int afn85_set_fn(BYTE pn, BYTE fn, const BYTE *buf, int len, int *param_len)
{
	int i;

	PRINTF("AFN=85H, Pn=%d, Fn=%d\n", pn, fn);
	for (i = 0; i < sizeof(fn_arr) / sizeof(struct st85_fn); i ++) {
		if (fn == fn_arr[i].fn)
			return (fn_arr[i].afn85_fn)(buf, len, param_len);
	}
	return ERR_BAD_VALUE;
}
