/*****************************************************************************
 *	C-Plan Concentrator 
 *
 *	(c) Copyright 2006-2007, Shenzhen Kaifa Technology Co.,Ltd. 
 *	All Rights Reserved 
 *		
 *****************************************************************************/

#ifndef _MT_FRZN_HR_H
#define _MT_FRZN_HR_H

#include "typedef.h"

void frzn_event(EVENT event);

void frzn_meter_change(void);

int mt_frzn(const struct tm *tm);

#endif /* _MT_FRZN_HR_H */
