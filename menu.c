/*
 * C-Plan Concentrator
 *
 * Copyright (C) 2008, Shenzhen Kaifa Technology Co.,Ltd.
 *
 * Author: jianping zhang
 * Created on: 2008-01-31
 */
#include "menu.h"
#include "t_mt.h"
#include "f_mt.h"
#include "common.h"
#include "misc.h"
#include "mt_access.h"
#include "afn.h"
#include "f_param.h"
#include "menu_str.h"
#include "input.h"
#include "t_485.h"
#include "mt_485.h"
#include "global.h"
#include "device.h"
#include "plc.h"
#include "lvc_test.h"
#include "display.h"
#include "f_alm.h"
#include "threads.h"

static const char *fmt_num = "0123456789";
static const char *fmt_hex = "0123456789ABCDEF";

typedef struct {
	int row;
	int col;
} POSITION;

static void spont_con_param_changed(BYTE msa, BYTE afn, BYTE pn, BYTE fn)
{
	BYTE alm_buf[5];

	if (afn == 0x04 && fn != 0) {
		alm_buf[0] = msa;
		pn_to_da(pn, alm_buf + 1);
		fn_to_dt(fn, alm_buf + 3);
		falm_add_con(0x0e, 3, alm_buf, 5);
	}
}

static void init_menu(MENU *menu)
{
	memset(menu, 0, sizeof(menu));
	menu->row = STATUS_BAR_LINE + 1;
	menu->col = 1;
	menu->vrow_num = MAX_WORKSPACE_LINE;
	menu->vcol_num = MAX_SCREEN_COL;
	menu->start_line = 1;
}

static void init_list_position(INPUT_LIST *list, POSITION *position, int cnt)
{
	int i;

	for (i = 0; i < cnt; i ++) {
		list[i].row = position[i].row;
		list[i].col = position[i].col;
	}
}

static void init_input_list(INPUT_LIST *list, int row, int col,
	WORD digit_num,	BYTE *idx_arr, WORD list_num,
	const char *const_list[MAX_LIST_NUM], WORD max_value)
{
	int i;
	
	list->row = row;
	list->col = col;
	list->digit_num = digit_num;
	memcpy(list->value_idx, idx_arr, digit_num);
	list->list_num = list_num;
	for (i = 0; i < list_num; i++) {
		list->const_list[i] = const_list[i];
	}
	list->max_value = max_value;
}

static void init_input_set(INPUT_STRING *input, int row, int col, int len,
	const char *str, int maxlen)
{
	input->row = row;
	input->col = col;
	len = min(len, maxlen);
	input->len = len;
	memcpy(input->str, str, len);
	input->maxlen = maxlen;
}

static BYTE wait_any_key(void)
{
	BYTE key;

	if ((key = getch_timeout()) == KEY_NONE)
		lcd_mode_set(0);
	return key;
}

static void next_char(const char *buf, char *ch, int direct)
{
	const char *ptr;
	int len, pos;

	len = strlen(buf);
	if ((ptr = memchr(buf, *ch, len)) != NULL) {
		pos = ptr - buf;
		if (direct == 0) {
			if (pos == len - 1)
				pos = 0;
			else
				pos ++;
		}
		else {
			if (pos == 0)
				pos = len - 1;
			else
				pos --;
		}
		*ch = buf[pos];
	}
}

static int input_string(int row, const char *name, const char *str,
	char *buf, int len)
{
	unsigned char key;
	int name_len, pos, ret;

	ret = 0;
	name_len = strlen(name);
	lcd_show_string(row, 1, name_len, name);
	pos = len - 1;
	lcd_show_arrow(1, 1, 1, 1);
	while (ret == 0) {
		lcd_show_string(row, name_len + 1, len, buf);
		lcd_show_cursor(row, name_len + 1 + pos, 0);
		key = getch_timeout();
		switch (key) {
		case KEY_UP:
			next_char(str, buf + pos, 0);
			break;
		case KEY_DOWN:
			next_char(str, buf + pos, 1);
			break;
		case KEY_LEFT:
			if (pos > 0)
				pos --;
			else
				pos = len - 1;
			break;
		case KEY_RIGHT:
			if (pos < len - 1)
				pos ++;
			else
				pos = 0;
			break;
		case KEY_ENTER:
			ret = 1;
			break;
		case KEY_ESC:
			ret = -1;
			break;
		case KEY_NONE:
			lcd_mode_set(0);
			ret = -1;
			break;
		default:
			break;
		}
	}
	lcd_show_arrow(0, 0, 0, 0);
	return (ret < 0) ? 0 : 1;
}

static int input_password(const char *prompt, char *buf, int len)
{
	const char *name = "  ";
	unsigned char key, data[MAX_SCREEN_COL + 1];
	int name_len, pos, row = 2, ret;

	if (len >= MAX_SCREEN_COL)
		return 0;
	memset(buf, '0', len - 1); buf[len - 1] = 0; len --;
	lcd_clean_workspace();
	lcd_show_string(row, 1, strlen(prompt), prompt);
	ret = 0;
	name_len = strlen(name);
	lcd_show_string(row + 1, 1, name_len, name);
	pos = 0;
	lcd_show_arrow(1, 1, 1, 1);
	while (ret == 0) {
		memset(data, '*', len);
		data[pos] = buf[pos];
		lcd_show_string(row + 1, name_len + 1, len, data);
		lcd_show_cursor(row + 1, name_len + 1 + pos, 0);
		key = getch_timeout();
		switch (key) {
		case KEY_UP:
			if (buf[pos] == '9')
				buf[pos] = '0';
			else
				buf[pos] += 1;
			break;
		case KEY_DOWN:
			if (buf[pos] == '0')
				buf[pos] = '9';
			else
				buf[pos] -= 1;
			break;
		case KEY_LEFT:
			if (pos > 0)
				pos --;
			else
				pos = len - 1;
			break;
		case KEY_RIGHT:
			if (pos < len - 1)
				pos ++;
			else
				pos = 0;
			break;
		case KEY_ENTER:
			ret = 1;
			break;
		case KEY_ESC:
			ret = -1;
			break;
		case KEY_NONE:
			lcd_mode_set(0);
			ret = -1;
			break;
		default:
			break;
		}
	}
	lcd_show_arrow(0, 0, 0, 0);
	return (ret < 0) ? 0 : 1;
}

static int input_pn(int row, const char *prompt, const char *info)
{
	char sample_no[] = "01";
	int tmp, pn = 0;
	
	while (!g_terminated) {
		lcd_clean_workspace();
		lcd_update_info(info);
		if (input_string(row, prompt, fmt_num, 
			sample_no, 2) == 0)
			break;
		tmp = atoi(sample_no);
		if (tmp >= 1 && tmp <= T_485_ROWS_CNT) {
			pn = tmp;
			break;
		}
	}
	return pn;
}

static void hexstr_to_str(void *dst, const void *src, int src_len)
{
	int i;
	BYTE low, high, ch;
	BYTE *dst_ptr = dst;
	const BYTE *src_ptr = src;

	for (i = 0; i < src_len / 2; i ++) {
		ch = *src_ptr ++;
		if (ch >= '0' && ch <= '9')
			high = ch - '0';
		else if (ch >= 'A' && ch <= 'F')
			high = ch - 'A' + 10;
		else if (ch >= 'a' && ch <= 'f')
			high = ch - 'a' + 10;
		else
			break;
		ch = *src_ptr ++;
		if (ch >= '0' && ch <= '9')
			low = ch - '0';
		else if (ch >= 'A' && ch <= 'F')
			low = ch - 'A' + 10;
		else if (ch >= 'a' && ch <= 'f')
			low = ch - 'a' + 10;
		else
			break;
		*dst_ptr ++ = high * 16 + low; 
	}
}

static void str_to_hexstr(void *dst, const void *src, int src_len)
{
	int i;
	BYTE low, high, ch;
	BYTE *dst_ptr = dst;
	const BYTE *src_ptr = src;
	const char *str = "0123456789ABCDEF";

	for (i = 0; i < src_len; i ++) {
		ch = *src_ptr ++;
		high = (ch >> 4) & 0x0f;
		low = ch & 0x0f;
		*dst_ptr ++ = str[high];
		*dst_ptr ++ = str[low];
	}
}

static int check_password(const char *info, int must)
{
	static long last_uptime = -60;
	time_t tt, fail_tt;
	int ret = 0, interval = 24 * 60 * 60, val, fail_cnt, allow, need;
	BYTE flag[5];
	char pass1[3], pass2[3], passwd[7];

	lcd_clean_workspace();
	lcd_update_info(info);
	if (!must && (uptime() - last_uptime) < 60) {
		last_uptime = uptime();
		return 1;
	}
	fparam_lock();
	fparam_get_value(FPARAM_USER_PASSWD, pass1, sizeof(pass1));
	fparam_get_value(FPARAM_USER_PASSWD_FLAG, flag, sizeof(flag));
	fparam_unlock();
	fail_cnt = flag[0]; fail_tt = ctol(flag + 1);
	if (fail_tt == 0) {
		allow = 1;
	}
	else {
		time(&tt);
		val = (tt >= fail_tt) ? (tt - fail_tt) : (fail_tt - tt);
		allow = (val >= interval || fail_cnt < 5);
	}
	if (!allow) { // not allow to input password
		lcd_update_info(c_menu_password_str[9]);
		lcd_clean_workspace();
		wait_any_key();
	}
	else if (!input_password(c_menu_password_str[6], passwd,
		sizeof(passwd))) {
		if (lcd_mode_get()) {
			lcd_update_info(c_menu_password_str[10]);
			lcd_clean_workspace();
			wait_any_key();
		}
	}
	else { // verify the password
		hexstr_to_str(pass2, passwd, 6);
		if (memcmp(pass1, pass2, sizeof(pass1)) == 0) { // correct password
			need = (fail_tt != 0);
			fail_cnt = 0; fail_tt = 0;
			ret = 1;
		}
		else { // invalid password
			need = 1;
			fail_cnt ++; fail_tt = time(NULL);
		}
		if (need) { // need update
			fparam_lock();
			flag[0] = fail_cnt; ltoc(flag + 1, fail_tt);
			fparam_set_value(FPARAM_USER_PASSWD_FLAG, flag, sizeof(flag), &val);
			fparam_unlock();
		}
		if (ret) {
			lcd_update_info(c_menu_password_str[7]);
			lcd_clean_workspace();
		}
		else {
			lcd_update_info(c_menu_password_str[8]);
			lcd_clean_workspace();
			wait_any_key();
		}
	}
	if (ret)
		last_uptime = uptime();
	else
		last_uptime = -60; // must input password at next time
	return ret;
}

static void view_sample_di(BYTE flag, void *para, const char *info)
{
	static const BYTE menu_line_num[] = {5, 5, 5, 5, 5, 5, 5, 5, 3, 3, 4, 4, 4};
	static const WORD di_arr[][4] = {
		{0x901f}, {0x902f}, {0x911f}, {0x912f},
		{0x913f}, {0x915f},	{0x916f}, {0x914f},
		{0xB611, 0xB612, 0xB613},
		{0xB621, 0xB622, 0xB623},
		{0xB630, 0xB631, 0xB632, 0xB633},
		{0xB640, 0xB641, 0xB642, 0xB643},
		{0xB650, 0xB651, 0xB652, 0xB653},
	};
	MTID mtid;
	MENU menu;
	char string[5][MAX_SCREEN_COL + 1];
	BYTE buf[20], *ptr = buf;
	int i, tmp_di, tmp_len, len, group, *param = (int *)para;

	lcd_clean_workspace();
	lcd_update_info(c_process_status[0]);
	t485_get_mtid(param[0] - 1, mtid); group = param[1];
	len = 0;
	if (group < 13 && memcmp(mtid, INVALID_MTID, MTID_LEN) != 0) {
		if (group >= 0 && group <= 7) {
			len = mt485_read(mtid, buf, sizeof(buf), di_arr[group][0]);
		}
		else if (group >= 8 && group <= 9) {
			len = mt485_read_arg(mtid, buf, sizeof(buf), 3, di_arr[group][0],
				di_arr[group][1], di_arr[group][2]);
		}
		else {
			len = mt485_read_arg(mtid, buf, sizeof(buf), 4, di_arr[group][0],
				di_arr[group][1], di_arr[group][2], di_arr[group][3]);
		}
	}
	init_menu(&menu);
	if (len > 0) {
		tmp_len = len / menu_line_num[group];
		menu.line_num = menu_line_num[group];
		for (i = 0; i < menu.line_num; i ++)
			menu.str[i] = string[i];
		if (group >= 0 && group <= 7) {
			tmp_di = di_arr[group][0] & (~0x0f);
			for (i = 0; i < menu.line_num; i ++) {
				format_di_str(string[i], sizeof(string[i]), tmp_di + i, ptr,
					tmp_len);
				ptr += tmp_len;
			}
		}
		else {
			for (i = 0; i < menu.line_num; i ++) {
				format_di_str(string[i], sizeof(string[i]), di_arr[group][i],
					ptr, tmp_len);
				ptr += tmp_len;
			}
		}
	}
	else {
		menu.line_num = 1;
		menu.str[0] = string[0];
		sprintf(string[0], c_process_status[2]);
	}
	process_menu(&menu, menu_name1_2_0[group]);
}

static void view_485mt(BYTE flag, void *para, const char *info)
{
	int mt_ord, i;
	ITEMS_MENU items_menu;
	int param[13][2];

	mt_ord = (int)para;
	memset(&items_menu, 0, sizeof(items_menu));
	init_menu(&items_menu.menu);
	items_menu.cur_line = 1;
	items_menu.menu.line_num = 13;
	for (i = 0; i < items_menu.menu.line_num; i ++) {
		items_menu.menu.str[i] = menu_name1_2_0[i];
		items_menu.func[i] = view_sample_di;
		param[i][0] = mt_ord; param[i][1] = i;
		items_menu.para[i] = (void *)param[i];
	}
	process_items(&items_menu, menu_name1_2[mt_ord - 1], 1);
}

static void view_usermt(BYTE flag, void *para, const char *info)
{
	static char input[MTID_LEN * 2 + 1] = "000000000000";
	BYTE req[PLC_APDU_LEN], resp[PLC_APDU_LEN], *data;
	int req_len, resp_len, data_len, resp_max_len;
	int i, ret, timeout;
	char string[7][MAX_SCREEN_COL + 1];
	BYTE phase[6] = {PHASE_A, PHASE_B, PHASE_C, PHASE_A, PHASE_B, PHASE_C};
	MENU menu;
	MTID mtid;

	// new changsha protocol, data length of DI_9A10 is 6
	resp_max_len = 1 + 7 + 4 * 3 + 6;
	req_len = my_pack(req, "cssssss", 0x01, 0xC010, 0xC011, 0x9010, 0x9020,
		0x9410, 0x9A10);
	lcd_clean_workspace();
	lcd_update_info(info);
	for (i = 0; i < 7; i ++)
		menu.str[i] = string[i];
	data = resp + 1; data_len = 0;
	if (input_string(2, c_usermt_realtime_str[0], fmt_num, input,
		sizeof(input) - 1)) {
		lcd_update_info(c_process_status[0]);
		hexstr_to_str(mtid, input, 2 * MTID_LEN);
		reverse(mtid, MTID_LEN);
		for (i = 0; i < sizeof(phase); i ++) {
			plc_lock();
			plc_send(0, TRUE, mtid, phase[i], NULL, req, req_len, resp_max_len,
				&timeout);
			ret = plc_recv(mtid, timeout, resp, sizeof(resp), &resp_len);
			plc_unlock();
			if (ret == COMM_OK && (resp[0] & 0xC0) != 0xC0) {
				data_len = resp_len - 1;
				break;
			}
		}
	}
	else
		return;
	init_menu(&menu);
	if (data_len >= 7 + 4 * 3 + 4) { // old meter DI_9A10 may be 4 bytes
		menu.line_num = 7;
		sprintf(string[0], "%s:%02x%02x%02x%02x%02x%02x",
			c_usermt_realtime_str[0], mtid[5], mtid[4], mtid[3], mtid[2],
			mtid[1], mtid[0]);
		sprintf(string[1], "%s:%02x-%02x-%02x", c_usermt_realtime_str[1],
			data[3], data[2], data[1]);
		sprintf(string[2], "%s:%02x:%02x:%02x", c_usermt_realtime_str[2],
			data[6], data[5], data[4]);
		sprintf(string[3], "%s:%02x%02x%02x.%02x", c_usermt_realtime_str[3],
			data[10], data[9], data[8], data[7]);
		sprintf(string[4], "%s:%02x%02x%02x.%02x", c_usermt_realtime_str[4],
			data[14], data[13], data[12], data[11]);
		sprintf(string[5], "%s:%02x%02x%02x.%02x", c_usermt_realtime_str[5],
			data[18], data[17], data[16], data[15]);
		sprintf(string[6], "%s:%02x%02x%02x.%02x", c_usermt_realtime_str[6],
			data[22], data[21], data[20], data[19]);
	}
	else {
		menu.line_num = 1;
		sprintf(string[0], c_process_status[2]);
	}
	process_menu(&menu, menu_name1_1);
}

static void hardware_init(BYTE flag, void *para, const char *info)
{
	int len = 0;
	
	if (check_password(info, 0)) {
		afn01_set_fn(0, 1, NULL, 0, &len);
		lcd_mode_set(0);
	}
}

static void data_init(BYTE flag, void *para, const char *info)
{
	int len = 0;
	
	if (check_password(info, 1)) {
		afn01_set_fn(0, 2, NULL, 0, &len);
		lcd_mode_set(0);
	}
}

static void all_data_init(BYTE flag, void *para, const char *info)
{
	int len = 0;
	
	if (check_password(info, 1)) {
		afn01_set_fn(0, 3, NULL, 0, &len);
		lcd_mode_set(0);
	}
}

static void con_init(BYTE flag, void *para, const char *info)
{
	ITEMS_MENU items_menu;

	memset(&items_menu, 0, sizeof(items_menu));
	init_menu(&items_menu.menu);
	items_menu.cur_line = 1;
	items_menu.menu.line_num = 3;
	items_menu.menu.str[0] = menu_name3_9_1;
	items_menu.func[0] = hardware_init;
	items_menu.menu.str[1] = menu_name3_9_2;
	items_menu.func[1] = data_init;
	items_menu.menu.str[2] = menu_name3_9_3;
	items_menu.func[2] = all_data_init;
	process_items(&items_menu, menu_name3_9, 1);
}

static void view_con_info(BYTE flag, void *para, const char *info)
{
	#define LCD_TERMINAL_LINE 15
	MENU menu;
	char string[LCD_TERMINAL_LINE][MAX_SCREEN_COL + 1];
	BYTE buf[MAX_LDU_LEN];
	int i;

	init_menu(&menu);
	menu.line_num = LCD_TERMINAL_LINE;
	for (i = 0; i < LCD_TERMINAL_LINE; i ++)
		menu.str[i] = string[i];
	fparam_lock();
	get_hardware_version(buf, 8); /* hareware version */
	sprintf(string[0], "%s:", c_terminal_info_str[0]);
	sprintf(string[1], "%02x%02x-%02x%02x-%02x-%02x%02x%02x",
		buf[0], buf[1], buf[2], buf[3], buf[4], buf[5], buf[6], buf[7]);
	get_software_version(buf, 8); /* software version */
	sprintf(string[2], "%s:", c_terminal_info_str[1]);
	sprintf(string[3], "%02x%02x-%02x%02x-%02x-%02x%02x%02x",
		buf[0], buf[1], buf[2], buf[3], buf[4], buf[5], buf[6], buf[7]);
	get_protocol_version(buf, 6); /* protocol version */
	sprintf(string[4], "%s:", c_terminal_info_str[2]);
	sprintf(string[5], "%02x%02x%02x-%02x-%02x-%02x",
		buf[0], buf[1], buf[2], buf[3], buf[4], buf[5]);
	fparam_get_value(FPARAM_CON_ADDRESS, buf, sizeof(buf));
	sprintf(string[6], "%s:%02x%02x%02x%02x", c_terminal_info_str[3],
		buf[0], buf[1], buf[2], buf[3]);
	fparam_get_value(FPARAM_AFN04_F6, buf, sizeof(buf));
	for (i = 0; i < 8; i++) {
		sprintf(string[LCD_TERMINAL_LINE - 8 + i], "%s%d:%02x",
			c_terminal_info_str[4], i, buf[i]);
	}
	fparam_unlock();
	process_menu(&menu, menu_name3_2);
}

static void calibrate_clock(BYTE flag, void *para, const char *info)
{
	PARAM_LIST param_list;
	char string[2][MAX_SCREEN_COL + 1];
	BYTE value_idx[MAX_INPUT_DIGIT_NUM], ret;
	struct tm tm;
	struct timeval tv;

	if (!check_password(info, 0))
		return;
	memset(&param_list, 0, sizeof(param_list));
	init_menu(&param_list.menu);
	param_list.menu.start_line = 1;
	param_list.menu.line_num = 2;
	param_list.menu.str[0] = string[0];
	param_list.menu.str[1] = string[1];
	sprintf(string[0], "%s:    -  -  ", c_calibrate_clock_str[0]);
	sprintf(string[1], "%s:  :  :  ", c_calibrate_clock_str[1]);
	param_list.group_num = 6;
	param_list.group_idx = 0;
	param_list.digit_idx = 0;
	my_time(&tm);
	tm.tm_year = tm.tm_year + 1900;
	value_idx[0] = (tm.tm_year / 1000) % 10;
	value_idx[1] = (tm.tm_year / 100) % 10;
	value_idx[2] = (tm.tm_year / 10) % 10;
	value_idx[3] = tm.tm_year % 10;
	init_input_list(&param_list.list[0], 1, 6, 4, value_idx, 10,
		c_numeric_list, 3000);
	tm.tm_mon ++;
	value_idx[0] = (tm.tm_mon / 10) % 10;
	value_idx[1] = tm.tm_mon % 10; /* month */
	init_input_list(&param_list.list[1], 1, 11, 2, value_idx, 10,
		c_numeric_list, 12);
	value_idx[0] = (tm.tm_mday / 10) % 10;
	value_idx[1] = tm.tm_mday % 10; /* day */
	init_input_list(&param_list.list[2], 1, 14, 2, value_idx, 10,
		c_numeric_list, 31);
	value_idx[0] = (tm.tm_hour / 10) % 10;
	value_idx[1] = tm.tm_hour % 10; /* hour */
	init_input_list(&param_list.list[3], 2, 6, 2, value_idx, 10,
	c_numeric_list, 23);
	value_idx[0] = (tm.tm_min / 10) % 10;
	value_idx[1] = tm.tm_min % 10; /* minute */
	init_input_list(&param_list.list[4], 2, 9, 2, value_idx, 10,
	c_numeric_list, 59);
	value_idx[0] = (tm.tm_sec / 10) % 10;
	value_idx[1] = tm.tm_sec % 10; /* second */
	init_input_list(&param_list.list[5], 2, 12, 2, value_idx, 10,
	c_numeric_list, 59);
	ret = process_param_list(&param_list, menu_name3_3);
	if (ret == KEY_ESC)
		return;
	tm.tm_year = param_list.list[0].value_idx[3]
		+ param_list.list[0].value_idx[2] * 10
		+ param_list.list[0].value_idx[1] * 100
		+ param_list.list[0].value_idx[0] * 1000;
	tm.tm_mon = param_list.list[1].value_idx[1]
		+ param_list.list[1].value_idx[0] * 10 - 1;
	tm.tm_mday = param_list.list[2].value_idx[1]
		+ param_list.list[2].value_idx[0] * 10;
	tm.tm_hour = param_list.list[3].value_idx[1]
		+ param_list.list[3].value_idx[0] * 10;
	tm.tm_min = param_list.list[4].value_idx[1]
		+ param_list.list[4].value_idx[0] * 10;
	tm.tm_sec = param_list.list[5].value_idx[1]
		+ param_list.list[5].value_idx[0] * 10;
	if (tm.tm_year > 1900 && tm.tm_year < 2100 
		&& tm.tm_mon < 12 && tm.tm_mday <= 31
		&& tm.tm_hour <= 23 && tm.tm_min <= 59) {
		tm.tm_year = tm.tm_year - 1900;
		tv.tv_sec = mktime(&tm);
		tv.tv_usec = 0;
		settimeofday(&tv, NULL);
		set_rtc();
	}
}

static void modify_menu_passwd(BYTE flag, void *para, const char *info)
{
	char pass[3], passwd[7], old_passwd[7], new_passwd[7], confirm[7];
	int param_len = 0;

	fparam_lock();
	fparam_get_value(FPARAM_USER_PASSWD, pass, sizeof(pass));
	fparam_unlock();
	str_to_hexstr(passwd, pass, 3); passwd[6] = 0x0;
	if (input_password(c_menu_password_str[0], old_passwd,
		sizeof(old_passwd)) <= 0)
		return;
	if (input_password(c_menu_password_str[1], new_passwd,
		sizeof(new_passwd)) <= 0)
		return;
	if (input_password(c_menu_password_str[2], confirm,
		sizeof(confirm)) <= 0)
		return;
	if (strcmp(new_passwd, confirm) != 0) {
		lcd_update_info(c_menu_password_str[3]);
	}
	else if (strcmp(passwd, old_passwd) != 0) {
		lcd_update_info(c_menu_password_str[5]);
	}
	else {
		hexstr_to_str(pass, new_passwd, 6);
		fparam_lock();
		fparam_set_value(FPARAM_USER_PASSWD, pass, sizeof(pass), &param_len);
		fparam_unlock();
		lcd_update_info(c_menu_password_str[4]);
	}
	lcd_clean_workspace();
	wait_any_key();
}

static void view_set_channel(BYTE flag, void *para, const char *info)
{
	BYTE chn[5], chn_bak[5];
	ITEMS_MENU items_menu;
	int param_len;

	if (!check_password(info, 0))
		return;
	fparam_lock();
	fparam_get_value(FPARAM_AFN04_F62, chn, 5);
	fparam_unlock();
	memcpy(chn_bak, chn, sizeof(chn));
	if (chn[0] > 3)
		chn[0] = 0;
	memset(&items_menu, 0, sizeof(items_menu));
	init_menu(&items_menu.menu);
	items_menu.cur_line = chn[0] + 1;
	items_menu.menu.line_num = 4;
	items_menu.menu.str[0] = c_up_channel_type_str[0];
	items_menu.func[0] = NULL;
	items_menu.menu.str[1] = c_up_channel_type_str[1];
	items_menu.func[1] = NULL;
	items_menu.menu.str[2] = c_up_channel_type_str[2];
	items_menu.func[2] = NULL;
	items_menu.menu.str[3] = c_up_channel_type_str[3];
	items_menu.func[3] = NULL;
	if (process_items(&items_menu, menu_name3_2_1, 1) == KEY_ENTER) {
		chn[0] = items_menu.cur_line - 1;
		if (memcmp(chn_bak, chn, sizeof(chn)) != 0) {
			fparam_lock();
			falm_lock();
			fparam_set_value(FPARAM_AFN04_F62, chn, 5, &param_len);
			spont_con_param_changed(0, 0x04, 0, 62);
			falm_unlock();
			fparam_unlock();
		}
	}
}

static void view_set_gprs(BYTE flag, void *para, const char *info)
{
	PARAM_SET param_set;
	char ip_addr[16 + 1], port_buf[5 + 1];
	char apn[16 + 1], apn_bak[16 + 1], buf[6], buf_bak[6];
	char username[32 + 1], username_bak[32 + 1], passwd[32 + 1], passwd_bak[32 + 1];
	int param_len = 0;
	unsigned short ip_port;
	unsigned long int ip;

	if (!check_password(info, 0))
		return;
	memset(&param_set, 0, sizeof(param_set));
	memset(ip_addr, 0, sizeof(ip_addr));
	memset(port_buf, 0, sizeof(port_buf));
	memset(apn, 0, sizeof(apn));
	memset(username, 0, sizeof(username));
	memset(passwd, 0, sizeof(passwd));
	fparam_lock();
	fparam_get_value(FPARAM_IP_PORT_PRI, buf, sizeof(buf));
	fparam_get_value(FPARAM_APN, apn, sizeof(apn) - 1);
	fparam_get_value(FPARAM_VPN_USER, username, sizeof(username) - 1);
	fparam_get_value(FPARAM_VPN_PASS, passwd, sizeof(passwd) - 1);
	fparam_unlock();
	memcpy(buf_bak, buf, sizeof(buf));
	memcpy(apn_bak, apn, sizeof(apn));
	memcpy(username_bak, username, sizeof(username));
	memcpy(passwd_bak, passwd, sizeof(passwd));
	sprintf(ip_addr, "%d.%d.%d.%d", buf[0], buf[1], buf[2], buf[3]);
	ip_port = (buf[5] << 8) + buf[4];
	sprintf(port_buf, "%5d", ip_port);
	init_menu(&param_set.menu);
	param_set.menu.line_num = 6;
	param_set.menu.str[0] = c_ip_address_str;
	param_set.menu.str[1] = NULL;
	param_set.menu.str[2] = c_port_str;
	param_set.menu.str[3] = "APN:";
	param_set.menu.str[4] = c_username_str;
	param_set.menu.str[5] = c_password_str;
	param_set.group_num = 5;
	param_set.group_idx = 0;
	init_input_set(&param_set.input[0], 2, 1, strlen(ip_addr), ip_addr, 15);
	param_set.keyboard_type[0] = _num_keyboard;
	init_input_set(&param_set.input[1], 3, 6, strlen(port_buf), port_buf, 5);
	param_set.keyboard_type[1] = _num_keyboard;
	init_input_set(&param_set.input[2], 4, 5, strlen(apn), apn, 16);
	param_set.keyboard_type[2] = _lower_letter_keyboard;
	init_input_set(&param_set.input[3], 5, 6, strlen(username), username, 16);
	param_set.keyboard_type[3] = _lower_letter_keyboard;
	init_input_set(&param_set.input[4], 6, 6, strlen(passwd), passwd, 16);
	param_set.keyboard_type[4] = _lower_letter_keyboard;
	process_param_set(&param_set, menu_name3_2_2);
	strcpy(ip_addr, param_set.input[0].str);
	ip = inet_addr(ip_addr);
	ltoc((BYTE *)buf, ip);
	strcpy(port_buf, param_set.input[1].str);
	ip_port = atoi(port_buf);
	buf[5] = ip_port >> 8; buf[4] = ip_port;
	strcpy(apn, param_set.input[2].str);
	strcpy(username, param_set.input[3].str);
	strcpy(passwd, param_set.input[4].str);
	fparam_lock();
	falm_lock();
	fparam_set_value(FPARAM_IP_PORT_PRI, buf, sizeof(buf), &param_len);
	fparam_set_value(FPARAM_APN, apn, sizeof(apn), &param_len);
	fparam_set_value(FPARAM_VPN_USER, username, sizeof(username), &param_len);
	fparam_set_value(FPARAM_VPN_PASS, passwd, sizeof(passwd),&param_len);
	if (memcmp(buf_bak, buf, sizeof(buf)) || memcmp(apn_bak, apn, sizeof(apn)))
		spont_con_param_changed(0, 0x04, 0, 3);
	if (memcmp(username_bak, username, sizeof(username))
		|| memcmp(passwd_bak, passwd, sizeof(passwd)))
		spont_con_param_changed(0, 0x04, 0, 16);
	falm_unlock();
	fparam_unlock();
}

static void unpack_phone(char *phone, const unsigned char *buf)
{
	const unsigned char *src = buf;
	char *dst = phone;
	BYTE high, low, ch;
	int i;

	for (i = 0; i < 8; i ++) {
		ch = *src ++;
		high = (ch >> 4) & 0x0F;
		low = ch & 0x0F;
		if (high <= 9)
			*dst ++ = high + '0';
		else if (high == 0x0a)
			*dst ++ = ',';
		else if (high == 0x0b)
			*dst ++ = '#';
		else if (high == 0x0f)
			break;
		if (low <= 9)
			*dst ++ = low + '0';
		else if (low == 0x0a)
			*dst ++ = ',';
		else if (low == 0x0b)
			*dst ++ = '#';
		else if (low == 0x0f)
			break;
	}
	*dst = 0x0;
}

static void pack_phone(unsigned char *buf, const char *phone)
{
	int idx;
	const char *src = phone;
	unsigned char *dst = buf;
	BYTE ch, data[2];

	memset(buf, 0xff, 8);
	idx = 0;
	while (*src != 0x0) {
		ch = *src ++;
		if (ch >= '0' && ch <= '9')
			data[idx ++] = ch - '0';
		else if (ch == ',')
			data[idx ++] = 0x0a;
		else if (ch == '#')
			data[idx ++] = 0x0b;
		if (idx == 2) {
			*dst ++ = (data[0] << 4) + data[1];
			idx = 0;
		}
	}
	if (idx == 1) {
		*dst = (data[0] << 4) + 0x0f;
	}
}

static void view_set_sms(BYTE flag, void *para, const char *info)
{
	BYTE buf[16], buf_bak[16];
	char host_phone[16 + 1], sms_center[16 + 1];
	PARAM_SET param_set;
	int param_len = 0;
	
	if (!check_password(info, 0))
		return;
	memset(buf, 0, sizeof(buf));
	memset(&param_set, 0, sizeof(param_set));
	fparam_lock();
	fparam_get_value(FPARAM_AFN04_F4, buf, 16);
	fparam_unlock();
	memcpy(buf_bak, buf, sizeof(buf));
	unpack_phone(host_phone, buf);
	unpack_phone(sms_center, buf + 8);
	init_menu(&param_set.menu);
	param_set.menu.line_num = 4;
	param_set.menu.str[0] = c_host_phone_str;
	param_set.menu.str[1] = NULL;
	param_set.menu.str[2] = c_sms_center_phone_str;
	param_set.menu.str[3] = NULL;
	param_set.group_num = 2;
	param_set.group_idx = 0;
	init_input_set(&param_set.input[0], 2, 1, strlen(host_phone),
		host_phone, 16);
	param_set.keyboard_type[0] = _num_keyboard;
	init_input_set(&param_set.input[1], 4, 1, strlen(sms_center),
		sms_center, 16);
	param_set.keyboard_type[1] = _num_keyboard;
	process_param_set(&param_set, menu_name3_2_3);
	strcpy(host_phone, param_set.input[0].str);
	strcpy(sms_center, param_set.input[1].str);
	pack_phone(buf, host_phone);
	pack_phone(buf + 8, sms_center);
	if (memcmp(buf_bak, buf, sizeof(buf)) != 0) {
		fparam_lock();
		falm_lock();
		fparam_set_value(FPARAM_AFN04_F4, buf, 16, &param_len);
		spont_con_param_changed(0, 0x04, 0, 4);
		falm_unlock();
		fparam_unlock();
	}
}

static void unpack_cascade_param(BYTE state, int *baud_idx, int *stop_idx,
	 int *parity_idx, int *bits_idx)
{
	*baud_idx = (state >> 5) & 0x07;
	if (state & 0x10)
		*stop_idx = 1;
	else
		*stop_idx = 0;
	if (state & 0x08)
		*parity_idx = (state >> 2) & 0x01;
	else
		*parity_idx = 2;
	*bits_idx = state & 0x03;
}

static void pack_cascade_param(BYTE *state, int baud_idx, int stop_idx,
	int parity_idx, int bits_idx)
{
	*state = 0;
	*state |= ((baud_idx << 5) & 0xE0);
	if (stop_idx == 1)
		*state |= 0x10;
	if (parity_idx == 0)
		*state |= 0x08;
	else if (parity_idx == 1)
		*state |= 0x0C;
	*state |= bits_idx & 0x03;
}

static void view_set_cascade(BYTE flag, void *para, const char *info)
{
	PARAM_SET param_set;
	BYTE buf[6], buf_bak[6];
	int baud_idx, parity_idx, stop_idx, bits_idx, i, idx = 0, param_len = 0;
	const char *c_baud_rate[] = {"300", "600", "1200", "2400", "4800", "7200",
		"9600", "19200"};
	const char *c_parity[] = {c_cascade_parameter_str[0],
		c_cascade_parameter_str[1], c_cascade_parameter_str[2]};
	const char *c_bits_num[] = {"5", "6", "7", "8"};
	const char *c_stop_bit[] = {"1", "2"};
	char temp[4];

	if (!check_password(info, 0))
		return;
	memset(&param_set, 0, sizeof(param_set));
	fparam_lock();
	fparam_get_value(FPARAM_AFN84_F10, buf, sizeof(buf));
	fparam_unlock();
	memcpy(buf_bak, buf, sizeof(buf));
	unpack_cascade_param(buf[0], &baud_idx, &stop_idx, &parity_idx, &bits_idx);
	init_menu(&param_set.menu);
	param_set.menu.line_num = 7;
	for (i = 0; i < param_set.menu.line_num; i ++)
		param_set.menu.str[i] = c_cascade_parameter_str[3 + i];
	param_set.group_num = 9;
	param_set.group_idx = 0;
	for (i = 0; i < 8; i ++)
		param_set.input[idx].const_list[i] = c_baud_rate[i];
	param_set.input[idx].list_idx = baud_idx;
	param_set.input[idx].list_num = i;
	init_input_set(&param_set.input[idx], 1, 8, strlen(c_baud_rate[baud_idx]),
		c_baud_rate[baud_idx], 5);
	param_set.keyboard_type[idx ++] = _none_keyboard;
	for (i = 0; i < 4; i ++)
		param_set.input[idx].const_list[i] = c_bits_num[i];
	param_set.input[idx].list_idx = bits_idx;
	param_set.input[idx].list_num = i;
	init_input_set(&param_set.input[idx], 1, 20, strlen(c_bits_num[bits_idx]),
		c_bits_num[bits_idx], 1);
	param_set.keyboard_type[idx ++] = _none_keyboard;
	for (i = 0; i < 3; i ++)
		param_set.input[idx].const_list[i] = c_parity[i];
	param_set.input[idx].list_idx = parity_idx;
	param_set.input[idx].list_num = i;
	init_input_set(&param_set.input[idx], 2, 6,
		strlen(c_parity[parity_idx]), c_parity[parity_idx], 2);
	param_set.keyboard_type[idx ++] = _none_keyboard;
	for (i = 0; i < 2; i ++)
		param_set.input[idx].const_list[i] = c_stop_bit[i];
	param_set.input[idx].list_idx = stop_idx;
	param_set.input[idx].list_num = i;
	init_input_set(&param_set.input[idx], 2, 17, strlen(c_stop_bit[stop_idx]),
		c_stop_bit[stop_idx], 1);
	param_set.keyboard_type[idx ++] = _none_keyboard;
	sprintf(temp, "%d", buf[1]);
	init_input_set(&param_set.input[idx], 3, 14, strlen(temp), temp, 3);
	param_set.keyboard_type[idx ++] = _num_keyboard;
	sprintf(temp, "%d", buf[2]);
	init_input_set(&param_set.input[idx], 4, 14, strlen(temp), temp, 3);
	param_set.keyboard_type[idx ++] = _num_keyboard;
	sprintf(temp, "%d", buf[3]);
	init_input_set(&param_set.input[idx], 5, 14, strlen(temp), temp, 3);
	param_set.keyboard_type[idx ++] = _num_keyboard;
	sprintf(temp, "%d", buf[4]);
	init_input_set(&param_set.input[idx], 6, 14, strlen(temp), temp, 3);
	param_set.keyboard_type[idx ++] = _num_keyboard;
	sprintf(temp, "%d", buf[5]);
	init_input_set(&param_set.input[idx], 7, 10, strlen(temp), temp, 3);
	param_set.keyboard_type[idx ++] = _num_keyboard;
	process_param_set(&param_set, menu_name3_2_4);
	baud_idx = param_set.input[0].list_idx;
	bits_idx = param_set.input[1].list_idx;
	parity_idx = param_set.input[2].list_idx;
	stop_idx = param_set.input[3].list_idx;
	pack_cascade_param(buf, baud_idx, stop_idx, parity_idx, bits_idx);
	buf[1] = atoi(param_set.input[4].str);
	buf[2] = atoi(param_set.input[5].str);
	buf[3] = atoi(param_set.input[6].str);
	buf[4] = atoi(param_set.input[7].str);
	buf[5] = atoi(param_set.input[8].str);
	if (memcmp(buf_bak, buf, sizeof(buf)) != 0) {
		fparam_lock();
		falm_lock();
		fparam_set_value(FPARAM_AFN84_F10, buf, sizeof(buf), &param_len);
		spont_con_param_changed(0, 0x84, 0, 10);
		falm_unlock();
		fparam_unlock();
	}
}

static void view_signal_value(BYTE flag, void *para, const char *info)
{
	BYTE type, value, level; 
	char buf[30];

	lcd_clean_workspace();
	if (!get_net_type_signal(&type, &value, &level))
		strcpy(buf, c_info_comm_str[5]);
	else
		snprintf(buf, sizeof(buf), "%s %d", c_info_comm_str[6], value);
	lcd_show_string(2, 1, strlen(buf), buf);
	wait_any_key();
}

static void show_sample_doc(int *pos, int sample_idx)
{
	char str[12][MAX_SCREEN_COL + 1], temp_str[MAX_SCREEN_COL + 1];
	char *baud[] = {"600", "1200", "2400", "4800", "7200", "9600", "19200"};
	const char *prot[] = {PROTOCOL_TYPE_INVALID_STR, PROTOCOL_TYPE_645_STR,
		PROTOCOL_TYPE_AC_SAMPLE_STR};
	BYTE buf[16];
	int i, temp;
	
	t485_lock();
	t485_read(sample_idx + 1, 10, buf, sizeof(buf));
	t485_unlock();
	sprintf(str[0], "%s:%02d", c_sample_doc_str[4], sample_idx + 1);
	sprintf(str[1], "%s:%d", c_sample_doc_str[5], buf[0]);
	sprintf(str[2], "%s:%d", c_sample_doc_str[6], buf[1] & 0x1F);
	temp = (buf[1] >> 5) & 0x07; /* baudrate */
	if (temp == 0) temp = 1 + 1; /* defaut baud */
	sprintf(str[3], "%s:%s", c_sample_doc_str[7], baud[temp - 1]);
	sprintf(str[4], "%s:%s", c_sample_doc_str[8], prot[buf[2]>2 ? 0 : buf[2]]);
	sprintf(str[5], c_sample_doc_str[9]); /* communication address */
	sprintf(str[6], "%02x%02x%02x%02x%02x%02x", buf[8], buf[7], buf[6], buf[5],
		buf[4], buf[3]);
	sprintf(str[7], c_sample_doc_str[10]);
	sprintf(str[8], "************");
	temp = ((buf[15] >> 2) & 0x03) + 4;
	sprintf(str[9], "%s:%d", c_sample_doc_str[11], temp);
	temp = (buf[15] & 0x03) + 1;
	sprintf(str[10], "%s:%d", c_sample_doc_str[12], temp);
	temp = (buf[15] >> 4) & 0x0f;
	sprintf(str[11], "%s:%d", c_sample_doc_str[13], temp);
	if (*pos < 0)
		*pos = 0;
	else if (*pos > 12 - MAX_WORKSPACE_LINE)
		*pos = 12 - MAX_WORKSPACE_LINE;
	temp = sprintf(temp_str, "%-*s", MAX_SCREEN_COL, str[0]);
	lcd_show_string(2, 1, temp, temp_str);
	for (i = 1; i < MAX_WORKSPACE_LINE; i ++) {
		temp = sprintf(temp_str, "%-*s", MAX_SCREEN_COL, str[*pos + i]);
		lcd_show_string(i + 2, 1, temp, temp_str);
	}
}

static void view_sample_doc(BYTE flag, void *para, const char *info)
{
	BYTE key;
	int pn, sample_idx, pos = 0, loop = 1;
	
	if ((pn = input_pn(2, c_sample_basic_str[0], info)) == 0)
		return;
	lcd_show_arrow(1, 1, 1, 1);
	sample_idx = pn - 1;
	lcd_show_underline(2, 1, MAX_SCREEN_COL, 1);
	while (loop && lcd_mode_get() != 0) {
		show_sample_doc(&pos, sample_idx);
		key = getch_timeout();
		switch(key) {
		case KEY_UP:
			pos --;
			break;
		case KEY_DOWN:
			pos ++;
			break;
		case KEY_LEFT:
			t485_lock();
			while (!t485_all_is_empty()) {
				if (sample_idx == 0)
					sample_idx = T_485_ROWS_CNT - 1;
				else
					sample_idx --;
				if (!t485_is_empty(sample_idx))
					break;
			}
			t485_unlock();
			break;
		case KEY_RIGHT:
			t485_lock();
			while (!t485_all_is_empty()) {
				if (sample_idx == T_485_ROWS_CNT - 1)
					sample_idx = 0;
				else
					sample_idx ++;
				if (!t485_is_empty(sample_idx))
					break;
			}
			t485_unlock();
			break;
		case KEY_ESC:
			loop = 0;
			break;
		case KEY_NONE:
			lcd_mode_set(0);
			loop = 0;
			break;
		}
	}
	lcd_show_underline(2, 1, MAX_SCREEN_COL, 0);
}

static void show_usermt_doc(int *pos, int mtidx)
{
	char str[11][MAX_SCREEN_COL + 1], temp_str[MAX_SCREEN_COL + 1];
	const char *c_meter_type[] = {
		METER_TYPE_RS485_STR, METER_TYPE_PLC_STR, METER_TYPE_SIMPLE_STR,
		METER_TYPE_MULTI_STR, METER_TYPE_REPEATER_STR, UNKNOWN_STR,
		UNKNOWN_STR, UNKNOWN_STR
	};
	const char *c_prepay_type[] = {PREPAY_TYPE_ENERGY_STR, PREPAY_TYPE_FEE_STR,
		PREPAY_TYPE_NONO_STR, UNKNOWN_STR, UNKNOWN_STR, UNKNOWN_STR,
		UNKNOWN_STR, UNKNOWN_STR};
	const char *c_connect_type[] = {CONNECT_TYPE_DIRECT_STR,
		CONNECT_TYPE_NON_DIRECT_STR};
	const char *c_sp_pp_meter[] = {SPMT_STR, PPMT_STR};
	const char *c_phase[] = {NO_PHASE_STR, PHASE_A_STR, PHASE_B_STR,
		PHASE_C_STR};
	const char *c_meter_user_type[] = {NORMAL_USER_STR, FOCUS_USER_STR};
	const char *c_select_type[] = {UNSELECT_STR, SELECT_STR};
	const char *c_relay[] = {NO_RELAY_STR, RELAY_STR};
	const char *c_poweroff[] = {ENABLE_POWEROFF_STR, DISABLE_POWEROFF_STR};
	int i, temp1, temp2, temp3, temp4, temp5;
	BYTE buf[22];
	
	tmt_lock();
	tmt_get(mtidx, buf, 0);
	tmt_unlock();
	sprintf(str[0], "%s:%04d", c_usermt_doc_str[6], ctos(buf));
	sprintf(str[1], "%s:%02x%02x%02x%02x%02x%02x ", c_usermt_doc_str[7],
		buf[7], buf[6], buf[5], buf[4], buf[3], buf[2]); // mtid
	// type of meter
	temp1 = (buf[8] >> 4) & 0x07; temp2 = buf[8] & 0x07;
	sprintf(str[2], "%s%s", c_prepay_type[temp2], c_meter_type[temp1]);
	// connect type: e.g. direct SPMT
	temp1 = buf[9] & 0x01; temp2 = (buf[9] >> 1) & 0x01;
	sprintf(str[3], "%s %s", c_connect_type[temp1], c_sp_pp_meter[temp2]);
	temp1 = buf[10] & 0x0F; temp2 = (buf[10] >> 4) & 0x0F;
	sprintf(str[4], "%s:%d %s:%d", c_usermt_doc_str[8], temp1,
		c_usermt_doc_str[9], temp2); // tariff
	temp1 = buf[11] & 0x03;
	temp2 = ((buf[11] & 0x04) == 0x04);
	temp3 = ((buf[11] & 0x08) == 0x08);
	temp4 = ((buf[11] & 0x10) == 0x10);
	temp5 = ((buf[11] & 0x20) == 0x20);
	sprintf(str[5], "%s %s %s", c_phase[temp1], c_relay[temp2],
		c_meter_user_type[temp3]);
	sprintf(str[6], "%s %s", c_select_type[temp4], c_poweroff[temp5]);
	sprintf(str[7], "%s:%02x%02x", c_usermt_doc_str[10], buf[13], buf[12]);
	sprintf(str[8], "%s:%02x%02x", c_usermt_doc_str[11], buf[15], buf[14]);
	sprintf(str[9], "%s:", c_usermt_doc_str[12]);
	sprintf(str[10], "%02x%02x%02x%02x%02x%02x", buf[21], buf[20], buf[19],
		buf[18], buf[17], buf[16]);
	if (*pos < 0)
		*pos = 0;
	else if (*pos > 11 - MAX_WORKSPACE_LINE)
		*pos = 11 - MAX_WORKSPACE_LINE;
	temp1 = sprintf(temp_str, "%-*s", MAX_SCREEN_COL, str[0]);
	lcd_show_string(2, 1, temp1, temp_str);
	for (i = 1; i < MAX_WORKSPACE_LINE; i ++) {
		temp1 = sprintf(temp_str, "%-*s", MAX_SCREEN_COL, str[*pos + i]);
		lcd_show_string(i + 2, 1, temp1, temp_str);
	}
}

static void view_usermt_doc(BYTE flag, void *para, const char *info)
{
	char order[] = "0001";
	BYTE key;
	int mtidx, pos = 0, loop = 1, empty;

	lcd_clean_workspace();
	lcd_update_info(info);
	if (input_string(2, c_usermt_doc_str[6], fmt_num, order, 4) == 0)
		return;
	lcd_show_arrow(1, 1, 1, 1);
	mtidx = METER_INDEX(atoi(order) % NBR_REAL_MT);
	lcd_show_underline(2, 1, MAX_SCREEN_COL, 1);
	tmt_lock();
	empty = tmt_is_empty(mtidx);
	tmt_unlock();
	if (empty) {
		lcd_show_string(2, 1, strlen(c_usermt_doc_str[13]),
			c_usermt_doc_str[13]);
		wait_any_key();
		lcd_show_underline(2, 1, MAX_SCREEN_COL, 0);
		return;
	}
	while (loop && lcd_mode_get() != 0) {
		show_usermt_doc(&pos, mtidx);
		key = getch_timeout();
		switch(key) {
		case KEY_UP:
			pos --;
			break;
		case KEY_DOWN:
			pos ++;
			break;
		case KEY_LEFT:
			tmt_lock();
			while (!tmt_all_is_empty()) {
				if (mtidx == 0)
					mtidx = NBR_REAL_MT - 1;
				else
					mtidx --;
				if (!tmt_is_empty(mtidx))
					break;
			}
			tmt_unlock();
			break;
		case KEY_RIGHT:
			tmt_lock();
			while (!tmt_all_is_empty()) {
				if (mtidx == NBR_REAL_MT - 1)
					mtidx = 0;
				else
					mtidx ++;
				if (!tmt_is_empty(mtidx))
					break;
			}
			tmt_unlock();
			break;
		case KEY_ESC:
			loop = 0;
			break;
		case KEY_NONE:
			lcd_mode_set(0);
			loop = 0;
			break;
		}
	}
	lcd_show_underline(2, 1, MAX_SCREEN_COL, 0);
}

static void set_sample_basic(BYTE flag, void *para, const char *info)
{
	PARAM_LIST param_list;
	BYTE buf[8], value_idx[MAX_INPUT_DIGIT_NUM], pn = 0, ret;
	char string[7][MAX_SCREEN_COL + 1];
	int param_len = 0, idx = 0, i, temp;
	INPUT_LIST *list1, *list2;

	memset(string, 0, sizeof(string));
	memset(&param_list, 0, sizeof(param_list));
	memset(&buf, 0, sizeof(buf));
	if ((pn = input_pn(2, c_sample_basic_str[0], info)) == 0)
		return;
	t485_lock();
	t485_read(pn, 25, buf, sizeof(buf));
	t485_unlock();
	init_menu(&param_list.menu);
	param_list.menu.line_num = 7;
	for (i = 0; i < param_list.menu.line_num; i ++) {
		param_list.menu.str[i] = string[i];
	}
	sprintf(string[0], c_sample_basic_str[1]);
	param_list.menu.str[1] = NULL;
	sprintf(string[2], c_sample_basic_str[2]);
	param_list.menu.str[3] = NULL;
	sprintf(string[4], "%s   .  V", c_sample_basic_str[3]);
	sprintf(string[5], "%s .  A", c_sample_basic_str[4]);
	sprintf(string[6], c_sample_basic_str[5]);
	param_list.group_num = 7;
	param_list.group_idx = 0;
	param_list.digit_idx = 0;
	temp = ctos(&buf[0]);
	value_idx[3] = temp % 10;
	value_idx[2] = (temp / 10) % 10;
	value_idx[1] = (temp / 100) % 10;
	value_idx[0] = (temp / 1000) % 10;
	init_input_list(&param_list.list[idx], 2, 1, 4, value_idx, 10,
		c_numeric_list, 9999);
	idx ++; /* voltage multiple */
	temp = ctos(&buf[2]);
	value_idx[3] = temp % 10;
	value_idx[2] = (temp / 10) % 10;
	value_idx[1] = (temp / 100) % 10;
	value_idx[0] = (temp / 1000) % 10;
	init_input_list(&param_list.list[idx], 4, 1, 4, value_idx, 10,
		c_numeric_list, 9999);
	idx ++; /* current multiple */
	value_idx[2] = (buf[4] >> 4) & 0x0F;
	value_idx[1] = buf[5] & 0x0f;
	value_idx[0] = (buf[5] >> 4) & 0x0f;
	init_input_list(&param_list.list[idx], 5, 10, 3, value_idx, 10,
		c_numeric_list, 999);
	idx ++; /* integer of voltage */
	value_idx[0] = buf[4] & 0x0F;
	init_input_list(&param_list.list[idx], 5, 14, 1, value_idx, 10,
		c_numeric_list, 9); idx ++; /* decimal of voltage */
	value_idx[0] = (buf[6] >> 4) & 0x0F;
	init_input_list(&param_list.list[idx], 6, 10, 1, value_idx, 10,
		c_numeric_list, 9);
	idx ++; /* integer of current */
	value_idx[0] = buf[6] & 0x0F;
	init_input_list(&param_list.list[idx], 6, 12, 1, value_idx, 10,
		c_numeric_list, 9);
	idx ++; /* decimal of current */
	value_idx[0] = buf[7] & 0x0F;
	init_input_list(&param_list.list[idx], 7, 14, 1, value_idx, 10,
		c_numeric_list, 3); /* connection type */
	ret = process_param_list(&param_list, menu_name2_2);
	if (ret == KEY_ESC)
		return;
	list1 = &param_list.list[0];
	stoc(&buf[0], list1->value_idx[0] * 1000 + list1->value_idx[1] * 100
		+ list1->value_idx[2] * 10 + list1->value_idx[3]);
	list1 = &param_list.list[1];
	stoc(&buf[2], list1->value_idx[0] * 1000 + list1->value_idx[1] * 100
		+ list1->value_idx[2] * 10 + list1->value_idx[3]);
	list1 = &param_list.list[2]; list2 = &param_list.list[3];
	stoc(&buf[4], (list1->value_idx[0] << 12) + (list1->value_idx[1] << 8)
		+ (list1->value_idx[2] << 4) + (list2->value_idx[0]));
	list1 = &param_list.list[4]; list2 = &param_list.list[5];
	buf[6] = (list1->value_idx[0] << 4)	+ list2->value_idx[0];
	list1 = &param_list.list[6];
	buf[7] = list1->value_idx[0] & 0x0F;
	t485_lock();
	t485_write(pn, 25, buf, sizeof(buf), &param_len);
	t485_unlock();
}

static void unpack_sample_limit(INPUT_LIST *list, BYTE *buf)
{
	WORD max_value[] = {9, 99, 999, 9999, 9999};
	int i, j;
	
	for (i = 0; i < 5; i ++) {
		list[i * 2].value_idx[0] = (buf[i * 2 + 1] >> 4) & 0x0F;
		list[i * 2].value_idx[1] = buf[i * 2 + 1] & 0x0F;
		list[i * 2].value_idx[2] = (buf[i * 2] >> 4) & 0x0F;
		list[i * 2].digit_num = 3;
		list[i * 2 + 1].value_idx[0] = buf[i * 2] & 0x0F;
		list[i * 2 + 1].digit_num = 1;
	}
	for (i = 0; i < 3; i ++) {
		list[10 + i * 2].value_idx[0] = (buf[10 + i * 2 + 1] >> 4) & 0x0F;
		list[10 + i * 2].value_idx[1] = buf[10 + i * 2 + 1] & 0x0F;
		list[10 + i * 2].digit_num = 2;
		list[10 + i * 2 + 1].value_idx[0] = (buf[10 + i * 2] >> 4) & 0x0F;
		list[10 + i * 2 + 1].value_idx[1] = buf[10 + i * 2] & 0x0F;
		list[10 + i * 2 + 1].digit_num = 2;
	}
	for (i = 0; i < 2; i ++) {
		list[16 + i * 2].value_idx[0] = (buf[16 + i * 3 + 2] >> 4) & 0x0F;
		list[16 + i * 2].value_idx[1] = buf[16 + i * 3 + 2] & 0x0F;
		list[16 + i * 2].digit_num = 2;
		list[16 + i * 2 + 1].value_idx[0] = (buf[16 + i * 3 + 1] >> 4) & 0x0F;
		list[16 + i * 2 + 1].value_idx[1] = buf[16 + i * 3 + 1] & 0x0F;
		list[16 + i * 2 + 1].value_idx[2] = (buf[16 + i * 3] >> 4) & 0x0F;
		list[16 + i * 2 + 1].value_idx[3] = buf[16 + i * 3] & 0x0F;
		list[16 + i * 2 + 1].digit_num = 4;
	}
	for (i = 0; i < 2; i ++) {
		list[20 + i * 2].value_idx[0] = (buf[22 + i * 2 + 1] >> 4) & 0x0F;
		list[20 + i * 2].value_idx[1] = buf[22 + i * 2 + 1] & 0x0F;
		list[20 + i * 2].value_idx[2] = (buf[22 + i * 2] >> 4) & 0x0F;
		list[20 + i * 2].digit_num = 3;
		list[20 + i * 2 + 1].value_idx[0] = buf[22 + i * 2] & 0x0F;
		list[20 + i * 2 + 1].digit_num = 1;
	}
	list[24].value_idx[0] = (buf[26] / 10) % 10;
	list[24].value_idx[1] = buf[26] % 10;
	list[24].digit_num = 2;
	for (i = 0; i < 25; i ++) {
		list[i].list_num = 10;
		list[i].max_value = max_value[list[i].digit_num - 1];
		for (j = 0; j < list[i].list_num; j ++) {
			list[i].const_list[j] = c_numeric_list[j];
		}
	}
}

static void pack_sample_limit(BYTE *buf, INPUT_LIST *list)
{
	int i;
	
	for (i = 0; i < 5; i ++) {
		buf[i * 2] = (list[i * 2].value_idx[2] << 4)
			+ list[i * 2 + 1].value_idx[0];
		buf[i * 2 + 1] = (list[i * 2].value_idx[0] << 4)
			+ list[i * 2].value_idx[1];
	}
	for (i = 0; i < 3; i ++) {
		buf[10 + i * 2] = (list[10 + i * 2 + 1].value_idx[0] << 4)
			+ list[10 + i * 2 + 1].value_idx[1];
		buf[10 + i * 2 + 1] = (list[10 + i * 2].value_idx[0] << 4)
			+ list[10 + i * 2].value_idx[1];
	}
	for (i = 0; i < 2; i ++) {
		buf[16 + i * 3] = (list[16 + i * 2 + 1].value_idx[2] << 4)
			+ list[16 + i * 2 + 1].value_idx[3];
		buf[16 + i * 3 + 1] = (list[16 + i * 2 + 1].value_idx[0] << 4)
			+ list[16 + i * 2 + 1].value_idx[1];
		buf[16 + i * 3 + 2] = (list[16 + i * 2].value_idx[0] << 4)
			+ list[16 + i * 2].value_idx[1];
	}
	for (i = 0; i < 2; i ++) {
		buf[22 + i * 2] = (list[20 + i * 2].value_idx[2] << 4)
			+ list[20 + i * 2 + 1].value_idx[0];
		buf[22 + i * 2 + 1] = (list[20 + i * 2].value_idx[0] << 4)
			+ list[20 + i * 2].value_idx[1];
	}
	buf[26] = list[24].value_idx[0] * 10 + list[24].value_idx[1];
}

static void set_sample_limit(BYTE flag, void *para, const char *info)
{
	PARAM_LIST param_list;
	BYTE buf[27], pn, ret;
	char string[18][MAX_SCREEN_COL + 1];
	int param_len = 0, i;
	POSITION list_position[] = {
		{1, 14}, {1, 18}, {2, 14}, {2, 18}, {3, 14}, {3, 18}, {4, 10}, {4, 14},
		{5, 10}, {5, 14}, {6, 10}, {6, 13}, {7, 14}, {7, 17}, {8, 14}, {8, 17},
		{10, 1}, {10, 4}, {12, 1}, {12, 4}, {14, 1}, {14, 5}, {16, 1}, {16, 5},
		{18, 1},
	};

	memset(string, 0, sizeof(string));
	memset(&param_list, 0, sizeof(param_list));
	memset(&buf, 0, sizeof(buf));
	if ((pn = input_pn(2, c_sample_limit_str[0], info)) == 0)
		return;
	t485_lock();
	t485_read(pn, 26, buf, sizeof(buf));
	t485_unlock();
	init_menu(&param_list.menu);
	param_list.menu.line_num = 18;
	for (i = 0; i < param_list.menu.line_num; i ++) {
		param_list.menu.str[i] = string[i];
		sprintf(string[i], c_sample_limit_str[i + 1]);
	}
	param_list.group_num = sizeof(list_position) / sizeof(POSITION);
	param_list.group_idx = 0;
	param_list.digit_idx = 0;
	unpack_sample_limit(param_list.list, buf);
	init_list_position(param_list.list, list_position, param_list.group_num);
	ret = process_param_list(&param_list, menu_name2_3);
	if (ret == KEY_ESC)
		return;
	pack_sample_limit(buf, param_list.list);
	t485_lock();
	t485_write(pn, 26, buf, sizeof(buf), &param_len);
	t485_unlock();
}

static void view_sample_stat(BYTE flag, void *para, const char *info)
{
	int i;
	BYTE buf[4];
	MENU menu;
	char string[4][MAX_SCREEN_COL + 1];

	t485_lock();
	t485_get_meters_type(buf, sizeof(buf));
	t485_unlock();
	init_menu(&menu);
	menu.line_num = 4;
	for (i = 0; i < menu.line_num; i ++)
		menu.str[i] = string[i];
	for (i = 0; i < menu.line_num; i ++)
		sprintf(string[i], "%s:%02x", c_sample_doc_str[i], buf[i]);
	process_menu(&menu, info);
}

static void view_usermt_stat(BYTE flag, void *para, const char *info)
{
	int i;
	BYTE buf[22];
	WORD mt_nbr[6];
	MENU menu;
	char string[6][MAX_SCREEN_COL + 1];

	tmt_lock();
	tmt_get_sort_nbr(buf, sizeof(buf));
	tmt_unlock();
	mt_nbr[0] = ctos(&buf[9]); mt_nbr[1] = ctos(&buf[0]);
	mt_nbr[2] = buf[4]; mt_nbr[3] = ctos(&buf[5]);
	mt_nbr[4] = ctos(&buf[7]); mt_nbr[5] = ctos(&buf[2]);
	init_menu(&menu);
	menu.line_num = 6;
	for (i = 0; i < menu.line_num; i ++)
		menu.str[i] = string[i];
	for (i = 0; i < menu.line_num; i ++)
		sprintf(string[i], "%s:%d", c_usermt_doc_str[i], mt_nbr[i]);
	process_menu(&menu, info);
}

static void view_read_usermt_status(BYTE flag, void *para, const char *info)
{
	BYTE buf[4];
	char string[4][MAX_SCREEN_COL + 1];
	int i;
	MENU menu;
	
	init_menu(&menu);
	menu.line_num = 4;
	for (i = 0; i < menu.line_num; i ++)
		menu.str[i] = string[i];
	tmt_lock();
	fparam_lock();
	fmt_lock();
	sprintf(string[0], "%s:%d", c_read_usermt_status_str[0],
		tmt_get_read_nbr());
	sprintf(string[1], "%s:%d", c_read_usermt_status_str[1],
		fmt_get_read_nbr());
	fparam_read(FPARAM_FDAY_FINISH_FLAG, buf, 1);
	sprintf(string[2], "%s:%s", c_read_usermt_status_str[2],
		(buf[0] == 0x00) ? c_read_usermt_status_str[3]
		: c_read_usermt_status_str[4]);
	fparam_read(FPARAM_FDAY_FINISH_TIME, buf, 3);
	sprintf(string[3], "%s:%02x%s%02x:%02x", c_read_usermt_status_str[5],
		buf[2], c_read_usermt_status_str[6], buf[1], buf[0]);
	fmt_unlock();
	fparam_unlock();
	tmt_unlock();
	process_menu(&menu, menu_name2_5);
}

static void view_event_stat(BYTE flag, void *para, const char *info)
{
	MENU menu;
	char string[8][MAX_SCREEN_COL + 1];
	/* cnt[8]: event count
	 *  concentrator: old, new, normal, important;
	 *  user meter: old, new, normal, important;
	 */
	int cnt[8], i;
	
	falm_lock();
	falm_get_stat_cnt(cnt, sizeof(cnt) / sizeof(int));
	falm_unlock();
	init_menu(&menu);
	menu.line_num = 8;
	for (i = 0; i < menu.line_num; i ++)
		menu.str[i] = string[i];
	sprintf(string[0], "%s %d", menu_name2_8_3, cnt[0]);
	sprintf(string[1], "%s %d", menu_name2_8_2, cnt[1]);
	sprintf(string[2], "%s %d", menu_name2_8_5, cnt[2]);
	sprintf(string[3], "%s %d", menu_name2_8_4, cnt[3]);
	for (i = 4; i < menu.line_num; i ++)
		sprintf(string[i], "%s %d", c_con_event_stat_str[i - 4], cnt[i]);
	process_menu(&menu, menu_name2_8_1);
}

static int get_alm_type(F_ALM *alm)
{
	int ret = 0;
	
	if (alm->flag == 2 && (alm->type == 0 || alm->type == 1))
		ret = 1;
	else if (alm->flag == 1 && (alm->type == 0 || alm->type == 1))
		ret = 2;
	else if (alm->flag == 2 && (alm->type == 2 || alm->type == 3))
		ret = 3;
	else if (alm->flag == 1 && (alm->type == 2 || alm->type == 3))
		ret = 4;
	if (ret > 0 && ret < 3 && alm->data[0] <= atoi(c_con_erc_record_str[0]))
		return ret;
	else if (ret > 2 && alm->data[0] <= atoi(c_meter_erc_record_str[0]))
		return ret;
	else
		return 0;
}

static void view_erc_info(BYTE flag, void *para, const char *info)
{
	int idx, type, i, mtidx, tmp;
	MTID mtid;
	F_ALM alm;
	MENU menu;
	BYTE erc;
	char string[7][MAX_SCREEN_COL + 1];

	idx = (int)para;
	falm_get_one(idx, &alm);
	init_menu(&menu);
	for (i = 0; i < sizeof(string) / sizeof(string[0]); i ++)
		menu.str[i] = string[i];
	type = get_alm_type(&alm);
	switch (type) {
	case 1: /* total-meter history event */
	case 2: /* total-meter new event */
		erc = alm.data[0];
		sprintf(string[0], "%s", c_con_erc_record_str[erc]);
		sprintf(string[1], "%s: %s", c_view_erc_info_str[0],
			(alm.type==1)?c_view_erc_info_str[1]:c_view_erc_info_str[2]);
		if (erc == 14) {
			/* power off/on event of concentrator */
			sprintf(string[2], "%s:", c_view_erc_info_str[3]);
			sprintf(string[3], "  %02x-%02x-%02x %02x:%02x",
				alm.data[6], alm.data[5], alm.data[4], alm.data[3],
				alm.data[2]);
			sprintf(string[4], "%s:", c_view_erc_info_str[4]);
			sprintf(string[5], "  %02x-%02x-%02x %02x:%02x", alm.data[11],
				alm.data[10], alm.data[9], alm.data[8], alm.data[7]);
			menu.line_num = 6;
		}
		else {
			sprintf(string[2], "%s:", c_view_erc_info_str[5]);
			sprintf(string[3], "  %02x-%02x-%02x %02x:%02x",
				alm.data[6], alm.data[5], alm.data[4], alm.data[3],
					alm.data[2]);
			if (erc == 21) {
				tmp = min(atoi(c_con_malfunction_str[0]) - 1, alm.data[7]);
				sprintf(string[4], "%s", c_con_malfunction_str[tmp]);
				menu.line_num = 5;
			}
			else
				menu.line_num = 4;
		}
		break;
	case 3: /* user meter history event */
	case 4: /* user meter new event */
		erc = alm.data[0];
		sprintf(string[0], "%s", c_meter_erc_record_str[erc]);
		sprintf(string[1], "%s: %s", c_view_erc_info_str[0],
			(alm.type==3)?c_view_erc_info_str[1]:c_view_erc_info_str[2]);
		if (erc == 7) { /* event of reading meter statisics */
			sprintf(string[2], "%s: %02x-%02x", c_view_erc_info_str[8],
				alm.data[3], alm.data[2]);
			sprintf(string[3], "%s: %d", c_view_erc_info_str[9],
				ctos(&alm.data[4]));
			sprintf(string[4], "%s: %d", c_view_erc_info_str[10],
				ctos(&alm.data[6]));
			mtidx = ctos(&alm.data[8]);
			sprintf(string[5], "%s: %d", c_view_erc_info_str[11],
				METER_ORDER(mtidx));
			tmt_get_mtid(mtidx, mtid);
			sprintf(string[6], "%s: %02x%02x%02x%02x%02x%02x",
				c_view_erc_info_str[7], mtid[5], mtid[4], mtid[3], mtid[2],
				mtid[1], mtid[0]);
			menu.line_num = 7;
		}
		else if (erc == 12) {
			sprintf(string[2], "%s:", c_view_erc_info_str[5]);
			sprintf(string[3], "  %02x-%02x-%02x %02x:%02x", alm.data[6],
				alm.data[5], alm.data[4], alm.data[3], alm.data[2]);
			tmp = min(atoi(c_con_malfunction_str[0]), alm.data[7]);
			sprintf(string[4], "%s", c_con_malfunction_str[tmp]);
			menu.line_num = 5;
		}
		else {
			mtidx = ctos(&alm.data[2]);
			sprintf(string[2], "%s: %d", c_view_erc_info_str[6],
				METER_ORDER(mtidx));
			tmt_get_mtid(mtidx, mtid);
			sprintf(string[3], "%s: %02x%02x%02x%02x%02x%02x",
				c_view_erc_info_str[7], mtid[5], mtid[4], mtid[3], mtid[2],
				mtid[1], mtid[0]);
			sprintf(string[4], "%s:", c_view_erc_info_str[4]);
			sprintf(string[5], "%02x-%02x-%02x %02x:%02x", alm.data[8],
				alm.data[7], alm.data[6], alm.data[5], alm.data[4]);
			menu.line_num = 6;
		}
		break;
	default: /* the record has not used */
		lcd_clean_workspace();
		lcd_update_info(c_no_this_event_str);
		wait_any_key();
		return;
	}
	process_menu(&menu, string[0]);
}

static void view_event_info(int type, int new_erc, const char *info)
{
	int erc_idx[512];
	BYTE erc_code[512];
	int i, cnt;
	ITEMS_MENU items_menu;
	char *string[MAX_MENU_LINE], *p_string;
	char info_str[MAX_SCREEN_COL + 1];

	falm_lock();
	cnt = falm_get_idx(type, new_erc, erc_idx, sizeof(erc_idx) / sizeof(int), erc_code, sizeof(erc_code));
	falm_unlock();
	if (cnt <= 0) {
		lcd_clean_workspace();
		sprintf(info_str, "%s%s", c_no_str, info);
		lcd_update_info(info_str);
		wait_any_key();
		return;
	}
	init_menu(&items_menu.menu);
	items_menu.cur_line = 1;
	items_menu.menu.line_num = min(MAX_MENU_LINE, cnt);
	p_string = (char *)malloc(items_menu.menu.line_num * (MAX_SCREEN_COL + 1));
	for (i = 0; i < items_menu.menu.line_num; i ++) {
		string[i] = &p_string[i * (MAX_SCREEN_COL + 1)];
		items_menu.menu.str[i] = string[i];
	}
	if (type == 0) {
		for (i = 0; i < items_menu.menu.line_num; i ++) {
			if (erc_code[i] >= 1 && erc_code[i] <= atoi(c_con_erc_record_str[0])) {
				sprintf(string[i], "%d.%s", i + 1, c_con_erc_record_str[erc_code[i]]);
				items_menu.func[i] = view_erc_info;
				items_menu.para[i] = (void *)erc_idx[i];
			}
		}
	}
	else {
		for (i = 0; i < items_menu.menu.line_num; i ++) {
			if (erc_code[i] >= 1 && erc_code[i] <= atoi(c_meter_erc_record_str[0])) {
				sprintf(string[i], "%d.%s", i + 1, c_meter_erc_record_str[erc_code[i]]);
				items_menu.func[i] = view_erc_info;
				items_menu.para[i] = (void *)erc_idx[i];
			}
		}
	}
	snprintf(info_str, sizeof(info_str), "%s%d", info, cnt);
	process_items(&items_menu, info_str, 0);
	free(p_string);
}

static void view_con_old_event(BYTE flag, void *para, const char *info)
{
	view_event_info(0, 0, info);
}

static void view_con_new_event(BYTE flag, void *para, const char *info)
{
	view_event_info(0, 1, info);
}

static void view_usermt_old_event(BYTE flag, void *para, const char *info)
{
	view_event_info(1, 0, info);
}

static void view_usermt_new_event(BYTE flag, void *para, const char *info)
{
	view_event_info(1, 1, info);
}

static void view_event(BYTE flag, void *para, const char *info)
{
	ITEMS_MENU items_menu;
	int idx = 0;

	init_menu(&items_menu.menu);
	items_menu.cur_line = 1;
	items_menu.menu.str[idx] = menu_name2_8_1;
	items_menu.func[idx ++] = view_event_stat;
	items_menu.menu.str[idx] = menu_name2_8_2;
	items_menu.func[idx ++] = view_con_new_event;
	items_menu.menu.str[idx] = menu_name2_8_3;
	items_menu.func[idx ++] = view_con_old_event;
	items_menu.menu.str[idx] = menu_name2_8_4;
	items_menu.func[idx ++] = view_usermt_new_event;
	items_menu.menu.str[idx] = menu_name2_8_5;
	items_menu.func[idx ++] = view_usermt_old_event;
	items_menu.menu.line_num = idx;
	process_items(&items_menu, menu_name2_8, 1);
}

static void view_set_param(BYTE flag, void *para, const char *info)
{
	ITEMS_MENU items_menu;

	memset(&items_menu, 0, sizeof(items_menu));
	init_menu(&items_menu.menu);
	items_menu.cur_line = 1;
	items_menu.menu.line_num = 8;
	items_menu.menu.str[0] = menu_name2_1;
	items_menu.func[0] = view_sample_doc;
	items_menu.menu.str[1] = menu_name2_2;
	items_menu.func[1] = set_sample_basic;
	items_menu.menu.str[2] = menu_name2_3;
	items_menu.func[2] = set_sample_limit;
	items_menu.menu.str[3] = menu_name2_4;
	items_menu.func[3] = view_sample_stat;
	items_menu.menu.str[4] = menu_name2_5;
	items_menu.func[4] = view_usermt_doc;
	items_menu.menu.str[5] = menu_name2_6;
	items_menu.func[5] = view_usermt_stat;
	items_menu.menu.str[6] = menu_name2_7;
	items_menu.func[6] = view_read_usermt_status;
	items_menu.menu.str[7] = menu_name2_8;
	items_menu.func[7] = view_event;
	process_items(&items_menu, menu_name2, 1);
}

static void set_con_comm(BYTE flag, void *para, const char *info)
{
	ITEMS_MENU items_menu;

	memset(&items_menu, 0, sizeof(items_menu));
	init_menu(&items_menu.menu);
	items_menu.menu.line_num = 5;
	items_menu.cur_line = 1;
	items_menu.menu.str[0] = menu_name3_2_1;
	items_menu.func[0] = view_set_channel;
	items_menu.flag[0] = 1;
	items_menu.menu.str[1] = menu_name3_2_2;
	items_menu.func[1] = view_set_gprs;
	items_menu.flag[1] = 1;
	items_menu.menu.str[2] = menu_name3_2_3;
	items_menu.func[2] = view_set_sms;
	items_menu.flag[2] = 1;
	items_menu.menu.str[3] = menu_name3_2_4;
	items_menu.func[3] = view_set_cascade;
	items_menu.flag[3] = 1;
	items_menu.menu.str[4] = menu_name3_2_5;
	items_menu.func[4] = view_signal_value;
	items_menu.flag[4] = 0;
	process_items(&items_menu, menu_name3_2, 1);
}

static void modify_con_addr(BYTE flag, void *para, const char *info)
{
	const char *prompt = "    ";
	char input[4 * 2 + 1];
	BYTE buf[4];
	int param_len;
	
	memset(input, 0, sizeof(input));
	if (check_password(info, 0)) {
		lcd_update_info(info);
		fparam_lock();
		fparam_get_value(FPARAM_CON_ADDRESS, buf, sizeof(buf));
		fparam_unlock();
		sprintf(input, "%02x%02x%02x%02x", buf[0], buf[1], buf[2], buf[3]);
		lcd_show_string(2, 1, strlen(con_address_str), con_address_str);
		if (input_string(3, prompt, fmt_hex, input,
			sizeof(input) - 1)) {
			hexstr_to_str(buf, input, 8);
			fparam_lock();
			falm_lock();
			fparam_set_value(FPARAM_CON_ADDRESS, buf, sizeof(buf), &param_len);
			falm_unlock();
			fparam_unlock();
		}
	}
}

static void modify_sample_addr(BYTE flag, void *para, const char *info)
{
	const char *prompt = "    ";
	char order[] = "01", input[MTID_LEN * 2 + 1];
	int sample_idx, pn;
	MTID mtid;
	
	if (check_password(info, 0) && (pn = input_pn(3, prompt, info)) > 0) {
		sample_idx = pn - 1;
		memset(input, 0, sizeof(input));
		t485_lock();
		t485_get_mtid(sample_idx, mtid);
		t485_unlock();
		reverse(mtid, MTID_LEN);
		str_to_hexstr((BYTE *)input, mtid, MTID_LEN);
		lcd_show_string(2, 1, strlen(c_sample_doc_str[4]),
			c_sample_doc_str[4]);
		lcd_show_string(3, strlen(prompt) + 1, strlen(order), order);
		lcd_show_string(4, 1, strlen(c_sample_doc_str[9]),
			c_sample_doc_str[9]);
		if (input_string(5, prompt, fmt_hex, input, sizeof(input) - 1)) {
			hexstr_to_str(mtid, input, MTID_LEN * 2);
			reverse(mtid, MTID_LEN);
			t485_lock();
			t485_set_default(sample_idx, mtid);
			t485_unlock();
		}
	}
}

static void con_font(BYTE flag, void *para, const char *info)
{
	int param_len;
	BYTE font_size;

	if (!check_password(info, 0))
		return;
	fparam_lock();
	fparam_get_value(FPARAM_FONT_SIZE, &font_size, 1);
	if (font_size == 16)
		font_size = 12;
	else
		font_size = 16;
	fparam_set_value(FPARAM_FONT_SIZE, &font_size, 1, &param_len);
	fparam_unlock();
	lcd_update_info(REBOOT_STR);
	lcd_clean_workspace();
	wait_any_key();
}

static void con_manage(BYTE flag, void *para, const char *info)
{
	ITEMS_MENU items_menu;
	int idx = 0;

	memset(&items_menu, 0, sizeof(items_menu));
	init_menu(&items_menu.menu);
	items_menu.cur_line = 1;
	items_menu.menu.str[idx] = menu_name3_1;
	items_menu.func[idx ++] = view_con_info;
	items_menu.menu.str[idx] = menu_name3_2;
	items_menu.func[idx ++] = set_con_comm;
	items_menu.menu.str[idx] = menu_name3_3;
	items_menu.func[idx ++] = calibrate_clock;
	items_menu.menu.str[idx] = menu_name3_4;
	items_menu.func[idx ++] = modify_menu_passwd;
	items_menu.menu.str[idx] = menu_name3_5;
	items_menu.func[idx ++] = modify_con_addr;
	items_menu.menu.str[idx] = menu_name3_6;
	items_menu.func[idx ++] = modify_sample_addr;
	items_menu.menu.str[idx] = menu_name3_8;
	items_menu.func[idx ++] = con_font;
	items_menu.menu.str[idx] = menu_name3_9;
	items_menu.func[idx ++] = con_init;
	items_menu.menu.line_num = idx;
	process_items(&items_menu, menu_name3, 1);
}

static void view_all_sample(BYTE flag, void *para, const char *info)
{
	ITEMS_MENU items_menu;
	int i;
	
	init_menu(&items_menu.menu);
	items_menu.cur_line = 1;
	items_menu.menu.line_num = 1 + T_485_ROWS_CNT;
	items_menu.menu.str[0] = menu_name1_1;
	items_menu.func[0] = view_usermt;
	for (i = 1; i < items_menu.menu.line_num; i++) {
		items_menu.menu.str[i] = menu_name1_2[i - 1];
		items_menu.func[i] = view_485mt;
		items_menu.para[i] = (void *)i; /* index of sample */
	}
	process_items(&items_menu, menu_name1, 1);
}

/* main menu */
void main_menu_init(ITEMS_MENU *items_menu)
{
	init_menu(&items_menu->menu);
	items_menu->cur_line = 1;
	items_menu->menu.line_num = 3;
	items_menu->menu.str[0] = menu_name1;
	items_menu->func[0] = view_all_sample;
	items_menu->menu.str[1] = menu_name2;
	items_menu->func[1] = view_set_param;
	items_menu->menu.str[2] = menu_name3;
	items_menu->func[2] = con_manage;
}
