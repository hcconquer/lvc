/*
 *  C-Plan Concentrator
 *
 *  Copyright (C) 2006-2007, Shenzhen Kaifa Technology Co.,Ltd.
 *
 *  cascade thread process function
 *
 *  Author: dajiang wan
 *  Created on: 2006-09-05
 */
#include "common.h"
#include "device.h"
#include "global.h"
#include "msg_proc.h"
#include "msg_que.h"
#include "threads.h"
#include "f_param.h"

#define CHAR_TIMEOUT		1000	// ms
#define PACKET_TIMEOUT		2500	// ms

static int cas_read_packet(void *buf, int max_len)
{
	static BYTE save_buf[MAX_LDU_LEN + 4];
	static int save_len;
	BYTE *ptr;
	int len;

	if (save_len == 0) {
		cas_lock();
		save_len = cas_read(save_buf, sizeof(save_buf), PACKET_TIMEOUT,
			CHAR_TIMEOUT);
		cas_unlock();
		PRINTB("From 485: ", save_buf, save_len);
	}
	if (save_len == 0 || (ptr = memchr(save_buf, 0x68, save_len)) == NULL) {
		if (save_len > 0)
			save_len = 0;
		return 0;
    }
	if (ptr != save_buf) {
		save_len -= (ptr - save_buf);
		memmove(save_buf, ptr, save_len);
	}
	if ((len = check_packet(save_buf, save_len)) == 0) {
		if (save_len > 0) {
			PRINTB("Bad packet: ", save_buf, save_len);
			save_len = 0;
		}
		return 0;
	}
	else if (len <= max_len) { // can copy the packet to the buffer
		memcpy(buf, save_buf, len);
		memmove(save_buf, save_buf + len, save_len - len);
		save_len -= len;
		return len;
	}
	else { // buffer is too small, discard the valid packet
		PRINTF("Buffer too small, discard the received packet\n");
		memmove(save_buf, save_buf + len, save_len - len);
		save_len -= len;
		return 0;
	}
}

static void cas_write_packet(const void *buf, int len)
{
	BYTE save_buf[MAX_LDU_LEN + 4], *ptr = save_buf;
	int save_len;

	*ptr ++ = 0xfe;
	*ptr ++ = 0xfe;
	*ptr ++ = 0xfe;
	*ptr ++ = 0xfe;
	memcpy(ptr, buf, len);
	ptr += len;
	save_len = ptr - save_buf;
	PRINTB("To 485: ", save_buf, save_len);
	cas_lock();
	cas_write(save_buf, save_len, PACKET_TIMEOUT);
	cas_unlock();
}

static int cas_process(void)
{
	BYTE buf[MAX_LDU_LEN];
	int len, ret = 0, max_len = sizeof(buf);

	len = cas_read_packet(buf, max_len);
	if (len > 0) {
		if (check_address(buf, len)) {
			msg_que_put(MSG_QUE_SLAVE_IN, buf, len, 0);
			msg_proc(MSG_QUE_SLAVE_IN, MSG_QUE_SLAVE_OUT, max_len);
		}
		ret = 1;
	}
	msg_que_get(MSG_QUE_SLAVE_OUT, buf, max_len, &len, 0);
	if (len > 0) {
		cas_write_packet(buf, len);
		ret = 1;
	}
	return ret;
}

void *cascade_thread(void *arg)
{
	long idle_time;

	idle_time = uptime();
	while (!g_terminated) {
		notify_watchdog();
		check_sched();
		if (uptime() - idle_time >= 7 * 60) { // recv nothing in 7 minutes
			self_diag(cas_open, cas_close);
			idle_time = uptime();
		}
		if (cas_process()) {
			idle_time = uptime();
		}
		if (!g_terminated && get_reboot_flag(1)) {
			msleep(500);
			g_terminated = 1; // we must reply first, then reboot
		}
	}
	return NULL;
}
