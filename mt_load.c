/*****************************************************************************
*	C-Plan Concentrator
*
*	(c) Copyright 2006-2007, Shenzhen Kaifa Technology Co.,Ltd.
*	All Rights Reserved
*
*	to read the load data from the focus user.
*
*	Author: fanhua zeng, djwan
*	Created on: 2006-07-31
*****************************************************************************/
#include "common.h"
#include "mt_load.h"
#include "mt_access.h"
#include "t_mt.h"
#include "t_stat.h"
#include "msg_code.h"
#include "f_mt.h"
#include "f_param.h"
#include "misc.h"
#include "err_code.h"
#include "msg_proc.h"

static int need_read(BYTE begin, BYTE end, BYTE *start, const BYTE *buf)
{
	int val, tmp;

	for (tmp = end; tmp >= begin; tmp --) {
		if (!energy_convert(buf + tmp * ENERGY_LEN, &val)) {
			*start = tmp;
			return 1;
		}
	}
	return 0;
}

static void check_samples(short mtidx, BYTE *samples)
{
	BYTE *ptr, err_data[ENERGY_LEN * NBR_SAMPLES];
	int i, min_val, val, err;

	memcpy(err_data, samples, sizeof(err_data));
	min_val = 0; ptr = samples; err = 0;
	for (i = 0; i < NBR_SAMPLES; i ++) {
		if (energy_convert(ptr, &val)) {
			if (min_val > val) {
				err = 1;
				memset(ptr, INVALID_VALUE, ENERGY_LEN);
			}
			else {
				min_val = val;
			}
		}
		ptr += ENERGY_LEN;
	}
	if (err)
		ERR_PRINTB("error pload:", mtidx, err_data, sizeof(err_data));
}

static int read_time(short mtidx, const MTID mtid, BYTE nbr_retry, BYTE *year,
	BYTE *mon, BYTE *day, BYTE *hour)
{
	BYTE resp[PLC_APDU_LEN];
	int resp_len;

	bzero(resp, sizeof(resp));
	resp_len = meter_read(mtidx, mtid, nbr_retry, resp, 2, 0xC011, 0xC010);
	if (resp_len >= 7) {
		*hour = bcd_to_bin(resp[2]);
		*day = bcd_to_bin(resp[4]);
		*mon = bcd_to_bin(resp[5]);
		*year = bcd_to_bin(resp[6]);
		return 1;
	}
	return 0;
}

static int read_samples(short mtidx, const MTID mtid, BYTE nbr_retry,
	BYTE year, BYTE mon, BYTE day, BYTE begin, BYTE end, BYTE *buf)
{
	BYTE resp[PLC_APDU_LEN], *ptr;
	int resp_len, hour, val, idx, ret = 0;

	idx = begin / 6, hour = idx * 6, ptr = resp;
	resp_len = meter_read(mtidx, mtid, nbr_retry, resp, 1, 0xe860 + idx);
	if (resp_len == 6 * 5) { // new changesha protocol
		for (idx = 0; idx < 6; idx ++) {
			if (hour <= end && energy_convert(ptr, &val)
				&& bcd_to_bin(ptr[4]) == day) {
				PRINTF("read_samples, year:%d, month:%d, day:%d, hour:%d\n",
					year, mon, day, hour);
				memcpy(buf + hour * 4, ptr, 4);
				ret = 1;
			}
			ptr += 5; hour ++;
		}
	}
	else if (resp_len == 6 * 4) {
		for (idx = 0; idx < 6; idx ++) {
			if (hour >= begin && hour <= end && energy_convert(ptr, &val)) {
				PRINTF("read_samples, year:%d, month:%d, day:%d, hour:%d\n",
					year, mon, day, hour);
				memcpy(buf + hour * 4, ptr, 4);
				ret = 1;
			}
			ptr += 4; hour ++;
		}
	}
	if (ret) {
		check_samples(mtidx, buf);
		fmt_lock();
		fmt_write_load(mtid, year, mon, day, buf);
		fmt_unlock();
	}
	return ret;
}

static int load_read(short mtidx, const MTID mtid)
{
	int ret, step;
	BYTE nbr_retry, year, mon, day, hour, yy, mm, dd, start, new_start;
	BYTE sample1[NBR_SAMPLES * ENERGY_LEN], sample2[NBR_SAMPLES * ENERGY_LEN];

	step = 0;
	nbr_retry = get_nbr_retry();
	if (read_time(mtidx, mtid, nbr_retry, &year, &mon, &day, &hour)) {
		memset(sample1, INVALID_VALUE, sizeof(sample1));
		memset(sample2, INVALID_VALUE, sizeof(sample2));
		yy = year, mm = mon, dd = day;
		previous_day(&yy, &mm, &dd);
		fmt_lock();
		fmt_read_load(mtid, yy, mm, dd, sample1);
		fmt_read_load(mtid, year, mon, day, sample2);
		fmt_unlock();
		if (need_read(0, hour, &start, sample2))
			ret = read_samples(mtidx, mtid, nbr_retry, year, mon, day, start,
				hour, sample2);
		else if (need_read(hour + 2, 23, &start, sample1))
			ret = read_samples(mtidx, mtid, nbr_retry, yy, mm, dd, start,
				23, sample1);
		else
			ret = 0;
		if (ret) {
			if (need_read(0, hour, &new_start, sample2))
				step = 1;
			else if (need_read(hour + 2, 23, &new_start, sample1))
				step = 1;
			if (step && new_start == start)
				step = 0;
		}
	}
	fparam_lock();
	set_last_proc(PROC_MT_LOAD);
	set_last_mtidx(mtidx);
	fparam_unlock();
	return step;
}

static int load_next_mtidx(short *mtidx, MTID mtid)
{
	while ((*mtidx = tmt_next(*mtidx)) >= 0) {
		if (!tmt_is_focus(*mtidx))
			continue;
		tmt_get_mtid(*mtidx, mtid);
		return 1;
	}
	return 0;
}

void load_event(EVENT event)
{
	// nothing to do
}

static int load_interval(void)
{
	BYTE buf[2];
	
	if (fparam_get_value(FPARAM_FLOAD_READ_TIME, buf, 1) <= 0)
		return 60;
	
	if (buf[0] == 0x01)
		return 60;
	if (buf[0] == 0x00)
		return 30;
	if (buf[0] == 0x02)
		return 120;
	if (buf[0] == 0x03)
		return 240;
	if (buf[0] == 0x04)
		return 360;
	if (buf[0] == 0x05)
		return 720;
	return 60;
}

static int load_cond(const struct tm *tm)
{
	return (tm->tm_min >= 5 && tm->tm_min <= 55);
}

int mt_load(const struct tm *tm)
{
	static long start_up;
	static short step = 0, mtidx = -1, start = 0;
	static MTID mtid;
	long up_now;
	short need;

	if (step) {
		need = 1;
	}
	else {
		need = 0;
		tmt_lock();
		fparam_lock();
		fmt_lock();
		if (load_cond(tm)) {
			if (!start) {
				up_now = uptime();
				if (start_up == 0
					|| (up_now - start_up >= load_interval() * 60)) {
					start_up = up_now;
					start = 1;
				}
			}
			if (start) {
				if ((need = load_next_mtidx(&mtidx, mtid)) == 0)
					start = 0;
			}
		}
		fmt_unlock();
		fparam_unlock();
		tmt_unlock();
	}
	if (need) {
		LOG_PRINTF("load: %d \n", mtidx);
		step = load_read(mtidx, mtid);
	}
	return need;
}
