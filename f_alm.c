/*****************************************************************************
*	C-Plan Concentrator 
*
*	(c) Copyright 2006-2007, Shenzhen Kaifa Technology Co.,Ltd. 
*	All Rights Reserved 
*
*	Alarm data management module
*
*	Author: dajiang wan
*	Created on: 2007-03-09
*****************************************************************************/
#include "common.h"
#include "msg_proc.h"
#include "msg_que.h"
#include "f_alm.h"
#include "f_param.h"
#include "t_mt.h"
#include "t_stat.h"
#include "err_code.h"

static F_ALM f_alm[F_ALM_CNT];
static F_ALM *p_f_alm = f_alm;
static sem_t sem_f_alm;
static int fd_f_alm;
static BYTE falm_ec[4];

void falm_lock(void)
{
	sem_wait(&sem_f_alm);
}

void falm_unlock(void)
{
	sem_post(&sem_f_alm);
}

static int compare_tv(const struct timeval *tv1, const struct timeval *tv2)
{
	long tmp;

	if ((tmp = tv1->tv_sec - tv2->tv_sec) != 0)
		return tmp;
	return tv1->tv_usec - tv2->tv_usec;
}

static void falm_init(void)
{
	int i, j;
	struct timeval tv[4];

	for (j = 0; j < 4; j ++) {
		falm_ec[j] = 0xff;
		tv[j].tv_sec = 0;
		tv[j].tv_usec = 0;
	}
	for (i = 0; i < F_ALM_CNT; i ++) {
		if (p_f_alm[i].flag) {
			j = p_f_alm[i].type;
			if (j < 4 && compare_tv(&tv[j], &p_f_alm[i].tv) < 0) {
				tv[j] = p_f_alm[i].tv;
				falm_ec[j] = p_f_alm[i].ec;
			}
		}
	}
}

static int falm_get_flag(BYTE afn, BYTE erc, BYTE *type, BYTE *need,
	BYTE *important)
{
	BYTE flag[16], pos, bit;

	if (afn == 0x0e) {
		*type = 0;
		fparam_get_value(FPARAM_AFN04_F8, flag, 16);
	}
	else {
		*type = 2;
		fparam_get_value(FPARAM_AFN84_F15, flag, 16);
	}
	*need = *important = 0;
	if (erc >= 1 && erc <= 64) {
		pos = (erc - 1) / 8; bit = (erc - 1) % 8;
		*need = (flag[pos] & (1 << bit)) ? 1 : 0;
		*important = (flag[pos + 8] & (1 << bit)) ? 1 : 0;
		if (*important == 0)
			*type = *type + 1;
		return 1;
	}
	return 0;
}

static int find_oldest_rec(void)
{
	int i, min_idx;
	struct timeval min_tv;

	min_idx = -1;
	min_tv.tv_sec = 0;
	min_tv.tv_usec = 0;
	for (i = 0; i < F_ALM_CNT; i ++) {
		if (p_f_alm[i].flag == 0)
			return i;
		if (min_idx < 0 || compare_tv(&min_tv, &p_f_alm[i].tv) > 0) {
			min_tv = p_f_alm[i].tv;
			min_idx = i;
		}
	}
	return min_idx;
}

static void falm_update(int idx, int flush_flag)
{
	lseek(fd_f_alm, idx * sizeof(F_ALM), SEEK_SET);
	safe_write(fd_f_alm, p_f_alm + idx, sizeof(F_ALM));
	if (flush_flag) falm_flush();
}

void falm_add_meter(short mtidx, BYTE erc, int fill, const void *buf, int len)
{
	int idx;
	BYTE *ptr, type, need, important;
	struct timeval tv;
	struct tm tm;
	F_ALM *rec;

	if (len <= 13 && falm_get_flag(0x8e, erc, &type, &need, &important)
		&& need != 0) {
		idx = find_oldest_rec();
		rec = p_f_alm + idx;
		falm_ec[type] = falm_ec[type] + 1;
		ptr = rec->data;
		gettimeofday(&tv, NULL);
		localtime_r(&tv.tv_sec, &tm);
		rec->tv = tv;
		rec->flag = 1;
		rec->type = type;
		rec->ec = falm_ec[type];
		*ptr ++ = erc;
		if (fill == 0) {
			*ptr ++ = 2 + len;
			stoc(ptr, mtidx); ptr += 2;
		}
		else {
			*ptr ++ = 7 + len;
			stoc(ptr, mtidx); ptr += 2;
			*ptr ++ = bin_to_bcd(tm.tm_min);
			*ptr ++ = bin_to_bcd(tm.tm_hour);
			*ptr ++ = bin_to_bcd(tm.tm_mday);
			*ptr ++ = bin_to_bcd(tm.tm_mon + 1);
			*ptr ++ = bin_to_bcd(tm.tm_year % 100);
		}
		memcpy(ptr, buf, len); ptr += len;
		rec->size = ptr - rec->data;
		falm_update(idx, 1);
	}
}

void falm_add_meter_alm7(short mtidx, BYTE erc)
{
	int idx;
	BYTE *ptr, type, need, important;
	struct timeval tv;
	struct tm tm;
	F_ALM *rec;

	if (falm_get_flag(0x8e, erc, &type, &need, &important) && need != 0) {
		idx = find_oldest_rec();
		rec = p_f_alm + idx;
		falm_ec[type] = falm_ec[type] + 1;
		ptr = rec->data;
		gettimeofday(&tv, NULL);
		localtime_r(&tv.tv_sec, &tm);
		rec->tv = tv;
		rec->flag = 1;
		rec->type = type;
		rec->ec = falm_ec[type];
		*ptr ++ = erc;
		*ptr ++ = 8;
		*ptr ++= bin_to_bcd(tm.tm_mday);
		*ptr ++= bin_to_bcd(tm.tm_mon + 1);
		stoc(ptr, tmt_get_read_nbr()); ptr += 2;
		stoc(ptr, 1); ptr += 2;
		stoc(ptr, mtidx); ptr += 2;
		rec->size = ptr - rec->data;
		falm_update(idx, 1);
	}
}

static void falm_event_change(BYTE erc)
{
	BYTE flag[8], pos, bit;
	int param_len;

	if (erc >= 1 && erc <= 64) {
		fparam_get_value(FPARAM_AFN0C_F8, flag, 8);
		pos = (erc - 1) / 8; bit = (erc - 1) % 8;
		flag[pos] |= (1 << bit);
		fparam_set_value(FPARAM_AFN0C_F8, flag, 8, &param_len);
	}
}

void falm_add_485(short mtidx, BYTE erc, const void *buf, int len)
{
	int idx;
	BYTE *ptr, type, need, important;
	struct timeval tv;
	struct tm tm;
	F_ALM *rec;

	if (len <= 13 && falm_get_flag(0x0e, erc, &type, &need, &important)
		&& need != 0) {
		idx = find_oldest_rec();
		rec = p_f_alm + idx;
		falm_ec[type] = falm_ec[type] + 1;
		ptr = rec->data;
		gettimeofday(&tv, NULL);
		localtime_r(&tv.tv_sec, &tm);
		rec->tv = tv;
		rec->flag = 1;
		rec->type = type;
		rec->ec = falm_ec[type];
		*ptr ++ = erc;
		*ptr ++ = 5 + len;
		*ptr ++ = bin_to_bcd(tm.tm_min);
		*ptr ++ = bin_to_bcd(tm.tm_hour);
		*ptr ++ = bin_to_bcd(tm.tm_mday);
		*ptr ++ = bin_to_bcd(tm.tm_mon + 1);
		*ptr ++ = bin_to_bcd(tm.tm_year % 100);
		memcpy(ptr, buf, len); ptr += len;
		rec->size = ptr - rec->data;
		falm_event_change(erc);
		falm_update(idx, 1);
	}
}

void falm_add_con(BYTE afn, BYTE erc, const void *buf, int len)
{
	int idx;
	BYTE *ptr, type, need, important;
	struct timeval tv;
	struct tm tm;
	F_ALM *rec;

	if (len <= 13 && falm_get_flag(afn, erc, &type, &need, &important)
		&& need != 0) {
		idx = find_oldest_rec();
		rec = p_f_alm + idx;
		falm_ec[type] = falm_ec[type] + 1;
		ptr = rec->data;
		gettimeofday(&tv, NULL);
		localtime_r(&tv.tv_sec, &tm);
		rec->tv = tv;
		rec->flag = 1;
		rec->type = type;
		rec->ec = falm_ec[type];
		if (afn == 0x0e && erc == 14) { // donot update contents
			*ptr ++ = erc;
			*ptr ++ = len;
			memcpy(ptr, buf, len); ptr += len;
		}
		else {
			*ptr ++ = erc;
			*ptr ++ = 5 + len;
			*ptr ++ = bin_to_bcd(tm.tm_min);
			*ptr ++ = bin_to_bcd(tm.tm_hour);
			*ptr ++ = bin_to_bcd(tm.tm_mday);
			*ptr ++ = bin_to_bcd(tm.tm_mon + 1);
			*ptr ++ = bin_to_bcd(tm.tm_year % 100);
			memcpy(ptr, buf, len); ptr += len;
		}
		rec->size = ptr - rec->data;
		if (afn == 0x0e)
			falm_event_change(erc);
		falm_update(idx, 1);
	}
}

void falm_get_ec(BYTE afn, BYTE *ec)
{
	if (afn == 0x0e) {
		ec[0] = falm_ec[0];
		ec[1] = falm_ec[1];
	}
	else {
		ec[0] = falm_ec[2];
		ec[1] = falm_ec[3];
	}
}

int falm_changed(BYTE afn, BYTE *ec)
{
	BYTE type;
	int i, ret = 0;

	falm_get_ec(afn, ec);
	for (i = 0; i < F_ALM_CNT; i++) {
		if (p_f_alm[i].flag == 1) {
			type = p_f_alm[i].type;
			if (afn == 0x0e && type == 0)
				ret = 1;
			else if (afn != 0x0e && type == 2)
				ret = 1;
		}
	}	
	return ret;
}

static int falm_check_ec(BYTE ec, BYTE pm, BYTE pn)
{
	if (pm <= pn)
		return ec >= pm && ec <= pn;
	else
		return ec >= pm || ec <= pn;
}

// fn=1 for importtant event, fn=2 for normal event
int falm_get(BYTE *buf, int len, BYTE afn, BYTE fn, BYTE pm, BYTE pn)
{
	BYTE size, type, cnt, *ptr = buf, real_pm;
	F_ALM *rec;
	int i;

	if (len < 4)
		return 0;
	if (afn == 0x0e) {
		*ptr ++ = falm_ec[0];
		*ptr ++ = falm_ec[1];
		type = 0;
	}
	else {
		*ptr ++ = falm_ec[2];
		*ptr ++ = falm_ec[3];
		type = 2;
	}
	ptr += 2;
	len -= 4;
	if (fn == 2)
		type ++;
	cnt = 0;
	real_pm = pm;
	rec = p_f_alm;
	for (i = 0; i < F_ALM_CNT; i ++) {
		if (rec->flag == 1 && rec->type == type) {
			if (falm_check_ec(rec->ec, pm, pn)) {
				size = rec->size;
				if (size == 0 || len < size || cnt >= 255)
					break;
				if (cnt == 0)
					real_pm = rec->ec;
				memcpy(ptr, rec->data, size);
				ptr += size; len -= size;
				rec->flag = 2;
				falm_update(i, 1);
				cnt ++;
			}
		}
		rec ++;
	}
	buf[2] = real_pm;
	buf[3] = (real_pm + cnt - 1) % 256;
	return ptr - buf;
}

void falm_spont(int que_idx, BYTE *buf, int len)
{
	int i, tmp;
	F_ALM *rec;
	BYTE data[MAX_APDU_LEN], *ptr, afn, seq_no, fn, pm, pn;

	rec = p_f_alm;
	for (i = 0; i < F_ALM_CNT; i ++) {
		if (rec->flag == 1 && (rec->type == 0 || rec->type == 2)
			&& sizeof(data) >= 8 + rec->size) {
			seq_no = 0, fn = 1, pm = pn = rec->ec % 256;
			ptr = data;
			memcpy(ptr, "\x00\x00\x00\00", 4); ptr[2] = 0x01 << (fn - 1);
			ptr += 4;
			if (rec->type == 0) {
				afn = 0x0e;
				*ptr ++ = falm_ec[0];
				*ptr ++ = falm_ec[1];
			}
			else {
				afn = 0x8e;
				*ptr ++ = falm_ec[2];
				*ptr ++ = falm_ec[3];
			}
			*ptr ++ = pm;
			*ptr ++ = pn;
			memcpy(ptr, rec->data, rec->size); ptr += rec->size;
			tmp = packet_any(buf, len, 4, afn, seq_no, data, ptr - data);
			if (tmp > 0) {
				msg_que_put(que_idx, buf, tmp, 0);
				rec->flag = 2;
				falm_update(i, 1);
			}
		}
		rec ++;
	}
}

int falm_get_stat_cnt(int *cnt, int max)
{
	int i;
	F_ALM *rec = p_f_alm;

	if (max < 8)
		return 0;
	for (i = 0; i < max; i ++)
		cnt[i] = 0;
	for (i = 0; i < F_ALM_CNT; i ++) {
		/* count of concentrator history event */
		if (rec->flag == 2 && (rec->type == 0 || rec->type == 1))
			cnt[0] ++;
		/* count of concentrator new event */
		else if (rec->flag == 1 && (rec->type == 0 || rec->type == 1))
			cnt[1] ++;
		/* count of user meter history event */
		else if (rec->flag == 2 && (rec->type == 2 || rec->type == 3))
			cnt[2] ++;
		/* count of user meter new event */
		else if (rec->flag == 1 && (rec->type == 2 || rec->type == 3))
			cnt[3] ++;
		rec ++;
	}
	rec = p_f_alm;
	for (i = 0; i < F_ALM_CNT; i ++) {
		/* count of importtant event about concentrator */
		if ((rec->flag == 1 || rec->flag == 2) && rec->type == 0)
			cnt[4] ++;
		/* count of normal event about concentrator */
		else if ((rec->flag == 1 || rec->flag == 2) && rec->type == 1)
			cnt[5] ++;
		/* count of importtant event about user meter*/
		else if ((rec->flag == 1 || rec->flag == 2) && rec->type == 2)
			cnt[6] ++;
		/* count of normal event about user meter */
		else if ((rec->flag == 1 || rec->flag == 2) && rec->type == 3)
			cnt[7] ++;
		rec ++;
	}
	return 1;
}

static int check_alm_type(F_ALM *rec, int mt_erc, int new_erc)
{
	if (rec->flag == 0 && rec->flag > 2)
		return 0;
	return ((mt_erc && rec->type >= 2) || (!mt_erc && rec->type < 2)) 
		&& ((new_erc && rec->flag == 1) || (!new_erc && rec->flag == 2));
}

static int get_newest_alm_tv(int mt_erc, int new_erc, struct timeval *tv)
{
	int i, found = 0;
	F_ALM *rec = p_f_alm;
	
	tv->tv_sec = 0; tv->tv_usec = 0;
	for (i = 0; i < F_ALM_CNT; i ++) {
		if (check_alm_type(rec, mt_erc, new_erc) && compare_tv(&rec->tv, tv) > 0) {
			*tv = rec->tv;
			found = 1;
		}
		rec ++;
	}
	if (found)
		return 1;
	else
		return 0;
}

int falm_get_idx(int mt_erc, int new_erc, int *erc_idx, int max1, BYTE *erc_code, int max2)
{
	int i, j, cnt = 0, found;
	F_ALM *rec = p_f_alm, *rec1;
	struct timeval tv1, tv2;

	if(!get_newest_alm_tv(mt_erc, new_erc, &tv1))
		return 0;
	tv1.tv_sec ++; // Make sure this time is newer than the time of newest event
	for (i = 0; i < F_ALM_CNT && i < max1 && i < max2; i ++) {
		rec = p_f_alm;
		found = 0;
		tv2.tv_sec = 0; tv2.tv_usec = 0;
		for (j = 0; j < F_ALM_CNT; j ++) {
			if (check_alm_type(rec, mt_erc, new_erc)) {
				if((compare_tv(&tv1, &rec->tv) > 0) && (compare_tv(&rec->tv, &tv2) > 0)) {
					tv2 = rec->tv;
					erc_idx[cnt] = j;
					found = 1;
				}
			}
			rec ++;
		}
		if (found) {
			rec1 = p_f_alm + erc_idx[cnt];
			erc_code[cnt] = rec1->data[0];
			cnt ++;
			tv1 = rec1->tv;
		}
		else
			break;
	}
	return cnt;
}

int falm_get_one(int idx, F_ALM *alm)
{
	if (idx >= 0 && idx < F_ALM_CNT) {
		memcpy(alm, p_f_alm + idx, sizeof(F_ALM));
		return 1;
	}
	else
		return 0;
}

void falm_open(void)
{
	int size = sizeof(F_ALM) * F_ALM_CNT;
	const char *name = F_ALM_NAME;

	sem_init(&sem_f_alm, 0, 1);
	if (!check_file(name, size))
		create_file(name, size);
	fd_f_alm = open(name, O_RDWR);
	safe_read(fd_f_alm, p_f_alm, size);
	falm_init();
}

void falm_flush(void)
{
	fdatasync(fd_f_alm);
}

void falm_close(void)
{
	fdatasync(fd_f_alm);
	close(fd_f_alm);
	sem_destroy(&sem_f_alm);
}
