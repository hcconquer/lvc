#ifndef _LVC_TEST_H
#define _LVC_TEST_H

#include "typedef.h"

#define TEST_MAX_NUM		11

#define TEST_LCD			0
#define TEST_MODEM_RELAY	1
#define TEST_PLC_RELAY		2
#define TEST_RTC			3
#define TEST_LED			4
#define TEST_PLC			5
#define TEST_RS485			6
#define TEST_HHU			7
#define TEST_MODEM			8
#define TEST_YX				9
#define TEST_KEY			10

void lvc_test_request(BYTE seq, int mt_cnt, const void *mt_arr);

int lvc_test_response(BYTE *out_buf, int max_len);

void lvc_test(void);

#endif // End of _LVC_TEST_H
