/*
 *  C-Plan Concentrator
 *
 *  Copyright (C) 2006-2007, Shenzhen Kaifa Technology Co.,Ltd.
 *
 *  Provide functions to process message from FEP.
 *
 *  Author: dajiang wan
 *  Created on: 2006-09-05
 */
#include "common.h"
#include "err_code.h"
#include "t_db.h"
#include "t_mt.h"
#include "t_stat.h"
#include "f_alm.h"
#include "f_param.h"
#include "misc.h"
#include "msg_proc.h"
#include "msg_que.h"
#include "msg_code.h"
#include "mt_frzn.h"
#include "mt_route.h"
#include "afn.h"

#define DIR_BIT				(1 << 7)
#define PRM_BIT				(1 << 6)
#define FCB_BIT				(1 << 5)
#define ACD_BIT				(1 << 5)
#define FCV_BIT				(1 << 4)
#define FUNC_MASK			0x0f

#define TPV_BIT				(1 << 7)
#define FIR_BIT				(1 << 6)
#define FIN_BIT				(1 << 5)
#define CON_BIT				(1 << 4)
#define SEQ_MASK			0x0f

#define MAX_PACK_CNT		15
#define MSG_BUF_LEN			(MAX_PACK_CNT * MAX_APDU_LEN)

static unsigned char msg_buf[3 * MSG_BUF_LEN];

static struct msg
{
	int que_in, que_out;/**/
	BYTE *buf;
	int saved_fcb;/*帧计数位FCB*/
	ST_MSG msg_in;/*收到的报文处理后的结构*/
	int msg_out_cnt;
	ST_MSG msg_out[MAX_PACK_CNT];
} msg_arr[3] = /*QUS ???*/
{
{ MSG_QUE_MASTER_IN, MSG_QUE_MASTER_OUT, msg_buf },
{ MSG_QUE_SLAVE_IN, MSG_QUE_SLAVE_OUT, msg_buf + MSG_BUF_LEN },
{ MSG_QUE_HHU_IN, MSG_QUE_HHU_OUT, msg_buf + MSG_BUF_LEN * 2 }, };

static void get_con_addr(BYTE *addr)
{
	BYTE temp[4];

	fparam_get_value(FPARAM_CON_ADDRESS, temp, 4);
	addr[0] = temp[1];
	addr[1] = temp[0];
	addr[2] = temp[3];
	addr[3] = temp[2];
}

static int check_tp(const BYTE *buf)
{
	BYTE sec, min, hour, day, day2;
	int val1, val2, diff, day_sec;
	time_t tt;
	struct tm tm;

	sec = bcd_to_bin(buf[1]);
	min = bcd_to_bin(buf[2]);
	hour = bcd_to_bin(buf[3]);
	day = bcd_to_bin(buf[4]);
	diff = bcd_to_bin(buf[5]) * 60;
	if (diff == 0)
		return 1;
	val1 = sec + min * 60 + hour * 60 * 60;
	time(&tt);
	localtime_r(&tt, &tm);
	val2 = tm.tm_sec + tm.tm_min * 60 + tm.tm_hour * 60 * 60;
	day2 = tm.tm_mday;
	day_sec = 24 * 60 * 60;
	if (day != day2)
	{
		if (day + 1 == day2)
			val2 += day_sec;
		else if (day == day2 + 1)
			val1 += day_sec;
		else if (day == 1 && (day2 >= 28 && day2 <= 31))
			val1 += day_sec;
		else if (day2 == 1 && (day >= 28 && day <= 31))
			val2 += day_sec;
		else
			return 0;
	}
	return abs(val1 - val2) <= diff;
}

/*
 * 解包,将数据写入到报文ST_MSG中
 * 参数
 * buf		收到的数据
 * msg		报文
 * */
static int unpack(const BYTE *buf, int len, ST_MSG *msg)
{
	BYTE afn, exist_pw, exist_tp, aux_size;
	const BYTE *ptr = buf + 6;

	msg->code = *ptr++;
	memcpy(msg->addr, ptr, 5);/*设置地址*/
	ptr += 5;
	afn = msg->afn = *ptr++;
	msg->seq = *ptr++;
	exist_pw = (afn == 0x01 || afn == 0x04 || afn == 0x05 || afn == 0x0f || afn
			== 0x10 || afn == 0x84 || afn == 0x85);/*就是说AFN=0f时存在PW,主意:这些报文大多是后台报文*/
	exist_tp = msg->seq & 0x80;
	if (exist_pw && exist_tp)
		aux_size = 2 + 6;
	else if (exist_pw)/*注意:规约中PW只有2个字节*/
		aux_size = 2;
	else if (exist_tp)/*注意:规约中TP有6个字节*/
		aux_size = 6;
	else
		aux_size = 0;
	msg->size = len - 16 - aux_size;/*链路用户数据的长度*/
	msg->data = (BYTE *) ptr;/*注意data的初始是链路用户数据*/
	ptr += msg->size;
	msg->aux_flag = 0;
	/*参考aux_flag*/
	if (exist_pw)
	{
		msg->aux_flag |= 1;
		memcpy(msg->aux_pw, ptr, 2);
		ptr += 2;
	}
	if (exist_tp)
	{
		msg->aux_flag |= 4;
		memcpy(msg->aux_tp, ptr, 6);
		ptr += 6;
		if (!check_tp(msg->aux_tp))
		{
			PRINTF("time stamp too old\n");
			return 0;
		}
	}
	return ptr - buf;
}

static int pack(BYTE *buf, int max_len, const ST_MSG *msg)
{
	BYTE sum, exist_ec, exist_tp, aux_size;
	WORD len;
	BYTE addr[5];
	BYTE *ptr = buf;

	exist_ec = msg->aux_flag & 0x02;
	exist_tp = msg->aux_flag & 0x04;
	if (exist_ec && exist_tp)
		aux_size = 2 + 6;
	else if (exist_ec)
		aux_size = 2;
	else if (exist_tp)
		aux_size = 6;
	else
		aux_size = 0;
	if (msg->size + 16 + aux_size <= max_len)
	{
		*ptr++ = 0x68;
		len = ((msg->size + 8 + aux_size) << 2) | 0x01;
		stoc(ptr, len);
		ptr += 2;
		stoc(ptr, len);
		ptr += 2;
		*ptr++ = 0x68;
		*ptr++ = msg->code;

		get_con_addr(addr);
		addr[4] = msg->addr[4] & 0xfe;
		memcpy(ptr, addr, 5);
		// memcpy(ptr, msg->addr, 5);

		ptr += 5;

		*ptr++ = msg->afn;
		*ptr++ = msg->seq;
		memcpy(ptr, msg->data, msg->size);
		ptr += msg->size;
		if (exist_ec)
		{
			memcpy(ptr, msg->aux_ec, 2);
			ptr += 2;
		}
		if (exist_tp)
		{
			memcpy(ptr, msg->aux_tp, 6);
			ptr += 6;
		}
		sum = check_sum(buf + 6, (ptr - buf) - 6);
		*ptr++ = sum;
		*ptr++ = 0x16;
	}
	return ptr - buf;
}

static int check_fcb(struct msg *msg_ptr)
{
	BYTE code, func, fcb;

	code = msg_ptr->msg_in.code;
	func = code & FUNC_MASK;
	if ((code & PRM_BIT) && (func == 1))
	{ // Reset command
		msg_ptr->saved_fcb = 0;
		return 0;
	}
	if (code & FCV_BIT)
	{
		fcb = (code & FCB_BIT) ? 1 : 0;
		if (fcb == msg_ptr->saved_fcb)
			return 1;
		msg_ptr->saved_fcb = fcb;
	}
	else
	{
		msg_ptr->saved_fcb = 0;
	}
	return 0;
}

static void fill_code1(const ST_MSG *msg_in, ST_MSG *msg_out)
{
	BYTE code, msg1, msg2;

	code = msg_in->code;
	if (code & DIR_BIT) // reverse DIR_BIT
		code &= ~DIR_BIT;
	else
		code |= DIR_BIT;
	if (code & PRM_BIT) // reverse PRM_BIT
		code &= ~PRM_BIT;
	else
		code |= PRM_BIT;
	if (msg_out->aux_flag & 2) // set/reset ACD_BIT
		code |= ACD_BIT;
	else
		code &= ~ACD_BIT;
	code &= ~FCV_BIT; // no FCV_BIT
	msg1 = code & FUNC_MASK;
	if (msg1 == 9)
	{
		msg2 = 11;
	}
	else if (msg1 == 10 || msg1 == 11)
	{
		msg2 = 8; // msg2 = 9;
	}
	else
	{
		msg2 = 0;
	}
	code = (code & ~FUNC_MASK) | msg2;
	msg_out->code = code;
}

static void fill_code2(const ST_MSG *msg_in, ST_MSG *msg_out, int idx, int fir,
		int fin)
{
	BYTE afn, seq, pseq, rseq;

	afn = msg_in->afn;
	seq = msg_in->seq;
	if (afn == 0x84 || afn == 0x85 || afn == 0x04 || afn == 0x05 || afn == 0x0f
			|| afn == 0x02 || afn == 0x01)
		afn = 0x00;
	pseq = seq & SEQ_MASK;
	rseq = (pseq + idx) & SEQ_MASK;
	seq = rseq;
	if (msg_out->aux_flag & 0x04)
		seq |= TPV_BIT;
	if (fir)
		seq |= FIR_BIT;
	if (fin)
		seq |= FIN_BIT;
	msg_out->afn = afn;
	msg_out->seq = seq;
}

static void fill_aux(const ST_MSG *msg_in, ST_MSG *msg_out)
{
	msg_out->aux_flag = 0;
	if (falm_changed(msg_out->afn, msg_out->aux_ec))
	{
		msg_out->aux_flag |= 2;
	}
	if (msg_in->aux_flag & 0x04)
	{
		msg_out->aux_flag |= 4;
		memcpy(msg_out->aux_tp, msg_in->aux_tp, 6);
	}
}

static void fill_data(struct msg *msg_ptr, BYTE *ptr, int len, int idx,
		int fir, int fin)
{
	fill_aux(&msg_ptr->msg_in, msg_ptr->msg_out + idx);
	msg_ptr->msg_out[idx].data = ptr;
	msg_ptr->msg_out[idx].size = len;
	fill_code1(&msg_ptr->msg_in, &msg_ptr->msg_out[idx]);
	fill_code2(&msg_ptr->msg_in, &msg_ptr->msg_out[idx], idx, fir, fin);
	memcpy(msg_ptr->msg_out[idx].addr, msg_ptr->msg_in.addr, 5);
}

static void msg_pack(struct msg *msg_ptr, int max_out_len, int *data_len,
		int data_cnt)
{
	int idx, len, fir, fin, seq;
	int max_aux_size = 8;
	BYTE *ptr;

	ptr = msg_ptr->buf;
	len = 0;
	fir = fin = 1;
	seq = 0;
	for (idx = 0; idx < data_cnt; idx++)
	{
		if (data_len[idx] + len + max_aux_size < max_out_len)
		{
			len += data_len[idx];
		}
		else
		{ // need send a new packet
			fin = 0;
			fill_data(msg_ptr, ptr, len, seq, fir, fin);
			fir = 0;
			seq++;
			ptr += len;
			len = data_len[idx];
		}
	}
	if (len > 0)
	{ // need send a packet
		fin = 1;
		msg_ptr->msg_out_cnt = seq + 1;
		fill_data(msg_ptr, ptr, len, seq, fir, fin);
	}
}

static int find_idx(int in_idx, int out_idx)
{
	int i;

	for (i = 0; i < sizeof(msg_arr) / sizeof(struct msg); i++)
	{
		if (in_idx == msg_arr[i].que_in && out_idx == msg_arr[i].que_out)
			return i;
	}
	return -1;
}

int check_address(const BYTE *buf, int len)
{
	BYTE addr[4], group_addr[16];
	int i, ret = 0;

	get_con_addr(addr);
	fparam_get_value(FPARAM_AFN04_F6, group_addr, 16);
	if (buf[7] == 0xff && buf[8] == 0xff && buf[9] == 0xff && buf[10] == 0xff)
	{ // broadcast address
		ret = 1;
	}
	else if (addr[0] == buf[7] && addr[1] == buf[8])
	{ // area code matchs
		if (buf[9] == 0xff && buf[10] == 0xff)
		{ // broadcast address
			ret = 1;
		}
		else if (buf[11] & 0x01)
		{ // group address
			for (i = 0; i < 8; i++)
			{
				if (group_addr[i * 2] == buf[9] && group_addr[i * 2 + 1]
						== buf[10])
					ret = 1;
			}
		}
		else
		{ // unique address
			if (addr[2] == buf[9] && addr[3] == buf[10])
				ret = 1;
		}
	}
	if (ret == 0)
		PRINTF("Bad address %02x%02x%02x%02x, must be %02x%02x%02x%02x\n",
				buf[7], buf[8], buf[9], buf[10], addr[0], addr[1], addr[2],
				addr[3]);
	return ret;
}

/*初步检查报文的有效性*/
int check_packet(const BYTE *buf, int buf_len)
{
	BYTE sum;
	int len, tmp;

	if (buf_len < 8 + 1 + 5 + 1 + 1 + 4)
		return 0;
	len = ctos(buf + 1);
	tmp = ctos(buf + 3);
	if (len != tmp || (len & 0x03) != 0x01) // D1..D0 = 01
		return 0;
	len = (len >> 2) & 0xffff; // D15..D2
	if (buf_len < len + 8) // two bytes for CHKSUM and FRAME_END
		return 0;
	sum = check_sum(buf + 6, len);
	if (buf[0] != 0x68 || buf[5] != 0x68 || buf[len + 6] != sum || buf[len + 7]
			!= 0x16)
		return 0;
	return len + 8;
}

int packet_any(BYTE *buf, int len, BYTE code, BYTE afn, BYTE seq_no,
		const BYTE *data, int data_len)
{
	BYTE *ptr = buf;
	BYTE addr[5], seq, sum;
	int tmp;

	get_con_addr(addr);
	addr[4] = 0x02;
	seq = FIR_BIT | FIN_BIT | CON_BIT | (seq_no & 0x0f);
	if (len > 1 + 2 + 2 + 1 + 8 + data_len + 1 + 1)
	{
		*ptr++ = 0x68;
		tmp = ((data_len + 8) << 2) | 0x01;
		stoc(ptr, tmp);
		ptr += 2;
		stoc(ptr, tmp);
		ptr += 2;
		*ptr++ = 0x68;
		*ptr++ = code | DIR_BIT | PRM_BIT;
		memcpy(ptr, addr, 5);
		ptr += 5;
		*ptr++ = afn;
		*ptr++ = seq;
		memcpy(ptr, data, data_len);
		ptr += data_len;
		sum = check_sum(buf + 6, (ptr - buf) - 6);
		*ptr++ = sum;
		*ptr++ = 0x16;
	}
	return ptr - buf;
}

int packet(BYTE *buf, int len, BYTE afn, BYTE seq_no, BYTE fn)
{
	BYTE data[4];

	memcpy(data, "\x00\x00\x00\00", 4);
	data[2] = 0x01 << (fn - 1);
	return packet_any(buf, len, 9, afn, seq_no, data, 4);
}

/*
 * message process报文处理
 * in_idx		报文来源
 * out_idx	返回报文去向
 * */
void msg_proc(int in_idx, int out_idx, int max_out_len)
{
	/*
	 * mas		MSA,主站地址
	 * */
	BYTE in_buf[MAX_LDU_LEN], out_buf[MAX_LDU_LEN], msa;
	int in_len, out_len, idx;
	int data_len[100], data_cnt;
	struct msg *msg_ptr;
	const BYTE *pass;

	max_out_len = min(sizeof(out_buf), max_out_len);
	msg_que_get(in_idx, in_buf, sizeof(in_buf), &in_len, 0);
	if (in_len <= 0 || (idx = find_idx(in_idx, out_idx)) < 0)
		return;
	msg_ptr = msg_arr + idx;
	if (!unpack(in_buf, in_len, &msg_ptr->msg_in))
		return;
	if (msg_ptr->msg_in.afn == 0x00)
	{ // answer from fep
		fep_response(msg_ptr->msg_in.seq & 0x0f);
		return;
	}
	if (!msg_ptr->msg_out_cnt || !check_fcb(msg_ptr))
	{
		msg_ptr->msg_out_cnt = 0;
		pass = (msg_ptr->msg_in.aux_flag & 1) ? msg_ptr->msg_in.aux_pw : NULL;
		msa = (msg_ptr->msg_in.addr[4] >> 1) & 0x7F;
		data_cnt = afn_proc(msa, msg_ptr->msg_in.afn, pass,
				msg_ptr->msg_in.data, msg_ptr->msg_in.size, msg_ptr->buf,
				MSG_BUF_LEN, data_len, sizeof(data_len) / sizeof(int));/*QUS data_len不是定义为100个成员的数组了么?*/
		msg_pack(msg_ptr, max_out_len, data_len, data_cnt);
	}
	for (idx = 0; idx < msg_ptr->msg_out_cnt; idx++)
	{
		out_len = pack(out_buf, max_out_len, msg_ptr->msg_out + idx);
		if (out_len > 0)
			msg_que_put(msg_ptr->que_out, out_buf, out_len, 0);
	}
}
