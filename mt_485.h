/*****************************************************************************
 *	C-Plan Concentrator 
 *
 *	(c) Copyright 2006-2007, Shenzhen Kaifa Technology Co.,Ltd. 
 *	All Rights Reserved 
 *		
 *****************************************************************************/
#ifndef _MT_485_H
#define _MT_485_H

#include "typedef.h"

int mt485_read(const MTID mtid, BYTE *out_buf, int max_len, int di);

int mt485_read_arg(const MTID mtid, BYTE *out_buf, int max_len, int cnt, ...);

int mt485_read_no_check(const MTID mtid, BYTE *out_buf, int max_len, int di);

void mt485_sync(void);

#endif /* _MT_485_H */
