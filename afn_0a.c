/*
 *  C-Plan Concentrator
 *
 *  Copyright (C) 2006-2007, Shenzhen Kaifa Technology Co.,Ltd.
 *
 *  Author: dajiang wan
 *  Created on: 2007-02-27
 */
#include "afn.h"
#include "msg_proc.h"
#include "global.h"
#include "threads.h"
#include "mt_sync.h"
#include "err_code.h"
#include "common.h"
#include "misc.h"
#include "dwl_con.h"
#include "f_param.h"
#include "device.h"
#include "t_mt.h"
#include "t_485.h"
#include "t_task.h"

static int afn0a_get(BYTE *out_buf, int max_len, int di, int *param_len)
{
	int ret;

	*param_len = 0;
	fparam_lock();
	ret = fparam_read(di, out_buf, max_len);
	fparam_unlock();
	return (ret < 0) ? 0 : ret;
}

static int afn0a_10(BYTE *out_buf, int max_len, int *param_len)
{
	int ret;

	*param_len = 0;
	t485_lock();
	ret = t485_read_all_meters(out_buf, max_len);
	t485_unlock();
	return ret;
}

static int afn0a_247(BYTE *out_buf, int max_len, const BYTE *param,
	int *param_len)
{
	short pack_no, pack_tmp;
	int name_len;
	const BYTE *ptr = param;

	if (*param_len < 4)
		return 0;
	pack_no = ctos(ptr); ptr += 2;
	pack_tmp = ctos(ptr); ptr += 2;
	if (pack_no == 0) { // file name
		name_len = strlen((const char *)ptr);
		*param_len = name_len + 4 + 1;
		return upload_name(out_buf, max_len, pack_tmp, ptr);
	}
	else { // file content
		*param_len = 4;
		return upload_file(out_buf, max_len, pack_no, pack_tmp);
	}
}

int afn0a_get_fn(BYTE pn, BYTE fn, BYTE *out_buf, int max_len,
	const BYTE *param, int *param_len)
{
	int tmp, tmp2;
	BYTE data[128];

	PRINTF("AFN=0AH, Fn=%d\n", fn);
	switch (fn) {
	case 1:
		return afn0a_get(out_buf, max_len, FPARAM_AFN04_F1, param_len);
	case 2:
		*param_len = 0;
		fparam_lock();
		tmp = fparam_read(FPARAM_AFN04_F2, data, sizeof(data));
		fparam_unlock();
		if (tmp <= 0)
			return 0;
		tmp2 = 1 + (data[0] & 0x7f) * 2;
		if (tmp < tmp2 || tmp2 > max_len)
			return 0;
		memcpy(out_buf, data, tmp2);
		return tmp2;
	case 3:
		*param_len = 0;
		if (max_len < 6 * 4 + 16)
			return 0;
		fparam_lock();
		fparam_read(FPARAM_IP_PORT_PRI, out_buf, 6);
		fparam_read(FPARAM_IP_PORT_SEC, out_buf + 6, 6);
		fparam_read(FPARAM_IP_PORT_GW, out_buf + 12, 6);
		fparam_read(FPARAM_IP_PORT_PROXY, out_buf + 18, 6);
		fparam_read(FPARAM_APN, out_buf + 24, 16);
		fparam_unlock();
		return 6 * 4 + 16;
	case 4:
		return afn0a_get(out_buf, max_len, FPARAM_AFN04_F4, param_len);
	case 5:
		return afn0a_get(out_buf, max_len, FPARAM_AFN04_F5, param_len);
	case 6:
		return afn0a_get(out_buf, max_len, FPARAM_AFN04_F6, param_len);
	case 7:
		return afn0a_get(out_buf, max_len, FPARAM_AFN04_F7, param_len);
	case 8:
		return afn0a_get(out_buf, max_len, FPARAM_AFN04_F8, param_len);
	case 9:
		*param_len = 0;
		if (max_len < 4)
			return 0;
		t485_lock();
		t485_get_meters_type(out_buf, max_len);
		t485_unlock();
		return 4;
	case 10:
		return afn0a_10(out_buf, max_len, param_len);
	case 11: // TODO
		return 0;
	case 12:
		return afn0a_get(out_buf, max_len, FPARAM_AFN04_F12, param_len);
	case 13: // TODO
		return 0;
	case 14: // TODO
		return 0;
	case 15: // TODO
		return 0;
	case 16:
		*param_len = 0;
		if (max_len < 32)
			return 0;
		fparam_lock();
		fparam_read(FPARAM_VPN_USER, data, sizeof(data));
		memcpy(out_buf, data, 16);
		fparam_read(FPARAM_VPN_PASS, data, sizeof(data));
		memcpy(out_buf + 16, data, 16);
		fparam_unlock();
		return 32;
	case 17:
		return afn0a_get(out_buf, max_len, FPARAM_AFN04_F17, param_len);
	case 18:
		return afn0a_get(out_buf, max_len, FPARAM_AFN04_F18, param_len);
	case 19:
		return afn0a_get(out_buf, max_len, FPARAM_AFN04_F19, param_len);
	case 20:
		return afn0a_get(out_buf, max_len, FPARAM_AFN04_F20, param_len);
	case 21:
		return afn0a_get(out_buf, max_len, FPARAM_AFN04_F21, param_len);
	case 22:
		return afn0a_get(out_buf, max_len, FPARAM_AFN04_F22, param_len);
	case 23:
		return afn0a_get(out_buf, max_len, FPARAM_AFN04_F23, param_len);
	case 24:
		return afn0a_get(out_buf, max_len, FPARAM_AFN04_F24, param_len);
	case 25: // fall through
	case 26: // fall through
	case 27: // fall through
	case 28: // fall through
		t485_lock();
		tmp = t485_read(pn, fn, out_buf, max_len);
		t485_unlock();
		return tmp <= 0 ? 0 : tmp;
	case 33: // TODO
	case 41: // TODO
	case 42: // TODO
	case 43: // TODO
	case 44: // TODO
	case 45: // TODO
	case 46: // TODO
	case 47: // TODO
	case 48: // TODO
		return 0;
	case 49: // TODO
		return 0;
	case 57:
		return afn0a_get(out_buf, max_len, FPARAM_AFN04_F57, param_len);
	case 58:
		return afn0a_get(out_buf, max_len, FPARAM_AFN04_F58, param_len);
	case 59:
		return afn0a_get(out_buf, max_len, FPARAM_AFN04_F59, param_len);
	case 60:
		return afn0a_get(out_buf, max_len, FPARAM_AFN04_F60, param_len);
	case 61:
		return afn0a_get(out_buf, max_len, FPARAM_AFN04_F61, param_len);
	case 62:
		return afn0a_get(out_buf, max_len, FPARAM_AFN04_F62, param_len);
	case 65:
		*param_len = 0;
		task_lock();
		tmp = task1_get(pn, out_buf, max_len);
		task_unlock();
		if (tmp == 0)
			PRINTF("task1_get error, task_id:%d\n", pn);
		return tmp;
	case 66:
		*param_len = 0;
		task_lock();
		tmp = task2_get(pn, out_buf, max_len);
		task_unlock();
		if (tmp == 0)
			PRINTF("task2_get error, task_id:%d\n", pn);
		return tmp;
	case 67:
		*param_len = 0;
		task_lock();
		tmp = task1_get_flag(pn, out_buf, max_len);
		task_unlock();
		if (tmp == 0)
			PRINTF("task1_get_flag error, task_id:%d\n", pn);
		return tmp;
	case 68:
		*param_len = 0;
		task_lock();
		tmp = task2_get_flag(pn, out_buf, max_len);
		task_unlock();
		if (tmp == 0)
			PRINTF("task2_get_flag error, task_id:%d\n", pn);
		return tmp;
	case 73: // TODO
	case 74: // TODO
	case 75: // TODO
	case 76: // TODO
		return 0;
	case 81: // TODO
	case 82: // TODO
	case 83: // TODO
		return 0;
	case 247:
		return afn0a_247(out_buf, max_len, param, param_len);
	default:
		return 0;
	}
}
