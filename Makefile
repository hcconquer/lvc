CROSS=arm-linux-
CC=$(CROSS)gcc
STRIP=$(CROSS)strip
CFLAGS=-O2 -Wall -Werror -I include
TABLE_OBJS=t_db.o t_stat.o t_mt.o t_rt.o t_485.o t_task.o
FILE_OBJS=f_param.o f_mt.o f_alm.o f_485.o
QUEUE_OBJS=msg_que.o
THREAD_OBJS=th_cascade.o th_fep.o th_hhu.o th_plc.o th_rs485.o th_iosys.o threads.o
AFN0_OBJS=afn_01.o afn_04.o afn_05.o afn_06.o afn_0a.o afn_0c.o \
	afn_0d.o afn_0e.o afn_0f.o afn_10.o
AFN8_OBJS=afn_84.o afn_85.o afn_8a.o afn_8c.o afn_8d.o afn_8e.o afn.o
OTHER_OBJS=global.o device.o plc.o main.o mt_di.o misc.o sms_wakeup.o \
	mt_route.o mt_day.o mt_month.o mt_load.o mt_access.o mt_485.o mt_alarm.o \
	mt_test.o mt_sync.o mt_frzn.o cb_access.o common.o dwl_con.o md5.o \
	msg_proc.o rtc_bkup.o fcs.o lvc_test.o
IOSYS_OBJS=lcd.o input.o menu.o menu_str.o font.o display.o
OBJS=$(TABLE_OBJS) $(FILE_OBJS) $(QUEUE_OBJS) $(THREAD_OBJS) $(OTHER_OBJS) \
	$(AFN0_OBJS) $(AFN8_OBJS) $(IOSYS_OBJS)

#CFLAGS += -DDEBUG_POWERFAIL
#CFLAGS += -DDEBUG_TIMEOUT
#CFLAGS += -DDEBUG_ROUTING  # never include this option in formal release.
#CFLAGS += -DDEBUG_X86		# simultor test in x86

all:	tags cscon cscon.dwl watchdog rtc_cal

cscon: $(OBJS)
	$(CC) -o cscon $(OBJS) -lpthread -lz
	$(STRIP) cscon
cscon.dwl: md5_append.c cscon
	gcc -c -o md5.oo md5.c -I include
	gcc -o md5_append md5_append.c md5.oo -I include
	gzip -9 -c cscon >cscon.dwl
	./md5_append cscon.dwl
	chmod 644 cscon.dwl
	rm -f md5.oo md5_append
watchdog: watchdog.o
	$(CC) -o watchdog watchdog.o
	$(STRIP) watchdog
rtc_cal: rtc_cal.o
	$(CC) -o rtc_cal rtc_cal.o
	$(STRIP) rtc_cal
clean:
	rm -rf *.o cscon cscon.dwl watchdog rtc_cal tags
tags: *.c include/*.h
	find . -name "*.[c,h,s]" | xargs ctags
