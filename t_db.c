/*
 *  C-Plan Concentrator
 *
 *  Copyright (C) 2006-2007, Shenzhen Kaifa Technology Co.,Ltd.
 *
 *  Provide function to open/close tables and semaphores. 
 *
 *  Author: dajiang wan
 *  Created on: 2006-09-05
 */
#include "common.h"
#include "t_db.h"
#include "t_mt.h"
#include "t_stat.h"
#include "t_rt.h"
#include "t_485.h"
#include "f_mt.h"
#include "f_485.h"
#include "f_alm.h"
#include "f_param.h"
#include "t_task.h"

/* To avoid dead lock, all the locks should be used according to the following
   sequence:
		sem_t_mt
		sem_t_485
		sem_t_stat
		sem_t_route
		sem_t_task
		sem_f_param
		sem_f_alm
		sem_f_mt
		sem_f_485
		sem_plc
		sem_cas
		sem_rs485
		sem_hhu
		sem_gpio */

void open_tables(void)
{
	tmt_open();
	t485_open();
	tstat_open();
	trt_open();
	task_open();
	fparam_init();
	falm_open();
	fmt_open();
	f485_open();
	PRINTF("Open CON files\n");
}

void flush_tables(void)
{
	falm_flush();
	fparam_flush();
	t485_flush();
	task_flush();
	tmt_flush();
	tstat_flush();
	trt_flush();
	fmt_flush();
	f485_flush();
}

void close_tables(void)
{
	f485_close();
	fmt_close();
	fparam_destroy();
	falm_close();
	task_unlock();
	trt_close();
	tstat_close();
	t485_close();
	tmt_close();
	PRINTF("Close CON files\n");
}

static time_t fmt_idx_convert(const BYTE *buf, int len)
{
	struct tm tm;

	memset(&tm, 0, sizeof(tm));
	tm.tm_year = 100 + buf[0];
	tm.tm_mon = (len >= 2) ? buf[1] - 1 : 0;
	tm.tm_mday = (len >= 3) ? buf[2] : 1;
	tm.tm_hour = (len >= 4) ? buf[3] : 0;
	tm.tm_min = (len >= 5) ? buf[4] : 0;
	tm.tm_sec = (len >= 6) ? buf[5] : 0;
	return mktime(&tm);
}

void fmt_idx_init(FMT_IDX *fmt, int idx, const MTID mtid, const BYTE *time,
	int time_len)
{
	time_t tt;
	FMT_IDX *ptr;

	tt = fmt_idx_convert(time, time_len);
	ptr = fmt + idx;
	ptr->flag = 1;
	memcpy(ptr->mtid, mtid, MTID_LEN);
	ptr->tt = tt;
}

int fmt_idx_read(FMT_IDX *fmt, int cnt, const MTID mtid, const BYTE *time,
	int time_len)
{
	int idx;
	time_t tt;
	FMT_IDX *ptr;

	tt = fmt_idx_convert(time, time_len);
	ptr = fmt;
	for (idx = 0; idx < cnt; idx ++) {
		if (ptr->flag && ptr->tt == tt && !memcmp(ptr->mtid, mtid, MTID_LEN))
			return idx;
		ptr ++;
	}
	return -1;
}

int fmt_idx_write(FMT_IDX *fmt, int cnt, const MTID mtid, const BYTE *time,
	int time_len)
{
	int idx, min_idx;
	time_t min_tt;
	FMT_IDX *ptr;

	if ((min_idx = fmt_idx_read(fmt, cnt, mtid, time, time_len)) < 0) {
		min_tt = 0;
		ptr = fmt;
		for (idx = 0; idx < cnt; idx ++) {
			if (ptr->flag == 0) {
				min_idx = idx;
				break;
			}
			if (min_idx == -1 || ptr->tt < min_tt) {
				min_idx = idx;
				min_tt = ptr->tt;
			}
			ptr ++;
		}
	}
	fmt_idx_init(fmt, min_idx, mtid, time, time_len);
	return min_idx;
}
