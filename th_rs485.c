/*****************************************************************************
 *	C-Plan Concentrator 
 *
 *	(c) Copyright 2006-2007, Shenzhen Kaifa Technology Co.,Ltd. 
 *	All Rights Reserved 
 *
 *	Functions to Realtime Access Meter
 *
 *	Author: dajiang wan
 *	Created on: 2008-01-14
 *****************************************************************************/
#include "common.h"
#include "global.h"
#include "threads.h"
#include "device.h"
#include "t_485.h"
#include "f_485.h"
#include "f_param.h"
#include "f_alm.h"
#include "mt_485.h"
#include "err_code.h"

static void rs485_proc(const struct tm *tm)
{
	int i;
	MTID mtid;
	BYTE time_str[7];
	T485_PARA para;
	T485_DATA data;
	struct tm tmp;

	for (i = 0; i < T_485_ROWS_CNT && !g_terminated; i ++) {
		if (t485_is_empty(i))
			continue;
		t485_lock();
		t485_get_mtid(i, mtid);
		t485_get_para(i, &para);
		t485_unlock();
		if (t485_read_real_para(mtid, time_str, &para)
			&& t485_read_real_data(mtid, &data)) {
			tmp = *tm;
			tmp.tm_sec = 0;
			t485_lock();
			fparam_lock();
			falm_lock();
			f485_lock();
			t485_check_para_data(tm, i, mtid, time_str, &para, &data);
			t485_set_para(&tmp, i, mtid, &para);
			t485_set_data(&tmp, i, mtid, &data);
			f485_set_data(&tmp, mtid, &data);
			f485_unlock();
			falm_unlock();
			fparam_unlock();
			t485_unlock();
		}
		else {
			PRINTF("rs485_proc, read real para and data error\n");
		}
	}
}

static int try_read_mtid(void)
{
	int len, ret;
	MTID mtid1 = {0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa};
	MTID mtid2 = {0x99, 0x99, 0x99, 0x99, 0x99, 0x99};
	MTID mtid;

	if ((len = mt485_read(mtid1, mtid, sizeof(mtid), 0xc032)) > 0)
		ret = 1;
	else if ((len = mt485_read(mtid2, mtid, sizeof(mtid), 0xc032)) > 0)
		ret = 1;
	else
		ret = 0;
	if (ret) {
		t485_lock();
		t485_set_default(0, mtid);
		t485_unlock();
	}
	return ret;
}

static int t485_ready_flag = 0;

int t485_is_ready(void)
{
	return t485_ready_flag;
}

void *rs485_thread(void *arg)
{
	struct tm tm;
	long idle_uptime = 0;
	int read_flag = 0, read_mtid_flag = 0;

	while (!g_terminated) {
		msleep(500);
		notify_watchdog();
		check_sched();
		if (read_mtid_flag == 0)
			read_mtid_flag = try_read_mtid();
		if (uptime() - idle_uptime >= 7 * 60) {
			rs485_lock();
			self_diag(rs485_open, rs485_close);
			rs485_unlock();
			idle_uptime = uptime();
		}
		if (!check_rtc() || t485_all_is_empty() || !read_mtid_flag)
			continue;
		my_time(&tm);
		if ((tm.tm_min % 3) == 0) {
			if (read_flag == 0) {
				rs485_proc(&tm);
				t485_ready_flag = 1;
				read_flag = 1;
			}
		}
		else {
			read_flag = 0;
		}
	}
	return NULL;
}
