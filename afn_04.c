/*
 *  C-Plan Concentrator
 *
 *  Copyright (C) 2006-2007, Shenzhen Kaifa Technology Co.,Ltd.
 *
 *  Author: dajiang wan
 *  Created on: 2008-01-07
 */
#include "afn.h"
#include "f_param.h"
#include "err_code.h"
#include "common.h"
#include "misc.h"
#include "device.h"
#include "t_485.h"
#include "t_task.h"

int afn04_set(const BYTE *buf, int len, int di, int *param_len)
{
	int ret, tmp;

	fparam_lock();
	ret = fparam_write(di, buf, len, &tmp);
	fparam_unlock();
	if (ret < 0)
		return ERR_BAD_VALUE;
	*param_len = ret;
	return ERR_OK;
}

int afn04_10(BYTE pn, BYTE fn, const BYTE *buf, int len, int *param_len)
{
	int ret = ERR_BAD_VALUE, tmp;
	BYTE cnt;
	const BYTE *ptr = buf;

	t485_lock();
	if (pn == 0 && len >= 1 && len >= 17 * buf[0] + 1) {
		cnt = *ptr ++, *param_len = 17 * cnt + 1;
		while (cnt -- > 0) {
			pn = *ptr ++;
			t485_write(pn, fn, ptr, 16, &tmp);
			ptr += 16;
		}
		ret = ERR_OK;
	}
	t485_unlock();
	return ret;
}

int afn04_set_fn(BYTE pn, BYTE fn, const BYTE *buf, int len, int *param_len)
{
	BYTE data[128];
	int tmp;

	PRINTF("AFN=04H, Pn=%d, Fn=%d\n", pn, fn);
	switch (fn) {
	case 1:
		return afn04_set(buf, len, FPARAM_AFN04_F1, param_len);
	case 2:
		if (len < 1 || (buf[0] & 0x7f) > 16 || len < 1 + (buf[0] & 0x7f) * 2)
			return ERR_BAD_VALUE;
		*param_len = 1 + (buf[0] & 0x7f) * 2;
		memset(data, 0, sizeof(data));
		memcpy(data, buf, *param_len);
		fparam_lock();
		fparam_write(FPARAM_AFN04_F2, data, sizeof(data), &tmp);
		fparam_unlock();
		return ERR_OK;
	case 3:
		if (len < 6 * 4 + 16)
			return ERR_BAD_VALUE;
		fparam_lock();
		fparam_write(FPARAM_IP_PORT_PRI, buf, 6, &tmp);
		fparam_write(FPARAM_IP_PORT_SEC, buf + 6, 6, &tmp);
		fparam_write(FPARAM_IP_PORT_GW, buf + 12, 6, &tmp);
		fparam_write(FPARAM_IP_PORT_PROXY, buf + 18, 6, &tmp);
		fparam_write(FPARAM_APN, buf + 24, 16, &tmp);
		fparam_unlock();
		*param_len = 6 * 4 + 16;
		return ERR_OK;
	case 4:
		return afn04_set(buf, len, FPARAM_AFN04_F4, param_len);
	case 5:
		return afn04_set(buf, len, FPARAM_AFN04_F5, param_len);
	case 6:
		return afn04_set(buf, len, FPARAM_AFN04_F6, param_len);
	case 7:
		return afn04_set(buf, len, FPARAM_AFN04_F7, param_len);
	case 8:
		return afn04_set(buf, len, FPARAM_AFN04_F8, param_len);
	case 9:
		return ERR_BAD_VALUE; // read-only
	case 10:
		return afn04_10(pn, fn, buf, len, param_len);
	case 11:	// TODO
		return ERR_BAD_VALUE;
	case 12:
		return afn04_set(buf, len, FPARAM_AFN04_F12, param_len);
	case 13:	// TODO
		return ERR_BAD_VALUE;
	case 14:	// TODO
		return ERR_BAD_VALUE;
	case 15:	// TODO
		return ERR_BAD_VALUE;
	case 16:
		if (len < 32)
			return ERR_BAD_VALUE;
		fparam_lock();
		memset(data, 0, sizeof(data)); memcpy(data, buf, 16);
		fparam_write(FPARAM_VPN_USER, data, sizeof(data), &tmp);
		memset(data, 0, sizeof(data)); memcpy(data, buf + 16, 16);
		fparam_write(FPARAM_VPN_PASS, data, sizeof(data), &tmp);
		fparam_unlock();
		*param_len = 32;
		change_gprs_password(buf, 16, buf + 16, 16);
		return ERR_OK;
	case 17:
		return afn04_set(buf, len, FPARAM_AFN04_F17, param_len);
	case 18:
		return afn04_set(buf, len, FPARAM_AFN04_F18, param_len);
	case 19:
		return afn04_set(buf, len, FPARAM_AFN04_F19, param_len);
	case 20:
		return afn04_set(buf, len, FPARAM_AFN04_F20, param_len);
	case 21:
		return afn04_set(buf, len, FPARAM_AFN04_F21, param_len);
	case 22:
		return afn04_set(buf, len, FPARAM_AFN04_F22, param_len);
	case 23:
		return afn04_set(buf, len, FPARAM_AFN04_F23, param_len);
	case 24:
		return afn04_set(buf, len, FPARAM_AFN04_F24, param_len);
	case 25: // fall through
	case 26: // fall through
	case 27: // fall through
	case 28: // fall through
		t485_lock();
		tmp = t485_write(pn, fn, buf, len, param_len);
		t485_unlock();
		return tmp ? ERR_OK : ERR_BAD_VALUE;
	case 33: // TODO
	case 41: // TODO
	case 42: // TODO
	case 43: // TODO
	case 44: // TODO
	case 45: // TODO
	case 46: // TODO
	case 47: // TODO
	case 48: // TODO
		return ERR_BAD_VALUE;
	case 49: // TODO
		return ERR_BAD_VALUE;
	case 57:
		return afn04_set(buf, len, FPARAM_AFN04_F57, param_len);
	case 58:
		return afn04_set(buf, len, FPARAM_AFN04_F58, param_len);
	case 59:
		return afn04_set(buf, len, FPARAM_AFN04_F59, param_len);
	case 60:
		return afn04_set(buf, len, FPARAM_AFN04_F60, param_len);
	case 61:
		return afn04_set(buf, len, FPARAM_AFN04_F61, param_len);
	case 62:
		return afn04_set(buf, len, FPARAM_AFN04_F62, param_len);
	case 65:
		task_lock();
		tmp = task1_set(pn, buf, len);
		task_unlock();
		if (tmp <= 0) {
			PRINTF("task1_set fail, task_id:%d\n", pn);
			return ERR_BAD_VALUE;
		}
		else {
			*param_len = tmp;
			return ERR_OK;
		}
	case 66:
		task_lock();
		tmp = task2_set(pn, buf, len);
		task_unlock();
		if (tmp <= 0) {
			PRINTF("task2_set fail, task_id:%d\n", pn);
			return ERR_BAD_VALUE;
		}
		else {
			*param_len = tmp;
			return ERR_OK;
		}
	case 67:
		task_lock();
		tmp = task1_set_flag(pn, buf, len);
		task_unlock();
		if (tmp <= 0) {
			PRINTF("task1_set_flag fail, task_id:%d\n", pn);
			return ERR_BAD_VALUE;
		}
		else {
			*param_len = tmp;
			return ERR_OK;
		}
	case 68:
		task_lock();
		tmp = task2_set_flag(pn, buf, len);
		task_unlock();
		if (tmp <= 0) {
			PRINTF("task2_set_flag fail, task_id:%d\n", pn);
			return ERR_BAD_VALUE;
		}
		else {
			*param_len = tmp;
			return ERR_OK;
		}
	case 73: // TODO
	case 74: // TODO
	case 75: // TODO
	case 76: // TODO
		return ERR_BAD_VALUE;
	case 81: // TODO
	case 82: // TODO
	case 83: // TODO
		return ERR_BAD_VALUE;
	default:
		return ERR_BAD_VALUE;
	}
}
