/*****************************************************************************
 *	C-Plan Concentrator 
 *
 *	(c) Copyright 2006-2007, Shenzhen Kaifa Technology Co.,Ltd. 
 *	All Rights Reserved 
 *		
 *****************************************************************************/

#ifndef _MT_ALARM_H
#define _MT_ALARM_H

#include "typedef.h"

void alarm_event(EVENT event);

int mt_alarm(const struct tm *tm);

#endif /* _MT_ALARM_H */
