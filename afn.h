#ifndef _AFN_H
#define _AFN_H

#include "typedef.h"

int check_pn(const BYTE *pn, int pn_cnt);

int check_fn(const BYTE *fn, int fn_cnt);

int da_to_pn(const BYTE *da, BYTE *pn);

int dt_to_fn(const BYTE *dt, BYTE *fn);

void pn_to_da(BYTE fn, BYTE *dt);

void fn_to_dt(BYTE fn, BYTE *dt);

int afn01_set_fn(BYTE pn, BYTE fn, const BYTE *buf, int len, int *param_len);

int afn04_set_fn(BYTE pn, BYTE fn, const BYTE *buf, int len, int *param_len);

int afn05_set_fn(BYTE pn, BYTE fn, const BYTE *buf, int len, int *param_len);

int afn06_get_fn(BYTE pn, BYTE fn, BYTE *out_buf, int max_len,
	const BYTE *param, int *param_len);

int afn0a_get_fn(BYTE pn, BYTE fn, BYTE *out_buf, int max_len,
	const BYTE *param, int *param_len);

int afn0c_get_fn(BYTE pn, BYTE fn, BYTE *out_buf, int max_len,
	const BYTE *param, int *param_len);

int afn0d_get_fn(BYTE pn, BYTE fn, BYTE *out_buf, int max_len,
	const BYTE *param, int *param_len);

int afn0e_get_fn(BYTE pn, BYTE fn, BYTE *out_buf, int max_len,
	const BYTE *param, int *param_len);

int afn0f_set_fn(BYTE pn, BYTE fn, const BYTE *buf, int len, int *param_len);

int afn10_get_fn(BYTE pn, BYTE fn, BYTE *out_buf, int max_len,
	const BYTE *param, int *param_len);

int afn84_set_fn(BYTE pn, BYTE fn, const BYTE *buf, int len, int *param_len);

int afn85_set_fn(BYTE pn, BYTE fn, const BYTE *buf, int len, int *param_len);

int afn8a_get_fn(BYTE pn, BYTE fn, BYTE *out_buf, int max_len,
	const BYTE *param, int *param_len);

int afn8c_get_fn(BYTE pn, BYTE fn, BYTE *out_buf, int max_len,
	const BYTE *param, int *param_len);

int afn8d_get_fn(BYTE pn, BYTE fn, BYTE *out_buf, int max_len,
	const BYTE *param, int *param_len);

int afn8e_get_fn(BYTE pn, BYTE fn, BYTE *out_buf, int max_len,
	const BYTE *param, int *param_len);

int is_wakeup_packet(const void *buf, int len);

void clean_check(void);

int afn_proc(BYTE msa, BYTE afn, const BYTE *pass, const BYTE *in_buf,
	int in_len, BYTE *out_buf, int max_len, int *data_len, int data_cnt);

#endif /* _AFN_H */
