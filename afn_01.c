/*
 *  C-Plan Concentrator
 *
 *  Copyright (C) 2006-2007, Shenzhen Kaifa Technology Co.,Ltd.
 *
 *  Author: dajiang wan
 *  Created on: 2008-01-08
 */
#include "afn.h"
#include "err_code.h"
#include "threads.h"
#include "common.h"
#include "t_db.h"
#include "f_param.h"
#include "f_alm.h"

static int clean_flag;
static BYTE addr[4];

int afn01_set_fn(BYTE pn, BYTE fn, const BYTE *buf, int len, int *param_len) {
	PRINTF("AFN=01H, Pn=%d, Fn=%d\n", pn, fn);
	switch (fn) {
	case 1: // Hardware init
		*param_len = 0;
		clean_flag = 1;
		set_reboot_flag(1);
		return ERR_OK;
	case 2: // data init
		*param_len = 0;
		clean_flag = 2;
		set_reboot_flag(1);
		return ERR_OK;
	case 3: // parameters and data init
		*param_len = 0;
		fparam_read(FPARAM_CON_ADDRESS, addr, 4);
		clean_flag = 3;
		set_reboot_flag(1);
		return ERR_OK;
	case 247: // parameters init
		*param_len = 0;
		fparam_read(FPARAM_CON_ADDRESS, addr, 4);
		clean_flag = 4;
		set_reboot_flag(1);
		return ERR_OK;
	default:
		return ERR_BAD_VALUE;
	}
}

static void alarm_record(BYTE *addr) {
	int param_len;
	BYTE alm_buf[9];
	time_t tt;

	open_tables();
	alm_buf[0] = 1;
	memset(alm_buf + 1, 0x00, 8);
	fparam_lock();
	falm_lock();
	falm_add_con(0x0e, 1, alm_buf, 9);
	time(&tt);
	fparam_set_value(FPARAM_POWERFAIL_TIME, &tt, sizeof(tt), &param_len);
	if (addr)
		fparam_set_value(FPARAM_CON_ADDRESS, addr, 4, &param_len);
	falm_unlock();
	fparam_unlock();
	close_tables();
}

void clean_check(void) {

	switch (clean_flag) {
	case 1:
		break; // nothing to do
	case 2:
		system("rm -rf f_alm.dat f_485.dat f_mt.dat t_*.dat");
		alarm_record(NULL);
		break;
	case 3:
		system("rm -rf *.dat");
		alarm_record(addr);
		break;
	case 4:
		system("rm -rf f_param.dat");
		alarm_record(addr);
		break;
	default:
		break; // nothing to do
	}
}
