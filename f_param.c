/*
 *  C-Plan Concentrator
 *
 *  Copyright (C) 2006-2007, Shenzhen Kaifa Technology Co.,Ltd.
 *
 *  Functions to operate on concentrator IDs.
 *
 *  Author: dajiang wan
 *  Created on: 2007-09-17
 */
#include "f_param.h"
#include "misc.h"
#include "common.h"
#include "err_code.h"
#include "f_alm.h"
#include "t_mt.h"
#include "device.h"
#include "threads.h"

static F_PARAM f_param;
static F_PARAM *p_f_param = &f_param;
static int fd_f_param;
static sem_t sem_f_param;

static int fparam_find(WORD id)
{
	int i;
	struct param *ptr = p_f_param->params;

	for (i = 0; i < FPARAM_COUNT; i ++) {
		if (ptr->id == id)
			return i;
		ptr ++;
	}
	return -1;
}

static void fparam_add(int idx, WORD id, WORD flag, WORD len,
	WORD offset, const void *buf)
{
	struct param *head = p_f_param->params + idx;
	unsigned char *data = p_f_param->data + offset;

	head->id = id;
	head->flag = flag;
	head->len = len;
	head->offset = offset;
	memcpy(data, buf, len);
}

static void fparam_update(int idx, int head_flag, int flush_flag)
{
	struct param *ptr = p_f_param->params + idx;

	if (head_flag) {
		lseek(fd_f_param, offsetof(F_PARAM, params), SEEK_SET);
		safe_write(fd_f_param, p_f_param->params, sizeof(p_f_param->params));
	}
	lseek(fd_f_param, offsetof(F_PARAM, data) + ptr->offset, SEEK_SET);
	safe_write(fd_f_param, p_f_param->data + ptr->offset, ptr->len);
	if (flush_flag) fparam_flush();
}

static int fparam_get_flag_and_len(WORD id, BYTE *flag, int *len)
{
	int idx;
	struct param *ptr = p_f_param->params;

	if ((idx = fparam_find(id)) >= 0) {
		ptr += idx;
		*flag = ptr->flag;
		*len = ptr->len;
		return 1;
	}
	*flag = 0;
	*len = 0;
	return 0;
}

void fparam_lock(void)
{
	sem_wait(&sem_f_param);
}

void fparam_unlock(void)
{
	sem_post(&sem_f_param);
}

int fparam_get_value(WORD id, void *buf, int max_len)
{
	int idx;
	struct param *ptr = p_f_param->params;

	if ((idx = fparam_find(id)) >= 0) {
		ptr += idx;
		if (ptr->len <= max_len) {
			memcpy(buf, p_f_param->data + ptr->offset, ptr->len);
			return ptr->len;
		}
	}
	return 0;
}

int fparam_set_value(WORD id, const void *buf, int max_len, int *param_len)
{
	int idx;
	struct param *ptr = p_f_param->params;

	*param_len = 0;
	if ((idx = fparam_find(id)) >= 0) {
		ptr += idx; *param_len = ptr->len;
		if (ptr->len <= max_len) {
			if (memcmp(p_f_param->data + ptr->offset, buf, ptr->len) != 0) {
				memcpy(p_f_param->data + ptr->offset, buf, ptr->len);
				fparam_update(idx, 0, 1);
			}
			return ptr->len;
		}
	}
	return 0;
}

int fparam_get_all(WORD id, void *buf, int max_len)
{
	int idx;
	struct param *head = p_f_param->params;
	BYTE *ptr = buf;

	for (idx = 0; idx < FPARAM_COUNT; idx ++) {
		if (head->id == id)
			break;
		head ++;
	}
	if (idx >= FPARAM_COUNT || 5 + head->len > max_len)
		return 0;
	*ptr ++ = head->flag;
	*ptr ++ = head->offset >> 8;
	*ptr ++ = head->offset;
	*ptr ++ = head->len >> 8;
	*ptr ++ = head->len;
	memcpy(ptr, p_f_param->data + head->offset, head->len);
	return 5 + head->len;
}

int fparam_set_all(WORD id, const void *buf, int max_len, int *param_len)
{
	struct param *head = p_f_param->params;
	const BYTE *ptr = buf;
	int i, idx;
	BYTE flag;
	WORD offset, len;

	if (max_len < 5)
		return 0;
	flag = ptr[0];
	offset = (ptr[1] << 8) + ptr[2];
	len = (ptr[3] << 8) + ptr[4];
	if (max_len < 5 + len)
		return 0;
	idx = -1;
	for (i = 0; i < FPARAM_COUNT; i ++) {
		if (head->id == id)
			idx = i;
		if (idx == -1 && head->id == 0x0000)
			idx = i;
		head ++;
	}
	if (idx >= 0) {
		fparam_add(idx, id, flag, len, offset, ptr + 5);
		fparam_update(idx, 1, 1);
		*param_len = 5 + len;
		return *param_len;
	}
	return 0;
}

static void fparam_fill(void)
{
	const BYTE cmnet[32] = "CMNET";
	BYTE all_ff[FPARAM_LENGTH], all_zero[FPARAM_LENGTH];
	BYTE default_alm_flag8e[] = "\xff\xff\xff\xff\xff\xff\xff\xff\x00\x00\x00\x00\x00\x00\x00\x00";
	BYTE default_alm_flag0e[] = "\xff\xff\xff\xff\xff\xff\xff\xff\x00\x23\x00\x30\x00\x00\x00\x00";
	int offset, idx, tmp;

	memset(p_f_param->params, 0x00, sizeof(struct param) * FPARAM_COUNT);
	memset(p_f_param->data, 0xff, FPARAM_LENGTH);
	memset(all_ff, 0xff, sizeof(all_ff));
	memset(all_zero, 0x00, sizeof(all_zero));

	idx = offset = 0;
	fparam_add(idx ++, FPARAM_CON_ADDRESS, 0x07, 4, offset, all_zero); offset += 4;
	fparam_add(idx ++, FPARAM_IP_PORT_PRI, 0x07, 6, offset, all_ff); offset += 6;
	fparam_add(idx ++, FPARAM_IP_PORT_SEC, 0x07, 6, offset, all_ff); offset += 6;
	fparam_add(idx ++, FPARAM_IP_PORT_GW, 0x07, 6, offset, all_ff); offset += 6;
	fparam_add(idx ++, FPARAM_IP_PORT_PROXY, 0x07, 6, offset, all_ff); offset += 6;
	fparam_add(idx ++, FPARAM_APN, 0x07, 16, offset, cmnet); offset += 16;
	fparam_add(idx ++, FPARAM_USER_PASSWD, 0x0B, 3, offset, "\x11\x11\x11"); offset += 3;
	fparam_add(idx ++, FPARAM_ADMIN_PASSWD, 0x05, 3, offset, "\x22\x22\x22"); offset += 3;
	fparam_add(idx ++, FPARAM_VPN_USER, 0x07, 32, offset, all_zero); offset += 32;
	fparam_add(idx ++, FPARAM_VPN_PASS, 0x07, 32, offset, all_zero); offset += 32;
	fparam_add(idx ++, FPARAM_ALLOW_REMOTE, 0x07, 1, offset, all_ff); offset += 1;
	fparam_add(idx ++, FPARAM_ALLOW_SPONT, 0x07, 1, offset, all_ff); offset += 1;

	// AFN-84H-F9 
	fparam_add(idx ++, FPARAM_CON_CASCADE, 0x07, 16, offset, all_ff); offset += 16;

	// AFN-84H-F10
	fparam_add(idx ++, FPARAM_AFN84_F10, 0x07, 6, offset, "\xCB\x64\x32\x05\x02\x05"); offset += 6;

	// AFN-84-F11
	fparam_add(idx ++, FPARAM_FMON_READ_TIME, 0x07, 2, offset, "\x01\x01"); offset += 2;
	fparam_add(idx ++, FPARAM_FDAY_READ_TIME, 0x07, 1, offset, "\x01"); offset += 1;  
	fparam_add(idx ++, FPARAM_FLOAD_READ_TIME, 0x07, 1, offset, "\x01"); offset += 1;  
	fparam_add(idx ++, FPARAM_FROZ_HOUR, 0x07, 1, offset, "\x00"); offset += 1;
	fparam_add(idx ++, FPARAM_BROAD_FROZ_HOUR, 0x07, 3, offset, "\xFF\xFF\xFF"); offset += 3;

	/* AFN-84H-F12, reading-disabled periods */
	tmp = 1 + 4 * NBR_UNREAD_PERIOD;
	fparam_add(idx ++, FPARAM_UNREAD_PERIOD, 0x07, tmp, offset, all_zero); offset += tmp;

	// AFN-84H-F13
	fparam_add(idx ++, FPARAM_RELAY_DEPTH, 0x07, 1, offset, "\x03"); offset += 1; //maximum depth of repeater, default value is 3.
	fparam_add(idx ++, FPARAM_RELAY_METHOD, 0x07, 1, offset, "\x00"); offset += 1;
	
	// AFN-84H-F15
	fparam_add(idx ++, FPARAM_AFN84_F15, 0x07, 16, offset, default_alm_flag8e); offset += 16;

	// AFN-85H-F9
	fparam_add(idx ++, FPARAM_AUTO_UNREAD, 0x07, 1, offset, "\x00"); offset += 1; // CON auto read meter

	// AFN-8CH-F2
	fparam_add(idx ++, FPARAM_FDAY_FINISH_TIME, 0x07, 3, offset, "\x00\x00\x00"); offset += 3;
	fparam_add(idx ++, FPARAM_FDAY_FINISH_FLAG, 0x07, 1, offset, "\x55"); offset += 1;

	fparam_add(idx ++, FPARAM_FMON_FINISH_TIME, 0x07, 3, offset, "\x00\x00\x00"); offset += 3;
	fparam_add(idx ++, FPARAM_FMON_FINISH_FLAG, 0x07, 1, offset, "\x55"); offset += 1;

	// AFN-04H
	fparam_add(idx ++, FPARAM_AFN04_F1,0x07, 6, offset, "\x01\x02\x03\x10\x00\x05"); offset += 6;
	fparam_add(idx ++, FPARAM_AFN04_F2, 0x07, 33, offset, all_zero); offset += 33;
	fparam_add(idx ++, FPARAM_AFN04_F4, 0x07, 16, offset, all_zero); offset += 16;
	fparam_add(idx ++, FPARAM_AFN04_F5, 0x07, 3, offset, all_zero); offset += 3;
	fparam_add(idx ++, FPARAM_AFN04_F6, 0x07, 16, offset, all_zero); offset += 16;
	fparam_add(idx ++, FPARAM_AFN04_F7, 0x07, 6, offset, all_zero); offset += 6;
	fparam_add(idx ++, FPARAM_AFN04_F8, 0x07, 16, offset, default_alm_flag0e); offset += 16;
	fparam_add(idx ++, FPARAM_AFN04_F12, 0x07, 3, offset, all_zero); offset += 3;
	fparam_add(idx ++, FPARAM_AFN04_F17, 0x07, 2, offset, all_ff); offset += 2;
	fparam_add(idx ++, FPARAM_AFN04_F18, 0x07, 12, offset, all_ff); offset += 11;
	fparam_add(idx ++, FPARAM_AFN04_F19, 0x07, 1, offset, all_ff); offset += 1;
	fparam_add(idx ++, FPARAM_AFN04_F20, 0x07, 1, offset, all_ff); offset += 1;
	fparam_add(idx ++, FPARAM_AFN04_F21, 0x07, 25, offset, all_ff); offset += 25;
	fparam_add(idx ++, FPARAM_AFN04_F22, 0x07, 56, offset, all_ff); offset += 56;
	fparam_add(idx ++, FPARAM_AFN04_F23, 0x07, 3, offset, all_ff); offset += 3;
	fparam_add(idx ++, FPARAM_AFN04_F24, 0x07, 1, offset, "\x15"); offset += 1;
	fparam_add(idx ++, FPARAM_AFN04_F57, 0x07, 3, offset, all_ff); offset += 3;
	fparam_add(idx ++, FPARAM_AFN04_F58, 0x07, 1, offset, all_ff); offset += 1;
	fparam_add(idx ++, FPARAM_AFN04_F59, 0x07, 4, offset, all_ff); offset += 4;
	fparam_add(idx ++, FPARAM_AFN04_F60, 0x07, 44, offset, all_ff); offset += 44;
	fparam_add(idx ++, FPARAM_AFN04_F61, 0x07, 1, offset, all_ff); offset += 1;
	fparam_add(idx ++, FPARAM_AFN04_F62, 0x07, 5, offset, "\xFF\x2C\x01\x03\x05"); offset += 5;

	// AFN-0CH
	fparam_add(idx ++, FPARAM_AFN0C_F1, 0x07, 30, offset, all_ff); offset += 30;
	fparam_add(idx ++, FPARAM_AFN0C_F3, 0x07, 31, offset, all_ff); offset += 31;
	fparam_add(idx ++, FPARAM_AFN0C_F8, 0x02, 8, offset, all_zero); offset += 8;
	fparam_add(idx ++, FPARAM_AFN0C_F9, 0x02, 2, offset, all_zero); offset += 2;

	// AFN-0EH
	fparam_add(idx ++, FPARAM_POWERFAIL_TIME, 0x02, 4, offset, all_zero); offset += 4;
	fparam_add(idx ++, FPARAM_POWERUP_TIME, 0x02, 4, offset, all_zero); offset += 4;

	// the following ids is added by kaifa
	fparam_add(idx ++, FPARAM_CON_SWD, 0x07, 2, offset, "\x00\x10"); offset += 2;
	fparam_add(idx ++, FPARAM_MFG_STATE, 0x07, 1, offset, "\xAA"); offset += 1;
	fparam_add(idx ++, FPARAM_PLC_RETRY, 0x07, 1, offset, "\x01"); offset += 1;
	fparam_add(idx ++, FPARAM_PLC_TZC, 0x07, 1, offset, "\x19"); offset += 1;
	fparam_add(idx ++, FPARAM_PLC_TFWOUT, 0x07, 1, offset, "\x3C"); offset += 1;
	fparam_add(idx ++, FPARAM_PLC_TAPPOUT, 0x07, 2, offset, "\xE8\x03"); offset += 2;
	fparam_add(idx ++, FPARAM_PROC_ENABLE_MASK, 0x07, 2, offset, all_ff); offset += 2;
	fparam_add(idx ++, FPARAM_LAST_PROC, 0x02, 1, offset, all_ff); offset += 1;
	fparam_add(idx ++, FPARAM_LAST_MTIDX, 0x02, 2, offset, all_ff); offset += 2;
	fparam_add(idx ++, FPARAM_CARRIER_BAUDRATE, 0x07, 1, offset, "\x03"); offset += 1;
	fparam_add(idx ++, FPARAM_485_BAUD1, 0x07, 4, offset, "\x80\x25\x00\x00"); offset += 4;
	fparam_add(idx ++, FPARAM_485_BAUD2, 0x07, 4, offset, "\xB0\x04\x00\x00"); offset += 4;
	fparam_add(idx ++, FPARAM_HHU_BAUD, 0x07, 4, offset, "\x80\x25\x00\x00"); offset += 4;
	fparam_add(idx ++, FPARAM_ELOSE_DATE, 0x02, 3, offset, all_ff); offset += 3;
	fparam_add(idx ++, FPARAM_COMMAND, 0x05, 1, offset, all_ff); // virtual
	fparam_add(idx ++, FPARAM_SYNC_CLOCK_FLAG, 0x07, 3, offset, "\x01\x00\x81"); offset += 3;
	fparam_add(idx ++, FPARAM_USER_PASSWD_FLAG, 0x07, 5, offset, all_zero); offset += 5;
	fparam_add(idx ++, FPARAM_FONT_SIZE, 0x07, 1, offset, "\x10"); offset += 1;
	fparam_add(idx ++, FPARAM_CHANGE_FREQ, 0x07, 1, offset, "\x01"); offset += 1;
	fparam_add(idx ++, FPARAM_LAST_NET_TYPE, 0x07, 1, offset, "\xff"); offset += 1;
}

void fparam_init(void)
{
	int size = sizeof(F_PARAM);
	const char *name = F_PARAM_NAME;

	sem_init(&sem_f_param, 0, 1);
	if (!check_file(name, size)) {
		/* File doesnot exist or file length is invalid. */
		PRINTF("File %s is created, size:%d\n", name, size);
		fd_f_param = open(name, O_CREAT | O_RDWR | O_TRUNC, 0600);
		fparam_fill();
		safe_write(fd_f_param, p_f_param, size);
		fdatasync(fd_f_param);
		close(fd_f_param);
	}
	fd_f_param = open(name, O_RDWR);
	safe_read(fd_f_param, p_f_param, size);
}

void fparam_flush(void)
{
	fdatasync(fd_f_param);
}

void fparam_destroy(void)
{
	fdatasync(fd_f_param);
	close(fd_f_param);
	sem_destroy(&sem_f_param);
}

int fparam_read(WORD id, void *buf, int max_len)
{
	int len;
	BYTE flag;

	if (fparam_get_flag_and_len(id, &flag, &len) >= 0) {
		if (flag & FPARAM_FLAG_READ && len <= max_len) {
			if (id == FPARAM_CON_SWD) // get status word (kaifa id)
				stoc(buf, get_con_statewd());
			else if (id == FPARAM_LAST_PROC) // this id is add by kaifa
				((BYTE *)buf)[0] = get_last_proc();
			else if (id == FPARAM_LAST_MTIDX) // this id is add by kaifa
				stoc(buf, get_last_mtidx());
			else
				fparam_get_value(id, buf, len);
			return len;
		}
	}
	return -ERR_BAD_VALUE;
}

static int process_command(BYTE cmd)
{
	switch (cmd) {
	case 1: // LVC reboot
		set_reboot_flag(1);
		return 1;
	case 2: // modem reboot
		set_modem_reboot();
		return 1;
	case 3: // EB reboot
		set_eb_reboot();
		return 1;
	default:
		return 0;
    }
}

int fparam_write(WORD id, const void *buf, int max_len, int *param_len)
{
	int len;
	BYTE flag;

	*param_len = 0;
	if (fparam_get_flag_and_len(id, &flag, &len) >= 0) {
		*param_len = len;
		if (flag & FPARAM_FLAG_WRITE && len <= max_len) {
			if (id == FPARAM_CON_SWD) {
				set_con_statewd(ctos(buf));
			}
			else if (id == FPARAM_COMMAND) {
				if (!process_command(((BYTE *)buf)[0]))
					return -ERR_BAD_VALUE;
			}
			else {
				fparam_set_value(id, buf, max_len, &len);
			}
			return len;
		}
	}
	return -ERR_BAD_VALUE;
}
