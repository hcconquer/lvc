/*****************************************************************************
*	C-Plan Concentrator
*
*	(c) Copyright 2006-2007, Shenzhen Kaifa Technology Co.,Ltd.
*	All Rights Reserved
*
*	To read the meters day frozen data.
*	This procedure end until all meter's day data are read, or a day is end.
*
*	Author: fanhua zeng, djwan
*	Created on: 2006-07-31
*****************************************************************************/
#include "common.h"
#include "t_mt.h"
#include "t_stat.h"
#include "msg_code.h"
#include "mt_access.h"
#include "mt_day.h"
#include "f_mt.h"
#include "f_alm.h"
#include "f_param.h"
#include "misc.h"
#include "err_code.h"

static void begin_mt_day(void)
{
	BYTE buf[128];
	int param_len;
	
	fparam_get_value(FPARAM_FDAY_FINISH_FLAG, buf, 1); 
	if (buf[0] != 0x55) {
		buf[0] = 0x55;
		fparam_set_value(FPARAM_FDAY_FINISH_FLAG, buf, 1, &param_len);
	}
}

static void end_mt_day(void)
{
	BYTE buf[128];
	int param_len;
	struct tm tm;

	fparam_get_value(FPARAM_FDAY_FINISH_FLAG, buf, 1); 
	if (buf[0] != 0x00) {
		buf[0] = 0x00;
		my_time(&tm);
		buf[1] = bin_to_bcd(tm.tm_min);
		buf[2] = bin_to_bcd(tm.tm_hour);
		buf[3] = bin_to_bcd(tm.tm_mday);
		fparam_set_value(FPARAM_FDAY_FINISH_FLAG, buf, 1, &param_len);
		fparam_set_value(FPARAM_FDAY_FINISH_TIME, buf + 1, 3, &param_len);
	}
}

static void day_read(const struct tm *tm, short mtidx, const MTID mtid)
{
	BYTE resp[PLC_APDU_LEN], data[21], nbr_retry;
	int resp_len, tmp, energy_cnt, err = 0;
	WORD di;
	time_t tt;

	/* to read stateword, time/date, frozen energy */
	nbr_retry = get_nbr_retry();
	bzero(resp, sizeof(resp));
	if (tmt_get_tariff(mtidx) > 1) {
		di = 0x9A1F;
		energy_cnt = 5;
	}
	else {
		di = 0x9A10;
		energy_cnt = 1;
	}
	if (read_frozen_energy(mtidx, mtid, nbr_retry, di, resp, &resp_len)) {
		fmt_lock();
		if (resp_len == 8 + 6 * energy_cnt) { // new changsha protocol
			BYTE *ptr1, *ptr2, mon, day;

			ptr1 = data, ptr2 = resp + 7;
			day = resp[4], mon = resp[5];
			*ptr1 ++ = *ptr2 ++; // save statewd
			for (tmp = 0; tmp < energy_cnt; tmp ++) {
				if (ptr2[4] == day && ptr2[5] == mon
					&& tm->tm_mday == bcd_to_bin(day)
					&& tm->tm_mon + 1 == bcd_to_bin(mon))
					memcpy(ptr1, ptr2, 4);
				else
					err = 1;
				ptr1 += 4, ptr2 += 6;
			}
		}
		else if (resp_len == 8 + 4 * energy_cnt) {
			memcpy(data, resp + 7, 1 + 4 * energy_cnt);
		}
		else { // bad return value
			err = 1;
		}
		if (err == 0) {
			fmt_write_day(mtid, tm->tm_year % 100, tm->tm_mon + 1, tm->tm_mday,
				data, 1 + 4 * energy_cnt);
		}
		fmt_unlock();
	}

	tstat_lock();
	tt = time(NULL);
	if (resp_len == 0) {
		tstat_set_time_nr(mtidx, tt);
		tmp = tstat_get_nbr_nr(mtidx);
		if (tmp < 255)
			tstat_set_nbr_nr(mtidx, tmp + 1);
	}
	else {
		tstat_set_time_reach(mtidx, tt);
		tstat_set_nbr_nr(mtidx, 0);
	}
	tstat_update(mtidx, 0);
	tstat_unlock();

	fparam_lock();
	set_last_proc(PROC_MT_DAY);
	set_last_mtidx(mtidx);
	fparam_unlock();
}

static void day_prev_read(BYTE year, BYTE mon, BYTE day, BYTE day_prev,
	short mtidx, const MTID mtid)
{
	BYTE resp[PLC_APDU_LEN], data[21], nbr_retry, *ptr1, *ptr2;
	int resp_len, tmp, energy_cnt, err;
	WORD di;

	nbr_retry = get_nbr_retry();
	bzero(resp, sizeof(resp));
	if (tmt_get_tariff(mtidx) > 1) {
		di = 0x9A1F;
		energy_cnt = 5;
	}
	else {
		di = 0x9A10;
		energy_cnt = 1;
	}
	while (day_prev > 0) {
		previous_day(&year, &mon, &day);
		di += 0x10;
		day_prev --;
	}
	resp_len = meter_read(mtidx, mtid, nbr_retry, resp, 2, 0xC020, di);
	if (resp_len == 1 + 6 * energy_cnt) { // new changsha protocol
		fmt_lock();
		ptr1 = data, ptr2 = resp;
		*ptr1 ++ = *ptr2 ++; // save statewd
		err = 0;
		for (tmp = 0; tmp < energy_cnt; tmp ++) {
			if (ptr2[4] == bin_to_bcd(day) && ptr2[5] == bin_to_bcd(mon))
				memcpy(ptr1, ptr2, 4);
			else
				err = 1;
			ptr1 += 4, ptr2 += 6;
		}
		if (err == 0)
			fmt_write_day(mtid, year, mon, day, data, 1 + 4 * energy_cnt);
		fmt_unlock();
	}
}

static int day_next_mtidx(short *mtidx, MTID mtid, BYTE year, BYTE mon,
	BYTE day, BYTE *day_prev)
{
	BYTE yy, mm, dd;
	int i;

	while ((*mtidx = tmt_next(*mtidx)) >= 0) {
		if (!tmt_is_selected(*mtidx))
			continue;
		yy = year, mm = mon, dd = day; *day_prev = 0;
		tmt_get_mtid(*mtidx, mtid);
		for (i = 0; i < 8; i ++) {
			if (!fmt_exist_day(mtid, yy, mm, dd))
				return 1;
			previous_day(&yy, &mm, &dd);
			(*day_prev) ++;
		}
	}
	return 0;
}

static int day_all_read(BYTE year, BYTE mon, BYTE day)
{
	short mtidx;
	MTID mtid;

	for (mtidx = 0; mtidx < NBR_REAL_MT; mtidx ++) {
		if (!tmt_is_empty(mtidx) && tmt_is_selected(mtidx)) {
			tmt_get_mtid(mtidx, mtid);
			if (!fmt_exist_day(mtid, year, mon, day))
				return 0;
		}
	}
	return 1;
}

static int day_cond(const struct tm *tm)
{
	int ret = 0;
	BYTE hour;

	if (!meter_read_disable(tm)) {
		fparam_get_value(FPARAM_FDAY_READ_TIME, &hour, 1);
		if (hour == 0xff || tm->tm_hour >= bcd_to_bin(hour))
			ret = 1;
	}
	return ret;
}

void day_event(EVENT event)
{
	// nothing to do
}

int mt_day(const struct tm *tm)
{
	static short mtidx = -1;
	static MTID mtid;
	short need;
	BYTE year, mon, day, day_prev;

	tmt_lock();
	fparam_lock();
	fmt_lock();
	year = tm->tm_year % 100;
	mon = tm->tm_mon + 1;
	day = tm->tm_mday;
	day_prev = 0;
	if (!day_cond(tm))
		need = 0;
	else
		need = day_next_mtidx(&mtidx, mtid, year, mon, day, &day_prev);
	fmt_unlock();
	fparam_unlock();
	tmt_unlock();
	if (need) {
		LOG_PRINTF("day: %d %d\n", mtidx, day_prev);
		if (day_prev)
			day_prev_read(year, mon, day, day_prev, mtidx, mtid);
		else
			day_read(tm, mtidx, mtid);
	}
	tmt_lock();
	fparam_lock();
	fmt_lock();
	if (day_all_read(year, mon, day))
		end_mt_day();
	else if (need)
		begin_mt_day();
	fmt_unlock();
	fparam_unlock();
	tmt_unlock();
	return need;
}
