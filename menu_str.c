/*
 * C-Plan Concentrator
 *
 * Copyright (C) 2008, Shenzhen Kaifa Technology Co.,Ltd.
 *
 * Author: jianping zhang
 * Created on: 2008-01-30
 */

#include "menu_str.h"
#include "lcd.h"
#include "lvc_test.h"
#include "common.h"
#include "t_485.h"

const char *c_lvc_test_str1[] = {
	"LCD测试",
	"MODEM继电器测试",
	"PLC继电器测试",
	"RTC",
	"LED测试",
	"PLC测试",
	"RS485测试",
	"HHU测试",
	"MODEM测试",
	"遥信测试",
	"键盘测试",
};

const char *c_lvc_test_str2[] = {
	"成功", "失败", "完成",
};

const char *c_pls_input_key_str[] = {
	"请输入按键 ←",
	"请输入按键 →",
	"请输入按键 ↑",
	"请输入按键 ↓",
	"请输入按键 Esc",
	"请输入按键 Enter",
};

const char *c_pls_input_yx_str[] = {
	"请输入遥信 YX1",
	"请输入遥信 YX2",
	"请输入遥信 YX3",
	"请输入遥信 YX4",
	"请输入门接点信号",
};

const char *c_lvc_test_cb_ok_str = "状态字OK";
const char *c_lvc_test_cb_ko_str = "状态字KO";

const char *c_lvc_test_prompt_info_str[] = {
	"4",
	"       提示       ",
	"请准备如上表号电表",
	"请连接两路RS485   ",
	"请确认MODEM已连接 ",
};

const char *PROTOCOL_TYPE_INVALID_STR = "无效";
const char *PROTOCOL_TYPE_645_STR = "DL/T645规约";
const char *PROTOCOL_TYPE_AC_SAMPLE_STR = "交流采样装置";
const char *METER_TYPE_RS485_STR = "普通RS485表";
const char *METER_TYPE_PLC_STR = "载波表";
const char *METER_TYPE_SIMPLE_STR = "简易多功能表";
const char *METER_TYPE_MULTI_STR = "多功能总表";
const char *METER_TYPE_REPEATER_STR = "中继器";
const char *PREPAY_TYPE_ENERGY_STR = "预付电量";
const char *PREPAY_TYPE_FEE_STR = "预付费";
const char *PREPAY_TYPE_NONO_STR = "非预付";
const char *CONNECT_TYPE_DIRECT_STR = "直接接线";
const char *CONNECT_TYPE_NON_DIRECT_STR = "经互感器接线";
const char *SPMT_STR = "单相表";
const char *PPMT_STR = "三相表";
const char *NO_PHASE_STR = "不确定";
const char *PHASE_STR = "相";
const char *PHASE_A_STR = "A相";
const char *PHASE_B_STR = "B相";
const char *PHASE_C_STR = "C相";
const char *NORMAL_USER_STR = "普通";
const char *FOCUS_USER_STR = "重点";
const char *UNSELECT_STR = "不选抄";
const char *SELECT_STR = "选抄";
const char *NO_RELAY_STR = "不带拉闸";
const char *RELAY_STR = "带拉闸";
const char *ENABLE_POWEROFF_STR = "允许断电";
const char *DISABLE_POWEROFF_STR = "不许断电";
const char *UNKNOWN_STR = "不确定";
const char *REBOOT_STR = "请重新启动系统";

#define SAMPLE(x) "测量点"#x
const char *menu_name1 = "测量点数据显示"; /* 1 */
const char *menu_name1_1 = "居民电表数据"; /* 1.1 */
const char *menu_name1_2[] = { /* 1.2 - 1.65 */
	SAMPLE(1), SAMPLE(2), SAMPLE(3), SAMPLE(4), SAMPLE(5), SAMPLE(6),
	SAMPLE(7), SAMPLE(8), SAMPLE(9), SAMPLE(10), SAMPLE(11), SAMPLE(12),
	SAMPLE(13), SAMPLE(14), SAMPLE(15), SAMPLE(16), SAMPLE(17), SAMPLE(18),
	SAMPLE(19), SAMPLE(20), SAMPLE(21), SAMPLE(22), SAMPLE(23), SAMPLE(24),
	SAMPLE(25), SAMPLE(26), SAMPLE(27), SAMPLE(28), SAMPLE(29), SAMPLE(30),
	SAMPLE(31), SAMPLE(32), SAMPLE(33), SAMPLE(34), SAMPLE(35), SAMPLE(36),
	SAMPLE(37), SAMPLE(38), SAMPLE(39), SAMPLE(40), SAMPLE(41), SAMPLE(42),
	SAMPLE(43), SAMPLE(44), SAMPLE(45), SAMPLE(46), SAMPLE(47), SAMPLE(48),
	SAMPLE(49), SAMPLE(50), SAMPLE(51), SAMPLE(52), SAMPLE(53), SAMPLE(54),
	SAMPLE(55), SAMPLE(56), SAMPLE(57), SAMPLE(58), SAMPLE(59), SAMPLE(60),
	SAMPLE(61), SAMPLE(62), SAMPLE(63), SAMPLE(64)
};

const char *menu_name1_2_0[] = {
	"正向有功电能",
	"反向有功电能",
	"正向无功电能",
	"反向无功电能",
	"一象限无功电能",
	"二象限无功电能",
	"三象限无功电能",
	"四象限无功电能",
	"电压",
	"电流",
	"有功功率",
	"无功功率",
	"功率因数",
};

const char *menu_name2 = "参数设置与查看";
const char *menu_name2_1 = "测量点档案";
const char *menu_name2_2 = "测量点基本参数";
const char *menu_name2_3 = "测量点限值参数";
const char *menu_name2_4 = "测量点统计";
const char *menu_name2_5 = "居民电表档案";
const char *menu_name2_6 = "居民电表统计";
const char *menu_name2_7 = "居民电表抄表进度";
const char *menu_name2_8 = "事件信息";
const char *menu_name2_8_1 = "事件统计";
const char *menu_name2_8_2 = "终端总表新事件";
const char *menu_name2_8_3 = "终端总表历史事件";
const char *menu_name2_8_4 = "低压集抄新事件";
const char *menu_name2_8_5 = "低压集抄历史事件";
const char *menu_name3 = "集中器管理与维护";
const char *menu_name3_1 = "集中器信息";
const char *menu_name3_2 = "通信参数";
const char *menu_name3_2_1 = "信道类型";
const char *menu_name3_2_2 = "GPRS参数";
const char *menu_name3_2_3 = "短信参数";
const char *menu_name3_2_4 = "级联通信参数";
const char *menu_name3_2_5 = "显示信号强度";
const char *menu_name3_3 = "集中器对时";
const char *menu_name3_4 = "修改界面密码";
const char *menu_name3_5 = "修改集中器地址";
const char *menu_name3_6 = "修改测量点地址";
const char *menu_name3_8 = "更改显示字体";
const char *menu_name3_9 = "初始化";
const char *menu_name3_9_1 = "硬件初始化";
const char *menu_name3_9_2 = "数据区初始化";
const char *menu_name3_9_3 = "所有数据初始化";

const char *c_numeric_list[] = {"0", "1", "2", "3", "4", "5", "6", "7", "8",
	"9"};

const char *c_terminal_info_str[] = {
	"硬件版本",
	"软件版本",
	"集抄协议版本",
	"集中器地址",
	"集中器组地址",
};

const char *c_calibrate_clock_str[] = {
	"日期", "时间",
};

const char *c_menu_password_str[] = {
	"请输入旧密码",
	"请输入新密码",
	"再次输入新密码",
	"新旧密码不一致！",
	"修改密码成功！",
	"修改密码失败！",
	"请输入密码",
	"密码验证通过！",
	"密码验证失败！",
	"密码错误次数太多！",
	"密码验证被取消！",
};

const char *c_up_channel_type_str[] = {
	"无效",
	"永久在线",
	"被动激活",
	"以太网",
};

const char *c_ip_address_str = "IP地址:";
const char *c_port_str = "端口:";
const char *c_username_str = "用户:";
const char *c_password_str = "密码:";
const char *c_host_phone_str = "主站电话号码:";
const char *c_sms_center_phone_str = "短信中心号码:";

const char *c_cascade_parameter_str[] = {
	"偶",
	"奇",
	"无",
	"波特率:       位数:",
	"校验:   停止位:",
	"报文超时时间:",
	"字节超时时间:",
	"传输延时时间:",
	"响应超时时间:",
	"重发次数:",
};

const char *c_sample_doc_str[] = {
	"测量点总数",
	"脉冲总数",
	"模拟量总数",
	"总加组总数",
	"电表序号",
	"所属测量点号",
	"端口号",
	"速率",
	"通信规约",
	"通信地址",
	"通信密码",
	"整数位数",
	"小数位数",
	"费率个数",
};

const char *c_usermt_doc_str[] = {
	"电表总数",
	"普通用户",
	"重点用户",
	"载波表",
	"RS485表",
	"多功能表",
	"表序号",
	"表号",
	"费率号",
	"方案",
	"线路编号",
	"表箱编号",
	"采集终端编号",
	"无此电表档案",
};

const char *c_sample_basic_str[] = {
	"测量点号:",
	"电压互感器倍率:",
	"电流互感器倍率:",
	"额定电压:",
	"额定电流:",
	"电源接线方式:",
};

const char *c_sample_limit_str[] = {
	"测量点号:",
	"电压合格上限:   .  V",
	"电压合格下限:   .  V",
	"电压断相门限:   .  V",
	"过压门限:   .  V",
	"欠压门限:   .  V",
	"过流门限:  .   A",
	"额定电流门限:  .   A",
	"零序电流上限:  .   A",
	"视在功率上上限:",
	"  .     KVA",
	"视在功率上限:",
	"  .     KVA",
	"电压不平衡限值:",
	"   . %%",
	"电流不平衡限值:",
	"   . %%",
	"连续失压时间限值:",
	"   分",
};

const char *c_con_event_stat_str[] = {
	"终端总表重要事件",
	"终端总表一般事件",
	"低压集抄重要事件",
	"低压集抄一般事件",
};

const char *c_no_str = "无";
const char *c_no_this_event_str = "无此事件信息";

const char *c_view_erc_info_str[] = {
	"类型",
	"一般事件",
	"重要事件",
	"停电时间",
	"上电时间",
	"发生时间",
	"电表序号",
	"表号",
	"抄表完成时间",
	"选抄表数",
	"不成功表数",
	"不成功表序号",
};

const char *c_con_erc_record_str[] = {
	"30", /* number of total meter event */
	"初始化和版本变更",
	"参数丢失记录",
	"参数变更记录",
	"状态量变位记录",
	"遥控跳闸记录",
	"功控跳闸记录",
	"电控跳闸记录",
	"电能表参数变更",
	"电流回路异常",
	"电压回路异常",
	"相序异常",
	"电能电时间超差",
	"电表故障信息",
	"终端停上电事件",
	"谐波越限告警",
	"直流模拟量越限记录",
	"U与I不平衡度越限",
	"电容器投切自锁记录",
	"购电参数设置记录",
	"消息认证错误记录",
	"终端故障记录",
	"有功总电能差动越限",
	"此事件类型未定义",
	"电压越限记录",
	"电流越限记录",
	"视在功率越限记录",
	"电度表示度下降记录",
	"电能量超差记录",
	"电能量飞走记录",
	"电能量停走记录",
};

const char *c_meter_erc_record_str[] = {
	"19", /* number of meter event */
	"遥控跳闸记录",
	"越门限跳闸记录",
	"电控跳闸记录",
	"电能表参数变更",
	"电流回路异常",
	"电压回路异常",
	"抄表情况记录",
	"电能表时间超差",
	"电表故障信息",
	"U和I不平衡度越限",
	"此事件类型未定义",
	"集中器故障记录",
	"电压越限记录",
	"电流越限记录",
	"视在功率越限记录",
	"电能表示度下降记录",
	"此事件类型未定义",
	"电能表飞走记录",
	"电能表停走记录",
};

const char *c_con_malfunction_str[] = {
	"7",
	"集中器主板内存故障",
	"时钟故障",
	"主板通信故障",
	"485抄表故障",
	"显示板故障",
	"抄集中器故障",
	"未知故障",
};

const char *c_read_usermt_status_str[] = {
	"选抄表数",
	"已抄表数",
	"轮抄结束",
	"是",
	"否",
	"结束时间",
	"日",
};

const char *c_usermt_realtime_str[] = {
	"表号",
	"日期",
	"时间",
	"正向有功",
	"反向有功",
	"上月冻结",
	"上日冻结",
};

const char *c_process_status[] = {
	"正在读电表数据...",
	"读电表成功。",
	"读电表失败！",
};

const char *con_address_str = "集中器通讯地址";
const char *c_info_sys_init_str = "系统初始化...";
const char *c_info_sys_ready_str = "轮显测量点1";
const char *c_info_main_menu_str = "主菜单";

const char *c_info_comm_str[] = {
	"未找到 MODEM",
	"初始化MODEM失败",
	"没有SIM卡",
	"GPRS网络",
	"CDMA网络",
	"未搜索到网络",
	"信号强度",
	"正在拨号...",
	"拨号失败!",
	"拨号成功!",
	"IP:",
	"注册成功!",
	"注册失败!",
	"本地通道",
	"无效通道",
	"正在搜索网络",
};

const char *c_info_task_exec_str[] = {
	"正在进行载波通信",
	"正在进行广播校时",
	"正在广播冻结时间",
	"正在与主站通信",
	"正在进行红外通信",
};

const char *c_numeric_keyboard_str[] = {
	" 退出 删除 确认 ",
	"  1    2    3   ",
	"  4    5    6   ",
	"  7    8    9   ",
	"  *    0    #   ",
	"  ,    .   字母 ",
};

const char *c_lower_letter_keyboard_str[] = {
	" 退出 删除 确认 ",
	" a b c d e f g  ",
	" h i j k l m n  ",
	" o p q r s t u  ",
	" v w x y z 空格 ",
	" 大写 符号 数字 ",
};

const char *c_upper_letter_keyboard_str[] = {
	" 退出 删除 确认 ",
	" A B C D E F G  ",
	" H I J K L M N  ",
	" O P Q R S T U  ",
	" V W X Y Z 空格 ",
	" 小写 符号 数字 ",
};

const char *c_arrow_str[] = {"↑", "↓", "←", "→"};

static const struct scroll_screen screens[] = {
	{0, 0x9010, offsetof(T485_DATA, di_901f) +  0, 4},
	{0, 0x9011, offsetof(T485_DATA, di_901f) +  4, 4},
	{0, 0x9012, offsetof(T485_DATA, di_901f) +  8, 4},
	{0, 0x9013, offsetof(T485_DATA, di_901f) + 12, 4},
	{0, 0x9014, offsetof(T485_DATA, di_901f) + 16, 4},

	{1, 0x9020, offsetof(T485_DATA, di_902f) +  0, 4},
	{1, 0x9021, offsetof(T485_DATA, di_902f) +  4, 4},
	{1, 0x9022, offsetof(T485_DATA, di_902f) +  8, 4},
	{1, 0x9023, offsetof(T485_DATA, di_902f) + 12, 4},
	{1, 0x9024, offsetof(T485_DATA, di_902f) + 16, 4},

	{2, 0x9110, offsetof(T485_DATA, di_911f) +  0, 4},
	{2, 0x9111, offsetof(T485_DATA, di_911f) +  4, 4},
	{2, 0x9112, offsetof(T485_DATA, di_911f) +  8, 4},
	{2, 0x9113, offsetof(T485_DATA, di_911f) + 12, 4},
	{2, 0x9114, offsetof(T485_DATA, di_911f) + 16, 4},

	{3, 0x9120, offsetof(T485_DATA, di_912f) +  0, 4},
	{3, 0x9121, offsetof(T485_DATA, di_912f) +  4, 4},
	{3, 0x9122, offsetof(T485_DATA, di_912f) +  8, 4},
	{3, 0x9123, offsetof(T485_DATA, di_912f) + 12, 4},
	{3, 0x9124, offsetof(T485_DATA, di_912f) + 16, 4},

	{4, 0x9130, offsetof(T485_DATA, di_913f) +  0, 4},
	{4, 0x9131, offsetof(T485_DATA, di_913f) +  4, 4},
	{4, 0x9132, offsetof(T485_DATA, di_913f) +  8, 4},
	{4, 0x9133, offsetof(T485_DATA, di_913f) + 12, 4},
	{4, 0x9134, offsetof(T485_DATA, di_913f) + 16, 4},

	{5, 0x9140, offsetof(T485_DATA, di_914f) +  0, 4},
	{5, 0x9141, offsetof(T485_DATA, di_914f) +  4, 4},
	{5, 0x9142, offsetof(T485_DATA, di_914f) +  8, 4},
	{5, 0x9143, offsetof(T485_DATA, di_914f) + 12, 4},
	{5, 0x9144, offsetof(T485_DATA, di_914f) + 16, 4},

	{6, 0x9150, offsetof(T485_DATA, di_915f) +  0, 4}, 
	{6, 0x9151, offsetof(T485_DATA, di_915f) +  4, 4}, 
	{6, 0x9152, offsetof(T485_DATA, di_915f) +  8, 4}, 
	{6, 0x9153, offsetof(T485_DATA, di_915f) + 12, 4},
	{6, 0x9154, offsetof(T485_DATA, di_915f) + 16, 4},

	{7, 0x9160, offsetof(T485_DATA, di_916f) +  0, 4},
	{7, 0x9161, offsetof(T485_DATA, di_916f) +  4, 4},
	{7, 0x9162, offsetof(T485_DATA, di_916f) +  8, 4},
	{7, 0x9163, offsetof(T485_DATA, di_916f) + 12, 4},
	{7, 0x9164, offsetof(T485_DATA, di_916f) + 16, 4},

	{10, 0xB611, offsetof(T485_DATA, di_b611), 2},
	{10, 0xB612, offsetof(T485_DATA, di_b612), 2},
	{10, 0xB613, offsetof(T485_DATA, di_b613), 2},

	{11, 0xB621, offsetof(T485_DATA, di_b621), 2},
	{11, 0xB622, offsetof(T485_DATA, di_b622), 2},
	{11, 0xB623, offsetof(T485_DATA, di_b623), 2},

	{12, 0xB630, offsetof(T485_DATA, di_b630), 3},
	{12, 0xB631, offsetof(T485_DATA, di_b631), 3},
	{12, 0xB632, offsetof(T485_DATA, di_b632), 3},
	{12, 0xB633, offsetof(T485_DATA, di_b633), 3},

	{13, 0xB640, offsetof(T485_DATA, di_b640), 2},
	{13, 0xB641, offsetof(T485_DATA, di_b641), 2},
	{13, 0xB642, offsetof(T485_DATA, di_b642), 2},
	{13, 0xB643, offsetof(T485_DATA, di_b643), 2},

	{14, 0xB650, offsetof(T485_DATA, di_b650), 2},
	{14, 0xB651, offsetof(T485_DATA, di_b651), 2},
	{14, 0xB652, offsetof(T485_DATA, di_b652), 2},
	{14, 0xB653, offsetof(T485_DATA, di_b653), 2},
};

void get_di_label(void *buf, int di)
{
	static const struct di_desc {
		unsigned short di;
		const char *desc;
	} di_desc_arr[] = {
		{0x901F, "正向有功"}, {0x902F, "反向有功"},
		{0x911F, "正向无功"}, {0x912F, "反向无功"},
		{0x913F, "一象限无功"}, {0x914F, "四象限无功"},
		{0x915F, "二象限无功"}, {0x916F, "三象限无功"},
		{0xB611, "Ａ相电压"}, {0xB612, "Ｂ相电压"}, {0xB613, "Ｃ相电压"},
		{0xB621, "Ａ相电流"}, {0xB622, "Ｂ相电流"}, {0xB623, "Ｃ相电流"},
		{0xB630, "瞬时有功"}, {0xB631, "Ａ相有功"},
		{0xB632, "Ｂ相有功"}, {0xB633, "Ｃ相有功"},
		{0xB640, "瞬时无功"}, {0xB641, "Ａ相无功"},
		{0xB642, "Ｂ相无功"}, {0xB643, "Ｃ相无功"},
		{0xB650, " 总功率因数 "}, {0xB651, "Ａ相功率因数"},
		{0xB652, "Ｂ相功率因数"}, {0xB653, "Ｃ相功率因数"},
		{0x0000, NULL}
	};
	static const char *tariff = "总块";
	const struct di_desc *di_desc_ptr = di_desc_arr;
	int tmp_di, label_len;
	const char *label;
	BYTE *ptr;

	if (di >= 0x9010 && di <= 0x916f) // energy
		tmp_di = di | 0x000f;
	else
		tmp_di = di;
	while (di_desc_ptr->di != 0x0000) {
		if (di_desc_ptr->di == tmp_di)
			break;
		di_desc_ptr ++;
	}
	label = di_desc_ptr->desc;
	label_len = strlen(label);
	strcpy(buf, label);
	if (di >= 0x9010 && di <= 0x916f) { // energy
		ptr = buf + label_len;
		tmp_di = di & 0x0f;
		if (tmp_di == 0x00) {
			*ptr ++ = tariff[0];
			*ptr ++ = tariff[1];
		}
		else if (tmp_di == 0x0f) {
			*ptr ++ = tariff[2];
			*ptr ++ = tariff[3];
		}
		else {
			*ptr ++ = '0' + (tmp_di / 10);
			*ptr ++ = '0' + (tmp_di % 10);
		}
		*ptr = 0x00;
	}
}

int format_di_str(void *buf, int len, int di, const void *data, int data_len)
{
	char label[20];
	const BYTE *ptr = data;

	get_di_label(label, di);
	if (di >= 0x9010 && di <= 0x912f) { // energy
		if (data_len >= 4)
			len = snprintf(buf, len, "%s %02x%02x%02x.%02x", label, ptr[3],
				ptr[2], ptr[1], ptr[0]);
		else
			len = snprintf(buf, len, "%s ------.--", label);
	}
	else if (di >= 0x9130 && di <= 0x916f) { // four quadrant energy
		if (data_len >= 4)
			len = snprintf(buf, len, "%s %02x%02x%02x", label, ptr[3],
				ptr[2], ptr[1]);
		else
			len = snprintf(buf, len, "%s ------", label);
	}
	else if (di >= 0xB611 && di <= 0xB613) { // voltage
		if (data_len >= 2)
			len = snprintf(buf, len, "%s %01x%02x V", label, ptr[1], ptr[0]);
		else
			len = snprintf(buf, len, "%s --- V", label);
	}
	else if (di >= 0xB621 && di <= 0xB623) { // current
		if (data_len >= 2)
			len = snprintf(buf, len, "%s %02x.%02x A", label, ptr[1], ptr[0]);
		else
			len = snprintf(buf, len, "%s --.-- A", label);
	}
	else if (di >= 0xB630 && di <= 0xB633) { // power
		if (data_len >= 3)
			len = snprintf(buf, len, "%s %02x.%02x%02x kW", label, ptr[2],
				ptr[1], ptr[0]);
		else
			len = snprintf(buf, len, "%s --.---- kW", label);
	}
	else if (di >= 0xB640 && di <= 0xB643) { // inactive power factor
		if (data_len >= 2)
			len = snprintf(buf, len, "%s %02x.%02x kvarh", label, ptr[1],
				ptr[0]);
		else
			len = snprintf(buf, len, "%s --.-- kvarh", label);
	}
	else if (di >= 0xB650 && di <= 0xB653) { // power factor
		if (data_len >= 2)
			len = snprintf(buf, len, "%s %1x.%1x%02x", label,
				(ptr[1] >> 4) & 0x0f, ptr[1] & 0x0f, ptr[0]);
		else
			len = snprintf(buf, len, "%s -.---", label);
	}
	else {
		len = snprintf(buf, len, "--------");
	}
	return len;
}

int get_scroll_screen(int idx, struct scroll_screen *screen, int max_cnt)
{
	int i, j;

	for (i = j = 0; i < sizeof(screens) / sizeof(struct scroll_screen); i ++) {
		if (screens[i].screen == idx && j < max_cnt)
			screen[j ++] = screens[i];
	}
	return j;
}
