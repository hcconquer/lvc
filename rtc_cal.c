/*
 *  C-Plan Concentrator
 *
 *  Copyright (C) 2006-2007, Shenzhen Kaifa Technology Co.,Ltd.
 *
 *  This program is used to write calibrate byte to the CONCENTRATOR I2C
 *  real-time clock.
 *
 *  Author: dajiang wan
 *  Created on: 2006-09-05
 */

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>

int set_rtc_ctl(int fd, unsigned char val)
{
	int ret;
	ret = write(fd, &val, 1);
	if (ret < 0)
		printf("set_rtc_ctl fail\n");
	return ret;
}

int get_rtc_ctl(int fd, unsigned char *val)
{
	int ret;
	ret = read(fd, val, 1);
	if (ret < 0)
		printf("get_rtc_ctl fail\n");
	return ret;
}

int main(int argc, char ** argv)
{
	int fd;
	unsigned char val;
	if ((fd = open("/dev/rtc", O_RDWR)) >= 0) {
		if (argc == 2) {
			val = atoi(argv[1]);
			set_rtc_ctl(fd, val);
		}
		else {
			if (get_rtc_ctl(fd, &val) >= 0)
				printf("val:%d\n", val);
		}
		close(fd);
	}
	return 0;
}
