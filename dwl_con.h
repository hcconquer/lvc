#ifndef _DWL_CON_H
#define _DWL_CON_H

int dwl_check_packet(const void *buf, int *len);

int dwl_packet(int cnt, int seq_off, const void *buf, int len);

int dwl_finish(void);

void dwl_check(void);

void ftp_download_file(const char *user, const char *pass,
	const char *host, int port, const char *file);

int upload_name(void *buf, int len, short pack_len, const void *name);

int upload_file(void *buf, int len, short pack_no, short pack_cnt);

#endif /* _DWL_CON_H */
