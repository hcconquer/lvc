/*
 *  C-Plan Concentrator
 *
 *  Copyright (C) 2006-2007, Shenzhen Kaifa Technology Co.,Ltd.
 *
 *  This program is used to protect the CONCENTRATOR program. The CONCENTRATOR
 *  program must send signals to this program periodically, otherwise the
 *  operating system will reboot.
 *
 *  Author: dajiang wan
 *  Created on: 2006-09-05
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <fcntl.h>
#include <time.h>
#include <errno.h>
#include <string.h>
#include <sys/sysinfo.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <linux/capability.h>
#include <linux/watchdog.h>

#define TIMER_COUNT		7
#define SIGWATCHDOG		(SIGRTMIN)  /* arm linux is 35 */

#define BIT_WATCHDOG		(1 << 5)
#define GPIOCTL_OUTP_SET	_IOW('G', 0x01, unsigned int)
#define GPIOCTL_OUTP_CLR	_IOW('G', 0x02, unsigned int)

struct TIMER {
	long timeout;
	long uptime;
} timers[TIMER_COUNT];

long uptime(void) {
	struct sysinfo info;

	sysinfo(&info);
	return info.uptime;
}

int safe_read(int fd, char * buf, int len) {
	int ret;
	char * ptr = buf;

	while (len > 0) {
		ret = read(fd, ptr, len);
		if (ret < 0 && errno == EINTR)
			continue;
		if (ret < 0)
			return ret;
		if (ret == 0)
			break;
		ptr += ret;
		len -= ret;
	}
	return ptr - buf;
}

int safe_write(int fd, const char * buf, int len) {
	int ret;
	const char * ptr = buf;

	while (len > 0) {
		ret = write(fd, ptr, len);
		if (ret < 0 && errno == EINTR)
			continue;
		if (ret < 0)
			return ret;
		if (ret == 0)
			break;
		ptr += ret;
		len -= ret;
	}
	return ptr - buf;
}

void signal_handler(int sig) {
	if (sig >= SIGWATCHDOG && sig < SIGWATCHDOG + TIMER_COUNT)
		timers[sig - SIGWATCHDOG].uptime = uptime();
}

void init_timers(long timeout) {
	int i;
	long new_uptime = uptime();

	for (i = 0; i < TIMER_COUNT; i++) {
		timers[i].timeout = timeout;
		timers[i].uptime = new_uptime;
		signal(SIGWATCHDOG + i, signal_handler);
	}
}

void do_truncate(const char *name, int delta) {
	int fd, size, new_size;
	char *buf;

	if ((fd = open(name, O_RDWR)) >= 0) {
		lockf(fd, F_LOCK, 0L);
		size = lseek(fd, 0L, SEEK_END) - delta;
		if (size > 0 && (buf = malloc(size)) != NULL) {
			lseek(fd, delta, SEEK_SET);
			new_size = safe_read(fd, buf, size);
			if (new_size > 0) {
				lseek(fd, 0L, SEEK_SET);
				safe_write(fd, buf, new_size);
				ftruncate(fd, new_size);
			}
			free(buf);
		}
		lseek(fd, 0L, SEEK_SET);
		lockf(fd, F_ULOCK, 0L);
		close(fd);
	}
}

void do_append(const char *name, const char *data, int data_len) {
	int fd, buf_len;
	char buf[256];
	struct tm tm;
	struct timeval tv;

	if ((fd = open(name, O_RDWR | O_CREAT, 0600)) >= 0) {
		gettimeofday(&tv, NULL);
		localtime_r(&tv.tv_sec, &tm);
		buf_len = snprintf(buf, sizeof(buf),
				"%04d-%02d-%02d %02d:%02d:%02d.%03ld\t", 1900 + tm.tm_year,
				tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec,
				tv.tv_usec / 1000);
		lockf(fd, F_LOCK, 0L);
		lseek(fd, 0L, SEEK_END);
		write(fd, buf, buf_len);
		write(fd, data, data_len);
		lockf(fd, F_ULOCK, 0L);
		close(fd);
	}
}

void do_log(const char *log_file, int log_max, int log_delta, const char *data,
		int data_len) {
	struct stat st;
	int prompt = 24; // length of the timestamp prompt, see do_append

	if (stat(log_file, &st) == 0 && st.st_size + prompt + data_len > log_max) {
		log_delta = st.st_size + prompt + data_len - (log_max - log_delta);
		do_truncate(log_file, log_delta);
	}
	do_append(log_file, data, data_len);
}

void check_timers(void) {
	int i;
	char buf[256];
	long new_uptime = uptime();

	for (i = 0; i < TIMER_COUNT; i++) {
		if (new_uptime - timers[i].uptime >= timers[i].timeout) {
			timers[i].uptime = new_uptime;
			snprintf(buf, sizeof(buf), "SIGNAL: %d time out \n", SIGWATCHDOG
			+ i);
			do_log("watchdog.log", 16 * 1024, 2 * 1024, buf, strlen(buf));
			sync();
			system("/sbin/reboot");
			exit(1); // hardware watchdog will reset if /sbin/reboot fails.
		}
	}
}

void usage(void) {
	printf("Usage: watchdog <timeout in seconds>\n");
}

static void msleep(int msec) {
	int ret;
	struct timeval tv;

	tv.tv_sec = 0;
	tv.tv_usec = msec * 1000;
	do {
		ret = select(0, NULL, NULL, NULL, &tv);
	} while (ret < 0 && errno == EINTR);
}

static void gpio_count(int fd) {
	unsigned int val;

	if (fd < 0)
		return;
	val = BIT_WATCHDOG;ioctl(fd, GPIOCTL_OUTP_SET, &val);
	msleep(10);
ioctl(fd, GPIOCTL_OUTP_CLR, &val);
}

int main(int argc, char **argv) {
	int i, fd, gpio_fd, timeout, count = 0;

	if (argc != 2 || (timeout = atol(argv[1])) < 0) {
		usage();
		return 1;
	}
	if (fork() != 0)
		exit(0);
	setsid();
	signal(SIGHUP, SIG_IGN);
	if (fork() != 0)
		exit(0);
	for (i = 3; i < 128; i++)
		close(i);
	chdir("/");
	if ((fd = open("/dev/watchdog", O_WRONLY)) < 0) {
		printf("Fail to open /dev/watchdog \n");
		exit(0);
	}
	if ((gpio_fd = open("/dev/gpio", O_RDWR)) < 0) {
		printf("Fail to open /dev/gpio\n");
		exit(0);
	}
	init_timers(timeout);
	while (1) {
		check_timers();
		write(fd, "1", 1); // write something to notify watchdog
		if (++count >= 10) {
			gpio_count(gpio_fd);
			count = 0;
		}
		sleep(1);
	}
	close(gpio_fd);
	close(fd);
	return 0;
}
