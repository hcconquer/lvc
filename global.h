/*****************************************************************************
 *	C-Plan Concentrator 
 *
 *	(c) Copyright 2006-2007, Shenzhen Kaifa Technology Co.,Ltd. 
 *	All Rights Reserved 
 *		
 *****************************************************************************/

#ifndef _GLOBAL_H
#define _GLOBAL_H

extern int g_terminated;

extern int g_silent;

extern int g_udp;

extern int g_lcd_type;

#endif /* _GLOBAL_H */
