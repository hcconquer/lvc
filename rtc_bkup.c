#include "plc.h"
#include "common.h"
#include "threads.h"
#include "global.h"
#include "mt_access.h"
#include "t_mt.h"
#include "t_stat.h"

static int rtc_error(void)
{
	struct tm tm;

	my_time(&tm);
	if ((tm.tm_year % 100) >= 70) 
		return 1;
	return 0;
}

static void set_datetime(time_t tt)
{
	struct timeval tv;

	tv.tv_sec = tt;
	tv.tv_usec = 0;
	settimeofday(&tv, NULL);
	set_rtc();
}

static time_t buf2time_t(BYTE *buf)
{
	struct tm tm;
	int year;
	
	my_time(&tm);
	tm.tm_sec = bcd_to_bin(buf[0]);
	tm.tm_min = bcd_to_bin(buf[1]);
	tm.tm_hour = bcd_to_bin(buf[2]);
	tm.tm_mday = bcd_to_bin(buf[4]);
	tm.tm_mon = bcd_to_bin(buf[5]) - 1;
	year = bcd_to_bin(buf[6]);
	tm.tm_year = (year > 70) ? year : 100 + year;
	return mktime(&tm);
}

static int read_mt_time(short mtidx, const MTID mtid, time_t *tt)
{
	int len;
	BYTE buf[PLC_APDU_LEN];
	time_t tt1, tt2;
	
	tt1 = uptime();
	len = meter_read(mtidx, mtid, 0, buf, 2, 0xC011, 0xC010); // no retry
	tt2 = uptime();
	if (len == 7) {
		*tt = buf2time_t(buf) + (tt2 - tt1) / 2;
		return 1;
	}
	return 0;
}

static int rtc_restore(void)
{
	int idx;
	time_t tt, last_tt, last_uptime;
	RPT_PATH path;
	MTID mtid;

	last_tt = last_uptime = 0;
	for (idx = 0; idx < T_MT_ROWS_CNT; idx ++) {
		notify_watchdog();
		if (tmt_is_empty(idx))
			continue;
		if (!tstat_get_path(idx, &path) || path.count > 0)
			continue;
		tmt_get_mtid(idx, mtid);
		if (!read_mt_time(idx, mtid, &tt))
			continue;
		if (last_tt == 0) {
			last_tt = tt;
			last_uptime = uptime();
		}
		else {
			if (abs(tt - (last_tt + (uptime() - last_uptime))) < 45) { // seconds
				set_datetime(tt);
				return 1;
			}
			else {
				last_tt = tt;
				last_uptime = uptime();
			}
		}
	}
	return 0;
}

int rtc_validate(void)
{
	int ret = 1;

	if (rtc_error()) {
		if (check_rtc()) {
			read_rtc();
			if (!rtc_error())
				return 1;
		}
		if ((ret = rtc_restore()) != 0)
			ERR_PRINTF("RTC is restored\n");
	}
	return ret;
}
