/*
 *  C-Plan Concentrator
 *
 *  Copyright (C) 2006-2007, Shenzhen Kaifa Technology Co.,Ltd.
 *
 *  FEP thread process function
 *
 *  Author: dajiang wan
 *  Created on: 2006-09-05
 */
#include "common.h"
#include "misc.h"
#include "device.h"
#include "global.h"
#include "msg_proc.h"
#include "f_alm.h"
#include "f_param.h"
#include "msg_que.h"
#include "threads.h"
#include "t_task.h"
#include "display.h"
#include "menu_str.h"

static int fep_fd = -1, interval;
static BYTE chn[5], comm_param[6], channel = 0xff;
static char ip_addr[16];
static WORD ip_port;

static void kill_pppd(const char *pid_file)
{
	int fd, len, pid = -1;
	char apid[20];

	if ((fd = open(pid_file, O_RDONLY)) != -1) {
		len = safe_read(fd, apid, sizeof(apid) - 1);
		close(fd);
		if (len > 0) {
			apid[len] = 0;
			sscanf(apid, "%d", &pid);
			if (pid >= 0) {
				kill(pid, SIGTERM);
				wait_delay(6000);
				kill(pid, SIGKILL);
			}
		}
	}
}

static int get_ip_addr_port(char *ip_addr, WORD *port)
{
	BYTE buf[6];

	fparam_get_value(FPARAM_IP_PORT_PRI, buf, sizeof(buf));
	sprintf(ip_addr, "%d.%d.%d.%d", buf[0], buf[1], buf[2], buf[3]);
	*port = (buf[5] << 8) + buf[4];
	if (!check_ipaddr_port(ip_addr, *port)) {
		fparam_get_value(FPARAM_IP_PORT_SEC, buf, sizeof(buf));
		sprintf(ip_addr, "%d.%d.%d.%d", buf[0], buf[1], buf[2], buf[3]);
		*port = (buf[5] << 8) + buf[4];
	}
	return check_ipaddr_port(ip_addr, *port);
}

static int fep_open(void)
{
	if (channel == 1 || channel == 2 || channel == 3) {
		lcd_update_comm_info(10);
		if (!g_udp)
			fep_fd = open_socket(ip_addr, ip_port, 6 * 1000);
		else
			fep_fd = open_udp(ip_addr, ip_port);
		return fep_fd >= 0;
	}
	return 0;
}

static void fep_close(int kill_flag)
{
	if (fep_fd >= 0) {
		close_socket(fep_fd);
		fep_fd = -1;
	}
	if (kill_flag && (channel == 1 || channel == 2))
		kill_pppd(modem_lockname);
}

static int fep_read_packet(void *buf, int max_len, int *out_len)
{
	int len = 5, ret, timeout = 2 * 60 * 1000;

	*out_len = 0;
	if (g_udp) {
		do {
			ret = read(fep_fd, buf, max_len);
		} while (ret < 0 && errno == EINTR);
		if (ret > 0)
			*out_len += ret;
		return ret;
	}
	else {
		if (max_len < len)
			return 0;
		ret = safe_read_timeout(fep_fd, buf, len, timeout);
		if (ret > 0)
			*out_len += ret;
		if (ret != len) {
			fep_close(0);
			return 0;
		}
		len = ctos(buf + 1); ret = ctos(buf + 3);
		if (len != ret || (len & 0x03) != 0x01) { // D1..D0
			fep_close(0);
			return 0;
		}
		len = ((len >> 2) & 0xffff) + 3; // D15..D2
		if (max_len < len + 5) {
			fep_close(0);
			return 0;
		}
		ret = safe_read_timeout(fep_fd, buf + 5, len, timeout);
		if (ret > 0)
			*out_len += ret;
		if (ret != len) {
			fep_close(0);
			return 0;
		}
		return len + 5;
	}
}

static int fep_write_packet(const BYTE *buf, int len)
{
	int ret, timeout = 2 * 60 * 1000;

	if (g_udp) {
		do {
			ret = write(fep_fd, buf, len);
		} while (ret < 0 && errno == EINTR);
	}
	else {
		ret = safe_write_timeout(fep_fd, buf, len, timeout);
	}
	return ret;
}

static int login, heartbeat, seq_no;

void fep_response(BYTE seq)
{
	if (seq_no == (seq & 0x0f)) {
		if (login == 1) {
			login = 2;
			lcd_update_comm_info(11);
		}
		else if (heartbeat == 1) {
			heartbeat = 0;
		}
	}
}

static void fep_login(void)
{
	BYTE buf[128];
	int len;

	if (fep_fd >= 0) {
		seq_no = (seq_no == 15) ? 0 : seq_no + 1;
		len = packet(buf, sizeof(buf), 0x02, seq_no, 1);
		PRINTB("To FEP[Login]: ", buf, len);
		fep_write_packet(buf, len);
		login = 1;
	}
	else {
		login = heartbeat = 0;
	}
}

static void fep_heartbeat(void)
{
	BYTE buf[128];
	int len;

	if (fep_fd >= 0) {
		seq_no = (seq_no == 15) ? 0 : seq_no + 1;
		//len = packet(buf, sizeof(buf), 0x02, seq_no, 3);
		len = packet(buf, sizeof(buf), 0x02, 0, 3);
		PRINTB("To FEP[Heartbeat]: ", buf, len);
		fep_write_packet(buf, len);
		heartbeat = 1;
	}
}

static void fep_timeout(void)
{
	PRINTF("fep timeout\n");
	if (channel == 1 || channel == 3) {
		if (login == 1 || heartbeat == 1) { // communication fail
			fep_close(1);
			login = heartbeat = 0;
		}
		else {
			fep_heartbeat();
		}
	}
	else if (channel == 2) {
		fep_close(1);
		login = heartbeat = 0;
	}
	if (login != 2)
		lcd_update_comm_info(12);
}

static int fep_recv(void *buf, int max_len)
{
	int len, ret = 0;

	if (fep_fd >= 0) {
		heartbeat = 0;
		if (fep_read_packet(buf, max_len, &len) <= 0
			|| check_packet(buf, len) == 0) {
			PRINTB("From FEP[Bad]: ", buf, len);
		}
		else {
			lcd_update_task_info(c_info_task_exec_str[3]);
			PRINTB("From FEP: ", buf, len);
			if (check_address(buf, len)) {
				msg_que_put(MSG_QUE_MASTER_IN, buf, len, 0);
				msg_proc(MSG_QUE_MASTER_IN, MSG_QUE_MASTER_OUT, max_len);
			}
			wait_delay(500);
			lcd_update_comm_info(-1);
			ret = 1;
		}
	}
	return ret;
}

static int fep_send(void *buf, int max_len)
{
	int len, ret = 0;

	if (fep_fd >= 0) {
		msg_que_get(MSG_QUE_MASTER_OUT, buf, max_len, &len, 0);
		if (len > 0) {
			lcd_update_task_info(c_info_task_exec_str[3]);
			PRINTB("To FEP: ", buf, len);
			fep_write_packet(buf, len);
			msleep(50);
			ret = 1;
			wait_delay(500);
			lcd_update_comm_info(-1);
		}
	}
	return ret;
}

static int get_phone_number(void *phone, const void *buf, int len)
{
	static const BYTE chs[16] = {'0', '1', '2', '3', '4', '5', '6',
		'7', '8', '9', ',', '#', '+', '*', 0, 0};
	const BYTE *in_ptr = buf;
	BYTE *out_ptr = phone;
	BYTE val;

	if (all_is_ff(buf, len))
		return 0;
	while (len > 0) {
		val = *in_ptr ++; len --;
		*out_ptr ++ = chs[val >> 4];
		*out_ptr ++ = chs[val & 0x0f];
	}
	return strlen(phone);
}

static void fep_channel(void)
{
	fparam_lock();
	fparam_get_value(FPARAM_AFN04_F62, chn, 5);
	fparam_get_value(FPARAM_AFN04_F1, comm_param, 6);
	if (get_opt_addr(ip_addr, &ip_port)) {
		channel = 0x03;
		interval = comm_param[5] * 60;
	}
	else {
		get_ip_addr_port(ip_addr, &ip_port);
		if (chn[0] == 0x01) {
			channel = 0x01;
			interval = comm_param[5] * 60;
		}
		else if (chn[0] == 0x02) {
			channel = 0x02;
			interval = chn[4] * 60;
		}
		else {
			channel = 0xff;
			interval = 5 * 60;
		}
	}
	fparam_unlock();
}

static int fep_setup(void)
{
	BYTE val[16], phone[20], sms_center[20], allow, ec[2], *tmp, net_type;

	if (!check_ipaddr_port(ip_addr, ip_port)) {
		lcd_update_comm_info(14);
		return 0;
	}
	if (channel == 0x03) {
		lcd_update_comm_info(13);
		return 1;
	}
	else if (channel == 0x01) {
		PRINTF("Connect FEP by GPRS/CDMA\n");
		if (check_pppd(modem_lockname)) {
			PRINTF("pppd exists, we donot need dialup\n");
			fparam_lock();
			fparam_get_value(FPARAM_LAST_NET_TYPE, &net_type, 1);
			fparam_unlock();
			set_net_icon(net_type, 31);
			lcd_update_head_info();
			return 1;
		}
		if (!modem_check(modem_device, modem_lockname, B115200)) {
			return 0;
		}
		return dialup_gprs(modem_device, modem_lockname, "115200",
			0, ctos(chn + 1));
	}
	else if (channel == 0x02) {
		fparam_lock();
		fparam_get_value(FPARAM_AFN04_F4, val, 16);
		fparam_get_value(FPARAM_ALLOW_SPONT, &allow, 1);
		fparam_unlock();
		PRINTF("Connect FEP by SMS wakeup(GPRS/CDMA)\n");
		if (check_pppd(modem_lockname)) {
			PRINTF("pppd exists, we donot need dialup\n");
			fparam_lock();
			fparam_get_value(FPARAM_LAST_NET_TYPE, &net_type, 1);
			fparam_unlock();
			set_net_icon(net_type, 31);
			lcd_update_head_info();
			return 1;
		}
		if (!modem_check(modem_device, modem_lockname, B115200))
			return 0;
		if (allow && (falm_changed(0x0e, ec) || falm_changed(0x8e, ec))) {
			PRINTF("Need dialup center by GPRS/CDMA for send data\n");
			return dialup_gprs(modem_device, modem_lockname, "115200",
				chn[3], 0);
		}
		if (get_phone_number(phone, val, 8)) {
			if (get_phone_number(sms_center, val + 8, 8))
				tmp = sms_center;
			else
				tmp = NULL;
			if (sms_wakeup(modem_device, modem_lockname, B115200, tmp,
				phone)) {
				PRINTF("Need dialup center by GPRS/CDMA for SMS wakeup\n");
				return dialup_gprs(modem_device, modem_lockname, "115200",
					chn[3], 0);
			}
		}
		return 0;
	}
	else {
		lcd_update_comm_info(14);
		return 0; // fail to setup channel
	}
}

int fep_connected(void)
{
	return (fep_fd >= 0) && (login == 2);
}

static void fep_spont(BYTE *buf, int max_len)
{
	if (fep_fd >= 0) {
		falm_spont(MSG_QUE_MASTER_OUT, buf, max_len);
		task_fill(MSG_QUE_MASTER_OUT, buf, max_len);
	}
}

void *fep_thread(void *arg)
{
	int wait_time = 1000, ret, max_len;
	long idle_time = 0;
	BYTE buf[MAX_LDU_LEN];

	while (!g_terminated) {
		notify_watchdog();
		check_sched();
		check_modem_reboot();
		fep_channel();
		if (fep_fd < 0 && fep_setup() && fep_open()) {
			fep_login();
			idle_time = uptime();
		}
		if (fep_fd < 0) {
			wait_delay(4000);
			continue;
		}
		ret = wait_for_ready(fep_fd, wait_time, 0);
		wait_time = 1000;
		if (ret > 0 && fep_recv(buf, sizeof(buf))) {
			wait_time = 0;
			idle_time = uptime();
		}
		if (fep_connected()) {
			if (g_udp)
				max_len = 1024;
			else
				max_len = sizeof(buf);
			fep_spont(buf, max_len);
			if (fep_send(buf, max_len)) {
				wait_time = 0;
				idle_time = uptime();
			}
		}
		if (uptime() - idle_time > interval) {
			fep_timeout();
			idle_time = uptime();
		}
		if (!g_terminated && get_reboot_flag(1)) {
			msleep(500);
			g_terminated = 1; // we must reply first, then reboot
		}
	}
	fep_close(0);
	return NULL;
}
