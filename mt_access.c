/*****************************************************************************
 *	C-Plan Concentrator 
 *
 *	(c) Copyright 2006-2007, Shenzhen Kaifa Technology Co.,Ltd. 
 *	All Rights Reserved 
 *
 *	Functions to Realtime Access Meter
 *
 *	Author: dajiang wan
 *	Created on: 2007-03-01
 *****************************************************************************/

#include "mt_access.h"
#include "mt_day.h"
#include "mt_month.h"
#include "mt_load.h"
#include "mt_sync.h"
#include "mt_month.h"
#include "mt_frzn.h"
#include "mt_route.h"
#include "mt_alarm.h"
#include "common.h"
#include "global.h"
#include "t_mt.h"
#include "t_stat.h"
#include "err_code.h"
#include "msg_code.h"
#include "plc.h"
#include "f_alm.h"
#include "f_param.h"
#include "misc.h"

/* ssmmhhwwMMddyy */
static int check_meter_clock(short mtidx, const BYTE *buf, time_t now)
{
	BYTE year, month, day, week, hour, minute, second;
	struct tm tm;
	time_t tt;

	second = bcd_to_bin(buf[0]);
	minute = bcd_to_bin(buf[1]);
	hour = bcd_to_bin(buf[2]);
	week = bcd_to_bin(buf[3]);
	day = bcd_to_bin(buf[4]);
	month = bcd_to_bin(buf[5]);
	year = bcd_to_bin(buf[6]);
	memset(&tm, 0, sizeof(tm));
	tm.tm_sec = second;
	tm.tm_min = minute;
	tm.tm_hour = hour;
	tm.tm_mday = day;
	tm.tm_mon = month - 1;
	tm.tm_year = year + 100;
	tt = mktime(&tm);
	if (check_time_t(tt, now, 5 * 60))
		return 1;
	PRINTF("Meter %d clock is not valid, %02d-%02d-%02d %02d:%02d:%02d\n",
		mtidx, year, month, day, hour, minute, second);
	return 0;
}

int check_meter_clock_state(short mtidx, const BYTE *buf, time_t tt)
{
	int ret;

	if (check_meter_clock(mtidx, buf, tt)) {
		tstat_set_alarm(mtidx, 1, 0, 0);
		ret = 1;
	}
	else {
		if (tstat_allow_alarm(mtidx, 1))
			falm_add_meter(mtidx, 8, 1, NULL, 0);
		ret = 0;
	}
	tstat_update_statewd(mtidx, buf[7]);
	return ret;
}

/* reading-disabled period, if time is in this period,
 * CON could not read the Month's, Day's energy of meters. */
int meter_read_disable(const struct tm *tm)
{
	BYTE flag, cnt, buf[1 + 4 * NBR_UNREAD_PERIOD], *ptr;
	int i, found;
	struct tm tmp_tm;
	time_t tt, tt0, tt1;
		
	tmp_tm = *tm;
	tt = mktime(&tmp_tm);
	fparam_get_value(FPARAM_AUTO_UNREAD, &flag, 1);
	fparam_get_value(FPARAM_UNREAD_PERIOD, buf, sizeof(buf));
	found = 0; cnt = buf[0]; ptr = buf + 1;
	for (i = 0; i < cnt; i ++) {
		tmp_tm.tm_hour = bcd_to_bin(ptr[3]);
		tmp_tm.tm_min = bcd_to_bin(ptr[2]);
		tt0 = mktime(&tmp_tm);
		tmp_tm.tm_hour = bcd_to_bin(ptr[1]);
		tmp_tm.tm_min = bcd_to_bin(ptr[0]);
		tt1 = mktime(&tmp_tm);
		if (tt0 <= tt && tt <= tt1) {
			found = 1;
			break;
		}
		ptr += 4;
	}
	return flag || found;
}

void brmt_init(ST_BRMT *brmt)
{
	const BYTE phase_arr[] = {PHASE_A, PHASE_B, PHASE_C};
	BYTE phase, rpt_phase[NBR_REAL_MT];
	short i, j, k, rpt;

	bzero(rpt_phase, sizeof(rpt_phase));
	for (i = 0; i < NBR_REAL_MT; i ++) {
		if (!tmt_is_empty(i)) {
			phase = tmt_get_phase(i);
			rpt = tstat_get_repeater(i);
			if (is_valid_phase(phase) && rpt > NO_RPT && rpt < NBR_REAL_MT)
				rpt_phase[rpt] = phase;
		}
	}
	k = 0;
	for (i = 0; i < 3; i ++) {
		phase = phase_arr[i];
		brmt->rpt_arr[k ++] = -phase; // for no repeater meters
		for (j = 0; j < NBR_REAL_MT; j ++) {
			if (rpt_phase[j] == phase)
				brmt->rpt_arr[k ++] = j;
		}
	}
	brmt->rpt_cnt = k;
	brmt->rpt_idx = 0;
}

int brmt_next(ST_BRMT *brmt, short *mtidx, BYTE *phase)
{
	short rpt;

	if (brmt->rpt_idx < brmt->rpt_cnt) {
		rpt = brmt->rpt_arr[brmt->rpt_idx ++];
		if (rpt < 0) { // for direct-reachable meters
			*mtidx = NO_RPT;
			*phase = -rpt; // see brmt_init()
		}
		else {
			*mtidx = rpt;
			*phase = tmt_get_phase(rpt);
		}
		return 1;
	}
	else {
		return 0;
	}
}

void event_change(EVENT event)
{
	sync_event(event);
	frzn_event(event);
	month_event(event);
	day_event(event);
	load_event(event);
	route_event(event);
	alarm_event(event);
}

static int buf_to_path(const BYTE *buf, RPT_PATH *path)
{
	const BYTE *ptr = buf;
	int idx;
	
	for (path->count = 0; path->count < NBR_RPT; path->count ++, ptr += 2) {
		idx = METER_INDEX(ctos(ptr));
		if (idx <= NO_RPT || idx >= NBR_REAL_MT)
			break;
		tmt_get_mtid(idx, path->rpts[path->count]);
	}
	return 1;
}

/* return 0 for set ok, else return error code(key error, unreachable etc.) */
int dlc_comm_set(const BYTE *req, int req_len, short mt_idx, const BYTE *rpt)
{
	int resp_len;
	BYTE resp[TLC_APDU_LEN], phase;
	RPT_PATH path;
	MTID mtid;

	tmt_get_mtid(mt_idx, mtid);
	phase = tmt_get_phase(mt_idx);
	if (!is_valid_phase(phase)) {
		PRINTF("dlc_comm_set: phase error\n");
		return ERR_BAD_PHASE;
	}
	if (rpt == NULL) {
		if (tstat_get_path(mt_idx, &path) == 0)
			return ERR_BAD_VALUE;
	}
	else {
		if (buf_to_path(rpt, &path) == 0)
			return ERR_BAD_VALUE;
	}
	return plc_sub_comm(mt_idx, mtid, phase, &path, get_nbr_retry(), req,
		req_len, resp, sizeof(resp), &resp_len, 1);
}

/* return length of response, else return zero */
int dlc_comm_get(const BYTE *req, int req_len, BYTE *resp, int resp_len,
	BYTE method, short mt_idx, const BYTE *rpt)
{
	BYTE phase;
	RPT_PATH path;
	MTID mtid;

	if (method == 2)
		return 0; // TODO
	tmt_get_mtid(mt_idx, mtid);
	phase = tmt_get_phase(mt_idx);
	if (!is_valid_phase(phase)) {
		PRINTF("dlc_comm_get: mtidx:%d, phase:%d error\n", mt_idx, phase);
		return 0;
	}
	if (method == 0 || rpt == NULL) {
		if (tstat_get_path(mt_idx, &path) == 0)
			return 0;
	}
	else {
		if (buf_to_path(rpt, &path) == 0)
			return 0;
	}

	if (plc_sub_comm(mt_idx, mtid, phase, &path, get_nbr_retry(), req,
		req_len, resp, resp_len, &resp_len, 1) != ERR_OK)
		return 0;
	return resp_len;
}

int meter_read(short mtidx, const MTID mtid, BYTE nbr_retry, void *buf,
	int id_cnt, ...)
{
	va_list args;
	int i, ret, di, req_len, resp_len;
	BYTE req[PLC_APDU_LEN], resp[PLC_APDU_LEN], *out_ptr, *in_ptr;

	va_start(args, id_cnt);
	out_ptr = buf;
	if ((tmt_get_type(mtidx) >> 4) & 0x07) { // support multi-di read or write
		in_ptr = req;
		*in_ptr ++ = MT_READ;
		for (i = 0; i < id_cnt; i ++) {
			di = va_arg(args, int);
			stoc(in_ptr, di);
			in_ptr += 2;
		}
		req_len = in_ptr - req;
		ret = plc_comm(mtidx, mtid, nbr_retry, req, req_len, resp,
			sizeof(resp), &resp_len);
		if (ret == ERR_OK && resp_len > 1) {
			memcpy(out_ptr, resp + 1, resp_len - 1);
			out_ptr += (resp_len - 1);
		}
	}
	else { // normal 485 meter, donot support multi-di write or read
		for (i = 0; i < id_cnt; i ++) {
			in_ptr = req;
			*in_ptr ++ = MT_READ;
			di = va_arg(args, int);
			stoc(in_ptr, di);
			in_ptr += 2;
			req_len = in_ptr - req;
			ret = plc_comm(mtidx, mtid, nbr_retry, req, req_len, resp,
				sizeof(resp), &resp_len);
			if (ret == ERR_OK && resp_len > 1) {
				memcpy(out_ptr, resp + 1, resp_len - 1);
				out_ptr += (resp_len - 1);
			}
			else {
				out_ptr = buf;
				break;
			}
		}
	}
	va_end(args);
	return out_ptr - (BYTE *)buf;
}

int meter_read_array(short mtidx, const MTID mtid, BYTE nbr_retry, void *buf,
	WORD *id_arr, int id_cnt)
{
	int i, ret, req_len, resp_len;
	BYTE req[PLC_APDU_LEN], resp[PLC_APDU_LEN], *out_ptr, *in_ptr;

	out_ptr = buf;
	if ((tmt_get_type(mtidx) >> 4) & 0x07) { // support multi-di read or write
		in_ptr = req;
		*in_ptr ++ = MT_READ;
		for (i = 0; i < id_cnt; i ++) {
			stoc(in_ptr, id_arr[i]);
			in_ptr += 2;
		}
		req_len = in_ptr - req;
		bzero(resp, sizeof(resp));
		ret = plc_comm(mtidx, mtid, nbr_retry, req, req_len, resp,
			sizeof(resp), &resp_len);
		if (ret == ERR_OK && resp_len > 1) {
			memcpy(out_ptr, resp + 1, resp_len - 1);
			out_ptr += (resp_len - 1);
		}
	}
	else { // normal 485 meter, donot support multi-di write or read
		for (i = 0; i < id_cnt; i ++) {
			in_ptr = req;
			*in_ptr ++ = MT_READ;
			stoc(in_ptr, id_arr[i]);
			in_ptr += 2;
			req_len = in_ptr - req;
			bzero(resp, sizeof(resp));
			ret = plc_comm(mtidx, mtid, nbr_retry, req, req_len, resp,
				sizeof(resp), &resp_len);
			if (ret == ERR_OK && resp_len > 1) {
				memcpy(out_ptr, resp + 1, resp_len - 1);
				out_ptr += (resp_len - 1);
			}
			else {
				out_ptr = buf;
				break;
			}
		}
	}
	return out_ptr - (BYTE *)buf;
}

int meter_write(short mtidx, const MTID mtid, BYTE nbr_retry, const void *key,
	int id_cnt, ...)
{
	va_list args;
	int i, ret, di, di_len, req_len, resp_len;
	const BYTE *di_ptr;
	BYTE req[PLC_APDU_LEN], resp[PLC_APDU_LEN], *ptr;

	va_start(args, id_cnt);
	ret = ERR_OK;
	if ((tmt_get_type(mtidx) >> 4) & 0x07) { // support multi-di read or write
		ptr = req;
		*ptr ++ = MT_WRITE;
		memcpy(ptr, key, 4);
		ptr += 4;
		for (i = 0; i < id_cnt; i ++) {
			di = va_arg(args, int);
			di_ptr = va_arg(args, unsigned char *);
			di_len = va_arg(args, int);
			stoc(ptr, di);
			ptr += 2;
			memcpy(ptr, di_ptr, di_len);
			ptr += di_len;
		}
		req_len = ptr - req;
		ret = plc_comm(mtidx, mtid, nbr_retry, req, req_len, resp,
			sizeof(resp), &resp_len);
	}
	else { // normal 485 meter, donot support multi-di write or read
		for (i = 0; i < id_cnt; i ++) {
			ptr = req;
			*ptr ++ = MT_WRITE;
			memcpy(ptr, key, 4);
			ptr += 4;
			di = va_arg(args, int);
			di_ptr = va_arg(args, unsigned char *);
			di_len = va_arg(args, int);
			stoc(ptr, di);
			ptr += 2;
			memcpy(ptr, di_ptr, di_len);
			ptr += di_len;
			req_len = ptr - req;
			ret = plc_comm(mtidx, mtid, nbr_retry, req, req_len, resp,
				sizeof(resp), &resp_len);
			if (ret != ERR_OK)
				break;
		}
	}
	va_end(args);
	return ret;
}

int read_frozen_energy(short mtidx, const MTID mtid, BYTE nbr_retry, short di,
	BYTE *resp, int *resp_len)
{
	time_t tt;

	/* to read time/date, stateword, energy */
	*resp_len = meter_read(mtidx, mtid, nbr_retry, resp, 4, 0xC011, 0xC010,
		0xC020, di);
	if (*resp_len >= 12) {
		tt = time(NULL);
		tstat_lock();
		fparam_lock();
		falm_lock();
		check_meter_clock_state(mtidx, resp, tt);
		falm_unlock();
		fparam_unlock();
		tstat_unlock();
		return 1;
	}
	else {
		*resp_len = 0;
		return 0;
	}
}
