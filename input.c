/*
 * C-Plan Concentrator
 *
 * Copyright (C) 2008, Shenzhen Kaifa Technology Co.,Ltd.
 *
 * Author: jianping zhang
 * Created on: 2008-01-18
 */
#include "input.h"
#include "menu_str.h"
#include "global.h"
#include "common.h"
#include "threads.h"
#include "display.h"
#include "device.h"

#define KEYBOARD_START_ROW	2
#define KEYBOARD_START_COL	3
#define KEYBOARD_HEIGHT		7
#define KEYBOARD_WIDTH		16

#define SWITCH_LOWER_LETTER	128
#define SWITCH_UPPER_LETTER	129
#define SWITCH_NUMERIC		130
#define SWITCH_SYMBOL		131

static const char c_numeric_key[] = {
	27, '\b', '\r',
	'1', '2', '3',
	'4', '5', '6',
	'7', '8', '9',
	'*', '0', '#',
	',', '.', SWITCH_LOWER_LETTER,
};

static const KEY_ATTR c_num_keyboard_attr[] = {
	{ 0, 15,  3,  2,  1, KEYBOARD_START_ROW + 1, KEYBOARD_START_COL + 1,  4},
	{ 1, 16,  4,  0,  2, KEYBOARD_START_ROW + 1, KEYBOARD_START_COL + 6,  4},
	{ 2, 17,  5,  1,  0, KEYBOARD_START_ROW + 1, KEYBOARD_START_COL + 11, 4},
	{ 3,  0,  6,  5,  4, KEYBOARD_START_ROW + 2, KEYBOARD_START_COL + 2,  1},
	{ 4,  1,  7,  3,  5, KEYBOARD_START_ROW + 2, KEYBOARD_START_COL + 7,  1},
	{ 5,  2,  8,  4,  3, KEYBOARD_START_ROW + 2, KEYBOARD_START_COL + 12, 1},
	{ 6,  3,  9,  8,  7, KEYBOARD_START_ROW + 3, KEYBOARD_START_COL + 2,  1},
	{ 7,  4, 10,  6,  8, KEYBOARD_START_ROW + 3, KEYBOARD_START_COL + 7,  1},
	{ 8,  5, 11,  7,  6, KEYBOARD_START_ROW + 3, KEYBOARD_START_COL + 12, 1},
	{ 9,  6, 12, 11, 10, KEYBOARD_START_ROW + 4, KEYBOARD_START_COL + 2,  1},
	{10,  7, 13,  9, 11, KEYBOARD_START_ROW + 4, KEYBOARD_START_COL + 7,  1},
	{11,  8, 14, 10,  9, KEYBOARD_START_ROW + 4, KEYBOARD_START_COL + 12, 1},
	{12,  9, 15, 14, 13, KEYBOARD_START_ROW + 5, KEYBOARD_START_COL + 2,  1},
	{13, 10, 16, 12, 14, KEYBOARD_START_ROW + 5, KEYBOARD_START_COL + 7,  1},
	{14, 11, 17, 13, 12, KEYBOARD_START_ROW + 5, KEYBOARD_START_COL + 12, 1},
	{15, 12,  0, 17, 16, KEYBOARD_START_ROW + 6, KEYBOARD_START_COL + 2,  1},
	{16, 13,  1, 15, 17, KEYBOARD_START_ROW + 6, KEYBOARD_START_COL + 7,  1},
	{17, 14,  2, 16, 15, KEYBOARD_START_ROW + 6, KEYBOARD_START_COL + 11, 4},
};

static const char c_lower_letter_key[] = {
	27, '\b', '\r',
	'a', 'b', 'c', 'd', 'e', 'f', 'g',
	'h', 'i', 'j', 'k', 'l', 'm', 'n',
	'o', 'p', 'q', 'r', 's', 't', 'u',
	'v', 'w', 'x', 'y', 'z', ' ',
	SWITCH_UPPER_LETTER, SWITCH_SYMBOL, SWITCH_NUMERIC,
};

static const char c_upper_letter_key[] = {
	27, '\b', '\r',
	'A', 'B', 'C', 'D', 'E', 'F', 'G',
	'H', 'I', 'J', 'K', 'L', 'M', 'N',
	'O', 'P', 'Q', 'R', 'S', 'T', 'U',
	'V', 'W', 'X', 'Y', 'Z', ' ',
	SWITCH_LOWER_LETTER, SWITCH_SYMBOL, SWITCH_NUMERIC,
};

static const KEY_ATTR c_letter_keyboard_attr[] = {
	{ 0, 30,  4,  2,  1, KEYBOARD_START_ROW + 1, KEYBOARD_START_COL + 1,  4},
	{ 1, 31,  6,  0,  2, KEYBOARD_START_ROW + 1, KEYBOARD_START_COL + 6,  4},
	{ 2, 32,  9,  1,  0, KEYBOARD_START_ROW + 1, KEYBOARD_START_COL + 11, 4},
	{ 3,  0, 10,  9,  4, KEYBOARD_START_ROW + 2, KEYBOARD_START_COL + 1,  1},
	{ 4,  0, 11,  3,  5, KEYBOARD_START_ROW + 2, KEYBOARD_START_COL + 3,  1},
	{ 5,  1, 12,  4,  6, KEYBOARD_START_ROW + 2, KEYBOARD_START_COL + 5,  1},
	{ 6,  1, 13,  5,  7, KEYBOARD_START_ROW + 2, KEYBOARD_START_COL + 7,  1},
	{ 7,  1, 14,  6,  8, KEYBOARD_START_ROW + 2, KEYBOARD_START_COL + 9,  1},
	{ 8,  2, 15,  7,  9, KEYBOARD_START_ROW + 2, KEYBOARD_START_COL + 11, 1},
	{ 9,  2, 16,  8,  3, KEYBOARD_START_ROW + 2, KEYBOARD_START_COL + 13, 1},
	{10,  3, 17, 16, 11, KEYBOARD_START_ROW + 3, KEYBOARD_START_COL + 1,  1},
	{11,  4, 18, 10, 12, KEYBOARD_START_ROW + 3, KEYBOARD_START_COL + 3,  1},
	{12,  5, 19, 11, 13, KEYBOARD_START_ROW + 3, KEYBOARD_START_COL + 5,  1},
	{13,  6, 20, 12, 14, KEYBOARD_START_ROW + 3, KEYBOARD_START_COL + 7,  1},
	{14,  7, 21, 13, 15, KEYBOARD_START_ROW + 3, KEYBOARD_START_COL + 9,  1},
	{15,  8, 22, 14, 16, KEYBOARD_START_ROW + 3, KEYBOARD_START_COL + 11, 1},
	{16,  9, 23, 15, 10, KEYBOARD_START_ROW + 3, KEYBOARD_START_COL + 13, 1},
	{17, 10, 24, 23, 18, KEYBOARD_START_ROW + 4, KEYBOARD_START_COL + 1,  1},
	{18, 11, 25, 17, 19, KEYBOARD_START_ROW + 4, KEYBOARD_START_COL + 3,  1},
	{19, 12, 26, 18, 20, KEYBOARD_START_ROW + 4, KEYBOARD_START_COL + 5,  1},
	{20, 13, 27, 19, 21, KEYBOARD_START_ROW + 4, KEYBOARD_START_COL + 7,  1},
	{21, 14, 28, 20, 22, KEYBOARD_START_ROW + 4, KEYBOARD_START_COL + 9,  1},
	{22, 15, 29, 21, 23, KEYBOARD_START_ROW + 4, KEYBOARD_START_COL + 11, 1},
	{23, 16, 29, 22, 17, KEYBOARD_START_ROW + 4, KEYBOARD_START_COL + 13, 1},
	{24, 17, 30, 29, 25, KEYBOARD_START_ROW + 5, KEYBOARD_START_COL + 1,  1},
	{25, 18, 30, 24, 26, KEYBOARD_START_ROW + 5, KEYBOARD_START_COL + 3,  1},
	{26, 19, 31, 25, 27, KEYBOARD_START_ROW + 5, KEYBOARD_START_COL + 5,  1},
	{27, 20, 31, 26, 28, KEYBOARD_START_ROW + 5, KEYBOARD_START_COL + 7,  1},
	{28, 21, 31, 27, 29, KEYBOARD_START_ROW + 5, KEYBOARD_START_COL + 9,  1},
	{29, 23, 32, 28, 24, KEYBOARD_START_ROW + 5, KEYBOARD_START_COL + 11, 4},
	{30, 25,  0, 32, 31, KEYBOARD_START_ROW + 6, KEYBOARD_START_COL + 1,  4},
	{31, 27,  1, 30, 32, KEYBOARD_START_ROW + 6, KEYBOARD_START_COL + 6,  4},
	{32, 29,  2, 31, 30, KEYBOARD_START_ROW + 6, KEYBOARD_START_COL + 11, 4},
};

static char idx_to_key(keyboard_t type, int idx)
{
	switch (type) {
		case _num_keyboard:
			if (idx < 0 || idx >= sizeof(c_numeric_key))
				return 0;
			return c_numeric_key[idx];
		case _lower_letter_keyboard:
			if (idx < 0 || idx >= sizeof(c_lower_letter_key))
				return 0;
			return c_lower_letter_key[idx];
		case _upper_letter_keyboard:
			if (idx < 0 || idx >= sizeof(c_upper_letter_key))
				return 0;
			return c_upper_letter_key[idx];
		case _symbol_keyboard:
			return 0;
		default:
			return 0;
	}
}

static int get_next_keyidx(keyboard_t type, direction_t dir, int idx)
{
	switch (type) {
		case _num_keyboard:
			switch (dir) {
				case _dir_up:
					return c_num_keyboard_attr[idx].up_idx;
				case _dir_down:
					return c_num_keyboard_attr[idx].down_idx;
				case _dir_left:
					return c_num_keyboard_attr[idx].left_idx;
				default:
					return c_num_keyboard_attr[idx].right_idx;
			}
		case _lower_letter_keyboard:
		case _upper_letter_keyboard:
			switch (dir) {
				case _dir_up:
					return c_letter_keyboard_attr[idx].up_idx;
				case _dir_down:
					return c_letter_keyboard_attr[idx].down_idx;
				case _dir_left:
					return c_letter_keyboard_attr[idx].left_idx;
				default:
					return c_letter_keyboard_attr[idx].right_idx;
			}
		case _symbol_keyboard:
			return -1;
		default:
			return -1;
	}
}

static void keyboard_reverse(keyboard_t type, int *idx)
{
	int row, col1, col2;
	switch (type) {
		case _num_keyboard:
			if (*idx > sizeof(c_numeric_key) - 1)
				*idx = sizeof(c_numeric_key) - 1;
			row = c_num_keyboard_attr[*idx].row;
			col1 = c_num_keyboard_attr[*idx].col;
			col2 = col1 + c_num_keyboard_attr[*idx].str_len - 1;
			lcd_reverse_string(row, col1, col2);
			break;
		case _lower_letter_keyboard:
		case _upper_letter_keyboard:
			if (*idx > sizeof(c_lower_letter_key) - 1)
				*idx = sizeof(c_lower_letter_key) - 1;
			row = c_letter_keyboard_attr[*idx].row;
			col1 = c_letter_keyboard_attr[*idx].col;
			col2 = col1 + c_letter_keyboard_attr[*idx].str_len - 1;
			lcd_reverse_string(row, col1, col2);
			break;
		case _symbol_keyboard:
			break;
		default:
			break;
	}
}

static void show_virtual_keyboard(keyboard_t type)
{
	int row, col, len;
	char space[MAX_SCREEN_COL];

	memset(space, ' ', sizeof(space));
	row = KEYBOARD_START_ROW;
	col = KEYBOARD_START_COL;
	len = KEYBOARD_WIDTH;
	switch (type) {
		case _num_keyboard:
			lcd_show_string(row + 1, col, len, c_numeric_keyboard_str[0]);
			lcd_show_string(row + 2, col, len, c_numeric_keyboard_str[1]);
			lcd_show_string(row + 3, col, len, c_numeric_keyboard_str[2]);
			lcd_show_string(row + 4, col, len, c_numeric_keyboard_str[3]);
			lcd_show_string(row + 5, col, len, c_numeric_keyboard_str[4]);
			lcd_show_string(row + 6, col, len, c_numeric_keyboard_str[5]);
			break;
		case _lower_letter_keyboard:
			lcd_show_string(row + 1, col, len, c_lower_letter_keyboard_str[0]);
			lcd_show_string(row + 2, col, len, c_lower_letter_keyboard_str[1]);
			lcd_show_string(row + 3, col, len, c_lower_letter_keyboard_str[2]);
			lcd_show_string(row + 4, col, len, c_lower_letter_keyboard_str[3]);
			lcd_show_string(row + 5, col, len, c_lower_letter_keyboard_str[4]);
			lcd_show_string(row + 6, col, len, c_lower_letter_keyboard_str[5]);
			break;
		case _upper_letter_keyboard:
			lcd_show_string(row + 1, col, len, c_upper_letter_keyboard_str[0]);
			lcd_show_string(row + 2, col, len, c_upper_letter_keyboard_str[1]);
			lcd_show_string(row + 3, col, len, c_upper_letter_keyboard_str[2]);
			lcd_show_string(row + 4, col, len, c_upper_letter_keyboard_str[3]);
			lcd_show_string(row + 5, col, len, c_upper_letter_keyboard_str[4]);
			lcd_show_string(row + 6, col, len, c_upper_letter_keyboard_str[5]);
			break;
		case _symbol_keyboard:
			break;
		default:
			break;
	}
}

BYTE key_getch(void)
{
	unsigned char ch;

	ch = lcd_key_in();
	switch(ch) {
	case 0x12:
		return KEY_LEFT;
	case 0x13:
		return KEY_UP;
	case 0x22:
		return KEY_RIGHT;
	case 0x23:
		return KEY_DOWN;
	case 0x32:
		return KEY_ESC;
	case 0x33:
		return KEY_ENTER;
	default:
		return KEY_NONE;
	}
}

BYTE getch_timeout(void)
{
	static int keywait_time = 60;
	long idle_time = 0;
	BYTE key = KEY_NONE;

	idle_time = uptime();
	while (!g_terminated) {
		notify_watchdog();
		key = key_getch();
		if (key != KEY_NONE)
			return key;
		else if (uptime() - idle_time >= keywait_time)
			return KEY_NONE;
		msleep(50);
	}
	return KEY_NONE;
}

/* get input value from virtual keyboard, all result is saved as ascii */
int get_input(keyboard_t type, int maxlen, char *buf, BYTE ispasswd)
{
	int ret = -1, cur_idx = 1, len = 0;
	int cursor_row = KEYBOARD_START_ROW, cursor_col = KEYBOARD_START_COL + 1;
	char keybuf[MAX_INPUT_STRING_LEN], virtual_key;
	BYTE key, refresh = 1, refresh_reverse = 1, exit = 0;
	void *save_ptr;
	int save_len;
	
	memset(keybuf, 0, sizeof(keybuf));
	lcd_save_window(KEYBOARD_START_ROW, KEYBOARD_START_COL,
		KEYBOARD_START_ROW + KEYBOARD_HEIGHT - 1,
		KEYBOARD_START_COL + KEYBOARD_WIDTH - 1, &save_ptr, &save_len);
	len = min(maxlen, strlen(buf));
	memcpy(keybuf, buf, len);
	lcd_show_string(cursor_row, cursor_col, len, keybuf);
	cursor_col += len;
	lcd_show_cursor(cursor_row, cursor_col, 0);
	while (1) {
		if (refresh == 1) {
			show_virtual_keyboard(type);
			keyboard_reverse(type, &cur_idx);
			refresh = 0; refresh_reverse = 0;
		}
		else if (refresh_reverse == 1) {
			refresh_reverse = 0;
			keyboard_reverse(type, &cur_idx);
		}
		key = getch_timeout();
		switch (key) {
		case KEY_UP:
			keyboard_reverse(type, &cur_idx);
			cur_idx = get_next_keyidx(type, _dir_up, cur_idx);
			refresh_reverse = 1;
			break;
		case KEY_DOWN:
			keyboard_reverse(type, &cur_idx);
			cur_idx = get_next_keyidx(type, _dir_down, cur_idx);
			refresh_reverse = 1;
			break;
		case KEY_LEFT:
			keyboard_reverse(type, &cur_idx);
			cur_idx = get_next_keyidx(type, _dir_left, cur_idx);
			refresh_reverse = 1;
			break;
		case KEY_RIGHT:
			keyboard_reverse(type, &cur_idx);
			cur_idx = get_next_keyidx(type, _dir_right, cur_idx);
			refresh_reverse = 1;
			break;
		case KEY_ENTER:
			virtual_key = idx_to_key(type, cur_idx);
			switch (virtual_key) {
			case '\r':
				memcpy(buf, keybuf, len);
				ret = len; exit = 1;
				break;
			case '\b':
				if (len > 0 && cursor_col > KEYBOARD_START_COL + 1) {
					keybuf[len - 1] = 0;
					len --;
					lcd_show_string(cursor_row, cursor_col - 1, 1, " ");
					lcd_show_cursor(cursor_row, cursor_col, 0);
					cursor_col --;
					lcd_show_cursor(cursor_row, cursor_col, 0);
				}
				break;
			case 27:
				ret = -1; exit = 1;
				break;
			case SWITCH_NUMERIC:
				type = _num_keyboard;
				refresh = 1;
				break;
			case SWITCH_LOWER_LETTER:
				type = _lower_letter_keyboard;
				refresh = 1;
				break;
			case SWITCH_UPPER_LETTER:
				type = _upper_letter_keyboard;
				refresh = 1;
				break;
			case SWITCH_SYMBOL:
				type = _num_keyboard;
				refresh = 1;
				break;
			default:
				if (len < maxlen) {
					if (!ispasswd) {
						lcd_show_string(cursor_row, cursor_col, 1,
							&virtual_key);
					}
					else {
						lcd_show_string(cursor_row, cursor_col, 1, "*");
					}
					keybuf[len] = virtual_key;
					len ++;
					cursor_col ++;
					lcd_show_cursor(cursor_row, cursor_col, 0);
				}
				break;
			} /* end of switch virtual_key */
			break;
		case KEY_ESC:
			ret = -1; exit = 1;
			break;
		case KEY_NONE:
			lcd_mode_set(0);
			ret = -1; exit = 1;
			break;
		default:
			break;
		}
		if (exit) {
			lcd_restore_window(KEYBOARD_START_ROW, KEYBOARD_START_COL,
				KEYBOARD_START_ROW + KEYBOARD_HEIGHT - 1,
				KEYBOARD_START_COL + KEYBOARD_WIDTH - 1, save_ptr, save_len);
			return ret;
		}
	}
}
