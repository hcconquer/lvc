#include "common.h"
#include "device.h"
#include "plc.h"
#include "mt_access.h"
#include "t_mt.h"

static BYTE req[PLC_APDU_LEN], resp[PLC_APDU_LEN];
static int req_len, resp_len;
static MTID mtid = {0x01, 0x03, 0x00, 0x18, 0x10, 0x02};

static BYTE pass[4] = {0x01, 0x01, 0x01, 0x01};
static int phase = PHASE_A;

static void fill_clock(BYTE *clock, int current)
{
	struct tm tm;

	my_time(&tm);
	if (current) {
		clock[0] = bin_to_bcd(tm.tm_sec);
		clock[1] = bin_to_bcd(tm.tm_min);
		clock[2] = bin_to_bcd(tm.tm_hour);
	}
	else {
		clock[0] = 0x05;
		clock[1] = 0x04;
		clock[2] = 0x03;
	}
	if (tm.tm_wday == 0)
		clock[3] = bin_to_bcd(7);
	else
		clock[3] = bin_to_bcd(tm.tm_wday);
	clock[4] = bin_to_bcd(tm.tm_mday);
	clock[5] = bin_to_bcd(tm.tm_mon + 1);
	clock[6] = bin_to_bcd(tm.tm_year % 100);
}

static void test_one(MTID mtid)
{
	RPT_PATH path;
	int broad, timeout;
	
	if (memcmp(mtid, BR_MTID, MTID_LEN) == 0)
		broad = 1;
	else
		broad = 0;
	path.count = 0;
	plc_lock();
	plc_send(0, !broad, mtid, phase, &path, req, req_len, PLC_APDU_LEN, &timeout);
	if (broad)
		msleep(timeout);
	else
		plc_recv(mtid, timeout, resp, PLC_APDU_LEN, &resp_len);
	plc_unlock();
	msleep(1000);
}

static void test_two(int cnt)
{
	RPT_PATH path;
	int timeout;
	int i;
	
	if (cnt <= 0 || cnt > 7)
		cnt = 7;
	path.count = cnt;
	for (i = 0; i < cnt; i ++) {
		memcpy(path.rpts[i], mtid, MTID_LEN);
		path.rpts[i][0] = i + 2;
	}
	mtid[0] = 0x01;
	plc_lock();
	plc_send(0, TRUE, mtid, phase, &path, req, req_len, PLC_APDU_LEN, &timeout);
	plc_recv(mtid, timeout, resp, PLC_APDU_LEN, &resp_len);
	plc_unlock();
	msleep(1000);
}

static void test(int type)
{
	if (type == 0)
		test_one((BYTE *)BR_MTID);
	else
		test_one(mtid);
}

static void test_write(const void *arg1, int len1, const void *arg2, int len2)
{
	req_len = my_pack(req, "cvv", 0x04, len1, arg1, len2, arg2);
	test(1);
}

static void test_read(const void *arg, int len)
{
	req_len = my_pack(req, "cv", 0x01, len, arg);
	test(1);
}

static void test_pass(const void *arg1, int len1, const void *arg2, int len2)
{
	req_len = my_pack(req, "cvv", 0x0f, len1, arg1, len2, arg2);
	test(1);
	memcpy(pass, arg2, 4);
}

static void test_write_clock(int flag)
{
	BYTE clock[7];
	fill_clock(clock, flag);
	req_len = my_pack(req, "cvsvsv", 0x04, 4, pass, 0xc011, 3, clock,
		0xc010, 4, clock + 3);
	test(1);
}

static void test_broad_clock(void)
{
	BYTE clock[7];
	fill_clock(clock, 1);
	req_len = my_pack(req, "cvv", 0x08, 3, clock, 3, clock + 4);
	test(0);
}

static void test_clean(void)
{
	req_len = my_pack(req, "cv", 0x09, 4, pass);
	test(1);
}

static void test_broad_frozen_energy(void)
{
	req_len = my_pack(req, "c", 0x12);
	test(0);
}

static void test_break_set(void)
{
	req_len = my_pack(req, "cvs", 0x04, 4, pass, 0xc03a);
	test(1);
}

static void test_break_clear(void)
{
	req_len = my_pack(req, "cvs", 0x04, 4, pass, 0xc03b);
	test(1);
}

static void test_break_off(void)
{
	req_len = my_pack(req, "cvs", 0x04, 4, pass, 0xc03c);
	test(1);
}

static void test_break_on(void)
{
	req_len = my_pack(req, "cvs", 0x04, 4, pass, 0xc03d);
	test(1);
}

static void test_broad_frozen_hour(const void *arg, int len)
{
	req_len = my_pack(req, "cv", 0x12, len, arg);
	test(0);
}

static void test_repeater(int cnt)
{
	req_len = my_pack(req, "cs", 0x01, 0x9010);
	test_two(cnt);
}

static void hexstr_to_str(void *dst, int dst_len, const void *src)
{
	int i, high, low, val;
	BYTE *ptr1 = dst;
	const BYTE *ptr2 = src;

	for (i = 0; i < dst_len; i ++) {
		high = *ptr2 ++;
		low = *ptr2 ++;
		if (high >= 'a' && high <= 'f')
			high = high - 'a' + 10;
		else if (high >= 'A' && high <= 'F')
			high = high - 'A' + 10;
		else
			high = high - '0';
		if (low >= 'a' && low <= 'f')
			low = low - 'a' + 10;
		else if (low >= 'A' && low <= 'F')
			low = low - 'A' + 10;
		else
			low = low - '0';
		val = high * 16 + low;
		*ptr1 ++ = val;
	}
}

static void split(char *line, int *cmd, char *arg1, int *len1,
	char *arg2, int *len2)
{
	char tmp1[1024], tmp2[1024];
	sscanf(line, "%d %s %s", cmd, tmp1, tmp2);
	*len1 = strlen(tmp1) / 2;
	*len2 = strlen(tmp2) / 2;
	hexstr_to_str(arg1, *len1, tmp1);
	hexstr_to_str(arg2, *len2, tmp2);
}

static void print_menu(void)
{
	printf("\n\n\t\tChangsha Meter Test\n");
	printf("\tAll args are hexstring format\n");
	printf("\t 0. Exit\n");
	printf("\t 1. Write fix clock with fix (no arg)\n");
	printf("\t 2. Write current clock (no arg)\n");
	printf("\t 3. Broadcast current clock (no arg)\n");
	printf("\t 4. Broadcast frozen energy (no arg)\n");
	printf("\t 5. Clean meter data (no arg)\n");
	printf("\t 6, Breaker alarm set (no arg)\n");
	printf("\t 7, Breaker alarm clear (no arg)\n");
	printf("\t 8, Breaker on (no arg)\n");
	printf("\t 9, Breaker off (no arg)\n");
	printf("\t10. Read data (args: DIs)\n");
	printf("\t11. Write data (args: pass DI-val)\n");
	printf("\t12. Broadcast frozen hour (args: hour)\n");
	printf("\t13. Change password (args: old_pass new_pass)\n");
	printf("\t14, Repeater test (args: count)\n");
	printf("\t15, Auto test (no arg)\n");
	printf("\t99, Set meter address and phase (args: hex_num phase)\n");
	printf("\tYour choice: ");
}

static int check_energy(const BYTE *buf, int miss, int *val)
{
	const BYTE *ptr = buf;
	BYTE ch, high, low, str[4];
	int i, val1, val2;

	for (i = 0; i < 4; i ++) {
		ch = *ptr ++;
		high = (ch >> 4) & 0x0f;
		low = ch & 0x0f;
		if (high >= 10 || low >= 10)
			return 0;
		str[i] = (high * 10 + low);
	}
	val1 = str[0] + str[1] * 100 + str[2] * 100 * 100
		+ str[3] * 100 * 100 * 100;
	for (i = 0; i < 4; i ++) {
		ch = *ptr ++;
		high = (ch >> 4) & 0x0f;
		low = ch & 0x0f;
		if (high >= 10 || low >= 10)
			return 0;
		str[i] = (high * 10 + low);
	}
	val2 = str[0] + str[1] * 100 + str[2] * 100 * 100
		+ str[3] * 100 * 100 * 100;
	if (val1 <= val2 && (val2 - val1) <= miss) {
		*val = val2;
		return 1;
	}
	else {
		return 0;
	}
}

static void meter_auto_test(void)
{
	short mtidx, di;
	int resp_len, miss, val;
	BYTE resp[PLC_APDU_LEN];
	MTID mtid;

	di = 0x9010; miss = 1; mtidx = 0;
	while (wait_for_ready(0, 0, 0) <= 0) {
		tmt_get_mtid(mtidx, mtid);
		if ((resp_len = meter_read(mtidx, mtid, 1, resp, 2, di, di)) == 8) {
			if (!check_energy(resp, miss, &val)) {
				ERR_PRINTB("mt_read_1", mtidx, resp, 8);
				exit(1);
			}
		}
		if (mtidx == 0)
			mtidx = 1;
		else
			mtidx = 0;
	}
}

void mt_test(void)
{
	int cmd, len1, len2, cnt;
	char line[1024], arg1[512], arg2[512];

	while (1) {
		print_menu();
		fgets(line, sizeof(line), stdin);
		split(line, &cmd, arg1, &len1, arg2, &len2);
		switch (cmd) {
		case 1:
			test_write_clock(0);
			break;
		case 2:
			test_write_clock(1);
			break;
		case 3:
			test_broad_clock();
			break;
		case 4:
			test_broad_frozen_energy();
			break;
		case 5:
			test_clean();
			break;
		case 6:
			test_break_set();
			break;
		case 7:
			test_break_clear();
			break;
		case 8:
			test_break_on();
			break;
		case 9:
			test_break_off();
			break;
		case 10:
			test_read(arg1, len1);
			break;
		case 11:
			test_write(arg1, len1, arg2, len2);
			break;
		case 12:
			test_broad_frozen_hour(arg1, len1);
			break;
		case 13:
			test_pass(arg1, len1, arg2, len2);
			break;
		case 14:
			test_repeater(arg1[0]);
			break;
		case 15:
			meter_auto_test();
			break;			
		case 99:
			for (cnt = 0; cnt < len1; cnt ++)
				mtid[cnt] = arg1[cnt];
			phase = arg2[0];
			break;
		default:
			return;
		}
	}
}
