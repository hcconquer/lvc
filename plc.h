/*****************************************************************************
 *	C-Plan Concentrator 
 *
 *	(c) Copyright 2006-2007, Shenzhen Kaifa Technology Co.,Ltd. 
 *	All Rights Reserved 
 *		
 *****************************************************************************/

#ifndef _PLC_H
#define _PLC_H

#include "typedef.h"

#define COMM_OK			0
#define COMM_ERR		1
#define COMM_RPT_FAIL	2

#define UPD_NUN			0x01
#define UPD_RT_INFO		0x02
#define UPD_ALL			0xff

void cb_protocol_detect(void);

int plc_send(int baud_idx, BOOL need_resp, const MTID mtid, int phase,
	const RPT_PATH *rpts, const void *buf, int len, int recv_max_len,
	int *timeout);

int plc_recv(const MTID mtid, int timeout, void *buf, int max_len, int *len);

int cb_send(const void *req, int len);

int cb_recv(void *buf, int max_len, int *len);

int plc_sub_comm(short mtidx, const MTID mtid, BYTE phase,
	const RPT_PATH *path, BYTE nbr_retry, const void *req, int req_len,
	void *resp, int resp_max_len, int *resp_len, BITS update_log);

int plc_comm(short mtidx, const MTID mtid, BYTE nbr_retry, const void *req,
	int req_len, void *resp, int resp_max_len, int *resp_len);

void broadcast_clock(short mtidx, BYTE nbr_retry, BYTE phase);

void broadcast_frzn_hour(short mtidx, BYTE nbr_retry, BYTE phase, BYTE hour);

int cb_comm(const void *req, int req_len, void *resp, int max_len,
	int *resp_len);

#endif /* _PLC_H */
