/*****************************************************************************
 *	C-Plan Concentrator 
 *
 *	(c) Copyright 2006-2007, Shenzhen Kaifa Technology Co.,Ltd. 
 *	All Rights Reserved 
 *		
 *****************************************************************************/
#ifndef _F_MT_H
#define _F_MT_H

#include "typedef.h"
#include "t_mt.h"

#define NBR_SAMPLES		24	/* number of load samples in a day */ 
#define NBR_LOAD_DATA	31
#define NBR_DAY_DATA	31
#define NBR_MONTH_DATA	13

typedef struct {
	BYTE flag;		/* 0 for not used, >= 1 for used */
	BYTE time[3];	/* year, month, day */
	MTID mtid;
	BYTE samples[NBR_SAMPLES * ENERGY_LEN];
} FMT_LOAD;

typedef struct {
	BYTE flag;		/* 0 for not used, >= 1 for used */
	BYTE time[3];	/* year, month, day */
	MTID mtid;
	BYTE statewd;
	BYTE energy_cnt;				/* count of energy */
	BYTE energy[5 * ENERGY_LEN];	/* total, T1, T2, T3, T4 */
} FMT_DAY;

typedef struct {
	BYTE flag;		/* 0 for not used, >= 1 for used */
	BYTE time[2];	/* year, month */
	MTID mtid;
	BYTE statewd;
	BYTE energy_cnt;				/* count of energy */
	BYTE energy[5 * ENERGY_LEN];	/* total, T1, T2, T3, T4 */
} FMT_MON;

#define FMT_LOAD_CNT	(NBR_FOCUS * NBR_LOAD_DATA)
#define FMT_DAY_CNT		(T_MT_ROWS_CNT * NBR_DAY_DATA)
#define FMT_MON_CNT		(T_MT_ROWS_CNT * NBR_MONTH_DATA)

typedef struct {
	FMT_LOAD load[FMT_LOAD_CNT];
	FMT_DAY day[FMT_DAY_CNT];
	FMT_MON mon[FMT_MON_CNT];
} F_MT;

void fmt_lock(void);

void fmt_unlock(void);

void fmt_open(void);

int fmt_read_load(const MTID mtid, BYTE year, BYTE mon, BYTE day, void *buf);

int fmt_read_day(const MTID mtid, BYTE year, BYTE mon, BYTE day, void *buf);

int fmt_read_mon(const MTID mtid, BYTE year, BYTE mon, void *buf);

void fmt_write_load(const MTID mtid, BYTE year, BYTE mon, BYTE day,
	const void *buf);

void fmt_write_day(const MTID mtid, BYTE year, BYTE mon, BYTE day,
	const void *buf, int len);

void fmt_write_mon(const MTID mtid, BYTE year, BYTE mon, const void *buf,
	int len);

void fmt_flush(void);

void fmt_close(void);

int fday_any_phase_failure(void);

void list_month_data(void);

void list_day_data(void);

void list_load_data(void);

int fmt_get_read_nbr(void);

int fmt_exist_mon(const MTID mtid, BYTE year, BYTE mon);

int fmt_exist_day(const MTID mtid, BYTE year, BYTE mon, BYTE day);

#endif /* _F_MT_H */
