/*****************************************************************************
 *	C-Plan Concentrator 
 *
 *	(c) Copyright 2006-2007, Shenzhen Kaifa Technology Co.,Ltd. 
 *	All Rights Reserved 
 *		
 *****************************************************************************/

#ifndef _T_STAT_H
#define _T_STAT_H

#include "typedef.h"

#define T_STAT_ROWS_CNT		NBR_REAL_MT

/* tightly aligned with TBL_MT row by row */
typedef struct {
	BYTE statewd;			/* state word of MT */

	BYTE alarm_flag[3];		/* byte 0 for ALARM_MT_UNREACH,
							   byte 1 for ALARM_MT_UNSYNC,
							   byte 2 for ALARM_MT_BADBATT */
	time_t alarm_time[3];	/* the alarm generated time */

	BYTE  nbr_nr;		/* contineous unreachable times for day-data reading */
	time_t time_nr;		/* time of the last unreachable day-data reading */ 
	time_t time_reach; 	/* time of the last successful day-data reading */

	short repeater; 	/* repeater meter seqno of the last repeater */

	BYTE data[19];
	BYTE unused[128 - 19];
} T_STAT;

void tstat_lock(void);

void tstat_unlock(void);

void tstat_init_row(int rowno);

void tstat_open(void);

void tstat_update(short mtidx, int flush_flag);

void tstat_flush(void);

void tstat_close(void);

void tstat_update_statewd(int mtidx, BYTE statewd);

int tstat_get_nbr_nr(short mtidx);

void tstat_set_nbr_nr(short mtidx, int nbr_nr);

time_t tstat_get_time_nr(short mtidx);

void tstat_set_time_nr(short mtidx, time_t tt);

time_t tstat_get_time_reach(short mtidx);

void tstat_set_time_reach(short mtidx, time_t tt);

int  tstat_get_repeater(short mtidx);

void tstat_set_repeater(short mtidx, short rpt_idx);

int tstat_set_route(short mtidx, BYTE depth, const BYTE *buf);

int tstat_get_route(short mtidx, BYTE *buf, int max_len);

int tstat_get_level(short mtidx);

int tstat_get_path_idx(short mtidx, RPT_PATH_IDX *path_idx);

int tstat_get_path(short mtidx, RPT_PATH *path);

int tstat_get_data(short mitdx, BYTE *data, int max_len);

void tstat_set_data(short mitdx, const BYTE *data, int len);

void tstat_set_alarm(int mtidx, int alarm, BYTE alarm_flag, time_t alarm_time);

int tstat_allow_alarm(int mtidx, int alarm);

void list_paths(void);

#endif /* _T_STAT_H */
