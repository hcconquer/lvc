#include "dwl_con.h"
#include "typedef.h"
#include "err_code.h"
#include "common.h"
#include "misc.h"
#include "md5.h"
#include "global.h"
#include "msg_proc.h"
#include "threads.h"
#include "t_db.h"
#include "f_param.h"
#include "f_alm.h"

static int pack_size = 512;
static int pack_cnt, upgrade;
static char ftp_cmd[1024];

/*
 * 开始新的下载过程
 * */
static void dwl_init(void)
{
	int dwl_fd;

	dwl_fd = open(DWL_NAME, O_CREAT | O_RDWR | O_TRUNC, 0600);
	close(dwl_fd);
}

/*
 * 写入文件块
 *
 * args:
 * pack_no	文件块号
 * buf		数据
 * len		数据长度
 * */
static void dwl_write(int pack_no, const void *buf, int len)
{
	int dwl_fd;

	dwl_fd = open(DWL_NAME, O_RDWR, 0600);
	if (dwl_fd >= 0)
	{
		lseek(dwl_fd, pack_no * pack_size, SEEK_SET);
		safe_write(dwl_fd, buf, len);
		close(dwl_fd);
	}
}

static int dwl_check_file(void)
{
	MD5_CTX md5;
	unsigned char hash[16], check_hash[16], buf[1024];
	int size, tmpsize, tmp, ret = 0;
	BYTE key[16] =
			"\xFE\xB0\xFE\xB1\xFE\xAB\x32\x00\x21\x13\xEB\xCC\xAF\xFE\x43\x90";
	BYTE time_stamp[8], zero;
	struct tm tm;
	time_t tt1, tt2;
	int dwl_fd;

	dwl_fd = open(DWL_NAME, O_RDWR, 0600);
	if (dwl_fd >= 0)
	{
		PRINTF("Try to compute digest\n");
		size = lseek(dwl_fd, 0, SEEK_END);
		if (size > 16)
		{
			bzero(check_hash, 16);
			lseek(dwl_fd, -16, SEEK_END);/*文件尾-16*/
			safe_read(dwl_fd, check_hash, 16);
			lseek(dwl_fd, 0, SEEK_SET);
			tmpsize = size - 16;
			memcpy(time_stamp, check_hash, 8);
			tm.tm_isdst = -1;
			tm.tm_year = (time_stamp[0] * 256 + time_stamp[1]) % 100 + 100;
			tm.tm_mon = time_stamp[2] - 1;
			tm.tm_mday = time_stamp[3];
			tm.tm_hour = time_stamp[4];
			tm.tm_min = time_stamp[5];
			tm.tm_sec = time_stamp[6];
			zero = time_stamp[7];
			tt1 = mktime(&tm);
			tt2 = time(NULL);
			MD5Init(&md5);
			while (tmpsize > 0)
			{
				if (tmpsize > sizeof(buf))
					tmp = sizeof(buf);
				else
					tmp = tmpsize;
				tmp = read(dwl_fd, buf, tmp);
				if (tmp < 0 && errno == EINTR)
					continue;
				if (tmp <= 0)
					break;
				MD5Update(&md5, buf, tmp);
				tmpsize -= tmp;
			}
			MD5Update(&md5, time_stamp, 8);
			MD5Update(&md5, key, sizeof(key));
			MD5Final(hash, &md5);
			if (tt2 <= tt1 && memcmp(hash + 8, check_hash + 8, 8) == 0)
			{
				PRINTF("File is correct!\n");
				ret = size - 16;
			}
			else
			{
				PRINTB("Digest in file:", check_hash, 16);
				PRINTB("Digest compute:", hash, 16);
			}
		}
		close(dwl_fd);
	}
	return ret;
}

/*
 *	检查报文数据单元的MD5
 *	return	0	MD5错误
 *			1	MD5验正确
 * */
int dwl_check_packet(const void *buf, int *len)
{
	MD5_CTX md5;
	unsigned char hash[16];

	if (*len > 8)
	{
		MD5Init(&md5);
		MD5Update(&md5, (unsigned char *) buf, *len - 8);/*MD5不包含自身*/
		MD5Final(hash, &md5);
		PRINTB("Half Hash:", hash, 8);
		PRINTB("Buf:", buf + (*len - 8), 8);
		if (memcmp(hash, buf + (*len - 8), 8) == 0)
		{
			*len -= 8;
			return 1;
		}
	}
	return 0;
}

/*
 * 接收包
 *
 * args:
 * cnt		count,文件计数
 * seq		sequence,序列
 * buf		数据
 * len		本段文件数据长度
 *
 * return
 * 			0	初始化下载
 * 			1	写入文件
 * */
int dwl_packet(int cnt, int seq, const void *buf, int len)
{
	int ret = 0;

	PRINTF("Receive seq:%d, cnt:%d, pack_cnt:%d\n", seq, cnt, pack_cnt);
	if (cnt != pack_cnt || seq == 0)
	{
		dwl_init();
		pack_size = len;
	}
	if (len <= pack_size)
	{
		dwl_write(seq, buf, len);
		pack_cnt = cnt;
		ret = 1;
	}
	return ret;
}

int dwl_finish(void)
{
	if (dwl_check_file())
	{
		upgrade = 1;
		set_reboot_flag(1);
		return 1;
	}
	else
	{
		return 0;
	}
}

void ftp_download_file(const char *user, const char *pass, const char *host,
		int port, const char *file)
{
	if (user == NULL || pass == NULL || *user == 0x0 || *pass == 0x0)
	{
		snprintf(ftp_cmd, sizeof(ftp_cmd) - 1, "ftpget -P %d %s %s %s", port,
				host, DWL_NAME, file);
	}
	else
	{
		snprintf(ftp_cmd, sizeof(ftp_cmd) - 1,
				"ftpget -P %d -u %s -p %s %s %s %s", port, user, pass, host,
				DWL_NAME, file);
	}
	upgrade = 2;
	set_reboot_flag(1);
}

void dwl_check(void)
{
	static const char *tmp_file = "cscon.tmp";
	char prg_name[PATH_MAX], new_ver[5], old_ver[5], alm_buf[9];
	int real_size;

	if (upgrade == 2)
	{
		printf("Try to execute:%s\n", ftp_cmd);
		system(ftp_cmd);
	}
	if ((real_size = dwl_check_file()) > 0)
	{
		PRINTF("Try to upgrade software\n");
		truncate(DWL_NAME, real_size);
		if (my_gunzip(DWL_NAME, tmp_file))
		{
			get_prog_name(prg_name, sizeof(prg_name));
			find_software_version(tmp_file, new_ver);
			find_software_version(prg_name, old_ver);
			if (new_ver[0] != 0 && old_ver[0] != 0)
			{
				unlink(prg_name);
				rename(tmp_file, prg_name);
				chmod(prg_name, 0755);
				unlink(tmp_file);
				alm_buf[0] = 2;
				memcpy(alm_buf + 1, old_ver, 4);
				memcpy(alm_buf + 5, old_ver, 4);
				fparam_lock();
				falm_lock();
				falm_add_con(0x0e, 1, alm_buf, 9);
				falm_unlock();
				fparam_unlock();
				PRINTF("Upgrade complete, reboot system\n");
			}
		}
	}
	unlink(DWL_NAME);
}

static const char *up_file = "cscon.up";
static int up_pack_len, up_pack_cnt, up_size;

int upload_name(void *buf, int len, short pack_len, const void *name)
{
	MD5_CTX md5;
	unsigned char hash[16], tmpbuf[1024];
	int tmpsize, tmp, up_fd, ret = 0;

	my_gzip(name, up_file);
	up_fd = open(up_file, O_RDONLY);
	if (up_fd >= 0)
	{
		up_size = lseek(up_fd, 0, SEEK_END);
		tmpsize = up_size;
		lseek(up_fd, 0, SEEK_SET);
		MD5Init(&md5);
		while (tmpsize > 0)
		{
			if (tmpsize > sizeof(tmpbuf))
				tmp = sizeof(tmpbuf);
			else
				tmp = tmpsize;
			tmp = read(up_fd, tmpbuf, tmp);
			if (tmp < 0 && errno == EINTR)
				continue;
			if (tmp <= 0)
				break;
			MD5Update(&md5, tmpbuf, tmp);
			tmpsize -= tmp;
		}
		MD5Final(hash, &md5);
		PRINTB("MD5 of file cscon.up:", hash, 16);
		up_pack_len = pack_len;
		if (up_size % up_pack_len)
			up_pack_cnt = (up_size / up_pack_len) + 1;
		else
			up_pack_cnt = (up_size / up_pack_len);
		if (len >= 6 + 8)
		{
			stoc(buf, 0);
			stoc(buf + 2, up_pack_len);
			stoc(buf + 4, up_pack_cnt);
			memcpy(buf + 6, hash, 8);
			ret = 6 + 8;
		}
		close(up_fd);
	}
	return ret;
}

int upload_file(void *buf, int len, short pack_no, short pack_cnt)
{
	BYTE *ptr = buf;
	int data_len, offset, up_fd;
	MD5_CTX md5;
	unsigned char hash[16];

	if (len <= 4 || pack_no > pack_cnt || pack_cnt != up_pack_cnt)
		return 0;
	stoc(ptr, pack_no);
	ptr += 2;
	len -= 2;
	stoc(ptr, pack_cnt);
	ptr += 2;
	len -= 2;
	offset = (pack_no - 1) * up_pack_len;
	if (pack_no == pack_cnt)
	{ // the last packet
		data_len = up_size - offset;
	}
	else
	{ // not the last packet
		data_len = up_pack_len;
	}
	if (len < data_len + 2 + 8)
		return 0;
	stoc(ptr, data_len);
	ptr += 2;
	len -= 2;
	if ((up_fd = open(up_file, O_RDONLY)) >= 0)
	{
		lseek(up_fd, offset, SEEK_SET);
		safe_read(up_fd, ptr, data_len);
		MD5Init(&md5);
		MD5Update(&md5, (unsigned char *) ptr, data_len);
		MD5Final(hash, &md5);
		ptr += data_len;
		len -= data_len;
		memcpy(ptr, hash, 8);
		ptr += 8;
		len -= 8;
		PRINTF("upload_file, pack_no:%d, pack_cnt:%d\n", pack_no, pack_cnt);
		PRINTB("upload_file, MD5 of data: ", hash, 16);
		close(up_fd);
		return ptr - (BYTE *) buf;
	}
	return 0;
}
