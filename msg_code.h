/*****************************************************************************
 *	C-Plan Concentrator 
 *
 *	(c) Copyright 2006-2007, Shenzhen Kaifa Technology Co.,Ltd. 
 *	All Rights Reserved 
 *		
 *****************************************************************************/

#ifndef _MSG_CODE_H
#define _MSG_CODE_H

/* TLC message codes: */
#define CN_READ 		0x01	/* read data */
#define CN_RT_WRITE		0x07	/* realtime write CON parameters */
#define CN_WRITE 		0x08	/* write CON parameters */
#define CN_SMS_WAKEUP 	0x0F	/* SMS wakeup */
#define CN_MT_READ 		0x11	/* realtime read meter */
#define CN_READ_DAILY	0x12	/* read month, day data */
#define CN_READ_LOAD	0x13	/* read load of focus user */
#define CN_CMD_RELAY 	0x14	/* command meter's relay */
#define CN_READ_MTINF 	0x15	/* read MT info in CON */
#define CN_CASCADE 		0x18	/* access SL CON */
#define CN_ALM 			0x19	/* alarm send,  */
#define CN_ALM_CONFIRM	0x1A	/* alarm confirmation */
#define CN_LOGIN 		0x21	/* login */
#define CN_LOGOUT 		0x22	/* logout */
#define CN_HEART 		0x24	/* heart beat test */
#define CN_CASCADE_CTR	0x28	/* cascade control command */

/* EXTEND BY KAIFA: */
#define CN_MT_WRITE 	0x31	/* write meter DIs */
#define CN_MT_SYNC		0x32	/* set the meter clock */
#define CN_SET_PSW		0x33	/* set the password of meters */
#define CN_READ_ROW		0x34	/* read a row of the inter table */
#define CN_WRITE_ROW	0x35	/* write a row of the inter table */
#define CN_COMMBOARD	0x3E	/* access the CommBoard parameters */

/* PLC message codes: */
#define MT_READ 		0x01	/* read MT data */
#define MT_WRITE 		0x04	/* Write MT DI */
#define MT_CLK_SYNC		0x08	/* sync MT clock */
#define MT_SET_PSW  	0x0F	/* change password */
#define MT_FROZEN_HOUR	0x12	/* broadcast day frozen hour */

/* CommBoard Message Code */
#define CB_READ 		0x00
#define CB_WRITE 		0x01
#define CB_ACK			0xff

#endif /* _MSG_CODE_H */
