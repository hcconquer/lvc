/*****************************************************************************
 *	C-Plan Concentrator 
 *
 *	(c) Copyright 2006-2007, Shenzhen Kaifa Technology Co.,Ltd. 
 *	All Rights Reserved 
 *		
 *****************************************************************************/

#ifndef _MISC_H
#define _MISC_H

#include "typedef.h"

/* PLC PROCEDURES ID */
#define PROC_MT_ACCESS		1
#define PROC_MT_RELAY		2
#define PROC_MT_SYNC		3
#define PROC_MT_MONTH		4
#define PROC_MT_DAY			5
#define PROC_MT_LOAD		6
#define PROC_MT_ALM			7
#define PROC_MT_ROUTE		8
#define PROC_MT_FRZN		9

/* PLC PROCEDURES MASK BIT */
#define PROC_MASK_ACCESS		0x0001
#define PROC_MASK_RELAY			0x0002
#define PROC_MASK_SYNC			0x0004
#define PROC_MASK_MONTH			0x0008
#define PROC_MASK_DAY			0x0010
#define PROC_MASK_LOAD			0x0020
#define PROC_MASK_ALARM			0x0040
#define PROC_MASK_ROUTE			0x0080
#define PROC_MASK_FRZN			0x0100

void set_prog_name(const char *name);

void get_prog_name(char *name, int len);

int get_opt_addr(void *ip_addr, WORD *ip_port);

void set_opt_addr(const void *ip_addr, WORD ip_port);

BOOL is_valid_phase(BYTE phase);

int next_phase(int phase);

void set_last_proc(BYTE procid);

void set_last_mtidx(short mtidx);

BYTE get_last_proc(void);

short get_last_mtidx(void);

WORD get_con_statewd(void);

void set_con_statewd(WORD val);

void init_con_statewd(void);

void software_version_str(void *ver);

void find_software_version(const void *name, void *ver);

void get_software_version(void *buf, int len);

void get_hardware_version(void *buf, int len);

void get_protocol_version(void *buf, int len);

BYTE get_nbr_retry(void);

void init_proc(void);

void event_60_sec(void);

int proc_is_enable(WORD flag);

int energy_convert(const BYTE *buf, int *val);

int bcd_ctos(const BYTE *buf, int *val);

int bcd_ctol(const BYTE *buf, int *val);

void bcd_stoc(BYTE *buf, int val);

void bcd_ltoc(BYTE *buf, int val);

int compare_month(int year, int mon);

int compare_today(int year, int mon, int day);

void previous_day(BYTE *year, BYTE *month, BYTE *day);

void next_day(BYTE *year, BYTE *month, BYTE *day);

void previous_month(BYTE *year, BYTE *month);

void next_month(BYTE *year, BYTE *month);

void previous_year(BYTE *year);

void next_year(BYTE *year);

int is_time_around_zero(const struct tm *tm);

int after_ddhh(BYTE dd, BYTE hh, const struct tm *tm);

int check_ipaddr_port(const char *ip_addr, WORD ip_port);

void meter_change(void);

void change_gprs_password(const void *user, int user_len, const void *pass,
	int pass_len);

int check_pppd(const char *lock_name);

int get_mtdi_len(BYTE tariff_cnt, WORD di);

void plc_set_idle(int flag);

int plc_is_idle(void);

int get_date_time(BYTE *buf, int max_len);

int set_date_time(const BYTE *buf, int max_len);

int dialup_gprs(const char *device_name, const char *lock_name,
	const char *baudstr, int try_cnt, int interval);

int sms_wakeup(const void *device, const void *lock_name, int baudrate,
	const void *center, const void *phone);

void tt_to_fmt_15(time_t tt, BYTE *buf);

void tt_to_fmt_17(time_t tt, BYTE *buf);

void system_status(int on);

void system_reboot(void);

int check_time_t(time_t tt1, time_t tt2, int range);

#endif /* _MISC_H */
