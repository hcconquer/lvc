/*****************************************************************************
*	C-Plan Concentrator 
*
*	(c) Copyright 2006-2007, Shenzhen Kaifa Technology Co.,Ltd. 
*	All Rights Reserved 
*
*	to manage the meter status information, which should be saved
*   in the disk.
*
*	Author: fanhua zeng
*	Created on: 2006-07-31
*****************************************************************************/
#include "common.h"
#include "t_mt.h"
#include "t_stat.h"
#include "t_rt.h"
#include "f_alm.h"
#include "err_code.h"

static T_STAT t_stat[T_STAT_ROWS_CNT];
static T_STAT *p_t_stat = t_stat;
static sem_t sem_t_stat;
static int fd_t_stat;

void tstat_lock(void)
{
	sem_wait(&sem_t_stat);
}

void tstat_unlock(void)
{
	sem_post(&sem_t_stat);
}

int tstat_get_nbr_nr(short mtidx)
{
	return (p_t_stat + mtidx)->nbr_nr;
}

void tstat_set_nbr_nr(short mtidx, int nbr_nr)
{
	(p_t_stat + mtidx)->nbr_nr = nbr_nr;
}

time_t tstat_get_time_nr(short mtidx)
{
	return (p_t_stat + mtidx)->time_nr;
}

void tstat_set_time_nr(short mtidx, time_t tt)
{
	(p_t_stat + mtidx)->time_nr = tt;
}

time_t tstat_get_time_reach(short mtidx)
{
	return (p_t_stat + mtidx)->time_reach;
}

void tstat_set_time_reach(short mtidx, time_t tt)
{
	(p_t_stat + mtidx)->time_reach = tt;
}

void tstat_set_alarm(int mtidx, int alarm, BYTE alarm_flag, time_t alarm_time)
{
	(p_t_stat + mtidx)->alarm_flag[alarm] = alarm_flag;
	(p_t_stat + mtidx)->alarm_time[alarm] = alarm_time;
}

int tstat_get_repeater(short mtidx)
{
	return (p_t_stat + mtidx)->repeater;
}

// TODO: to write the file immediately.
void tstat_set_repeater(short mtidx, short rpt_idx)
{
	if ((p_t_stat + mtidx)->repeater != rpt_idx) {
		(p_t_stat + mtidx)->repeater = rpt_idx;
		trt_add_bkup_rpt(mtidx, rpt_idx);
		tstat_update(mtidx, 1);
		trt_update(mtidx, 1);
	}
}

int tstat_get_data(short mtidx, BYTE *data, int max_len)
{
	if (mtidx < 0 || mtidx >= T_MT_ROWS_CNT || max_len < 19)
		return 0;
	memcpy(data, p_t_stat[mtidx].data, 19);
	return 19;
}

void tstat_set_data(short mtidx, const BYTE *data, int len)
{
	if (mtidx >= 0 && mtidx < T_MT_ROWS_CNT)
		memcpy(p_t_stat[mtidx].data, data, min(19, len));
}

int tstat_get_level(short mtidx)
{
	BYTE level = 0;
	int tmp = mtidx;

	do {
		tmp = tstat_get_repeater(tmp);
		if (tmp < NO_RPT)
			return -1;
		if (tmp >= 0)
			level ++;
		if (level > NBR_RPT)
			return -1;
	} while(tmp != NO_RPT);

	return level;
}

int tstat_get_path_idx(short mtidx, RPT_PATH_IDX *path_idx)
{
	short src, i;

	path_idx->count = 0;
	src = mtidx;
	while (1) {
		src = tstat_get_repeater(src);
		if (src <= NO_RPT || path_idx->count >= NBR_RPT)
			break;
		path_idx->rpts[path_idx->count++] = src;
	}
	if (src == NO_RPT) {
		for (i = 0; i < path_idx->count / 2; i++) {
			src = path_idx->rpts[i];
			path_idx->rpts[i] = path_idx->rpts[path_idx->count - i -1];
			path_idx->rpts[path_idx->count - i - 1] = src;
		}
		return 1;
	}
	else {
		path_idx->count = 0;
		return 0;
	}
}

int tstat_get_path(short mtidx, RPT_PATH *path)
{
	int i;
	RPT_PATH_IDX path_idx;

	if (tstat_get_path_idx(mtidx, &path_idx)) {
		path->count = path_idx.count;
		for (i = 0; i < path_idx.count; i++)
			tmt_get_mtid(path_idx.rpts[i], path->rpts[i]);
		return 1;
	}
	return 0;
}

/* master station set the router table */
int tstat_set_route(short mtidx, BYTE depth, const BYTE *buf)
{
	short rpt;
	if (depth > NBR_RPT || mtidx >= T_MT_ROWS_CNT)
		return 0;
	if (depth == 0) {
		tstat_set_repeater(mtidx, NO_RPT);
	}
	else {
		rpt = ctos(buf + 2 * (depth - 1));
		tstat_set_repeater(mtidx, METER_INDEX(rpt));
		PRINTF("tstat_set_route, mtidx:%d, rpt:%d\n", mtidx, METER_INDEX(rpt));
	}
	return 2 * depth;
}

/* master station get the router table, buf: depth, rpts ... */
int tstat_get_route(short mtidx, BYTE *buf, int max_len)
{
	short i, rpt;
	RPT_PATH_IDX path_idx;
	BYTE *ptr = buf, *count;
	
	if (mtidx >= T_MT_ROWS_CNT || max_len < 3)
		return 0;
	stoc(ptr, METER_ORDER(mtidx)); ptr += 2; max_len -= 2;
	count = ptr; ptr ++; max_len --; *count = 0;
	if (tstat_get_path_idx(mtidx, &path_idx) && path_idx.count >= 1) {
		if (max_len < 2 * path_idx.count)
			return 0;
		*count = path_idx.count;
		for (i = 0; i < path_idx.count; i++) {
			rpt = path_idx.rpts[i];
			stoc(ptr, METER_ORDER(rpt));
			ptr += 2;
		}
		PRINTF("tstat_get_route, mtidx:%d, rpt:%d\n",
			mtidx, path_idx.rpts[path_idx.count - 1]);
	}
	return ptr - buf;
}

int tstat_allow_alarm(int mtidx, int alarm)
{
	int ret;
	T_STAT *p_stat = p_t_stat + mtidx;
	time_t tt, interval = 60 * 60 * 24;

	if (alarm >= 0 && alarm <= 2) {
		tt = time(NULL);
		if (p_stat->alarm_flag[alarm] == 0
			|| tt - p_stat->alarm_time[alarm] > interval) {
			p_stat->alarm_time[alarm] = tt;
			p_stat->alarm_flag[alarm] = 1;
			tstat_update(mtidx, 0);
			ret = 1;
		}
		else {
			ret = 0;
		}
	}
	else {
		ret = 1;
	}
	return ret;
}

void tstat_init_row(int rowno)
{
	T_STAT *p_stat = p_t_stat + rowno - 1;

	bzero(p_stat, sizeof(T_STAT));
	p_stat->repeater = NO_RPT;
}

void tstat_update_statewd(int mtidx, BYTE statewd)
{
	BYTE alm_buf[1];

	if (p_t_stat[mtidx].statewd != statewd) {
		p_t_stat[mtidx].statewd = statewd;
		tstat_update(mtidx, 0);
		if (statewd & (1 << 2)) { // battery lose voltage
			alm_buf[0] = 1 << 4;
			if (tstat_allow_alarm(mtidx, 2))
				falm_add_meter(mtidx, 9, 1, alm_buf, 1);
		}
		else {
			tstat_set_alarm(mtidx, 2, 0, 0);
		}
	}
}

void list_paths(void)
{
	int idx, i, ret;
	MTID mtid;
	RPT_PATH_IDX path_idx;
	short rpt_idx;
	double csr;

	printf("\n************************ PATHS ***************************\n");
	printf("mtseq\tmtid\t\tphase\tcsr(rpt_seq)\tcurrent path(r1->r2->...\n");
	printf("**********************************************************\n");
	for (idx = 0; idx < NBR_REAL_MT; idx++) {
		if (!tmt_is_empty(idx)) {
			ret = tstat_get_path_idx(idx, &path_idx);
			tmt_get_mtid(idx, mtid);
			printf("%d\t%02x%02x%02x%02x%02x%02x\t%d\t", METER_ORDER(idx)
				, mtid[5], mtid[4], mtid[3], mtid[2], mtid[1], mtid[0]
				, tmt_get_phase(idx));
			if (trt_get_rpt_csr(idx, &rpt_idx, &csr))
				printf("%6.2f(%4d)\t", csr, METER_ORDER(rpt_idx));
			else 
				printf("\t\t\t\t");
			if (ret) {
				if (path_idx.count == 0)
					printf("0");
				else {
					for (i = 0; i < path_idx.count; i++) {
						printf("%d\t", METER_ORDER(path_idx.rpts[i]));
					}
					printf("\n\t\t\t\t\t\t");
					for (i = 0; i < path_idx.count; i++) {
						tmt_get_mtid(path_idx.rpts[i], mtid);
						printf("%02x%02x%02x\t"
							, mtid[2], mtid[1], mtid[0]);
					}
				}
			}
			else {
				printf("-1");
			}
			printf("\n");
		}
	}
	printf("**********************************************************\n");
}

void tstat_open(void)
{
	int mtidx, size;
	const char *name = T_MT_STAT_NAME;
	size = sizeof(T_STAT) * T_STAT_ROWS_CNT;
	sem_init(&sem_t_stat, 0, 1);
	if (!check_file(name, size)) {
		/* File doesnot exist or file length is invalid. */
		PRINTF("File %s is created, size:%d\n", name, size);
		fd_t_stat = open(name, O_CREAT | O_RDWR | O_TRUNC, 0600);
		for (mtidx = 0; mtidx < T_STAT_ROWS_CNT; mtidx ++)
			tstat_init_row(mtidx + 1);
		safe_write(fd_t_stat, p_t_stat, size);
		fdatasync(fd_t_stat);
		close(fd_t_stat);
	}
	fd_t_stat = open(name, O_RDWR);
	safe_read(fd_t_stat, p_t_stat, size);
}

void tstat_update(short mtidx, int flush_flag)
{
	lseek(fd_t_stat, mtidx * sizeof(T_STAT), SEEK_SET);
	safe_write(fd_t_stat, p_t_stat + mtidx, sizeof(T_STAT));
	if (flush_flag) tstat_flush();
}

void tstat_flush(void)
{
	fdatasync(fd_t_stat);
}

void tstat_close(void)
{
	fdatasync(fd_t_stat);
	close(fd_t_stat);
	sem_destroy(&sem_t_stat);
}
