#ifndef _T_TASK_H
#define _T_TASK_H

#include "typedef.h"

#define TASK_COUNT				64
#define TASK_DI_COUNT			128
#define TASK_METER_COUNT		255

typedef struct {
	time_t sent; // the last sent time
	BYTE flag;	 // 0x55 start, 0xAA stop, others invalid
	BYTE cycle;
	BYTE clock[6];
	BYTE multi;
	BYTE di_cnt;
	BYTE di_buf[TASK_DI_COUNT * 4];
} TASK_485;

typedef struct {
	time_t sent; // the last sent time
	BYTE flag;	 // 0x55 start, 0xAA stop, others invalid
	BYTE cycle;
	BYTE clock[6]; // SSMMHHDDMMYY
	BYTE di_cnt;
	BYTE di_buf[TASK_DI_COUNT * 4];
	BYTE mt_cnt;
	BYTE mt_buf[TASK_METER_COUNT * 2];
} TASK_MT;

typedef struct {
	TASK_485 task1[TASK_COUNT];
	TASK_485 task2[TASK_COUNT];
	TASK_MT  task3[TASK_COUNT];
} TASK;

void task_lock(void);

void task_unlock(void);

void task_open(void);

void task_fill(int que_idx, BYTE *buf, int max_len);

void task_flush(void);

void task_close(void);

int task1_set(int task_id, const BYTE *buf, int len);

int task2_set(int task_id, const BYTE *buf, int len);

int task3_set(int task_id, const BYTE *buf, int len);

int task1_get(int task_id, BYTE *buf, int max_len);

int task2_get(int task_id, BYTE *buf, int max_len);

int task3_get(int task_id, BYTE *buf, int max_len);

int task1_set_flag(int task_id, const BYTE *buf, int len);

int task2_set_flag(int task_id, const BYTE *buf, int len);

int task3_set_flag(int task_id, const BYTE *buf, int len);

int task1_get_flag(int task_id, BYTE *buf, int max_len);

int task2_get_flag(int task_id, BYTE *buf, int max_len);

int task3_get_flag(int task_id, BYTE *buf, int max_len);

#endif /* _T_TASK_H */
