#ifndef _DISPLAY_H
#define _DISPLAY_H

#include "typedef.h"
#include "lcd.h"

int lcd_mode_get(void);

void lcd_mode_set(int mode);

void process_menu(MENU *menu, const char *info);

BYTE process_items(ITEMS_MENU *items, const char *info, int align_center);

BYTE process_param_list(PARAM_LIST *param, const char *info);

BYTE process_param_set(PARAM_SET *param, const char *info);

void lcd_update_head_enable(int flag);

void lcd_update_info_enable(int flag);

int update_head_is_enable(void);

void lcd_update_comm_info(int flag);

void lcd_update_task_info(const char *buf);

#endif /* endif _DISPLAY_H */
