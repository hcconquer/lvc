/*****************************************************************************
 *	C-Plan Concentrator 
 *
 *	(c) Copyright 2006-2007, Shenzhen Kaifa Technology Co.,Ltd. 
 *	All Rights Reserved 
 *		
 *****************************************************************************/

#ifndef _MT_ROUTE_H
#define _MT_ROUTE_H

#include "typedef.h"

void route_event(EVENT event);

void route_meter_change(void);

int mt_route(const struct tm *tm);

#endif /* _MT_ROUTE_H */
