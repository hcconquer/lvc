/*
 *  C-Plan Concentrator
 *
 *  Copyright (C) 2006-2007, Shenzhen Kaifa Technology Co.,Ltd.
 *
 *  Author: dajiang wan
 *  Created on: 2008-01-08
 */
#include "common.h"
#include "device.h"

static void transmit(BYTE port, BYTE ctrl, int to1, int to2, const void *req,
	int req_len, void *resp, int max_len, int *resp_len)
{
	*resp_len = 0;
	PRINTB("Send [485]:", req, req_len);
	if (port == 1) {
		cas_lock();
		cas_write(req, req_len, to1);
		*resp_len = cas_read(resp, max_len, to1, to2);
		cas_unlock();
	}
	else if (port == 2) {
		rs485_lock();
		rs485_write(req, req_len, to1);
		*resp_len = rs485_read(resp, max_len, to1, to2);
		rs485_unlock();
	}
	PRINTB("Recv [485]:", resp, *resp_len);
}

int afn10_get_fn(BYTE pn, BYTE fn, BYTE *out_buf, int max_len,
	const BYTE *param, int *param_len)
{
	int tmp;

	PRINTF("AFN=10H, Pn=%d, Fn=%d\n", pn, fn);
	switch (fn) {
	case 1:
		if (*param_len < 5 || *param_len < 5 + param[4] || max_len < 256)
			return 0;
		*param_len = 5 + param[4];
		transmit(param[0], param[1], param[2] * 10, param[3] * 10,
			param + 5, param[4], out_buf + 1, max_len - 1, &tmp);
		out_buf[0] = tmp;
		return tmp + 1;
	default:
		return 0;
	}
}
