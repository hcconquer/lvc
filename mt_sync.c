/*****************************************************************************
*	C-Plan Concentrator 
*
*	(c) Copyright 2006-2007, Shenzhen Kaifa Technology Co.,Ltd. 
*	All Rights Reserved 
*
*	To synchronized all meters's clock by broadcast way.
*
*****************************************************************************/
#include "common.h"
#include "mt_sync.h"
#include "mt_access.h"
#include "mt_485.h"
#include "t_mt.h"
#include "t_stat.h"
#include "msg_code.h"
#include "plc.h"
#include "f_alm.h"
#include "f_param.h"
#include "misc.h"

static ST_BRMT brmt;
static int step, br_hour = -1, br_day = -1;

static int check_month(const struct tm *tm, BYTE start_hour, BYTE interval)
{
	struct tm tmp_tm;
	time_t tt;
	int tmp_tt, day_sec = 24 * 60 * 60, val1, val2;

	if (tm->tm_hour == start_hour) {
		tmp_tm = *tm; tt = mktime(&tmp_tm);
		tmp_tt = tt - (tt % day_sec);
		if (br_day == -1 || tmp_tt < br_day) { // the first time
			br_day = tmp_tt;
			return 1;
		}
		tt = br_day; localtime_r(&tt, &tmp_tm);
		if (tm->tm_mday == tmp_tm.tm_mday) {
			val1 = tm->tm_year - tmp_tm.tm_year;
			val2 = tm->tm_mon - tmp_tm.tm_mon;
			if (interval == 0 || (val1 * 12 + val2) % interval == 0) {
				br_day = tmp_tt;
				return 1;
			}
		}
	}
	return 0;
}

static int check_day(const struct tm *tm, BYTE start_hour, BYTE interval)
{
	struct tm tmp_tm;
	time_t tt;
	int tmp_tt, day_sec = 24 * 60 * 60;

	if (tm->tm_hour == start_hour) { // must be equal to the start hour
		tmp_tm = *tm; tt = mktime(&tmp_tm);
		tmp_tt = tt - (tt % day_sec);
		if (br_day == -1 || tmp_tt < br_day) { // the first time
			br_day = tmp_tt;
			return 1;
		}
		if (interval == 0 || (tmp_tt - br_day) / day_sec == interval) {
			br_day = tmp_tt;
			return 1;
		}
	}
	return 0;
}

static int check_hour(const struct tm *tm, BYTE start_hour, BYTE interval)
{
	int found = 0;

	while (1) {
		if (tm->tm_hour == start_hour) {
			found = 1;
			break;
		}
		if (interval == 0 || start_hour >= 24)
			break;
		start_hour += interval;
	}
	return found;
}

static int sync_cond(const struct tm *tm)
{
	BYTE buf[3], high, low, start_hour;

	fparam_lock();
	fparam_get_value(FPARAM_SYNC_CLOCK_FLAG, buf, sizeof(buf));
	fparam_unlock();
	if (buf[0] == 0) // not allow broadcast time
		return 0;
	start_hour = bcd_to_bin(buf[1]); // start hour
	high = (buf[2] >> 6) & 0x03;
	low = buf[2] & 0x3f;
	if (high == 0x01) // hour periodly
		return check_hour(tm, start_hour, low);
	else if (high == 0x02) // day periodly
		return check_day(tm, start_hour, low);
	else if (high == 0x03) // month periodly
		return check_month(tm, start_hour, low);
	return 0;
}

void sync_event(EVENT event)
{
	if (event == EVENT_DAY_CHANGE) {
		br_hour = -1;
		step = 0;
	}
}

static int check_minute(const struct tm *tm)
{
	return (tm->tm_min >= 5 && tm->tm_min <= 55);
}

int mt_sync(const struct tm *tm)
{
	short mtidx;
	BYTE phase;

	if (tm->tm_hour == br_hour || !check_minute(tm))
		return 0;
	if (step == 0 && sync_cond(tm)) {
		brmt_init(&brmt);
		step = 1;
		mt485_sync();
	}
	if (step == 1) {
		if (brmt_next(&brmt, &mtidx, &phase)) {
			PRINTF("sync: mtidx:%d, phase:%d\n", mtidx, phase);
			broadcast_clock(mtidx, get_nbr_retry(), phase);
			fparam_lock();
			set_last_proc(PROC_MT_SYNC);
			set_last_mtidx(mtidx);
			fparam_unlock();
		}
		else {
			br_hour = tm->tm_hour;
			step = 0;
		}
	}
	return step;
}
