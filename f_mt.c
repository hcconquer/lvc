/*
 *  C-Plan Concentrator
 *
 *  Copyright (C) 2006-2007, Shenzhen Kaifa Technology Co.,Ltd.
 *
 *  Provide function to open/close tables and semaphores. 
 *
 *  Author: dajiang wan
 *  Created on: 2008-01-10
 */
#include "t_mt.h"
#include "t_db.h"
#include "f_mt.h"
#include "misc.h"
#include "common.h"

static sem_t sem_f_mt;
static int fd_f_mt;
static FMT_IDX idx_load[FMT_LOAD_CNT];
static FMT_IDX idx_day[FMT_DAY_CNT];
static FMT_IDX idx_mon[FMT_MON_CNT];

void fmt_lock(void)
{
	sem_wait(&sem_f_mt);
}

void fmt_unlock(void)
{
	sem_post(&sem_f_mt);
}

static void fmt_load_init(void)
{
	int idx;
	FMT_LOAD rec[FMT_LOAD_CNT];

	lseek(fd_f_mt, offsetof(F_MT, load), SEEK_SET);
	safe_read(fd_f_mt, rec, sizeof(rec));
	memset(idx_load, 0, sizeof(idx_load));
	for (idx = 0; idx < FMT_LOAD_CNT; idx ++) {
		if (rec[idx].flag) {
			fmt_idx_init(idx_load, idx, rec[idx].mtid, rec[idx].time,
				sizeof(rec[idx].time));
		}
	}
}

static void fmt_day_init(void)
{
	int idx;
	FMT_DAY rec[FMT_DAY_CNT];

	lseek(fd_f_mt, offsetof(F_MT, day), SEEK_SET);
	safe_read(fd_f_mt, rec, sizeof(rec));
	memset(idx_day, 0, sizeof(idx_day));
	for (idx = 0; idx < FMT_DAY_CNT; idx ++) {
		if (rec[idx].flag) {
			fmt_idx_init(idx_day, idx, rec[idx].mtid, rec[idx].time,
				sizeof(rec[idx].time));
		}
	}
}

static void fmt_mon_init(void)
{
	int idx;
	FMT_MON rec[FMT_MON_CNT];

	lseek(fd_f_mt, offsetof(F_MT, mon), SEEK_SET);
	safe_read(fd_f_mt, rec, sizeof(rec));
	memset(idx_mon, 0, sizeof(idx_mon));
	for (idx = 0; idx < FMT_MON_CNT; idx ++) {
		if (rec[idx].flag) {
			fmt_idx_init(idx_mon, idx, rec[idx].mtid, rec[idx].time,
				sizeof(rec[idx].time));
		}
	}
}

void fmt_write_load(const MTID mtid, BYTE year, BYTE mon, BYTE day,
	const void *buf)
{
	int idx;
	BYTE time[] = {year, mon, day};
	FMT_LOAD rec;

	rec.flag = 1;
	memcpy(rec.time, time, sizeof(rec.time));
	memcpy(rec.mtid, mtid, MTID_LEN);
	memcpy(rec.samples, buf, sizeof(rec.samples));

	idx = fmt_idx_write(idx_load, FMT_LOAD_CNT, mtid, rec.time, sizeof(rec.time));
	lseek(fd_f_mt, offsetof(F_MT, load) + idx * sizeof(rec), SEEK_SET);
	safe_write(fd_f_mt, &rec, sizeof(rec));
	fmt_flush();
	PRINTF("fmt_write_load, idx:%d, size:%d\n", idx, sizeof(rec));
	//PRINTB("samples: ", buf, sizeof(rec.samples));
}

void fmt_write_day(const MTID mtid, BYTE year, BYTE mon, BYTE day,
	const void *buf, int len)
{
	int idx;
	const BYTE *ptr = buf;
	BYTE time[] = {year, mon, day};
	FMT_DAY rec;

	rec.flag = 1;
	memcpy(rec.time, time, sizeof(rec.time));
	memcpy(rec.mtid, mtid, MTID_LEN);
	rec.statewd = *ptr ++;
	rec.energy_cnt = (len - 1) / ENERGY_LEN;
	memcpy(rec.energy, ptr, len - 1);

	idx = fmt_idx_write(idx_day, FMT_DAY_CNT, mtid, rec.time, sizeof(rec.time));
	lseek(fd_f_mt, offsetof(F_MT, day) + idx * sizeof(rec), SEEK_SET);
	safe_write(fd_f_mt, &rec, sizeof(rec));
	fmt_flush();
	PRINTF("fmt_write_day, idx:%d, size:%d\n", idx, sizeof(rec));
}

void fmt_write_mon(const MTID mtid, BYTE year, BYTE mon, const void *buf,
	int len)
{
	int idx;
	const BYTE *ptr = buf;
	BYTE time[] = {year, mon};
	FMT_MON rec;

	rec.flag = 1;
	memcpy(rec.time, time, sizeof(rec.time));
	memcpy(rec.mtid, mtid, MTID_LEN);
	rec.statewd = *ptr ++;
	rec.energy_cnt = (len - 1) / ENERGY_LEN;
	memcpy(rec.energy, ptr, len - 1);

	idx = fmt_idx_write(idx_mon, FMT_MON_CNT, mtid, rec.time, sizeof(rec.time));
	lseek(fd_f_mt, offsetof(F_MT, mon) + idx * sizeof(rec), SEEK_SET);
	safe_write(fd_f_mt, &rec, sizeof(rec));
	fmt_flush();
	PRINTF("fmt_write_mon, idx:%d, size:%d\n", idx, sizeof(rec));
}

int fmt_read_load(const MTID mtid, BYTE year, BYTE mon, BYTE day, void *buf)
{
	int idx;
	BYTE *ptr = buf;
	BYTE time[] = {year, mon, day};
	FMT_LOAD rec;

	idx = fmt_idx_read(idx_load, FMT_LOAD_CNT, mtid, time, sizeof(time));
	if (idx >= 0) {
		lseek(fd_f_mt, offsetof(F_MT, load) + idx * sizeof(rec), SEEK_SET);
		safe_read(fd_f_mt, &rec, sizeof(rec));
		memcpy(ptr, rec.samples, sizeof(rec.samples));
		return sizeof(rec.samples);
	}
	return 0;
}

int fmt_read_day(const MTID mtid, BYTE year, BYTE mon, BYTE day, void *buf)
{
	int idx;
	BYTE *ptr = buf;
	BYTE time[] = {year, mon, day};
	FMT_DAY rec;

	idx = fmt_idx_read(idx_day, FMT_DAY_CNT, mtid, time, sizeof(time));
	if (idx >= 0) {
		lseek(fd_f_mt, offsetof(F_MT, day) + idx * sizeof(rec), SEEK_SET);
		safe_read(fd_f_mt, &rec, sizeof(rec));
		*ptr ++ = rec.statewd;
		memcpy(ptr, rec.energy, ENERGY_LEN * rec.energy_cnt);
		return 1 + ENERGY_LEN * rec.energy_cnt;
	}
	return 0;
}

int fmt_read_mon(const MTID mtid, BYTE year, BYTE mon, void *buf)
{
	int idx;
	BYTE *ptr = buf;
	BYTE time[] = {year, mon};
	FMT_MON rec;

	idx = fmt_idx_read(idx_mon, FMT_MON_CNT, mtid, time, sizeof(time));
	if (idx >= 0) {
		lseek(fd_f_mt, offsetof(F_MT, mon) + idx * sizeof(rec), SEEK_SET);
		safe_read(fd_f_mt, &rec, sizeof(rec));
		*ptr ++ = rec.statewd;
		memcpy(ptr, rec.energy, ENERGY_LEN * rec.energy_cnt);
		return 1 + ENERGY_LEN * rec.energy_cnt;
	}
	return 0;
}

int fmt_exist_day(const MTID mtid, BYTE year, BYTE mon, BYTE day)
{
	BYTE buf[128];

	return fmt_read_day(mtid, year, mon, day, buf);
}

int fmt_exist_mon(const MTID mtid, BYTE year, BYTE mon)
{
	BYTE buf[128];

	return fmt_read_mon(mtid, year, mon, buf);
}

int fmt_get_read_nbr(void)
{
	int mtidx, cnt;
	struct tm tm;
	MTID mtid;
	BYTE year, mon, day;

	cnt = 0;
	my_time(&tm);
	year = tm.tm_year % 100;
	mon = tm.tm_mon + 1;
	day = tm.tm_mday;
	for (mtidx = 0; mtidx < NBR_REAL_MT; mtidx ++) {
		if (!tmt_is_empty(mtidx)) {
			tmt_get_mtid(mtidx, mtid);
			if (fmt_exist_day(mtid, year, mon, day))
				cnt ++;
		}
	}
	return cnt;
}

int fday_any_phase_failure(void)
{
	int i, phase, mtidx, selected_cnt[3], ok_cnt[3];
	struct tm tm;
	MTID mtid;
	BYTE year, mon, day;
	
	my_time(&tm);
	year = tm.tm_year % 100;
	mon = tm.tm_mon + 1;
	day = tm.tm_mday;
	bzero(selected_cnt, sizeof(selected_cnt));
	bzero(ok_cnt, sizeof(ok_cnt));
	for (mtidx = 0; mtidx < NBR_REAL_MT; mtidx ++) {
		if (tmt_is_empty(mtidx))
			continue;
		phase = tmt_get_phase(mtidx);
		if (phase < 1 || phase > 3)
			continue;
		selected_cnt[phase - 1] ++;
		if (fmt_exist_day(mtid, year, mon, day))
			ok_cnt[phase - 1] ++;
	}
	for (i = 0; i < 3; i ++) {
		if (selected_cnt[i] > 0 && ok_cnt[i] == 0)
			return 1;
	}
	return 0;
}

void list_load_data(void)
{
	int i, j;
	MTID mtid;
	BYTE year, mon, day, data[128];
	struct tm tm;
	
	my_time(&tm);
	year = tm.tm_year % 100;
	mon = tm.tm_mon + 1;
	day = tm.tm_mday;
	printf("\n************************LOAD DATA**************************\n");
	printf("mtseq\tmtid\t\tdate\t\tenergy\n");
	for (i = 0; i < NBR_LOAD_DATA; i ++) {
		for (j = 0; j < NBR_REAL_MT; j ++) {
			if (tmt_is_empty(j) || !tmt_is_focus(j))
				continue;
			tmt_get_mtid(j, mtid);
			if (fmt_read_load(mtid, year, mon, day, data)) {
				printf("%02x%02x%02x%02x%02x%02x, date:%02d-%02d-%02d\n",
					mtid[5], mtid[4], mtid[3], mtid[2], mtid[1], mtid[0],
					year, mon, day);
				PRINTB("load:", data, 96);
			}
		}
		previous_day(&year, &mon, &day);
	}
	printf("\n");
}

void list_day_data(void)
{
	int i, j;
	MTID mtid;
	BYTE year, mon, day, data[128];
	struct tm tm;

	my_time(&tm);
	year = tm.tm_year % 100;
	mon = tm.tm_mon + 1;
	day = tm.tm_mday;
	printf("\n************************DAY DATA**************************\n");
	printf("mtseq\tmtid\t\tdate\t\tenergy\n");
	for (i = 0; i < NBR_DAY_DATA; i ++) {
		for (j = 0; j < NBR_REAL_MT; j ++) {
			if (tmt_is_empty(j) || !tmt_is_selected(j))
				continue;
			tmt_get_mtid(j, mtid);
			if (fmt_read_day(mtid, year, mon, day, data)) {
				printf("%d\t%02x%02x%02x%02x%02x%02x\t%02d-%02d-%02d\t%02x%02x%02x.%02x\n",
					METER_ORDER(j),
					mtid[5], mtid[4], mtid[3], mtid[2], mtid[1], mtid[0],
					year, mon, day,
					data[4], data[3], data[2], data[1]);
			}
		}
		previous_day(&year, &mon, &day);
	}
	printf("\n");
}

void list_month_data(void)
{
	int i, j;
	MTID mtid;
	BYTE year, mon, data[128];
	struct tm tm;
	
	my_time(&tm);
	year = tm.tm_year % 100;
	mon = tm.tm_mon + 1;
	printf("\n************************MONTH DATA*************************\n");
	printf("mtseq\tmtid\t\tdate\t\tenergy\n");
	for (i = 0; i < NBR_MONTH_DATA; i ++) {
		for (j = 0; j < NBR_REAL_MT; j ++) {
			if (tmt_is_empty(j) || !tmt_is_selected(j))
				continue;
			tmt_get_mtid(j, mtid);
			if (fmt_read_mon(mtid, year, mon, data)) {
				printf("%d\t%02x%02x%02x%02x%02x%02x\t%02d-%02d-%02d\t%02x%02x%02x.%02x\n",
					METER_ORDER(j),
					mtid[5], mtid[4], mtid[3], mtid[2], mtid[1], mtid[0],
					year, mon, 1,
					data[4], data[3], data[2], data[1]);
			}
		}
		previous_month(&year, &mon);
	}
	printf("\n");
}

void fmt_open(void)
{
	int size = sizeof(F_MT);
	const char *name = F_MT_NAME;

	sem_init(&sem_f_mt, 0, 1);
	if (!check_file(name, size))
		create_file(name, size);
	fd_f_mt = open(name, O_RDWR);
	fmt_load_init();
	fmt_day_init();
	fmt_mon_init();
}

void fmt_flush(void)
{
	fdatasync(fd_f_mt);
}

void fmt_close(void)
{
	fdatasync(fd_f_mt);
	close(fd_f_mt);
	sem_destroy(&sem_f_mt);
}
