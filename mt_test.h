/*****************************************************************************
 *	C-Plan Concentrator 
 *
 *	(c) Copyright 2006-2007, Shenzhen Kaifa Technology Co.,Ltd. 
 *	All Rights Reserved 
 *		
 *****************************************************************************/

#ifndef _MT_TEST_H
#define _MT_TEST_H

#include "typedef.h"

void mt_test(void);

void type_test(void);

#endif // _MT_TEST_H
