/*
 *  C-Plan Concentrator
 *
 *  Copyright (C) 2006-2007, Shenzhen Kaifa Technology Co.,Ltd.
 *
 *  Author: dajiang wan
 *  Created on: 2007-02-27
 */
#include "err_code.h"
#include "common.h"
#include "dwl_con.h"
#include "afn.h"

static int ftp_download(const BYTE *buf, int len)
{
	int ret = 0, port;
	char *host, *file, *user, *pass, *ptr, url[256];

	if (len >= 9 && len - 6 < sizeof(url) && memcmp(buf, "ftp://", 6) == 0)
	{
		memcpy(url, buf + 6, len - 6);
		url[len - 6] = 0;
		if ((ptr = strchr(url, '/')) != NULL)
		{
			*ptr = 0x0;
			file = ptr + 1;
			if ((ptr = strchr(url, '@')) != NULL)
			{
				*ptr = 0x0;
				host = ptr + 1;
				user = url;
				if ((ptr = strchr(user, ':')) != NULL)
				{
					*ptr = 0x0;
					pass = ptr + 1;
				}
				else
				{
					pass = NULL;
				}
			}
			else
			{
				host = url;
				user = NULL;
				pass = NULL;
			}
			if ((ptr = strchr(host, ':')) != NULL)
			{
				*ptr = 0x0;
				port = atoi(ptr + 1);
			}
			else
			{
				port = 21;
			}
			if (strlen(file) > 0 && strlen(host) > 0 && port > 0)
			{
				ftp_download_file(user, pass, host, port, file);
				ret = 1;
			}
		}
	}
	return ret;
}

/*
 * pn
 * fn
 * buf
 * len
 * param_len
 * */
int afn0f_set_fn(BYTE pn, BYTE fn, const BYTE *buf, int len, int *param_len)
{
	const BYTE *ptr = buf;
	/*
	 * flag			结束标记
	 * 				非0表示文件尾
	 * ret			调用函数返回值
	 * seg_cnt		count,文件块计数
	 * seg_off
	 * seg_len		文件段长度，除最后一段，都是512Byte
	 * */
	int flag, ret, seg_cnt, seg_off, seg_len;

	PRINTF("AFN=0FH, Pn=%d, Fn=%d\n", pn, fn);
	switch (fn)
	{
	case 1:
		if (!dwl_check_packet(ptr, &len))
			return ERR_BAD_VALUE;
		if (len < 11)
			return ERR_BAD_VALUE;
		/* QUS 难道每次都要把文件属性发过来? */
		ptr += 2; /*skip for file id, file attribute*/
		flag = ptr[0];
		ptr++; /* file instruction */
		seg_cnt = ctos(ptr);
		ptr += 2;
		seg_off = ctol(ptr);
		ptr += 4;
		seg_len = ctos(ptr);
		ptr += 2;
		if (len < 11 + seg_len)
			return ERR_BAD_VALUE;
		*param_len = 11 + seg_len + 8;
		if (flag)
			ret = dwl_finish();
		else
			ret = dwl_packet(seg_cnt, seg_off, ptr, seg_len);
		if (ret)
			return ERR_OK;
		else
			return ERR_BAD_VALUE;
	case 2:
		if (ftp_download(buf, len))
			return ERR_OK;
		else
			return ERR_BAD_VALUE;
	default:
		return ERR_BAD_VALUE;
	}
}
