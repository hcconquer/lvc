/*****************************************************************************
 *	C-Plan Concentrator 
 *
 *	(c) Copyright 2006-2007, Shenzhen Kaifa Technology Co.,Ltd. 
 *	All Rights Reserved 
 *
 *	Global Macros, Date Types, Constants Definition
 *		
 *****************************************************************************/

#ifndef _TYPEDEF_H
#define _TYPEDEF_H

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <semaphore.h>
#include <signal.h>
#include <stddef.h>
#include <stdarg.h>
#include <time.h>
#include <fcntl.h>
#include <ctype.h>
#include <termios.h>
#include <errno.h>
#include <dirent.h>
#include <pthread.h>
#include <zlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/select.h>
#include <sys/wait.h>
#include <sys/sysinfo.h>
#include <sys/reboot.h>
#include <sys/ioctl.h>
#include <sys/mount.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <net/if.h>
#include <netdb.h>
#include <linux/reboot.h>
#include <linux/rtc.h>

#ifndef min
	#define min(x,y)	((x)<=(y)?(x):(y))
	#define max(x,y)	((x)>=(y)?(x):(y))
#endif

#ifndef FALSE
	#define	FALSE		0
	#define TRUE		1
#endif

/* communication phase codes */
#define PHASE_A			1
#define PHASE_B			2
#define PHASE_C			3
#define MAX_PHASE_CNT 	3

/* file names */
#define T_MT_NAME		"t_mt.dat"
#define T_MT_STAT_NAME	"t_stat.dat"
#define T_RPT_NAME		"t_rpt.dat"
#define T_ROUTE_NAME	"t_rt.dat"
#define T_REQ_NAME		"t_req.dat"
#define T_TASK_NAME		"t_task.dat"
#define F_PARAM_NAME	"f_param.dat"
#define F_ALM_NAME		"f_alm.dat"
#define F_MT_NAME		"f_mt.dat"
#define T_485_NAME		"t_485.dat"
#define F_485_NAME		"f_485.dat"

#define LOG_NAME		"cscon.log"
#define ERR_NAME		"cscon.err"
#define DWL_NAME		"cscon.dwl"

#define MAX_APDU_LEN	16383
#define MAX_LDU_LEN		(8 + MAX_APDU_LEN)

#define KEY_LEN			3
#define HHU_DATA_LEN	200
#define HHU_APDU_LEN	(HHU_DATA_LEN + 13) /* max length of HHU Application
											   Protocol Data Unit */
#define TLC_DATA_LEN	200
#define TLC_APDU_LEN	(TLC_DATA_LEN + 13) /* max length of Application
											   Protocol Data Unit to MSTA */
#define PLC_APDU_LEN	122  /* max length of PLC APpliation Data Unit */

#define MAX_DIFF_TIME	15	/* max number of different seconds which does not
							   need to sync MT clock */
#define DIGEST_LEN		16
#define MTID_LEN		6
#define ENERGY_LEN		4

#define AUTO_RPT		0	/* use the auto repeater */
#define NO_RPT			-1	/* directly reachable */
#define UNKNOWN_RPT		-2	/* unknown path */

#define NBR_SPMT		1000 /* Max number of Single Phase Meter */
#define NBR_PPMT		200	 /* Max number of Poly Phase Meter */
#define NBR_FOCUS		20	 /* Max number of Focus user meter */
#define NBR_REAL_MT		(NBR_SPMT + NBR_PPMT)/* total meter included */

#define INVALID_MTID	"\xff\xff\xff\xff\xff\xff"
#define BR_MTID			"\x99\x99\x99\x99\x99\x99"	/* broadcast address */

#define INVALID_VALUE		0xee

#define NBR_UNREAD_PERIOD	10
#define MAX_TASK_CNT 		64

/* mask code for CON state word */
#define CS_POWER_UP		0x0001		/* POWER UP HAPPEN */
#define CS_MT_DEFINED	0x0002		/* TBL_MT IS CONFIGURED */
#define CS_CLK_VALID	0x0008		/* CON CLOCK IS VALID */
#define CS_PLC_ACT		0x0010		/* PLC IS ACTIVED */
#define CS_UPCHN_FAIL	0x0100		/* UP CHANNEL FAIL, GRPS CHANNEL */
#define CS_DNCHN_FAIL	0x0200		/* DOWN CHANNEL FAIL, ARM BOARD TO EB */
#define CS_DATA_ERR		0x0400		/* MONTH DATA or/and DAY DATA FILE ERROR */

#define NBR_RPT			7

typedef unsigned char	BITS;
typedef unsigned char	BOOL;
typedef unsigned char	BYTE;
typedef unsigned short	WORD;
typedef unsigned int	DWORD;

typedef BYTE MTID[MTID_LEN];	/* Meter Identification */
typedef BYTE ENERGY[4];			/* energy date type */
typedef BYTE STATEWD[1];		/* meter state word */
typedef BYTE DIGEST[DIGEST_LEN];

typedef enum {
	EVENT_DAY_CHANGE, EVENT_DAY_BEGIN, EVENT_DAY_END, EVENT_POWER_ON
} EVENT;

typedef struct {
	BYTE count;					/* number of repeaters */
	short rpts[NBR_RPT];	/* repeater chain,[0] is the first repeater */
} RPT_PATH_IDX; /* repeater path (index) */

typedef struct {
	BYTE count;					/* number of repeaters */
	MTID rpts[NBR_RPT];	/* repeater chain,[0] is the first repeater */
} RPT_PATH; /* repeater path */

#endif /* _TYPEDEF_H */
