#define _GNU_SOURCE

#include "misc.h"
#include "err_code.h"
#include "f_alm.h"
#include "f_param.h"
#include "t_mt.h"
#include "mt_frzn.h"
#include "mt_route.h"
#include "device.h"
#include "common.h"
#include "cb_access.h"
#include "msg_proc.h"
#include "display.h"
#include "menu_str.h"

static char prog[PATH_MAX];

void set_prog_name(const char *name)
{
	realpath(name, prog);
}

void get_prog_name(char *name, int len)
{
	strncpy(name, prog, len - 1);
}

static char opt_addr[16];
static WORD opt_port = 0;

int get_opt_addr(void *ip_addr, WORD *ip_port)
{
	strcpy(ip_addr, opt_addr);
	*ip_port = opt_port;
	return check_ipaddr_port(opt_addr, opt_port);
}

void set_opt_addr(const void *ip_addr, WORD ip_port)
{
	strcpy(opt_addr, ip_addr);
	opt_port = ip_port;
}

BOOL is_valid_phase(BYTE phase)
{
	return phase == PHASE_A || phase == PHASE_B || phase == PHASE_C;
}

int next_phase(int phase)
{
	if (phase == PHASE_A)
		return PHASE_B;
	if (phase == PHASE_B)
		return PHASE_C;
	return -1;
}

static BYTE last_procid = 0;
static short last_mtidx = -1;

void set_last_proc(BYTE procid)
{
	last_procid = procid;
}

void set_last_mtidx(short mtidx)
{
	last_mtidx = mtidx;
}

BYTE get_last_proc(void)
{
	return last_procid;
}

short get_last_mtidx(void)
{
	return last_mtidx;
}

static WORD con_state = 0x0000;

static void flush_con_statewd(void)
{
	int led;

	if (con_state & (CS_UPCHN_FAIL | CS_DNCHN_FAIL))
		led = 2;
	else 
		led = 1;
	device_status_led(led);
}

static void update_con_statewd(void)
{
	if (tmt_all_is_empty())
		con_state &= ~CS_MT_DEFINED;
	else
		con_state |= CS_MT_DEFINED;
	if (check_rtc())
		con_state |= CS_CLK_VALID;
	else 
		con_state &= ~CS_CLK_VALID;
 	flush_con_statewd();
}

static void set_con_statewd_bit(WORD mask)
{
	BYTE err_code;

	if (!(con_state & mask)) {
		con_state |= mask;
 		flush_con_statewd();
		if (mask & CS_DNCHN_FAIL) {
			err_code = ALARM_CON_PLC_ERR;
			falm_add_con(0x0e, 21, &err_code, 1);
			falm_add_con(0x8e, 12, &err_code, 1);
		}
	}
}

static void clr_con_statewd_bit(WORD mask)
{
	BYTE err_code;

	if (con_state & mask) {
		con_state &= ~mask;
		flush_con_statewd();
		if (mask & CS_CLK_VALID) {
			err_code = ALARM_CON_CLOCK_ERR;
			falm_add_con(0x0e, 21, &err_code, 1);
			falm_add_con(0x8e, 12, &err_code, 1);
		}
	}
}

WORD get_con_statewd(void)
{
	WORD val;

	val = con_state;
	return val;
}

void set_con_statewd(WORD val)
{
	val = CS_POWER_UP | CS_UPCHN_FAIL | CS_DNCHN_FAIL | CS_DATA_ERR;
	con_state &= ~val;
	flush_con_statewd();
}

void init_con_statewd(void)
{
	con_state = CS_POWER_UP;
	con_state |= CS_PLC_ACT;
	//if (t_data_err()) // TODO
	//	con_state |= CS_DATA_ERR;
	//else
	//	con_state &= ~CS_DATA_ERR;
	update_con_statewd();
}

/* CCXX-AITT-FF-NNNNNN, CC: MFG CODE(BCD), XX: SOFT TYPE, A: MAJOR VER,
   I: MINOR VER, T: REVISION, FF: CommBoard SOFT VER, NNNNNN: SPECICAL INFO */
#define MAJOR_VER	1
#define MINOR_VER	2
#define REVISION_1	0
#define REVISION_2	5

static BYTE software_ver[] = {
	'C', 'o', 'n', 'c', 'e', 'n', 't', 'r', 'a', 't', 'o', 'r', ' ',
	'S', 'o', 'f', 't', 'w', 'a', 'r', 'e', ' ',
	'V', 'e', 'r', 's', 'i', 'o', 'n', 0,
	0x03, 0x01, (MAJOR_VER << 4) | MINOR_VER,
	(REVISION_1 << 4) | REVISION_2, 0x00, 0x86, 0x07, 0x31};
static BYTE hardware_ver[] = {0x03, 0x01, 0x10, 0x12, 0x00, 0x86, 0x07, 0x31};
static BYTE protocol_ver[] = {0x05, 0x00, 0x02, 0x14, 0x02, 0x07};

void software_version_str(void *ver)
{
	sprintf(ver, "%1x.%1x.%1x.%1x", MAJOR_VER, MINOR_VER, REVISION_1, REVISION_2);
}

void get_software_version(void *buf, int len)
{
	int tmp = sizeof(software_ver) - 8;

	memcpy(buf, software_ver + tmp, min(len, 8));
}

void find_software_version(const void *name, void *ver)
{
	BYTE *buf, *ptr;
	int fd, len, tmp = sizeof(software_ver) - 8;

	*((char *)ver) = 0x00;
	if ((fd = open(name, O_RDONLY)) > 0) {
		len = lseek(fd, 0, SEEK_END);
		lseek(fd, 0, SEEK_SET);
		if (len > 0 && (buf = malloc(len)) != NULL) {
			if (safe_read(fd, buf, len) == len) {
				if ((ptr = memmem(buf, len, software_ver, tmp)) != NULL) {
					ptr += tmp;
					sprintf(ver, "%02x%02x", ptr[2], ptr[3]);
				}
			}
			free(buf);
		}
		close(fd);
	}
}

void get_hardware_version(void *buf, int len)
{
	memcpy(buf, hardware_ver, min(len, 8));
}

void get_protocol_version(void *buf, int len)
{
	memcpy(buf, protocol_ver, min(len, 6));
}

BYTE get_nbr_retry(void)
{
	BYTE nbr_retry = 1;

	fparam_get_value(FPARAM_PLC_RETRY, &nbr_retry, 1);
	return nbr_retry;
}

void init_proc(void)
{
	BYTE buf[2];

	init_con_statewd();
	if (get_cb_softver(buf))
		software_ver[4] = ((buf[0] << 4) & 0xf0) | (buf[1] & 0x0F);
	reset_cb_state();
}

void event_60_sec(void)
{
	static int fail_cnt = 0;
	#define MIN_REBOOT_INTVAL	(3 * 3600)
	static long fail_time = -MIN_REBOOT_INTVAL;
	int rtc_ok, cb_ok;
	BYTE buf[256];

	rtc_ok = check_rtc();
	cb_ok = get_cb_state(buf);
	
	fparam_lock();
	falm_lock();
	if (rtc_ok) {
		set_con_statewd_bit(CS_CLK_VALID);
	}
	else {
		clr_con_statewd_bit(CS_CLK_VALID);
		ERR_PRINTF("RTC fail\n");
	}
	if (cb_ok) {
		clr_con_statewd_bit(CS_DNCHN_FAIL);
		fail_cnt = 0;
	}
	else {
		set_con_statewd_bit(CS_DNCHN_FAIL);
		fail_cnt ++;
	}
	falm_unlock();
	fparam_unlock();
	if (cb_ok && buf[0] == 0xff) { // CB rebooted
		ERR_PRINTF("reset comboard state\n");
		reset_cb_state();
	}
	if (fail_cnt >= 5 && uptime() - fail_time >= MIN_REBOOT_INTVAL) {
		ERR_PRINTF("fail read comboard\n");
		set_eb_reboot();
		fail_cnt = 0;
		fail_time = uptime();
	}
}

int proc_is_enable(WORD flag)
{
	BYTE proc_mask[2];

	fparam_get_value(FPARAM_PROC_ENABLE_MASK, proc_mask, 2);
	return ctos(proc_mask) & flag;
}

int energy_convert(const BYTE *buf, int *val)
{
	int i;
	BYTE tmp, high, low;

	for (i = 0; i < 4; i ++) {
		tmp = buf[i];
		high = (tmp >> 4) & 0x0f;
		low = tmp & 0x0f;
		if (low >= 10 || high >= 10) {
			*val = 0;
			return 0;
		}
	}
	*val = bcd_to_bin(buf[0]) + bcd_to_bin(buf[1]) * 100
		+ bcd_to_bin(buf[2]) *10000 + bcd_to_bin(buf[3]) * 1000000;
	return 1;
}

int bcd_ctos(const BYTE *buf, int *val)
{
	const BYTE *ptr = buf;
	BYTE ch, high, low, str[4];
	int i;

	*val = 0;
	for (i = 0; i < 2; i ++) {
		ch = *ptr ++;
		high = (ch >> 4) & 0x0f;
		low = ch & 0x0f;
		if (high >= 10 || low >= 10)
			return 0;
		str[i] = (high * 10 + low);
	}
	*val = str[0] + str[1] * 100;
	return 1;
}

int bcd_ctol(const BYTE *buf, int *val)
{
	const BYTE *ptr = buf;
	BYTE ch, high, low, str[4];
	int i;

	*val = 0;
	for (i = 0; i < 4; i ++) {
		ch = *ptr ++;
		high = (ch >> 4) & 0x0f;
		low = ch & 0x0f;
		if (high >= 10 || low >= 10)
			return 0;
		str[i] = (high * 10 + low);
	}
	*val = str[0] + str[1] * 100 + str[2] * 100 * 100
		+ str[3] * 100 * 100 * 100;
	return 1;
}

void bcd_stoc(BYTE *buf, int val)
{
	BYTE ch, high, low, *ptr = buf;
	int i;

	for (i = 0; i < 2; i ++) {
		ch = val % 100;
		high = ch / 10;
		low = ch % 10;
		*ptr ++ = (high << 4) + low;
		val = val / 100;
	}
}

void bcd_ltoc(BYTE *buf, int val)
{
	BYTE ch, high, low, *ptr = buf;
	int i;

	for (i = 0; i < 4; i ++) {
		ch = val % 100;
		high = ch / 10;
		low = ch % 10;
		*ptr ++ = (high << 4) + low;
		val = val / 100;
	}
}

int compare_month(int year, int mon)
{
	static char days[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
	struct tm tm;
	time_t tt1, tt2, day_tt = 24 * 60 * 60;
	int day;

	if (is_leap_year(year) && mon == 2)
		day = 29;
	else
		day = days[mon - 1];
	time(&tt1);
	localtime_r(&tt1, &tm);
	tm.tm_year = year - 1900;
	tm.tm_mon = mon - 1;
	tm.tm_mday = day;
	tt2 = mktime(&tm);
	return (tt2 - tt1) / day_tt;
}

int compare_today(int year, int mon, int day)
{
	struct tm tm;
	time_t tt1, tt2, day_tt = 24 * 60 * 60;

	time(&tt1);
	localtime_r(&tt1, &tm);
	tm.tm_year = year - 1900;
	tm.tm_mon = mon - 1;
	tm.tm_mday = day;
	tt2 = mktime(&tm);
	return (tt2 - tt1) / day_tt;
}

void previous_day(BYTE *year, BYTE *month, BYTE *day)
{
	static const BYTE month_day[] = {31,28,31,30,31,30,31,31,30,31,30,31};
	BYTE max_day;

	if (*day == 1) {
		if (*month == 1) {
			*month = 12;
			if (*year == 0)
				*year = 99;
			else
				*year = *year - 1;
		}
		else {
			*month = *month - 1;
		}
		max_day = month_day[*month - 1];
		if (*month == 2 && is_leap_year(*year + 2000))
			max_day ++;
		*day = max_day;
	}
	else {
		*day = *day - 1;
	}
}

void next_day(BYTE *year, BYTE *month, BYTE *day)
{
	static const BYTE month_day[] = {31,28,31,30,31,30,31,31,30,31,30,31};
	BYTE max_day;

	max_day = month_day[*month - 1];
	if (*month == 2 && is_leap_year(*year + 2000))
		max_day ++;
	if (*day == max_day) {
		*day = 1;
		if (*month == 12) {
			*month = 1;
			*year = *year + 1;
		}
		else {
			*month = *month + 1;
		}
	}
	else {
		*day = *day + 1;
	}
}

void previous_month(BYTE *year, BYTE *month)
{
	if (*month == 1) {
		*month = 12;
		if (*year == 0) {
			*year = 99;
		}
		else {
			*year = *year - 1;
		}
	}
	else {
		*month = *month - 1;
	}
}

void next_month(BYTE *year, BYTE *month)
{
	if (*month == 12) {
		*month = 1;
		*year = *year + 1;
	}
	else {
		*month = *month + 1;
	}
}

void previous_year(BYTE *year)
{
	*year = (*year == 0) ? 99 : (*year - 1);
}

void next_year(BYTE *year)
{
	*year = (*year == 99) ? 0 : (*year + 1);
}

/* check if time is in [23:50 - 00:10] */
int is_time_around_zero(const struct tm *tm)
{
	return ((tm->tm_hour == 23 && tm->tm_min >= 50) ||
		(tm->tm_hour == 0 && tm->tm_min <= 10));
}

/* check if time tm is after specified DDHH */
int after_ddhh(BYTE dd, BYTE hh, const struct tm *tm)
{
	return tm->tm_mday > dd || (tm->tm_mday == dd && tm->tm_hour >= hh);
}

int check_ipaddr_port(const char *ip_addr, WORD ip_port)
{
	if (ip_port > 0 && ip_port < 0xffff)
		return 1;
	return 0;
}

void meter_change(void)
{
	fparam_lock();
	update_con_statewd();
	fparam_unlock();

	frzn_meter_change();
	route_meter_change();
}

static int ppp_user_update(const char *file_name, const char *prompt,
	const void *user, int user_len, const void *pass, int pass_len)
{
	char buf[1024];
	int fd;

	fd = open(file_name, O_RDWR | O_TRUNC | O_SYNC | O_CREAT, 0600);
	if (fd >= 0) {
		snprintf(buf, sizeof(buf), "# Secrets for authentication using %s\n"
			"# client\tserver\tsecret\tIP addresses\n", prompt);
		if (((BYTE *)user)[0] != 0x0)
			escape_string(buf + strlen(buf), user, user_len);
		else
			strcat(buf, "\"*\"");
		strcat(buf, "\t*\t");
		escape_string(buf + strlen(buf), pass, pass_len);
		strcat(buf, "\t*\n");
		strcat(buf, "\"*\"\t*\t\"\"\t*\n");
		lseek(fd, 0, SEEK_SET);
		safe_write(fd, buf, strlen(buf));
		close(fd);
	}
	return fd;
}

void change_gprs_password(const void *user, int user_len, const void *pass,
	int pass_len)
{
	mount("/dev/hda1", "/", "ext2", MS_REMOUNT, NULL);
	PRINTF("Update chap-secrets, pap-secrets\n");
	ppp_user_update("/etc/ppp/chap-secrets", "CHAP", user, user_len, pass, pass_len);
	ppp_user_update("/etc/ppp/pap-secrets", "PAP", user, user_len, pass, pass_len);
	mount("/dev/hda1", "/", "ext2", MS_REMOUNT | MS_RDONLY, NULL);
}

int check_pppd(const char *lock_name)
{
	int lock_pid;
	char path1[PATH_MAX], path2[PATH_MAX];

	lock_pid = read_lock_file(lock_name);
	if (lock_pid > 0) {
		snprintf(path1, sizeof(path1), "/proc/%d/exe", lock_pid);
		if (realpath(path1, path2) != NULL) {
			if (strcmp(path2, "/usr/sbin/pppd") == 0)
				return 1;
		}
	}
	return 0;
}

volatile int plc_idle_flag = 1;

void plc_set_idle(int flag)
{
	plc_idle_flag = flag;
}

int plc_is_idle(void)
{
	return plc_idle_flag;
}

/* buf: ssmmhhDDMMYY bcd code */
int get_date_time(BYTE *buf, int max_len)
{
	struct tm tm;
	struct timeval tv;
	BYTE *ptr = buf;
	BYTE wmon, mon;
	gettimeofday(&tv, NULL);
	localtime_r(&tv.tv_sec, &tm);
	if (max_len < 6)
		return 0;
	*ptr ++ = bin_to_bcd(tm.tm_sec);
	*ptr ++ = bin_to_bcd(tm.tm_min);
	*ptr ++ = bin_to_bcd(tm.tm_hour);
	*ptr ++ = bin_to_bcd(tm.tm_mday);
	mon = bin_to_bcd(tm.tm_mon + 1);
	if (tm.tm_wday == 0)
		wmon = 7;
	else
		wmon = tm.tm_wday;
	*ptr ++ = (wmon << 5) | mon;
	*ptr ++ = bin_to_bcd(tm.tm_year % 100);
	return ptr - buf;
}

int set_date_time(const BYTE *buf, int max_len)
{
	struct tm tm;
	struct timeval tv;
	const BYTE *ptr = buf;
	BYTE mon;
	int year;

	if (max_len < 6)
		return 0;
	tm.tm_isdst = -1;
	tm.tm_sec = bcd_to_bin(*ptr); ptr ++;
	tm.tm_min = bcd_to_bin(*ptr); ptr ++;
	tm.tm_hour = bcd_to_bin(*ptr); ptr ++;
	tm.tm_mday = bcd_to_bin(*ptr); ptr ++;
	mon = *ptr ++; mon &= 0x1f; tm.tm_mon = bcd_to_bin(mon) - 1;
	year = bcd_to_bin(*ptr); ptr ++;
	tm.tm_year = (year < 70) ? (year + 100) : year;

	tv.tv_sec = mktime(&tm);
	tv.tv_usec = 0;
	settimeofday(&tv, NULL);
	set_rtc();
	return ptr - buf;
}

static void dialup(const char *device_name, const char *baudstr,
	const char *user_name, const char *phone, const char *apn,
	const char *pdp)
{
	int pid, stat;
	char env_phone[60], env_apn[60], env_pdp[60];
	char device[60], baud[60], user[60];
	char *argv[20];
	char *envp[] = {env_phone, env_apn, env_pdp, NULL};

	PRINTF("dialup %s, user:%s, phone:%s, apn:%s, pdp:%s\n",
		device_name, user_name, phone, apn, pdp);
	pid = fork();
	if (pid == -1)
		return;
	if (pid == 0) {
		setsid();
		pid = fork();
		if (pid != 0)
			exit(127);
		strcpy(device, device_name);
		strcpy(baud, baudstr);
		snprintf(user, sizeof(user), "\"%s\"", user_name);
		snprintf(env_phone, sizeof(env_phone), "TELEPHONE=%s", phone);
		snprintf(env_apn, sizeof(env_apn), "APN=%s", apn);
		snprintf(env_pdp, sizeof(env_pdp), "PDP=%s", pdp);
		argv[0] = "/usr/sbin/pppd";
		argv[1] = device;
		argv[2] = baud;
		argv[3] = "user";
		argv[4] = user;
		argv[5] = "connect";
		argv[6] = "/etc/ppp/ppp-on-gprs";
		argv[7] = "debug";
		argv[8] = "nodetach";
		argv[9] = "noauth";
		argv[10] = "noipdefault";
		argv[11] = "usepeerdns";
		argv[12] = "0.0.0.0:0.0.0.0";
		argv[13] = NULL;
		close_all(0);
		chdir("/");
		umask(0);
		signal(SIGINT, SIG_IGN);
		signal(SIGTERM, SIG_IGN);
		signal(SIGHUP, SIG_IGN);
		signal(SIGUSR1, SIG_IGN);
		signal(SIGCHLD, SIG_IGN);
		execve(argv[0], argv, envp);
		exit(127); // exit when execve fail
	}
	waitpid(pid, &stat, 0);
}

int dialup_gprs(const char *device_name, const char *lock_name,
	const char *baudstr, int try_cnt, int interval)
{
	static int last_time;
	char apn[16], pdp[60], phone[60], user[32];
	int ret = 0, i;
	char addr[16], dstaddr[16];

	if (fparam_get_value(FPARAM_APN, apn, sizeof(apn)) <= 0)
		return 0;
	if (fparam_get_value(FPARAM_VPN_USER, user, sizeof(user)) <= 0)
		return 0;
	strcpy(phone, "*99***1#");
	strcpy(pdp, "0.0.0.0");
	if (try_cnt == 0) {
		if (last_time == 0 || uptime() - last_time >= interval) {
			lcd_update_comm_info(7);
			dialup(device_name, baudstr, user, phone, apn, pdp);
			for (i = 0; i < 30; i ++) {
				wait_delay(1000);
				if (check_pppd(lock_name) && get_ppp_addr(addr, dstaddr)) {
					set_net_icon(0xff, 31);
					lcd_update_head_info();
					last_time = uptime();
					wait_delay(2000);
					ret = 1;
					break;
				}
			}
		}
		else
			return 0;
	}
	else {
		last_time = 0;
		do {
			lcd_update_comm_info(7);
			dialup(device_name, baudstr, user, phone, apn, pdp);
			for (i = 0; i < 30; i ++) {
				wait_delay(1000);
				if (check_pppd(lock_name) && get_ppp_addr(addr, dstaddr)) {
					set_net_icon(0xff, 31);
					lcd_update_head_info();
					wait_delay(2000);
					ret = 1;
					break;
				}
			}
		} while (--try_cnt >= 0 && ret == 0);
	}
	if (!ret)
		lcd_update_comm_info(8);
	else
		lcd_update_comm_info(9);
	return ret;
}

void tt_to_fmt_15(time_t tt, BYTE *buf)
{
	struct tm tm;

	if (tt != 0) {
		localtime_r(&tt, &tm);
		buf[0] = bin_to_bcd(tm.tm_min);
		buf[1] = bin_to_bcd(tm.tm_hour);
		buf[2] = bin_to_bcd(tm.tm_mday);
		buf[3] = bin_to_bcd(tm.tm_mon + 1);
		buf[4] = bin_to_bcd(tm.tm_year % 100);
	}
	else {
		buf[0] = buf[1] = buf[2] = buf[3] = buf[4] = 0;
	}
}

void tt_to_fmt_17(time_t tt, BYTE *buf)
{
	struct tm tm;

	if (tt != 0) {
		localtime_r(&tt, &tm);
		buf[0] = bin_to_bcd(tm.tm_min);
		buf[1] = bin_to_bcd(tm.tm_hour);
		buf[2] = bin_to_bcd(tm.tm_mday);
		buf[3] = bin_to_bcd(tm.tm_mon + 1);
	}
	else {
		buf[0] = buf[1] = buf[2] = buf[3] = 0;
	}
}

void system_status(int on)
{
	time_t tt1, tt2;
	BYTE buf[10];
	int param_len;

	fparam_lock();
	falm_lock();
	time(&tt1);
	if (on) {
		fparam_set_value(FPARAM_POWERUP_TIME, &tt1, sizeof(tt1), &param_len);
		LOG_PRINTF("System ready\n");
		if (fparam_get_value(FPARAM_POWERFAIL_TIME, &tt2, sizeof(tt2))) {
			tt_to_fmt_15(tt2, buf);
			tt_to_fmt_15(tt1, buf + 5);
			falm_add_con(0x0e, 14, buf, 10);
		}
	}
	else if (!power_is_fail()) {
		fparam_set_value(FPARAM_POWERFAIL_TIME, &tt1, sizeof(tt1), &param_len);
		LOG_PRINTF("System reboot\n");
	}
	falm_unlock();
	fparam_unlock();
}

void system_reboot(void)
{
	system("/sbin/reboot");
	exit(1);
}

int check_time_t(time_t tt1, time_t tt2, int range)
{
	if (tt1 <= tt2)
		return tt2 - tt1 <= range;
	else
		return tt1 - tt2 <= range;
}
