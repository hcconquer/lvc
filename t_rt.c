/*****************************************************************************
 *	C-Plan Concentrator
 *
 *	(c) Copyright 2006-2007, Shenzhen Kaifa Technology Co.,Ltd.
 *	All Rights Reserved
 *
 *	to manage the communcation statistic information, which is used
 *	by the routing procedure to improve the effect of routing.
 *	those information is udpated every time when communcating to meter
 *
 *	Author: fanhua zeng
 *	Created on: 2006-07-31
 *****************************************************************************/
#include "common.h"
#include "t_mt.h"
#include "t_rt.h"

static T_RT t_rt[T_RT_ROWS_CNT];
static T_RT *p_t_rt = t_rt;
static sem_t sem_t_rt;
static int fd_t_rt;
static BYTE nun[NBR_REAL_MT]; /* number of successive not-reachable */

int get_nun(int mtidx) {
	return nun[mtidx];
}

void set_nun(int mtidx, int n) {
	if (n <= 255)
		nun[mtidx] = n;
}

static void inc_nun(int mtidx) {
	if (nun[mtidx] < 255)
		nun[mtidx] += 1;
}

void update_nun(short mtidx, RPT_PATH_IDX *path_idx, int fail_rpt_idx) {
	int i;

	for (i = 0; i < path_idx->count; i++) {
		if (path_idx->rpts[i] == fail_rpt_idx) {
			inc_nun(path_idx->rpts[i]);
			break;
		} else {
			set_nun(path_idx->rpts[i], 0);
		}
	}
	if (i == path_idx->count) {
		if (fail_rpt_idx == mtidx)
			inc_nun(mtidx);
		else
			set_nun(mtidx, 0);
	}
}

void trt_lock(void) {
	sem_wait(&sem_t_rt);
}

void trt_unlock(void) {
	sem_post(&sem_t_rt);
}

static void init_rpt_stat(RPT_STATS *p, int rpt_idx) {
	p->mtidx = rpt_idx;
	p->smpl_cnt = 0;
	p->smpl_idx = 0;
	bzero(p->smpl, sizeof(p->smpl));
}

void trt_init_row(short rowno) {
	int i;
	RPT_STATS *p;

	p_t_rt[rowno - 1].nbr_rpt = 0;
	p_t_rt[rowno - 1].idx = 0xff;
	p = p_t_rt[rowno - 1].rpts;
	for (i = 0; i < NBR_BKUP_RPT; i++, p++) {
		init_rpt_stat(p, UNKNOWN_RPT);
	}
}

static void set_smpl(BYTE *smpl, int idx) {
	smpl[idx / 8] |= (1 << (idx % 8));
}

static void clr_smpl(BYTE *smpl, int idx) {
	smpl[idx / 8] &= ~(1 << (idx % 8));
}

static int chk_smpl(BYTE *smpl, int idx) {
	return ((smpl[idx / 8] & (1 << (idx % 8))) != 0);
}

static double get_st_mean(RPT_STATS *p) {
	int i, sum;

	if (p->smpl_cnt == 0)
		return 0.0;
	sum = 0;
	for (i = 0; i < p->smpl_cnt; i++) {
		if (chk_smpl(p->smpl, i))
			sum++;
	}
	return 1.0 * sum / p->smpl_cnt;
}

static double get_csr(int mtidx) {
	double csr = 1.0;
	int i, idx;
	T_RT *rt;

	idx = mtidx;
	for (i = 0; i < NBR_RPT; i++) {
		rt = p_t_rt + idx;
		if (rt->nbr_rpt == 0)
			return 0.0;
		csr *= get_st_mean(rt->rpts + rt->idx);
		idx = rt->rpts[rt->idx].mtidx;
		if (idx == NO_RPT)
			break;
	}
	if (i >= NBR_RPT)
		csr = 0.0;
	return csr;
}

static double get_bkup_path_csr(RPT_STATS *p) {
	return get_st_mean(p) * get_csr(p->mtidx);
}

int trt_get_rpt_csr(short mtidx, short *rpt_idx, double *csr) {
	int idx = p_t_rt[mtidx].idx;

	if (p_t_rt[mtidx].nbr_rpt > 0 && idx < p_t_rt[mtidx].nbr_rpt) {
		*rpt_idx = p_t_rt[mtidx].rpts[idx].mtidx;
		*csr = get_bkup_path_csr(p_t_rt[mtidx].rpts + idx);
		return 1;
	}
	return 0;
}

void prn_route(short mtidx) {
	int i;
	RPT_STATS *p = p_t_rt[mtidx].rpts;

	printf("mtseq\trpt_seq\tCSR  \tst_mean\tsp_cnt\tsmpl_idx\tsmpl\n");
	if (p_t_rt[mtidx].nbr_rpt == 0) {
		printf(" %-d\n", METER_ORDER(mtidx));
	} else {
		for (i = 0; i < p_t_rt[mtidx].nbr_rpt; i++) {
			printf(
					"%c%-d\t%-d\t%-5.2f\t%-5.2f\t%-d\t%-d\t%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x\n",
					i == p_t_rt[mtidx].idx ? '*' : ' ', METER_ORDER(mtidx), METER_ORDER(p->mtidx),
					get_bkup_path_csr(p), get_st_mean(p), p->smpl_cnt, p->smpl_idx,
					p->smpl[0], p->smpl[1], p->smpl[2], p->smpl[3],
					p->smpl[4], p->smpl[5], p->smpl[6], p->smpl[7],
					p->smpl[8], p->smpl[9]
					);
					p++;
				}
			}
			printf("\n");
		}

void list_route_info(void) {
	int idx;

	printf("\n*********************** route info ************************\n");
	for (idx = 0; idx < NBR_REAL_MT; idx++) {
		if (!tmt_is_empty(idx)) {
			prn_route(idx);
		}
	}
	printf("***********************************************************\n");
}

struct mt_rate {
	int mtidx;
	double csr;
};

/* descending order */
static int comp_rates(const void *p1, const void *p2) {
	return ((struct mt_rate *) p2)->csr - ((struct mt_rate *) p1)->csr;
}

/* return the backuped repeater sets, ordered by the CSR
 * in descending sequence
 */
void trt_get_bkup_rpts(int mtidx, int *rpts, int *n, double *csr) {
	int i;
	struct mt_rate mt_rates[NBR_BKUP_RPT];
	T_RT *p = p_t_rt + mtidx;

	*n = p->nbr_rpt;
	if (*n == 0)
		return;
	for (i = 0; i < p->nbr_rpt; i++) {
		mt_rates[i].mtidx = p->rpts[i].mtidx;
		mt_rates[i].csr = get_bkup_path_csr(p->rpts + i);
	}
	qsort(mt_rates, p->nbr_rpt, sizeof(struct mt_rate), comp_rates);
	for (i = 0; i < p->nbr_rpt; i++) {
		*rpts++ = mt_rates[i].mtidx;
		*csr++ = mt_rates[i].csr;
	}
	prn_route(mtidx);
}

/*
 * log the communication samples, update the CSR of the path.
 */
static void update_reach(int mtidx, int rpt_idx, int reach) {
	int i;
	RPT_STATS *p_rpt = NULL;

	if (p_t_rt[mtidx].nbr_rpt == 0 || p_t_rt[mtidx].idx >= NBR_BKUP_RPT)
		return;
	if (p_t_rt[mtidx].rpts[p_t_rt[mtidx].idx].mtidx == rpt_idx) {
		p_rpt = p_t_rt[mtidx].rpts + p_t_rt[mtidx].idx;
	} else { /* in routing procedure, the rpt_idx may is not the default rpt */
		for (i = 0; i < p_t_rt[mtidx].nbr_rpt; i++) {
			if (p_t_rt[mtidx].rpts[i].mtidx == rpt_idx) {
				p_rpt = p_t_rt[mtidx].rpts + i;
				break;
			}
		}
		if (p_rpt == NULL)
			return;
	}
	if (reach)
		set_smpl(p_rpt->smpl, p_rpt->smpl_idx);
	else
		clr_smpl(p_rpt->smpl, p_rpt->smpl_idx);
	if (p_rpt->smpl_idx < NBR_SMPL - 1)
		p_rpt->smpl_idx++;
	else
		p_rpt->smpl_idx = 0;
	if (p_rpt->smpl_cnt < NBR_SMPL)
		p_rpt->smpl_cnt++;
}

/* if nbr_rpt == NBR_BKUP_RPT overwrite the repeater with min .rate,
 * else add one bkup repeater
 */
void trt_add_bkup_rpt(int mtidx, int rpt_idx) {
	int i, idx = -1, found = 0;
	T_RT *p = p_t_rt + mtidx;
	double csr, min_csr = 1.0;

	for (i = 0; i < p->nbr_rpt; i++) {
		csr = get_bkup_path_csr(p->rpts + i);
		if (csr < min_csr) {
			min_csr = csr;
			idx = i;
		}
		if (p->rpts[i].mtidx == rpt_idx) {
			/* one time communication ok */
			p->idx = i;
			found = 1;
			break;
		}
	}
	if (!found) { /* new one */
		if (p->nbr_rpt < NBR_BKUP_RPT) {
			idx = p->nbr_rpt;
			p->nbr_rpt++;
		}
		if (idx < 0)
			return;
		p->idx = idx;
		/* set other param here */
		init_rpt_stat(p->rpts + idx, rpt_idx);
		update_reach(mtidx, rpt_idx, 1);
	}
}

void trt_update_statistic(short mtidx, RPT_PATH_IDX *path_idx, int fail_rpt_idx) {
	int i, reach;

	for (i = 0; i < path_idx->count; i++) {
		reach = (fail_rpt_idx != path_idx->rpts[i]);
		update_reach(path_idx->rpts[i],
				i == 0 ? NO_RPT : path_idx->rpts[i - 1], reach);
		if (!reach)
			break;
	}

	if (i == path_idx->count) {
		update_reach(mtidx, path_idx->count ? path_idx->rpts[path_idx->count
				- 1] : NO_RPT, fail_rpt_idx != mtidx);
	}
}

void trt_open(void) {
	int mtidx, size;
	const char *name = T_ROUTE_NAME;

	size = sizeof(T_RT) * T_RT_ROWS_CNT;
	sem_init(&sem_t_rt, 0, 1);
	if (!check_file(name, size)) {
		/* File doesnot exist or file length is invalid. */
		PRINTF("File %s is created, size:%d\n", name, size);
		fd_t_rt = open(name, O_CREAT | O_RDWR | O_TRUNC, 0600);
		for (mtidx = 0; mtidx < T_RT_ROWS_CNT; mtidx++)
			trt_init_row(mtidx + 1);
		safe_write(fd_t_rt, p_t_rt, size);
		fdatasync(fd_t_rt);
		close(fd_t_rt);
	}
	fd_t_rt = open(name, O_RDWR);
	safe_read(fd_t_rt, p_t_rt, size);
}

void trt_update(short mtidx, int flush_flag) {
	lseek(fd_t_rt, mtidx * sizeof(T_RT), SEEK_SET);
	safe_write(fd_t_rt, p_t_rt + mtidx, sizeof(T_RT));
	if (flush_flag)
		trt_flush();
}

void trt_update_all(void) {
	int mtidx;

	for (mtidx = 0; mtidx < T_RT_ROWS_CNT; mtidx++)
		trt_update(mtidx, 0);
	trt_flush();
}

void trt_flush(void) {
	fdatasync(fd_t_rt);
}

void trt_close(void) {
	fdatasync(fd_t_rt);
	close(fd_t_rt);
	sem_destroy(&sem_t_rt);
}
