/*****************************************************************************
*	C-Plan Concentrator 
*
*	(c) Copyright 2006-2007, Shenzhen Kaifa Technology Co.,Ltd. 
*	All Rights Reserved 
*
*	The plc activities thread routines 
*
*	Author: fanhua zeng
*	Created on: 2006-07-31
*****************************************************************************/
#include "common.h"
#include "device.h"
#include "global.h"
#include "misc.h"
#include "f_param.h"
#include "f_alm.h"
#include "cb_access.h"
#include "mt_access.h"
#include "mt_day.h"
#include "mt_load.h"
#include "mt_sync.h"
#include "mt_month.h"
#include "mt_route.h"
#include "mt_frzn.h"
#include "mt_alarm.h"
#include "t_mt.h"
#include "f_mt.h"
#include "msg_proc.h"
#include "threads.h"

static int day_changed(struct tm *tm, struct tm *day_tm)
{
	return (tm->tm_year != day_tm->tm_year || tm->tm_mon != day_tm->tm_mon
		|| tm->tm_mday != day_tm->tm_mday);
}

void *plc_thread(void *arg)
{
	int rtc_validate(void);
	struct tm day_tm, tm;
	time_t tt;
	long last_uptime = 0, idle_uptime = 0, clock_alarm_uptime = 0;
	int day_begin_flag = 0, day_end_flag = 0, step = 1, plc_checked = 0;
	int clock_alarm_flag = 0;
	BYTE clock_err_code = 2;
	
	my_time(&day_tm);
	event_change(EVENT_POWER_ON);
	while (!g_terminated) {
		msleep(500);
		notify_watchdog();
		check_sched();
		check_eb_reboot();
		if (uptime() - idle_uptime >= 7 * 60) {
			self_diag(plc_open, plc_close);
			idle_uptime = uptime();
		}
		if (uptime() - last_uptime >= 60) {
			event_60_sec();
			last_uptime = uptime();
		}
		if (!rtc_validate()) {
			if (clock_alarm_flag == 0) {
				clock_alarm_uptime = uptime();
				clock_alarm_flag = 1;
				fparam_lock();
				falm_lock();
				falm_add_con(0x0e, 21, &clock_err_code, 1);
				falm_add_con(0x8e, 12, &clock_err_code, 1);
				falm_unlock();
				fparam_unlock();
			}
			else {
				if (uptime() - clock_alarm_uptime >= 30 * 60) {
					clock_alarm_flag = 0;
					clock_alarm_uptime = uptime();
				}
			}
			continue;
		}
		clock_alarm_flag = 0;
		time(&tt); localtime_r(&tt, &tm);
		if (day_changed(&tm, &day_tm)) {
			event_change(EVENT_DAY_CHANGE);
			day_tm = tm;
		}
		if (tm.tm_hour == 0 && tm.tm_min > 10) {
			if (day_begin_flag == 0) {
				event_change(EVENT_DAY_BEGIN);
				day_begin_flag = 1;
			}
		}
		else if (tm.tm_hour != 0 && day_begin_flag) {
			day_begin_flag = 0;
		}
		if (tm.tm_hour == 23 && tm.tm_min > 50) {
			plc_set_idle(1); // avoid plc is always busy when error occur
			if (day_end_flag == 0) {
				event_change(EVENT_DAY_END);
				day_end_flag = 1;
			}
		}
		else if (tm.tm_hour != 23 && day_end_flag) {
			day_end_flag = 0;
		}
		if (tmt_all_is_empty())
			continue;
		/* 10:00 - 10:03 to check if plc failure. if yes, to reset it */
		if (tm.tm_hour == 10 && tm.tm_min < 3) {
			if (!plc_checked) {
				if (fday_any_phase_failure())
					set_eb_reboot();
				plc_checked = 1;
			}
		}
		else if (tm.tm_hour != 10 && plc_checked) {
			plc_checked = 0;
		}
		if (is_time_around_zero(&tm) || !plc_is_idle())
			continue;

		if (proc_is_enable(PROC_MASK_SYNC) && mt_sync(&tm))
			continue; 

		if (proc_is_enable(PROC_MASK_FRZN) && mt_frzn(&tm))
			continue;

		switch (step) {
		case 1: 
			if (!proc_is_enable(PROC_MASK_MONTH) || !mt_month(&tm))
				step = 2;
			break;
		case 2:
			if (!proc_is_enable(PROC_MASK_DAY) || !mt_day(&tm))
				step = 3;
			break;
		case 3: 
			if (!proc_is_enable(PROC_MASK_LOAD) || !mt_load(&tm))
				step = 4;
			break;
		case 4: 
			if (!proc_is_enable(PROC_MASK_ROUTE) || !mt_route(&tm))
				step = 1;
			break;
		}
		if (proc_is_enable(PROC_MASK_ALARM))
			mt_alarm(&tm);
	}
	return NULL;
}
