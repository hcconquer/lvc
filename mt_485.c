/*
 *  C-Plan Concentrator
 *
 *  Copyright (C) 2006-2007, Shenzhen Kaifa Technology Co.,Ltd.
 *
 *  Provide function to open/close tables and semaphores. 
 *
 *  Author: dajiang wan
 *  Created on: 2008-01-07
 */
#include "mt_485.h"
#include "device.h"
#include "common.h"
#include "input.h"
#include "display.h"

typedef struct {
	WORD di;
	BYTE len;
} MTDI_ITEM;

static MTDI_ITEM mt_di[] = {
	{0x9010, 4}, {0x9011, 4}, {0x9012, 4}, {0x9013, 4}, {0x9014, 4},
	{0x9020, 4}, {0x9021, 4}, {0x9022, 4}, {0x9023, 4}, {0x9024, 4},
	{0x9110, 4}, {0x9111, 4}, {0x9112, 4}, {0x9113, 4}, {0x9114, 4},
	{0x9120, 4}, {0x9121, 4}, {0x9122, 4}, {0x9123, 4}, {0x9124, 4},
	{0x9130, 4}, {0x9131, 4}, {0x9132, 4}, {0x9133, 4}, {0x9134, 4},
	{0x9140, 4}, {0x9141, 4}, {0x9142, 4}, {0x9143, 4}, {0x9144, 4},
	{0x9150, 4}, {0x9151, 4}, {0x9152, 4}, {0x9153, 4}, {0x9154, 4},
	{0x9160, 4}, {0x9161, 4}, {0x9162, 4}, {0x9163, 4}, {0x9164, 4},

	{0x9410, 4}, {0x9411, 4}, {0x9412, 4}, {0x9413, 4}, {0x9414, 4},
	{0x9420, 4}, {0x9421, 4}, {0x9422, 4}, {0x9423, 4}, {0x9424, 4},
	{0x9510, 4}, {0x9511, 4}, {0x9512, 4}, {0x9513, 4}, {0x9514, 4},
	{0x9520, 4}, {0x9521, 4}, {0x9522, 4}, {0x9523, 4}, {0x9524, 4},
	{0x9530, 4}, {0x9531, 4}, {0x9532, 4}, {0x9533, 4}, {0x9534, 4},
	{0x9540, 4}, {0x9541, 4}, {0x9542, 4}, {0x9543, 4}, {0x9544, 4},
	{0x9550, 4}, {0x9551, 4}, {0x9552, 4}, {0x9553, 4}, {0x9554, 4},
	{0x9560, 4}, {0x9561, 4}, {0x9562, 4}, {0x9563, 4}, {0x9564, 4},

	{0x9810, 4}, {0x9811, 4}, {0x9812, 4}, {0x9813, 4}, {0x9814, 4},
	{0x9820, 4}, {0x9821, 4}, {0x9822, 4}, {0x9823, 4}, {0x9824, 4},
	{0x9910, 4}, {0x9911, 4}, {0x9912, 4}, {0x9913, 4}, {0x9914, 4},
	{0x9920, 4}, {0x9921, 4}, {0x9922, 4}, {0x9923, 4}, {0x9924, 4},
	{0x9930, 4}, {0x9931, 4}, {0x9932, 4}, {0x9933, 4}, {0x9934, 4},
	{0x9940, 4}, {0x9941, 4}, {0x9942, 4}, {0x9943, 4}, {0x9944, 4},
	{0x9950, 4}, {0x9951, 4}, {0x9952, 4}, {0x9953, 4}, {0x9954, 4},
	{0x9960, 4}, {0x9961, 4}, {0x9962, 4}, {0x9963, 4}, {0x9964, 4},

	{0xA010, 3}, {0xA011, 3}, {0xA012, 3}, {0xA013, 3}, {0xA014, 3},
	{0xA020, 3}, {0xA021, 3}, {0xA022, 3}, {0xA023, 3}, {0xA024, 3},
	{0xA110, 3}, {0xA111, 3}, {0xA112, 3}, {0xA113, 3}, {0xA114, 3},
	{0xA120, 3}, {0xA121, 3}, {0xA122, 3}, {0xA123, 3}, {0xA124, 3},
	{0xA130, 3}, {0xA131, 3}, {0xA132, 3}, {0xA133, 3}, {0xA134, 3},
	{0xA140, 3}, {0xA141, 3}, {0xA142, 3}, {0xA143, 3}, {0xA144, 3},
	{0xA150, 3}, {0xA151, 3}, {0xA152, 3}, {0xA153, 3}, {0xA154, 3},
	{0xA160, 3}, {0xA161, 3}, {0xA162, 3}, {0xA163, 3}, {0xA164, 3},

	{0xA410, 3}, {0xA411, 3}, {0xA412, 3}, {0xA413, 3}, {0xA414, 3},
	{0xA420, 3}, {0xA421, 3}, {0xA422, 3}, {0xA423, 3}, {0xA424, 3},
	{0xA510, 3}, {0xA511, 3}, {0xA512, 3}, {0xA513, 3}, {0xA514, 3},
	{0xA520, 3}, {0xA521, 3}, {0xA522, 3}, {0xA523, 3}, {0xA524, 3},
	{0xA530, 3}, {0xA531, 3}, {0xA532, 3}, {0xA533, 3}, {0xA534, 3},
	{0xA540, 3}, {0xA541, 3}, {0xA542, 3}, {0xA543, 3}, {0xA544, 3},
	{0xA550, 3}, {0xA551, 3}, {0xA552, 3}, {0xA553, 3}, {0xA554, 3},
	{0xA560, 3}, {0xA561, 3}, {0xA562, 3}, {0xA563, 3}, {0xA564, 3},

	{0xA810, 3}, {0xA811, 3}, {0xA812, 3}, {0xA813, 3}, {0xA814, 3},
	{0xA820, 3}, {0xA821, 3}, {0xA822, 3}, {0xA823, 3}, {0xA824, 3},
	{0xA910, 3}, {0xA911, 3}, {0xA912, 3}, {0xA913, 3}, {0xA914, 3},
	{0xA920, 3}, {0xA921, 3}, {0xA922, 3}, {0xA923, 3}, {0xA924, 3},
	{0xA930, 3}, {0xA931, 3}, {0xA932, 3}, {0xA933, 3}, {0xA934, 3},
	{0xA940, 3}, {0xA941, 3}, {0xA942, 3}, {0xA943, 3}, {0xA944, 3},
	{0xA950, 3}, {0xA951, 3}, {0xA952, 3}, {0xA953, 3}, {0xA954, 3},
	{0xA960, 3}, {0xA961, 3}, {0xA962, 3}, {0xA963, 3}, {0xA964, 3},

	{0xB010, 4}, {0xB011, 4}, {0xB012, 4}, {0xB013, 4}, {0xB014, 4},
	{0xB020, 4}, {0xB021, 4}, {0xB022, 4}, {0xB023, 4}, {0xB024, 4},
	{0xB110, 4}, {0xB111, 4}, {0xB112, 4}, {0xB113, 4}, {0xB114, 4},
	{0xB120, 4}, {0xB121, 4}, {0xB122, 4}, {0xB123, 4}, {0xB124, 4},
	{0xB130, 4}, {0xB131, 4}, {0xB132, 4}, {0xB133, 4}, {0xB134, 4},
	{0xB140, 4}, {0xB141, 4}, {0xB142, 4}, {0xB143, 4}, {0xB144, 4},
	{0xB150, 4}, {0xB151, 4}, {0xB152, 4}, {0xB153, 4}, {0xB154, 4},
	{0xB160, 4}, {0xB161, 4}, {0xB162, 4}, {0xB163, 4}, {0xB164, 4},

	{0xB410, 4}, {0xB411, 4}, {0xB412, 4}, {0xB413, 4}, {0xB414, 4},
	{0xB420, 4}, {0xB421, 4}, {0xB422, 4}, {0xB423, 4}, {0xB424, 4},
	{0xB510, 4}, {0xB511, 4}, {0xB512, 4}, {0xB513, 4}, {0xB514, 4},
	{0xB520, 4}, {0xB521, 4}, {0xB522, 4}, {0xB523, 4}, {0xB524, 4},
	{0xB530, 4}, {0xB531, 4}, {0xB532, 4}, {0xB533, 4}, {0xB534, 4},
	{0xB540, 4}, {0xB541, 4}, {0xB542, 4}, {0xB543, 4}, {0xB544, 4},
	{0xB550, 4}, {0xB551, 4}, {0xB552, 4}, {0xB553, 4}, {0xB554, 4},
	{0xB560, 4}, {0xB561, 4}, {0xB562, 4}, {0xB563, 4}, {0xB564, 4},

	{0xB810, 4}, {0xB811, 4}, {0xB812, 4}, {0xB813, 4}, {0xB814, 4},
	{0xB820, 4}, {0xB821, 4}, {0xB822, 4}, {0xB823, 4}, {0xB824, 4},
	{0xB910, 4}, {0xB911, 4}, {0xB912, 4}, {0xB913, 4}, {0xB914, 4},
	{0xB920, 4}, {0xB921, 4}, {0xB922, 4}, {0xB923, 4}, {0xB924, 4},
	{0xB930, 4}, {0xB931, 4}, {0xB932, 4}, {0xB933, 4}, {0xB934, 4},
	{0xB940, 4}, {0xB941, 4}, {0xB942, 4}, {0xB943, 4}, {0xB944, 4},
	{0xB950, 4}, {0xB951, 4}, {0xB952, 4}, {0xB953, 4}, {0xB954, 4},
	{0xB960, 4}, {0xB961, 4}, {0xB962, 4}, {0xB963, 4}, {0xB964, 4},

	{0xB210, 4}, {0xB211, 4}, {0xB212, 2}, {0xB213, 2}, {0xB214, 3},
	{0xB310, 2}, {0xB311, 2}, {0xB312, 2}, {0xB313, 2},
	{0xB320, 3}, {0xB321, 3}, {0xB322, 3}, {0xB323, 3},
	{0xB330, 4}, {0xB331, 4}, {0xB332, 4}, {0xB333, 4},
	{0xB340, 4}, {0xB341, 4}, {0xB342, 4}, {0xB343, 4},
	{0xB611, 2}, {0xB612, 2}, {0xB613, 2},
	{0xB621, 2}, {0xB622, 2}, {0xB623, 2},
	{0xB630, 3}, {0xB631, 3}, {0xB632, 3},
	{0xB633, 3}, {0xB634, 2}, {0xB635, 2},
	{0xB640, 2}, {0xB641, 2}, {0xB642, 2}, {0xB643, 2},
	{0xB650, 2}, {0xB651, 2}, {0xB652, 2}, {0xB653, 2},

	{0xC010, 4}, {0xC011, 3},
	{0xC020, 1}, {0xC021, 1}, {0xC022, 1},
	{0xC030, 3}, {0xC031, 3}, {0xC032, 6}, {0xC033, 6}, {0xC034, 6},
	{0xC111, 1}, {0xC112, 1}, {0xC113, 1}, {0xC114, 1}, {0xC115, 1},
	{0xC116, 1}, {0xC117, 2}, {0xC118, 1}, {0xC119, 4}, {0xC11A, 4},
	{0xC211, 2}, {0xC212, 4},

	{0xC510, 4}, {0xC511, 2},
};

static MTDI_ITEM mt_di_set[] = {
	{0x901F, 20}, {0x902F, 20}, {0x911F, 20}, {0x912F, 20},
	{0x913F, 20}, {0x914F, 20}, {0x915F, 20}, {0x916F, 20},
             
	{0x941F, 20}, {0x942F, 20}, {0x951F, 20}, {0x952F, 20},
	{0x953F, 20}, {0x954F, 20}, {0x955F, 20}, {0x956F, 20},
             
	{0x981F, 20}, {0x982F, 20}, {0x991F, 20}, {0x992F, 20},
	{0x993F, 20}, {0x994F, 20}, {0x995F, 20}, {0x996F, 20},
             
	{0xA01F, 15}, {0xA02F, 15}, {0xA11F, 15}, {0xA12F, 15},
	{0xA13F, 15}, {0xA14F, 15}, {0xA15F, 15}, {0xA16F, 15},
             
	{0xA41F, 15}, {0xA42F, 15}, {0xA51F, 15}, {0xA52F, 15},
	{0xA53F, 15}, {0xA54F, 15}, {0xA55F, 15}, {0xA56F, 15},
             
	{0xA81F, 15}, {0xA82F, 15}, {0xA91F, 15}, {0xA92F, 15},
	{0xA93F, 15}, {0xA94F, 15}, {0xA95F, 15}, {0xA96F, 15},
             
	{0xB01F, 20}, {0xB02F, 20}, {0xB11F, 20}, {0xB12F, 20},
	{0xB13F, 20}, {0xB14F, 20}, {0xB15F, 20}, {0xB16F, 20},
             
	{0xB41F, 20}, {0xB42F, 20}, {0xB51F, 20}, {0xB52F, 20},
	{0xB53F, 20}, {0xB54F, 20}, {0xB55F, 20}, {0xB56F, 20},
             
	{0xB81F, 20}, {0xB82F, 20}, {0xB91F, 20}, {0xB92F, 20},
	{0xB93F, 20}, {0xB94F, 20}, {0xB95F, 20}, {0xB96F, 20},
};

static int mt485_di_len(int di)
{
	int i, j, k, di_len = 0;

	if ((di & 0x0f) == 0x0f) { // data set
		for (i = 0; i < sizeof(mt_di_set) / sizeof(MTDI_ITEM); i ++) {
			if (mt_di_set[i].di == di) {
				di_len = mt_di_set[i].len;
				break;
			}
		}
		return di_len;
	}
	for (i = 0; i < sizeof(mt_di) / sizeof(MTDI_ITEM); i ++) {
		if (mt_di[i].di == di) {
			di_len = mt_di[i].len;
			break;
		}
	}
	if (di_len == 0 && (di & 0xc000) == 0xc000) { // check vary di
		i = (di >> 8) & 0x0f; j = (di >> 4) & 0x0f; k = di & 0x0f;
		if (i == 0x03) {
			if (j == 0x01) {
				if (k >= 0 && k <= 4)
					di_len = 1;
			}
			else if (j == 0x02) {
				if (k >= 1 && k <= 0x0e)
					di_len = 3;
			}
			else if (j >= 0x3 && j <= 0x0a) {
				if (k >= 1 && k <= 0x0e)
					di_len = 3;
			}
		}
		else if (i == 0x04) {
			if (j == 0x01) {
				if (k >= 0x01 && k <= 0x0d)
					di_len = 3;
				else if (k == 0x0e)
					di_len = 1;
			}
		}
	}
	return di_len;
}

static int mt485_read_sub(const MTID mtid, BYTE *out_buf, int max_len, int di)
{
	BYTE req[255], resp[255], *ptr;
	BYTE di0, di1, sum;
	int req_len, resp_len, recv_len, i, ok, ret;

	di0 = (di & 0xff) + 0x33;
	di1 = ((di >> 8) & 0xff) + 0x33;
	ptr = req;
	*ptr ++ = 0xfe;
	*ptr ++ = 0xfe;
	*ptr ++ = 0xfe;
	*ptr ++ = 0xfe;
	*ptr ++ = 0x68;
	memcpy(ptr, mtid, MTID_LEN); ptr += MTID_LEN;
	*ptr ++ = 0x68;
	*ptr ++ = 0x01;
	*ptr ++ = 0x02;
	*ptr ++ = di0;
	*ptr ++ = di1;
	sum = check_sum(req + 4, ptr - req - 4);
	*ptr ++ = sum;
	*ptr ++ = 0x16;
	req_len = ptr - req;
	ok = ret = 0;
	for (i = 0; i < 3 && !ok; i ++) {
		//PRINTB("To 485-2: ", req, req_len);
		rs485_lock();
		rs485_write(req, req_len, 400);
		resp_len = rs485_read(resp, sizeof(resp), 1000, 400);
		rs485_unlock();
		//PRINTB("From 485-2: ", resp, resp_len);
		if (resp_len > 0 && (ptr = memchr(resp, 0x68, resp_len)) != NULL) {
			recv_len = resp_len;
			resp_len -= (ptr - resp); // discard prefix
			if (resp_len >= 12 && ptr[0] == 0x68 && ptr[7] == 0x68
				&& ptr[9] + 12 <= resp_len && ptr[ptr[9] + 11] == 0x16) {
				resp_len = ptr[9] + 12; // discard suffix
				if (check_sum(ptr, resp_len - 2) == ptr[resp_len - 2]) {
					ok = 1;
				}
				else {
					PRINTB("From 485-2[Bad]: ", resp, recv_len);
					msleep(200);
				}
			}
		}
	}
	if (ok && resp_len >= 14 && ptr[8] == 0x81 && ptr[10] == di0
		&& ptr[11] == di1) { // normal answer
		if (resp_len - 14 <= max_len) {
			ret = resp_len - 14;
			for (i = 0; i < ret; i ++)
				out_buf[i] = ptr[i + 12] - 0x33;
		}
	}
	return ret;
}

int mt485_read_no_check(const MTID mtid, BYTE *out_buf, int max_len, int di)
{
	return mt485_read_sub(mtid, out_buf, max_len, di);
}

int mt485_read(const MTID mtid, BYTE *out_buf, int max_len, int di)
{
	BYTE buf[255];
	int len, tmp;

	len = mt485_read_sub(mtid, buf, sizeof(buf), di);
	tmp = mt485_di_len(di);
	if (tmp == 0 || len < tmp || max_len < tmp)
		return 0;
	memcpy(out_buf, buf, tmp);
	return tmp;
}

int mt485_read_arg(const MTID mtid, BYTE *out_buf, int max_len, int cnt, ...)
{
	va_list args;
	BYTE buf[255], *ptr;
	int len, idx, di;

	va_start(args, cnt);
	ptr = out_buf;
	for (idx = 0; idx < cnt; idx ++) {
		di = va_arg(args, int);
		len = mt485_read(mtid, buf, sizeof(buf), di);
		if (len > 0 && max_len >= len) {
			memcpy(ptr, buf, len);
			ptr += len;
			max_len -= len;
		}
		else {
			ptr = out_buf;
			break;
		}
	}
	va_end(args);
	return ptr - out_buf;
}

void mt485_sync(void)
{
	BYTE sum, buf[255], *ptr;
	int len;
	struct tm tm;

	rs485_lock();
	my_time(&tm);
	ptr = buf;
	*ptr ++ = 0xfe;
	*ptr ++ = 0xfe;
	*ptr ++ = 0xfe;
	*ptr ++ = 0xfe;
	*ptr ++ = 0x68;
	memcpy(ptr, BR_MTID, MTID_LEN); ptr += MTID_LEN;
	*ptr ++ = 0x68;
	*ptr ++ = 0x08;
	*ptr ++ = 0x06;
	*ptr ++ = bin_to_bcd(tm.tm_sec) + 0x33;
	*ptr ++ = bin_to_bcd(tm.tm_min) + 0x33;
	*ptr ++ = bin_to_bcd(tm.tm_hour) + 0x33;
	*ptr ++ = bin_to_bcd(tm.tm_mday) + 0x33;
	*ptr ++ = bin_to_bcd(tm.tm_mon + 1) + 0x33;
	*ptr ++ = bin_to_bcd(tm.tm_year % 100) + 0x33;
	sum = check_sum(buf + 4, ptr - buf - 4);
	*ptr ++ = sum;
	*ptr ++ = 0x16;
	len = ptr - buf;
	PRINTB("To 485-2: ", buf, len);
	rs485_write(buf, len, 200);
	rs485_unlock();
	msleep(200);
}
