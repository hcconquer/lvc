/*****************************************************************************
 *	C-Plan Concentrator 
 *
 *	(c) Copyright 2006-2007, Shenzhen Kaifa Technology Co.,Ltd. 
 *	All Rights Reserved 
 *		
 *****************************************************************************/

#ifndef _MT_MONTH_H
#define _MT_MONTH_H

#include "typedef.h"

void month_event(EVENT event);

int mt_month(const struct tm *tm);

#endif // _MT_MONTH_H
