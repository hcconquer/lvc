/*****************************************************************************
 *	C-Plan Concentrator 
 *
 *	(c) Copyright 2006-2007, Shenzhen Kaifa Technology Co.,Ltd. 
 *	All Rights Reserved 
 *		
 *****************************************************************************/

#ifndef _T_MT_H
#define _T_MT_H

#include "typedef.h"

#define NULL_LINE		0x00
#define NULL_GRP		0x0000
#define NULL_MS_NO		"\x00\x00\x00\x00\x00\x00"
#define MS_NO_LEN		6

#define FIRST_METER		1
#define METER_INDEX(x)	((x) - FIRST_METER)
#define METER_ORDER(x)	((x) + FIRST_METER)

typedef struct {		/* total 27 bytes without mtidx */
	MTID mtid;
	BYTE mt_type;	/*	D7,D3:	reserved
						D6-D4 (G2G1G0):
							000:	normal 485 meter
							001:	PLC meter
							010:	simple multiple meter
							011:	multiple function meter
							100:	repeater
						D2-D0 (H2H1H0):
							000:	prepay energy meter
							001:	prepay fee meter
							010:	NOT prepay meter
					 */	
	BYTE connect_type;	/*	D2-D7:	reserved
							D0:	0: direct connect
								1: NOT direct connect
							D1:	0: SPMT
								1: PPMT
						 */	
	BYTE tariff;		/*	D7-D5:
								0000: single tariff meter
								0001 - 1000 tariff number be used
								others is reserved
							D4-D0:
								0000: total energy
								0001: reserved
								0010: energy 2
								0011: energy 3
								0100: energy 4
						 */
	BYTE mt_state;		/*	D1-D0:
								00: unknown
								01: phase A
								10: phase B
								11: phase C
							D2 (S):		0: without relay	1: relay
							D3 (H):		0: normal meter		1: load meter
							D4 (K):		0: not selected, 	1: selected
							D5 (F):		0: enable power off	1: disable power off
							D6-D7:		reserved
						 */
	
	WORD line_no;		/* 	D3-D0: number of the third line
						 	D7-D4: number of the second line
				   		 	D11-D8: number of the first line
						 	D15-D12: number of main line
					 	 */
	WORD grp_no;		/* meter box no */
	BYTE collect[MS_NO_LEN];		/* collector no */
	BYTE reserve[128];
} T_MT;

#define T_MT_ROWS_CNT       NBR_REAL_MT

void tmt_lock(void);

void tmt_unlock(void);

void tmt_get_mtid(short mtidx, MTID mtid);

short tmt_get_mtidx(const MTID mtid);

int tmt_get_phase(short mtidx);

void tmt_set_phase(short mtidx, int phase);

int tmt_get_tariff(short mtidx);

int tmt_is_focus(short mtidx);

int tmt_get_type(short mtidx);

int tmt_set_allow_poweroff(short mtidx, int allow);

int tmt_get_allow_poweroff(short mtidx);

int tmt_is_empty(short mtidx);

int tmt_all_is_empty(void);

int tmt_is_selected(short mtidx);

int tmt_get_read_nbr(void);

int tmt_get_select(BYTE *buf, int len, BYTE type);

void tmt_set_select(short mtidx, BYTE select);

int tmt_set(short mtidx, const BYTE *buf, BYTE option);

int tmt_get(short mtidx, BYTE *buf, int skip);

int tmt_first(void);

int tmt_next(short mtidx);

int tmt_next2(short mtidx);

void tmt_set_meter_focus(short mtidx, int focus);

int tmt_get_all_focus_order(BYTE *buf, int len);

int tmt_get_sort_nbr(BYTE *buf, int len);

void tmt_get_lines(short mtidx, BYTE *line, BYTE *line1, BYTE *line2,
	BYTE *line3);

WORD tmt_get_grp_no(short mtidx);

void tmt_get_ms_no(short mtidx, BYTE *ms_no);

void path_idx_to_path(RPT_PATH_IDX *path_idx, RPT_PATH *path);

int add_to_path_idx(short mtidx, RPT_PATH_IDX *path);

int tmt_insert_meter(short idx, const BYTE *buf);

int tmt_modify_meter(short idx, const BYTE *buf);

int tmt_del_meter(short idx, BYTE flag);

int tmt_del_all_meters(BYTE flag);

void tmt_open(void);

void tmt_update(short mtidx, int flush_flag);

void tmt_flush(void);

void tmt_close(void);

#endif /* _T_MT_H */
