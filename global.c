/*
 *  C-Plan Concentrator
 *
 *  Copyright (C) 2006-2007, Shenzhen Kaifa Technology Co.,Ltd.
 *
 *  Author: dajiang wan
 *  Created on: 2006-09-05
 */
#include "global.h"

/* whether to exit the application */
int g_terminated = 0;

/* whether to output the debug info */
int g_silent = 0;

int g_udp = 0; /* connect FEP by UDP */

int g_lcd_type = 0;
