/*****************************************************************************
 *	C-Plan Concentrator 
 *
 *	(c) Copyright 2006-2007, Shenzhen Kaifa Technology Co.,Ltd. 
 *	All Rights Reserved 
 *		
 *****************************************************************************/

#ifndef _DEVICE_H
#define _DEVICE_H

#include "typedef.h"

extern const char *modem_device;
extern const char *modem_lockname;

void cas_open(void);

void cas_close(void);

void rs485_open(void);

void rs485_close(void);

void hhu_open(void);

void hhu_close(void);

void plc_open(void);

void plc_close(void);

void rtc_lock(void);

void rtc_unlock(void);

void set_nonblocking(int fd, int which);

int open_socket(const char *addr, int port, int timeout);

int open_udp(const char *addr, int port);

void close_socket(int fd);

int open_serial(const char *serial, int baud, int char_size, int parity);

void close_serial(int fd);

int write_serial(int fd, const void *buf, int len, int timeout);

int read_serial(int fd, void *buf, int len, int timeout);

void set_baudrate(int fd, int baud);

void open_devices(void);

void close_devices(void);

void handler_power_fail(void);

int test_modem(const char *device_name, const char *lock_name);

int check_modem_relay(const char *device_name, const char *lock_name);

int modem_check(const char *device_name, const char *lock_name, int baudrate);

void self_diag(void (*dev_open)(void), void (*dev_close)(void));

void gpio_lock(void);

void gpio_unlock(void);

void cas_lock(void);

void cas_unlock(void);

void rs485_lock(void);

void rs485_unlock(void);

void hhu_lock(void);

void hhu_unlock(void);

void plc_lock(void);

void plc_unlock(void);

int cas_read(void *buf, int len, int timeout1, int timeout2);

void cas_write(const void *buf, int len, int timeout);

int rs485_read(void *buf, int len, int timeout1, int timeout2);

void rs485_write(const void *buf, int len, int timeout);

int hhu_read(void *buf, int len, int timeout1, int timeout2);

void hhu_write(const void *buf, int len, int timeout);

int plc_read(void *buf, int len, int timeout1, int timeout2);

void plc_write(const void *buf, int len, int timeout);

int at_cmd(int fd, const char *send, char *recv, int max_len,
	int timeout1, int timeout2);

int at_cmd_full(int fd, const char *send, char **recv,
	int *recv_len, int timeout1, int timeout2);

int get_net_type_signal(BYTE *type, BYTE *value, BYTE *level);

void set_net_icon(BYTE type, BYTE signal);

int get_ppp_addr(char *addr, char *dstaddr);

int power_is_fail(void);

void set_eb_reboot(void);

void eb_power_off(void);

void eb_power_on(void);

void set_modem_reboot(void);

void check_eb_reboot(void);

void check_modem_reboot(void);

void device_status_led(int on);

void device_lcd_light(int on);

void device_lcd_reset(void);

unsigned int device_signal_input(void);

#endif /* _DEVICE_H */
