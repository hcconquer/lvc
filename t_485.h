/*****************************************************************************
 *	C-Plan Concentrator 
 *
 *	(c) Copyright 2006-2007, Shenzhen Kaifa Technology Co.,Ltd. 
 *	All Rights Reserved 
 *		
 *****************************************************************************/
#ifndef _T_485_H
#define _T_485_H

#include "typedef.h"

#define MAX_FRZN_CNT		64
#define T_485_ROWS_CNT		64

typedef struct {
	BYTE flag;		/* 0 for not used, others for used */
	BYTE time[5];	/* YYMMDDHHMM, BCD */

	BYTE di_c030[3];
	BYTE di_c031[3];
	BYTE di_c033[6];
	BYTE di_c034[6];

	BYTE di_c111[1];
	BYTE di_c112[1];
	BYTE di_c113[1];
	BYTE di_c114[1];
	BYTE di_c115[1];
	BYTE di_c116[1];
	BYTE di_c117[2];
	BYTE di_c118[1];
	BYTE di_c119[4];
	BYTE di_c11a[4];

	BYTE di_c211[2];
} T485_PARA;

typedef struct {
	BYTE flag;		/* 0 for not used, others for used */
	BYTE time[5];	/* YYMMDDHHMM, BCD */

	BYTE di_901f[20];
	BYTE di_902f[20];
	BYTE di_911f[20];
	BYTE di_912f[20];
	BYTE di_913f[20];
	BYTE di_914f[20];
	BYTE di_915f[20];
	BYTE di_916f[20];

	BYTE di_a01f[15];
	BYTE di_a02f[15];
	BYTE di_a11f[15];
	BYTE di_a12f[15];
	BYTE di_a13f[15];
	BYTE di_a14f[15];
	BYTE di_a15f[15];
	BYTE di_a16f[15];

	BYTE di_b01f[20];
	BYTE di_b02f[20];
	BYTE di_b11f[20];
	BYTE di_b12f[20];
	BYTE di_b13f[20];
	BYTE di_b14f[20];
	BYTE di_b15f[20];
	BYTE di_b16f[20];

	BYTE di_b210[4];
	BYTE di_b211[4];
	BYTE di_b212[2];
	BYTE di_b213[2];
	BYTE di_b214[3];

	BYTE di_b310[2];
	BYTE di_b311[2];
	BYTE di_b312[2];
	BYTE di_b313[2];

	BYTE di_b320[3];
	BYTE di_b321[3];
	BYTE di_b322[3];
	BYTE di_b323[3];

	BYTE di_b330[4];
	BYTE di_b331[4];
	BYTE di_b332[4];
	BYTE di_b333[4];

	BYTE di_b340[4];
	BYTE di_b341[4];
	BYTE di_b342[4];
	BYTE di_b343[4];

	BYTE di_b611[2];
	BYTE di_b612[2];
	BYTE di_b613[2];

	BYTE di_b621[2];
	BYTE di_b622[2];
	BYTE di_b623[2];

	BYTE di_b630[3];
	BYTE di_b631[3];
	BYTE di_b632[3];
	BYTE di_b633[3];
	BYTE di_b634[2];
	BYTE di_b635[2];

	BYTE di_b640[2];
	BYTE di_b641[2];
	BYTE di_b642[2];
	BYTE di_b643[2];

	BYTE di_b650[2];
	BYTE di_b651[2];
	BYTE di_b652[2];
	BYTE di_b653[2];

	BYTE di_c020[1];
	BYTE di_c021[1];
} T485_DATA;

typedef struct {
	BYTE collect;	/* 0 - 64, 0 for not used, 1 - 64 for measured point */
	BYTE comm;		/* D4..D0, comm port, 1 -31,
					   D7..D5, 0 for default baud,
						 1-7 for 600, 1200, 2400, 4800, 7200, 9600, 19200 */
	BYTE prot;		/* 0 - not valid, 1 - DL/T645, 2 - collect device,
					   others is reserved */
	MTID mtid;		/* 0-999999999999 */
	BYTE pass[6];	/* password of meter */
	BYTE flag;		/* D1-D0, 0-3 fraction bits for 1-4
					   D3-D2, 0-3 intergal bits for 4-7
					   D7-D4, tariff count, 1-14 */
	/* above is for AFN04-F10 */

	BYTE base[8];	/* AFN04-F25 */
	BYTE limit[27];	/* AFN04-F26 */
	BYTE frzn_cnt;	/* AFN04-F27, 1..MAX_FRZN_CNT */
	BYTE frzn[2 * MAX_FRZN_CNT];
	BYTE pfl[4];	/* AFN04-F28 */
	T485_PARA para;
	T485_DATA current;	/* the current data */
	BYTE data_index;	/* next index to stored in data field, 0..7 */
	T485_DATA data[8];  /* 4 times in one hour, total 2 hours data */
	T485_DATA day_data;	/* data of day begin */
	BYTE di_9010[4];	/* it is used for meter stop alarm */
	time_t tt_9010;		/* it is used for meter stop alarm */
	BYTE ex_time_flag;	/* time is not correct */
	BYTE ex_vol_flag[3]; /* voltage is not correct */
	BYTE ex_cur_flag[3]; /* current is not correct */
	BYTE ex_pow_flag[3]; /* apparent power is not correct */
	BYTE unused[110];
} T_485;

void t485_lock(void);

void t485_unlock(void);

int t485_all_is_empty(void);

int t485_is_empty(short mtidx);

int t485_read_all_meters(BYTE *out_buf, int max_len);

void t485_get_mtid(short mtidx, MTID mtid);

void t485_get_para(short mtidx, T485_PARA *data);

void t485_get_current(short mtidx, T485_DATA *data);

void t485_get_day_data(short mtidx, T485_DATA *data);

int t485_get_meters_type(void *out_buf, int max_len);

void t485_set_default(int mtidx, const MTID mtid);

int t485_find_frzn(short mtidx, BYTE fn);

int t485_fill_frzn_data(short mtidx, const BYTE *time_stamp, int di_len,
	int offset, BYTE *buf);

int t485_read_real_para(const MTID mtid, BYTE *time_str, T485_PARA *data);

int t485_read_real_data(const MTID mtid, T485_DATA *data);

void t485_check_para_data(const struct tm *tm, short mtidx, const MTID mtid,
	const BYTE *time_str, const T485_PARA *para, const T485_DATA *data);

void t485_set_para(const struct tm *tm, short mtidx, const MTID mtid,
	const T485_PARA *data);

void t485_set_data(const struct tm *tm, short mtidx, const MTID mtid,
	const T485_DATA *data);

int t485_is_ready(void);

int t485_read(BYTE pn, BYTE fn, void *out_buf, int max_len);

int t485_write(BYTE pn, BYTE fn, const void *buf, int len, int *param_len);

void t485_open(void);

void t485_update(short mtidx, int flush_flag);

void t485_flush(void);

void t485_close(void);

#endif /* _T_485_H */

