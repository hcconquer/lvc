#ifndef _THREADS_H
#define _THREADS_H

void *fep_thread(void * arg);

void *cascade_thread(void *arg);

void *hhu_thread(void *arg);

void *plc_thread(void *arg);

void *rs485_thread(void *args);

void *iosys_thread(void *args);

void *poll_thread(void *args);

void cascade_get_baud(void);

void cascade_set_baud(int baud);

void threads_create(void);

void threads_join(void);

void set_reboot_flag(int flag); // flag=0 for all threads, else only the thread

int get_reboot_flag(int flag); // flag=0 for all threads, else only the thread

void init_watchdog(void);

int kill_watchdog(void);

void notify_watchdog(void);

int fep_connected(void);

int disable_thread_sched(int flag);

void check_sched(void);

#endif /* _THREADS_H */
