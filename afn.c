/*
 *  C-Plan Concentrator
 *
 *  Copyright (C) 2006-2007, Shenzhen Kaifa Technology Co.,Ltd.
 *
 *  Author: dajiang wan, jianping zhang
 *  Created on: 2007-02-27
 */
#include "afn.h"
#include "msg_proc.h"
#include "common.h"
#include "misc.h"
#include "threads.h"
#include "f_param.h"
#include "f_alm.h"

typedef int (*GET_FN)(BYTE pn, BYTE fn, BYTE *out_buf, int max_len,
		const BYTE *param, int *param_len);

typedef int (*SET_FN)(BYTE pn, BYTE fn, const BYTE *in_buf, int in_len,
		int *param_len);

static const struct afn_st
{
	BYTE afn;
	GET_FN get_fn;
	SET_FN set_fn;
} afn_arr[] =
{
{ 0x01, NULL, afn01_set_fn },
{ 0x04, NULL, afn04_set_fn },
{ 0x05, NULL, afn05_set_fn },
{ 0x06, afn06_get_fn, NULL },
{ 0x0a, afn0a_get_fn, NULL },
{ 0x0c, afn0c_get_fn, NULL },
{ 0x0d, afn0d_get_fn, NULL },
{ 0x0e, afn0e_get_fn, NULL },
{ 0x0f, NULL, afn0f_set_fn },
{ 0x10, afn10_get_fn, NULL },
{ 0x84, NULL, afn84_set_fn },
{ 0x85, NULL, afn85_set_fn },
{ 0x8a, afn8a_get_fn, NULL },
{ 0x8c, afn8c_get_fn, NULL },
{ 0x8d, afn8d_get_fn, NULL },
{ 0x8e, afn8e_get_fn, NULL }, };

static int all_is_ok(const BYTE *ret, int cnt)
{
	int i;
	for (i = 0; i < cnt; i++)
	{
		if (ret[i])
			return 0;
	}
	return 1;
}

static int all_is_fail(const BYTE *ret, int cnt)
{
	int i;
	for (i = 0; i < cnt; i++)
	{
		if (ret[i] == 0)
			return 0;
	}
	return 1;
}

/* return zero when da is error, else return count of pn */
int da_to_pn(const BYTE *da, BYTE *pn)
{
	int i, j, k, m, n;

	if (da[0] == 0 && da[1] == 0)
	{
		pn[0] = 0;
		return 1;
	}
	else if (da[0] == 0xff && da[1] == 0xff)
	{
		pn[0] = 0xff;
		return 1;
	}
	else
	{
		m = da[1];
		for (i = 0; i < 8; i++)
		{
			if (m & (1 << i))
			{
				k = 0, m = da[0], n = i * 8;
				for (j = 0; j < 8; j++)
				{
					if (m & (1 << j))
						pn[k++] = n + j + 1;
				}
				return k;
			}
		}
		return 0;
	}
}

/* return zero when dt is error, else return count of fn */
int dt_to_fn(const BYTE *dt, BYTE *fn)
{
	int i, j, k, m;

	if (dt[1] > 30)
	{
		return 0;
	}
	else
	{
		j = dt[1] * 8;
		k = 0;
		m = dt[0];
		for (i = 0; i < 8; i++)
		{
			if (m & (1 << i))
				fn[k++] = j + i + 1;
		}
		return k;
	}
}

void pn_to_da(BYTE pn, BYTE *da)
{
	if (pn == 0)
	{
		da[0] = 0;
		da[1] = 0;
	}
	else
	{
		da[0] = 1 << ((pn - 1) % 8);
		da[1] = 1 << ((pn - 1) / 8);
	}
}

void fn_to_dt(BYTE fn, BYTE *dt)
{
	dt[0] = 1 << ((fn - 1) % 8);
	dt[1] = (fn - 1) / 8;
}

int check_pn(const BYTE *pn, int pn_cnt)
{
	return pn_cnt == 1;
}

int check_fn(const BYTE *fn, int fn_cnt)
{
	return fn_cnt != 0;
}

static int update_fn(BYTE *fn_ptr, const BYTE *fn, const BYTE *fail_fn,
		int fn_cnt)
{
	BYTE real_fn[8];
	int i, bit, real_cnt, dt;

	real_cnt = 0;
	for (i = 0; i < fn_cnt; i++)
	{
		if (!fail_fn[i])
			real_fn[real_cnt++] = fn[i];
	}
	if (real_cnt)
	{
		dt = 0;
		for (i = 0; i < real_cnt; i++)
		{
			bit = (real_fn[i] - 1) % 8;
			dt |= (1 << bit);
		}
		fn_ptr[0] = dt; // note: fn_ptr[1] keep the old value
	}
	return real_cnt;
}

static int afn_set(BYTE msa, BYTE afn, SET_FN set_fn, const BYTE *in_buf,
		int in_len, BYTE *out_buf, int max_len, int *data_len, int data_cnt)
{
	int i, tmp, di_cnt, param_len;
	BYTE pn[8], fn[8], pn_cnt, fn_cnt, alm_buf[5];
	BYTE *da_ptr, *da_buf, *dt_ptr, *dt_buf, *err_ptr, *err_buf;
	const BYTE *in_ptr = in_buf;
	BYTE *out_ptr = out_buf;

	da_buf = da_ptr = malloc(data_cnt * 2);
	dt_buf = dt_ptr = malloc(data_cnt * 2);
	err_buf = err_ptr = malloc(data_cnt);
	di_cnt = 0;
	while (in_len >= 4 && data_cnt > 0)
	{
		memcpy(da_ptr, in_ptr, 2);
		in_ptr += 2;
		in_len -= 2;
		memcpy(dt_ptr, in_ptr, 2);
		in_ptr += 2;
		in_len -= 2;
		pn_cnt = da_to_pn(da_ptr, pn);
		fn_cnt = dt_to_fn(dt_ptr, fn);
		if (!check_pn(pn, pn_cnt) || !check_fn(fn, fn_cnt))
		{
			free(da_buf);
			free(dt_buf);
			free(err_buf);
			return 0;
		}
		*err_ptr = 0;
		for (i = 0; i < fn_cnt; i++)
		{
			param_len = in_len;
			tmp = (*set_fn)(pn[0], fn[i], in_ptr, in_len, &param_len);
			if (tmp && *err_ptr == 0)
			{
				*err_ptr = tmp;
			}
			else if (afn == 0x04)
			{
				alm_buf[0] = msa;
				pn_to_da(pn[0], alm_buf + 1);
				fn_to_dt(fn[i], alm_buf + 3);
				fparam_lock();
				falm_lock();
				falm_add_con(0x0e, 3, alm_buf, 5);
				falm_unlock();
				fparam_unlock();
			}
			if (in_len >= param_len)
			{
				in_ptr += param_len;
				in_len -= param_len;
			}
			else
			{
				in_ptr += in_len;
				in_len = 0;
			}
		}
		da_ptr += 2;
		dt_ptr += 2;
		err_ptr++;
		di_cnt++;
		data_cnt--;
	}
	if (in_len != 0)
		return 0;
	memcpy(out_ptr, da_buf, 2);
	out_ptr += 2;
	if (all_is_ok(err_buf, di_cnt))
	{
		memcpy(out_ptr, "\x01\x00", 2);
		out_ptr += 2;
	}
	else if (all_is_fail(err_buf, di_cnt))
	{
		memcpy(out_ptr, "\x02\x00", 2);
		out_ptr += 2;
	}
	else
	{
		memcpy(out_ptr, "\x04\x00", 2);
		out_ptr += 2;
		*out_ptr++ = afn;
		da_ptr = da_buf;
		dt_ptr = dt_buf;
		err_ptr = err_buf;
		for (i = 0; i < di_cnt; i++)
		{
			memcpy(out_ptr, da_ptr, 2);
			out_ptr += 2;
			da_ptr += 2;
			memcpy(out_ptr, dt_ptr, 2);
			out_ptr += 2;
			dt_ptr += 2;
			*out_ptr++ = *err_ptr++;
		}
	}
	*data_len = out_ptr - out_buf;
	free(da_buf);
	free(dt_buf);
	free(err_buf);
	return 1;
}

static int afn_get(BYTE afn, GET_FN get_fn, const BYTE *in_buf, int in_len,
		BYTE *out_buf, int max_len, int *data_len, int data_cnt)
{
	int i, tmp, di_cnt, di_len, param_len;
	BYTE da[2], dt[2], pn[8], fn[8], fail_fn[8], pn_cnt, fn_cnt;
	const BYTE *in_ptr = in_buf;
	BYTE *out_ptr = out_buf, *fn_ptr;

	di_cnt = 0;
	while (in_len >= 4 && max_len >= 4 && data_cnt > 0)
	{
		memcpy(da, in_ptr, 2);
		in_ptr += 2;
		in_len -= 2;
		memcpy(dt, in_ptr, 2);
		in_ptr += 2;
		in_len -= 2;
		pn_cnt = da_to_pn(da, pn);
		fn_cnt = dt_to_fn(dt, fn);
		if (!check_pn(pn, pn_cnt) || !check_fn(fn, fn_cnt))
			return 0;
		memset(fail_fn, 0, sizeof(fail_fn));
		memcpy(out_ptr, da, 2);
		out_ptr += 2;
		max_len -= 2;
		fn_ptr = out_ptr;
		memcpy(out_ptr, dt, 2);
		out_ptr += 2;
		max_len -= 2;
		di_len = 4;
		for (i = 0; i < fn_cnt; i++)
		{
			param_len = in_len;
			tmp = (*get_fn)(pn[0], fn[i], out_ptr, max_len, in_ptr, &param_len);
			if (tmp <= 0)
			{
				fail_fn[i] = 1;
			}
			else
			{
				out_ptr += tmp;
				max_len -= tmp;
				di_len += tmp;
			}
			if (in_len >= param_len)
			{
				in_ptr += param_len;
				in_len -= param_len;
			}
			else
			{
				in_ptr += in_len;
				in_len = 0;
			}
		}
		if (!update_fn(fn_ptr, fn, fail_fn, fn_cnt))
		{ // all fn fails
			out_ptr -= 4;
			max_len += 4;
		}
		else
		{
			*data_len++ = di_len;
			data_cnt--;
			di_cnt++;
		}
	}
	if (in_len != 0)
		return 0;
	if (di_cnt == 0)
	{ // all di fails
		memcpy(out_ptr, da, 2);
		memcpy(out_ptr + 2, dt, 2); // memcpy(out_buf + 2, "\x00\x00", 2);
		*data_len = 4;
		return 1;
	}
	return di_cnt;
}

int is_wakeup_packet(const void *buf, int len)
{
	BYTE fn[8], fn_cnt;
	const BYTE *dt_ptr, *afn;

	if (check_packet(buf, len) && len >= 20)
	{
		afn = buf + 12;
		dt_ptr = buf + 16;
		fn_cnt = dt_to_fn(dt_ptr, fn);
		if (*afn == 5 && fn_cnt == 1 && fn[0] == 38)
			return 1;
	}
	return 0;
}

/*
 * msa		MSA,主站地址
 * afn			AFN
 * pass
 * in_buf		链路用户数据
 * in_len		链路用户数据的长度
 * out_buf	要返回的链路用户数据
 * max_len
 * data_len
 * data_cnt
 *
 * */
int afn_proc(BYTE msa, BYTE afn, const BYTE *pass, const BYTE *in_buf,
		int in_len, BYTE *out_buf, int max_len, int *data_len, int data_cnt)
{
	BYTE pw[3];
	int i;

	fparam_get_value(FPARAM_AFN04_F5, pw, 3);
	if (pass != NULL && !(pass[0] == pw[1] && pass[1] == pw[2]))
	{
		memcpy(out_buf, in_buf, 2); // da
		fn_to_dt(2, out_buf + 2); // dt
		data_len[0] = 4;
		return 1;
	}
	for (i = 0; i < sizeof(afn_arr) / sizeof(struct afn_st); i++)
	{
		if (afn_arr[i].afn == afn)
		{
			if (afn_arr[i].get_fn != NULL)
			{
				return afn_get(afn, afn_arr[i].get_fn, in_buf, in_len, out_buf,
						max_len, data_len, data_cnt);
			}
			else if (afn_arr[i].set_fn != NULL)
			{
				return afn_set(msa, afn, afn_arr[i].set_fn, in_buf, in_len,
						out_buf, max_len, data_len, data_cnt);
			}
		}
	}
	return 0;
}
