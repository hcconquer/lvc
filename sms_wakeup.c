/*
 *  C-Plan Concentrator
 *
 *  Copyright (C) 2006-2007, Shenzhen Kaifa Technology Co.,Ltd.
 *
 *  Provide some functions to read mobile short message.
 *
 *  Author: dajiang wan
 *  Created on: 2006-09-05
 */

#include "common.h"
#include "device.h"
#include "global.h"
#include "afn.h"

#define CONVERTDEC(c)		((c)-'0')
#define CONVERTHEX_alpha(c)	(((c)>='A'&&(c)<='F')?((c)-'A'+10):((c)-'a'+10))
#define CONVERTHEX(c)		(isdigit(c)?((c)-'0'):CONVERTHEX_alpha(c))

static int get_num(const void *buf, int *num)
{
	unsigned int sign = 0, val = 0;
	const BYTE *ptr = buf;

	while (isspace(*ptr))
		ptr ++;
	if (ptr[0] == '0' && (ptr[1] == 'x' || ptr[1] == 'X')) {
		ptr += 2;
		while (isxdigit(*ptr)) {
			val = (val << 4) + CONVERTHEX(*ptr);
			ptr ++;
		}
	}
	else {
		if (ptr[0] == '-' || ptr[0] == '+') {
			if (ptr[0] == '-')
				sign = 1;
			ptr ++;
		}
		while (isdigit(*ptr)) {
			val = val * 10 + CONVERTDEC(*ptr);
			ptr ++;
		}
	}
	*num = (sign) ? -val : val;
	return ptr - (const BYTE *)buf;
}

static int get_hex(const void *in_buf, void *out_buf, int out_len)
{
	const BYTE *in_ptr = in_buf;
	BYTE *out_ptr = out_buf;
	BYTE high, low, cnt;

	high = low = cnt = 0;
	while (out_len > 0 && isxdigit(*in_ptr)) {
		if (cnt == 0) {
			high = CONVERTHEX(*in_ptr);
			cnt ++;
		}
		else {
			low = CONVERTHEX(*in_ptr);
			cnt = 0;
			*out_ptr++ = (high << 4) + low; out_len --;
		}
		in_ptr ++;
	}
	return (out_len == 0) ? (in_ptr - (BYTE *)in_buf) : 0;
}

static int get_addr(const void *in_buf, void *out_buf, int *out_len, int type)
{
	const BYTE *in_ptr = in_buf;
	BYTE *out_ptr = out_buf;
	int x, y;

	// Lenth of address, Type-of-address
	if ((x = get_hex(in_ptr, out_ptr, 2)) <= 0)
		return 0;
	if (type == 0) // SMSC address
		y = out_ptr[0] - 1;
	else // Sender address
		y = (out_ptr[0] + 1) / 2;
	in_ptr += x; out_ptr += 2;
	// Phone number
	if ((x = get_hex(in_ptr, out_ptr, y)) <= 0)
		return 0;
	in_ptr += x; out_ptr += y;
	*out_len = out_ptr - (BYTE *)out_buf;
	return in_ptr - (BYTE *)in_buf;
}

static void get_phone(BYTE *phone, const void *buf, int len)
{
	const char *str = "0123456789ABCDEF";
	BYTE *tmp = phone;
	const BYTE *ptr = buf;
	int i, j, k, val, high, low;

	j = *ptr; ptr += 2; // length of address, type-of-address
	if (j & 0x01) // odd
		k = j + 1;
	else
		k = j;
	for (i = 0; i < k / 2; i ++) {
		val = * ptr ++;
		high = str[(val >> 4) & 0x0f];
		low = str[val & 0x0f];
		*tmp++ = low;
		*tmp++ = high;
	}
	phone[j] = 0x0;
}

static void get_stamp(BYTE *stamp, const void *buf, int len)
{
	int i;
	BYTE ch, high, low;
	const BYTE *ptr = buf;

	for (i = 0; i < len; i ++) {
		ch = *ptr ++;
		high = (ch >> 4) & 0x0f;
		low = ch & 0x0f;
		ch = (low << 4) | high;
		*stamp ++ = bcd_to_bin(ch);
	}
}

static void c8_to_c7(const BYTE *in_buf, int in_len, void *out_buf)
{
	int i, j, k, b;
	int in_val, out_val;
	BYTE *out_ptr = out_buf;

	k = out_val = 0;
	for (i = 0; i < in_len; i ++) {
		in_val = *in_buf ++;
		for (j = 0; j < 8; j ++) {
			b = (in_val >> j) & 0x01;
			out_val = out_val + (b << k);
			if (++k == 7) {
				*out_ptr ++ = out_val;
				k = out_val = 0;
			}
		}
	}
	if (k != 0) *out_ptr ++ = out_val;
}

static int get_data(const void *buf, BYTE protocol, BYTE encode,
	int data_len, void *data)
{
	int type, x, y;
	BYTE tmp[256];

	type = (encode >> 2) & 0x03;
	if (type  == 0x00) { // 7 bit
		y = (data_len * 7 - 1) / 8 + 1;
		if ((x = get_hex(buf, tmp, y)) <= 0)
			return 0;
		c8_to_c7(tmp, y, data);
		return x;
	}
	else {
		return get_hex(buf, data, data_len);
	}
}

static int get_pdu(const void *buf, void *sender, void *stamp,
	void *data, int *data_len)
{
	const BYTE *in_ptr = buf;
	BYTE tmp[256];
	int x, y;

	*((BYTE* )sender) = 0;
	*((BYTE *)stamp) = 0;
	*data_len = 0;
	// SMSC information
	if ((x = get_addr(in_ptr, tmp, &y, 0)) <= 0)
		return 0;
	in_ptr += x;
	PRINTB("smsc: ", tmp, y);
	// First octet of deliver
	if ((x = get_hex(in_ptr, tmp, 1)) <= 0)
		return 0;
	in_ptr += x;
	PRINTB("first octet: ", tmp, 1);
	// Sender information
	if ((x = get_addr(in_ptr, tmp, &y, 1)) <= 0)
		return 0;
	in_ptr += x;
	PRINTB("sender: ", tmp, y);
	get_phone(sender, tmp, y);
	// Protocol identifier(1), Data coding scheme(1),
	// Time stamp(7), User data length
	if ((x = get_hex(in_ptr, tmp, 10)) <= 0)
		return 0;
	in_ptr += x;
	PRINTB("other: ", tmp, 10);
	get_stamp(stamp, tmp + 2, 7);
	*data_len = tmp[9];
	// User data
	if ((x = get_data(in_ptr, tmp[0], tmp[1], *data_len, data)) <= 0)
		return 0;
	in_ptr += x;
	PRINTB("user data: ", data, *data_len);
	return in_ptr - (const BYTE *)buf;
}

static int get_sms(const void *buf, int *idx, int *stat,
	void *sender, void *stamp, void *data, int *data_len)
{
	int tmp;
	const char *ptr;
	// AT+CMGL=4 command return
	// Siemens MC35i modem return like this
	// PDU mode AT+CMGF=0 and command successful
	// +CMGL: <index>,<stat>,[<alpha>],<length>
	// <pdu>
	// [...]
	// OK
	// another modem given by Enel return like this
	// PDU mode AT+CMGF=0 and command successful
	// +CMGL: <index>,<stat>,<length>
	// <pdu>
	// [...]
	// OK
	if ((ptr = strstr(buf, "+CMGL: ")) != NULL) {
		ptr += 7;
		if ((tmp = get_num(ptr, idx)) <= 0)
			return 0;
		ptr += tmp;
		if (*ptr != ',')
			return 0;
		ptr ++;
		if ((tmp = get_num(ptr, stat)) <= 0)
			return 0;
		ptr += tmp;
		if (*ptr != ',')
			return 0;
		ptr ++;
		while (*ptr != '\n' && *ptr != 0x0)
			ptr ++;
		if (*ptr != '\n')
			return 0;
		ptr ++;
		PRINTF("sms list, idx: %d, status: %d\n", *idx, *stat);
		if ((tmp = get_pdu(ptr, sender, stamp, data, data_len)) > 0)
			ptr += tmp;
		while (*ptr != '\n' && *ptr != 0x0)
			ptr ++;
		return ptr - (const char *)buf;
	}
	return 0;
}

static int check_sms(const void *sender, const void *stamp,
	const void *data, int data_len, const void *phone)
{
	int offset, chk1, chk2;
	time_t tt, now;
	double diff;
	struct tm tm;
	const BYTE *ptr = stamp;

	tm.tm_isdst = -1;
	tm.tm_year = (ptr[0] < 70) ? ptr[0] + 100 : ptr[0];
	tm.tm_mon = ptr[1] - 1;
	tm.tm_mday = ptr[2];
	tm.tm_hour = ptr[3];
	tm.tm_min = ptr[4];
	tm.tm_sec = ptr[5];
	tt = mktime(&tm);
	offset = ptr[6] & 0x00ff;
	if (offset & 0x80) offset = -(offset & 0x7f);
	tt = tt + offset * (15 * 60);
	now = time(NULL);
	diff = difftime(tt, now);
	// time must be in 24 hours
	chk1 = abs(diff) < 24 * 60 * 60;
	/* phone number may have country and city prefix */
	chk2 = strstr(sender, phone) || strstr(phone, sender);
	PRINTF("\n\tmessage send time: 0x%08x, current time: 0x%08x, time is %s\n",
		tt, now, chk1 ? "valid" : "invalid");
	PRINTF("\n\tsender: %s, master station: %s, phone number %s\n",
		sender, phone, chk2 ? "matched" : "not matched");
	if (chk1 && chk2)
		return is_wakeup_packet(data, data_len);
	return 0;
}

int sms_wakeup(const void *device, const void *lock_name, int baudrate,
	const void *center, const void *phone)
{
	int ret = 0, fd, lock_pid, tmp, idx, stat, data_len = 0;
	char cmd[1024], resp[1024], sender[256], stamp[256], data[256];
	char *save, *ptr;

	if (!write_lock_file(lock_name)) {
		lock_pid = read_lock_file(lock_name);
		PRINTF("%s is locked by %d, pppd cannnot lock the device\n",
			lock_name, lock_pid);
		return 0;
	}
	fd = open_serial(device, baudrate, 8, 0);
	if (fd >= 0) {
		tcflush(fd, TCIOFLUSH);
		if (!g_terminated)
			at_cmd(fd, "ATE0&C1&D2&S0\r", resp, sizeof(resp), 1000, 500);
		if (!g_terminated && center) {
			snprintf((char *)cmd, sizeof(cmd), "AT+CSCA=%s\r", (char *)center);
			at_cmd(fd, cmd, resp, sizeof(resp), 1000, 500);
		}
		if (!g_terminated)
			at_cmd(fd, "AT+CMGF=0\r", resp, sizeof(resp), 1000, 500);
		if (!g_terminated
			&& at_cmd_full(fd, "AT+CMGL=4\r", &save, &tmp, 2000, 500) > 0) {
			ptr = save;
			PRINTF("SMS:%s\n", ptr);
			while (!g_terminated && (tmp = get_sms(ptr, &idx, &stat, sender,
				stamp, data, &data_len)) > 0) {
				ptr += tmp;
				if (check_sms(sender,stamp,data,data_len,phone)) {
					PRINTF("sms wakeup ok\n");
					ret = 1;
				}
				snprintf((char *)cmd, sizeof(cmd), "AT+CMGD=%d\r", idx);
				at_cmd(fd, cmd, resp, sizeof(resp), 1000, 500);
			}
			free(save);
		}
	}
	close_serial(fd);
	remove(lock_name);
	return ret;
}
