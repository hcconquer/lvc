/*
 *  C-Plan Concentrator
 *
 *  Copyright (C) 2006-2007, Shenzhen Kaifa Technology Co.,Ltd.
 *
 *  Author: dajiang wan
 *  Created on: 2008-01-08
 */
#include "afn.h"
#include "f_param.h"
#include "err_code.h"
#include "common.h"
#include "misc.h"
#include "mt_sync.h"

int afn05_set_fn(BYTE pn, BYTE fn, const BYTE *buf, int len, int *param_len)
{
	BYTE zero = 0, one = 1;
	int tmp;

	PRINTF("AFN=05H, Pn=%d, Fn=%d\n", pn, fn);
	switch (fn) {
	case 1:	// TODO
	case 2:	// TODO
		return ERR_BAD_VALUE;
	case 9:	// TODO
	case 10:	// TODO
	case 11:	// TODO
	case 12:	// TODO
	case 15:	// TODO
	case 16:	// TODO
		return ERR_BAD_VALUE;
	case 17:	// TODO
	case 18:	// TODO
	case 19:	// TODO
	case 20:	// TODO
	case 23:	// TODO
	case 24:	// TODO
		return ERR_BAD_VALUE;
	case 25:	// TODO 0
	case 26:	// TODO 0
		return ERR_BAD_VALUE;
	case 27:
		fparam_lock();
		fparam_write(FPARAM_ALLOW_REMOTE, &one, 1, &tmp);
		fparam_unlock();
		*param_len = 0;
		return ERR_OK;
	case 28:	// TODO 0
		return ERR_BAD_VALUE;
	case 29:
		fparam_lock();
		fparam_write(FPARAM_ALLOW_SPONT, &one, 1, &tmp);
		fparam_unlock();
		*param_len = 0;
		return ERR_OK;
	case 31:
		if (len < 6)
			return ERR_BAD_VALUE;
		set_date_time(buf, len);
		sync_event(EVENT_DAY_CHANGE);
		*param_len = 6;
		return ERR_OK;
	case 32:	// TODO 0
		return ERR_BAD_VALUE;
	case 33:	// TODO 0
	case 34:	// TODO 0
		return ERR_BAD_VALUE;
	case 35:
		fparam_lock();
		fparam_write(FPARAM_ALLOW_REMOTE, &zero, 1, &tmp);
		fparam_unlock();
		*param_len = 0;
		return ERR_OK;
	case 36:	// TODO 0
		return ERR_BAD_VALUE;
	case 37:
		fparam_lock();
		fparam_write(FPARAM_ALLOW_SPONT, &zero, 1, &tmp);
		fparam_unlock();
		*param_len = 0;
		return ERR_OK;
	case 38:	// TODO 0
		return ERR_BAD_VALUE;
	case 41:	// TODO
	case 42:	// TODO
		return ERR_BAD_VALUE;
	default:
		return ERR_BAD_VALUE;
	}
}
