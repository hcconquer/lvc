/*****************************************************************************
 *	C-Plan Concentrator 
 *
 *	(c) Copyright 2006-2007, Shenzhen Kaifa Technology Co.,Ltd. 
 *	All Rights Reserved 
 *		
 *****************************************************************************/

#ifndef _MT_LOAD_H
#define _MT_LOAD_H

#include "typedef.h"

void load_event(EVENT event);

int mt_load(const struct tm *tm);

#endif // _MT_LOAD_H
