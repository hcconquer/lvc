/*
 * C-Plan Concentrator
 *
 * Copyright (C) 2008, Shenzhen Kaifa Technology Co.,Ltd.
 *
 * Author: jianping zhang
 * Created on: 2008-01-18
 */
#include "common.h"
#include "device.h"
#include "global.h"
#include "threads.h"
#include "menu.h"
#include "menu_str.h"
#include "input.h"
#include "t_485.h"
#include "mt_485.h"
#include "display.h"
#include "f_param.h"
#include "f_alm.h"
#include "lvc_test.h"

static int scroll_interval = 8;

static void refresh_scroll_screen(const void *screen, int row1, int row2)
{
	const BYTE *ptr;
	int row;

	ptr = screen + (row1 - 1) * MAX_SCREEN_COL;
	for (row = row1; row <= row2; row ++) {
		lcd_show_string(row, 1, MAX_SCREEN_COL, ptr);
		ptr += MAX_SCREEN_COL;
	}
}

static void update_scroll_screen(void *screen, int row,
	const struct scroll_screen *desc, const T485_DATA *current)
{
	char buf[128], *ptr;
	const char *data;
	int col, len, data_len;
	MTID mtid;

	t485_get_mtid(0, mtid);
	data = ((const char *)current) + desc->data_off;
	if (memcmp(mtid, INVALID_MTID, MTID_LEN) == 0 || current->flag == 0
		|| !t485_is_ready())
		data_len = 0;
	else
		data_len = desc->data_len;
	len = format_di_str(buf, sizeof(buf), desc->di, data, data_len);
	ptr = screen + (row - 1) * MAX_SCREEN_COL;
	memcpy(ptr, buf, len); ptr += len;
	for (col = len; col < MAX_SCREEN_COL; col ++)
		*ptr ++ = ' ';
}

static void process_scroll_show(void)
{
	struct scroll_screen desc[MAX_SCREEN_ROW];
	BYTE scr_idx, desc_cnt, desc_idx, row;
	BYTE screen[MAX_SCREEN_COL * MAX_SCREEN_ROW];
	long last_uptime;
	T485_DATA current;
 
	lcd_clean_workspace();
	lcd_show_string(2, 1, strlen(c_process_status[0]), c_process_status[0]);
	lcd_show_arrow(0, 0, 0, 0);
	last_uptime = 0;
	scr_idx = 0;
	lcd_update_info_enable(1);
	lcd_update_comm_info(-1);
	while (!g_terminated && lcd_mode_get() == 0) {
		notify_watchdog();
		if (uptime() - last_uptime >= scroll_interval) {
			while ((desc_cnt = get_scroll_screen(scr_idx, desc,
				sizeof(desc) / sizeof(struct scroll_screen))) == 0)
				scr_idx ++;
			memset(screen, ' ', sizeof(screen));
			t485_lock();
			t485_get_current(0, &current);
			t485_unlock();
			row = 2;
			for (desc_idx = 0; desc_idx < desc_cnt; desc_idx ++) {
				update_scroll_screen(screen, row, desc + desc_idx, &current);
				row ++;
			}
			refresh_scroll_screen(screen, 2, MAX_SCREEN_ROW - 2);
			last_uptime = uptime();
			scr_idx ++;
		}
		if (key_getch() != KEY_NONE)
			lcd_mode_set(1);
		msleep(100);
	}
	lcd_update_info_enable(0);
}

void *iosys_thread(void *arg)
{
	ITEMS_MENU im_main;
	int mode, lcd_ok = 0;
	
	while (!g_terminated) {
		notify_watchdog();
		if (!lcd_ok) {
			if (!lcd_is_ok()) {
				wait_delay(4000);
				continue;
			}
			lcd_ok = 1;
			lcd_update_info(c_info_sys_init_str);
			while (key_getch() != KEY_NONE)
				;
			main_menu_init(&im_main);
		}
		mode = lcd_mode_get();
		if (mode == 0) { // scrolling show
			process_scroll_show();
		}
		else if (mode == 3) {
			if (disable_thread_sched(1) && !g_terminated)
				lvc_test();
			disable_thread_sched(0);
			lcd_mode_set(0);
		}
		else { // menu operate
			if (process_items(&im_main, c_info_main_menu_str, 1) == KEY_ESC)
				lcd_mode_set(0);
		}
		if (!g_terminated && get_reboot_flag(1))
			g_terminated = 1;
	}
	lcd_clear_screen();
	return NULL;
}

void *poll_thread(void *arg)
{
	int i, param_len;
	BYTE val, tmp, old_val[2], buf[2];

	while (!g_terminated) {
		notify_watchdog();
		check_sched();
		if (lcd_is_ok()) {
			if (update_head_is_enable())
				lcd_update_head_info();
		}
		val = device_signal_input();
		fparam_lock();
		falm_lock();
		fparam_get_value(FPARAM_AFN0C_F9, old_val, 2);
		if (val != old_val[0]) {
			PRINTF("YX1:%d->%d,YX2:%d->%d,YX3:%d->%d,YX4:%d->%d,Door:%d->%d\n",
				(old_val[0] >> 0) & 1, (val >> 0) & 1,
				(old_val[0] >> 1) & 1, (val >> 1) & 1,
				(old_val[0] >> 2) & 1, (val >> 2) & 1,
				(old_val[0] >> 3) & 1, (val >> 3) & 1,
				(old_val[0] >> 4) & 1, (val >> 4) & 1
			);
			tmp = old_val[1];
			for (i = 0; i < 5; i ++) {
				if ((old_val[0] & (1 << i)) != (val & (1 << i))) {
					tmp |= (1 << i);
				}
			}
			buf[0] = old_val[0] ^ val;
			buf[1] = val;
			old_val[0] = val;
			old_val[1] = tmp;
			fparam_set_value(FPARAM_AFN0C_F9, old_val, 2, &param_len);
			falm_add_con(0x0e, 4, buf, 2);
		}
		falm_unlock();
		fparam_unlock();
		if (!g_terminated)
			msleep(800);
	}
	return NULL;
}
