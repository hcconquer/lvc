/*****************************************************************************
 *	C-Plan Concentrator 
 *
 *	(c) Copyright 2006-2007, Shenzhen Kaifa Technology Co.,Ltd. 
 *	All Rights Reserved 
 *		
 *****************************************************************************/
#ifndef _F_485_H
#define _F_485_H

#include "typedef.h"
#include "t_485.h"

typedef struct {
	BYTE di_9010[4];
	BYTE di_9020[4];
	BYTE di_9110[4];
	BYTE di_9120[4];

	BYTE di_b611[2];
	BYTE di_b612[2];
	BYTE di_b613[2];

	BYTE di_b621[2];
	BYTE di_b622[2];
	BYTE di_b623[2];

	BYTE di_b630[3];
	BYTE di_b631[3];
	BYTE di_b632[3];
	BYTE di_b633[3];

	BYTE di_b640[2];
	BYTE di_b641[2];
	BYTE di_b642[2];
	BYTE di_b643[2];

	BYTE di_b650[2];
	BYTE di_b651[2];
	BYTE di_b652[2];
	BYTE di_b653[2];

} F485_LOAD_REC;

typedef struct {
	BYTE di_901f[20];
	BYTE di_902f[20];
	BYTE di_911f[20];
	BYTE di_912f[20];
	BYTE di_913f[20];
	BYTE di_914f[20];
	BYTE di_915f[20];
	BYTE di_916f[20];

	BYTE di_a01f[20];
	BYTE di_a02f[20];
	BYTE di_a11f[20];
	BYTE di_a12f[20];
	BYTE di_a13f[20];
	BYTE di_a14f[20];
	BYTE di_a15f[20];
	BYTE di_a16f[20];

	BYTE di_b01f[20];
	BYTE di_b02f[20];
	BYTE di_b11f[20];
	BYTE di_b12f[20];
	BYTE di_b13f[20];
	BYTE di_b14f[20];
	BYTE di_b15f[20];
	BYTE di_b16f[20];

	BYTE di_b310[2];
	BYTE di_b311[2];
	BYTE di_b312[2];
	BYTE di_b313[2];

	BYTE di_b320[3];
	BYTE di_b321[3];
	BYTE di_b322[3];
	BYTE di_b323[3];

	BYTE di_b330[4];
	BYTE di_b331[4];
	BYTE di_b332[4];
	BYTE di_b333[4];

	BYTE di_b340[4];
	BYTE di_b341[4];
	BYTE di_b342[4];
	BYTE di_b343[4];

} F485_DAY_REC;

typedef struct {
	BYTE di_901f[20];
	BYTE di_902f[20];
	BYTE di_911f[20];
	BYTE di_912f[20];
	BYTE di_913f[20];
	BYTE di_914f[20];
	BYTE di_915f[20];
	BYTE di_916f[20];

	BYTE di_a01f[20];
	BYTE di_a02f[20];
	BYTE di_a11f[20];
	BYTE di_a12f[20];
	BYTE di_a13f[20];
	BYTE di_a14f[20];
	BYTE di_a15f[20];
	BYTE di_a16f[20];

	BYTE di_b01f[20];
	BYTE di_b02f[20];
	BYTE di_b11f[20];
	BYTE di_b12f[20];
	BYTE di_b13f[20];
	BYTE di_b14f[20];
	BYTE di_b15f[20];
	BYTE di_b16f[20];

} F485_MON_REC;

typedef struct {
	BYTE			flag;		/* 0 for not used, >= 1 for used */
	BYTE			time[5];	/* year, month, day, hour, minute */
	MTID			mtid;
	F485_LOAD_REC	rec;
} F485_LOAD;

typedef struct {
	BYTE			flag;		/* 0 for not used, >= 1 for used */
	BYTE			time[3];	/* year, month, day */
	MTID			mtid;
	F485_DAY_REC	rec;
} F485_DAY;

typedef struct {
	BYTE			flag;		/* 0 for not used, >= 1 for used */
	BYTE			time[2];	/* year, month */
	MTID			mtid;
	F485_MON_REC	rec;
} F485_MON;

#define F485_LOAD_CNT		(T_485_ROWS_CNT * 96)
#define F485_DAY_CNT		(T_485_ROWS_CNT * 30)
#define F485_MON_CNT		(T_485_ROWS_CNT * 6)

typedef struct {
	F485_LOAD load[F485_LOAD_CNT];
	F485_DAY day[F485_DAY_CNT];
	F485_MON mon[F485_MON_CNT];
} F_485;

void f485_lock(void);

void f485_unlock(void);

void f485_open(void);

int f485_read_load(F485_LOAD_REC *load_rec, const MTID mtid,
	BYTE year, BYTE mon, BYTE day, BYTE hour, BYTE min);

int f485_read_day(F485_DAY_REC *day_rec, const MTID mtid,
	BYTE year, BYTE mon, BYTE day);

int f485_read_mon(F485_MON_REC *mon_rec, const MTID mtid,
	BYTE year, BYTE mon);

void f485_set_data(const struct tm *tm, const MTID mtid,
	const T485_DATA *data);

void f485_flush(void);

void f485_close(void);

#endif /* _F_485_H */
