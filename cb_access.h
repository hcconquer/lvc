/*****************************************************************************
 *	C-Plan Concentrator 
 *
 *	(c) Copyright 2006-2007, Shenzhen Kaifa Technology Co.,Ltd. 
 *	All Rights Reserved 
 *		
 *****************************************************************************/

#ifndef _CB_ACCESS_H
#define _CB_ACCESS_H

#include "typedef.h"

int get_cb_softver(BYTE *buf);

int reset_cb_state(void);

int get_cb_state(BYTE *buf);

int get_cb_data(const BYTE *in_buf, int in_len, BYTE *out_buf, int max_out,
	int *out_len);

#endif /* _CB_ACCESS_H */
