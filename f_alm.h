/*****************************************************************************
 *	C-Plan Concentrator 
 *
 *	(c) Copyright 2006-2007, Shenzhen Kaifa Technology Co.,Ltd. 
 *	All Rights Reserved 
 *		
 *****************************************************************************/

#ifndef _F_ALM_H
#define _F_ALM_H

#include "typedef.h"

#define F_ALM_CNT				2048

#define ALARM_CON_MEMO_ERR		1  // con main board memory failure
#define ALARM_CON_CLOCK_ERR		2  // con clock failure
#define ALARM_CON_PLC_ERR		3  // plc modem failure
#define ALARM_CON_485_ERR		4  // 485 failure
#define ALARM_CON_LCD_ERR		5  // display board failure
#define ALARM_CON_READ_ERR		6  // read concentrator failure

typedef struct {
	struct timeval tv;	/* occur time */
	BYTE flag;			/* 0 for not used, 1 for not sent, 2 for sent */
	BYTE type;			/* 0-1 for total-meter event(1-normal,0-important)
						 * 2-3 for user-meter event(3-normal,2-important) */
	BYTE ec;			/* error count */
	BYTE size;			/* length of data */
	BYTE data[20];		/* event data buffer */
} F_ALM;

void falm_lock(void);

void falm_unlock(void);

void falm_open(void);

void falm_close(void);

void falm_flush(void);

void falm_get_ec(BYTE afn, BYTE *ec);

void falm_add_meter(short mtidx, BYTE erc, int fill, const void *buf, int len);

void falm_add_meter_alm7(short mtidx, BYTE erc);

void falm_add_485(short mtidx, BYTE erc, const void *buf, int len);

void falm_add_con(BYTE afn, BYTE erc, const void *buf, int len);

int falm_changed(BYTE afn, BYTE *ec);

int falm_get(BYTE *buf, int len, BYTE afn, BYTE fn, BYTE pm, BYTE pn);

int falm_get_idx(int mt_erc, int new_erc, int *erc_idx, int max1, BYTE *erc_code, int max2);

int falm_get_one(int idx, F_ALM *alm);

void falm_spont(int que_idx, BYTE *buf, int len);

int falm_get_stat_cnt(int *cnt, int max);

#endif /* _F_ALM_H */
