/*****************************************************************************
 *	C-Plan Concentrator 
 *
 *	(c) Copyright 2006-2007, Shenzhen Kaifa Technology Co.,Ltd. 
 *	All Rights Reserved 
 *		
 *****************************************************************************/

#ifndef _F_PARAM_H
#define _F_PARAM_H

#include "typedef.h"

#define FPARAM_COUNT				128
#define FPARAM_LENGTH				3072
#define FPARAM_FLAG_WRITE			(1 << 0)
#define FPARAM_FLAG_READ			(1 << 1)
#define FPARAM_FLAG_HIGH			(1 << 2)
#define FPARAM_FLAG_LOW				(1 << 3)

#define FPARAM_IP_PORT_PRI			0x8010	// 6 bytes, 4 for IP address, 2 for port
#define FPARAM_IP_PORT_SEC			0x8011	// 6 bytes, 4 for IP address, 2 for port
#define FPARAM_IP_PORT_GW			0x8014	// 6 bytes, 4 for IP address, 2 for port
#define FPARAM_IP_PORT_PROXY		0x8012	// 6 bytes, 4 for IP address, 2 for port
#define FPARAM_APN					0x8015	// 16 bytes
#define FPARAM_VPN_USER				0x8019	// 32 bytes
#define FPARAM_VPN_PASS				0x801A	// 32 bytes
#define FPARAM_ALLOW_REMOTE			0x801C	// 1 byte, non-zero allow remote talk
#define FPARAM_ALLOW_SPONT			0x801D	// 1 byte, non-zero allow spont alarm to FEP
#define FPARAM_USER_PASSWD			0x8021	// 3 bytes, password of menu in the LCD
#define FPARAM_ADMIN_PASSWD			0x8022	// 3 bytes, administrator password

#define FPARAM_FDAY_READ_TIME		0x8815	// 1 byte, read daily data, format hh
#define FPARAM_FLOAD_READ_TIME		0x8816	// 1 byte, read pload interval, 0-5
#define FPARAM_RELAY_METHOD			0x8817	// 1 byte, 0 for use auto repeater, 1 for use fix repeater
#define FPARAM_CON_CASCADE			0x881D	// 16 bytes, 4 slave-concentrators address
#define FPARAM_FMON_READ_TIME		0x8820	// 2 bytes, read monthly data, format hhdd

#define FPARAM_CON_ADDRESS			0x9001	// 4 bytes, 2 for area, 2 for station
#define FPARAM_RELAY_DEPTH			0x9002	// 1 byte, maximum relay depth, default is 3
#define FPARAM_SYNC_CLOCK_FLAG		0x9003	// 3 bytes, broadcase sync meter clock flag, afn-fn: 85H/f10
#define FPARAM_AUTO_UNREAD			0x9004	// 1 byte, auto read meter,
#define FPARAM_FROZ_HOUR			0x9005	// 1 byte, meter frozen hour, format is hh, default 00
#define FPARAM_BROAD_FROZ_HOUR		0x9006	// 3 byte, broadcast frozen hour time
#define FPARAM_UNREAD_PERIOD		0x9008	// 4*NBR_UNREAD_PERIOD+1 bytes
#define FPARAM_FDAY_FINISH_TIME		0x9009	// 3 bytes, day read finished time, mm/hh/dd
#define FPARAM_FDAY_FINISH_FLAG		0x900A	// 1 byte, day read finished flag, 0x00 finished, 0x55 not finished 
#define FPARAM_FMON_FINISH_TIME		0x900B	// 3 bytes, month read finished time, mm/hh/dd
#define FPARAM_FMON_FINISH_FLAG		0x900C	// 1 byte, month read finished flag, 0x00 finished, 0x55 not finished 

#define FPARAM_AFN04_F1				0x9101	// 6 bytes
#define FPARAM_AFN04_F2				0x9102	// 33 bytes
#define FPARAM_AFN04_F4				0x9104	// 16 bytes
#define FPARAM_AFN04_F5				0x9105	// 3 bytes
#define FPARAM_AFN04_F6				0x9106	// 16 bytes
#define FPARAM_AFN04_F7				0x9107	// 6 bytes
#define FPARAM_AFN04_F8				0x9108	// 16 bytes
#define FPARAM_AFN04_F12			0x910C	// 3 bytes
#define FPARAM_AFN04_F17			0x9111	// 2 bytes
#define FPARAM_AFN04_F18			0x9112	// 12 bytes
#define FPARAM_AFN04_F19			0x9113	// 1 byte
#define FPARAM_AFN04_F20			0x9114	// 1 byte
#define FPARAM_AFN04_F21			0x9115	// 25 bytes
#define FPARAM_AFN04_F22			0x9116	// 56 bytes
#define FPARAM_AFN04_F23			0x9117	// 3 bytes
#define FPARAM_AFN04_F24			0x9118	// 1 byte
#define FPARAM_AFN04_F57			0x9139	// 3 bytes
#define FPARAM_AFN04_F58			0x913A	// 1 byte
#define FPARAM_AFN04_F59			0x913B	// 4 bytes
#define FPARAM_AFN04_F60			0x913C	// 44 bytes
#define FPARAM_AFN04_F61			0x913D	// 1 byte
#define FPARAM_AFN04_F62			0x913E	// 5 bytes

#define FPARAM_AFN0C_F1				0x9201	// 30 bytes
#define FPARAM_AFN0C_F3				0x9203	// 31 bytes
#define FPARAM_AFN0C_F8				0x9403	// 8 bytes, event change flag
#define FPARAM_AFN0C_F9				0x9404	// 2 bytes, 1st byte(bit 0..5) for yx1..yx4 and door_node

#define FPARAM_AFN84_F10			0x930A	// 6 bytes, AFN84-F10
#define FPARAM_AFN84_F15			0x930F	// 16 bytes, event record flag

#define FPARAM_POWERFAIL_TIME		0x9401	// 4 bytes, power fail time
#define FPARAM_POWERUP_TIME			0x9402	// 4 bytes, power up time

/* Expended by kaifa */
#define FPARAM_CON_SWD				0x8880	// 2 bytes
#define FPARAM_MFG_STATE			0x8881	// 1 byte, 0xAA: manufacture
#define FPARAM_PLC_RETRY			0x8882	// 1 byte
#define FPARAM_PLC_TZC				0x8883	// 1 byte
#define FPARAM_PLC_TFWOUT			0x8884	// 1 byte
#define FPARAM_PLC_TAPPOUT			0x8885	// 2 bytes
#define FPARAM_PROC_ENABLE_MASK		0x8886	// 2 bytes
#define FPARAM_LAST_PROC			0x8887	// 1 byte
#define FPARAM_LAST_MTIDX			0x8888	// 2 byte
#define FPARAM_CARRIER_BAUDRATE		0x8889	// 1 byte, first carrier baudrate(bit 3-2), retry carrier baudrate(bit 1-0)
											//	0 for 1600 BPS, 1 for 800 BPS, 2 for 400 BPS, 3 for 200 BPS
#define FPARAM_485_BAUD1			0x888A	// first 485 port baudrate
#define FPARAM_485_BAUD2			0x888B	// second 485 port baudrate
#define FPARAM_HHU_BAUD				0x888C	// hhu port baudrate
#define FPARAM_DOWNLOAD_FILE		0x888D	// download files to CON, not used
#define FPARAM_UPLOAD_FILE			0x888E	// upload files from CON, not used
#define FPARAM_ELOSE_DATE			0x888F	// 3 byte ddmmyy, not used
#define FPARAM_COMMAND				0x8890	// 1 byte reboot, 1=LVC,2=modem,3=EB
#define FPARAM_USER_PASSWD_FLAG		0x8891	// 5 bytes, fail flag(count(1) and date_time(4))
#define FPARAM_FONT_SIZE			0x8892	// 1 bytes, font size(only 16 and 12 are ok)
#define FPARAM_CHANGE_FREQ			0x8893	// 1 bytes, none zero need change PLC freq, 0 for not changed
#define FPARAM_LAST_NET_TYPE		0x8894	// 1 bytes, the ppp is online when application running, the byte is last net type

struct param {
	WORD id;		/* DI */
	WORD flag;		/* bit 0 for write permit,
					   bit 1 for read permit,
					   bit 2 when write need high auth,
					   bit 3 when write need low auth */
	WORD len;		/* length of DI */
	WORD offset;	/* offset of data */
};

typedef struct f_param {
	struct param params[FPARAM_COUNT];
	BYTE data[FPARAM_LENGTH];
} F_PARAM;

void fparam_lock(void);

void fparam_unlock(void);

int fparam_get_value(WORD id, void *buf, int max_len);

int fparam_set_value(WORD id, const void *buf, int max_len, int *param_len);

int fparam_get_all(WORD id, void *buf, int max_len);

int fparam_set_all(WORD id, const void *buf, int max_len, int *param_len);

int fparam_read(WORD id, void *buf, int max_len);

int fparam_write(WORD id, const void *buf, int max_len, int *param_len);

void fparam_init(void);

void fparam_flush(void);

void fparam_destroy(void);

#endif /* _F_PARAM_H */
