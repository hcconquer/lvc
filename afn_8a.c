/*****************************************************************************
*	ChangSha AMR Concentrator 
*
*	(c) Copyright 2006-2007, Shenzhen Kaifa Technology Co.,Ltd. 
*	All Rights Reserved 
*
*	read and write concentrator parameters and meter records in concentrator
*	
*	Author: jianping zhang, dajiang wan
*	Created on: 2007-02-08
*****************************************************************************/
#include "afn.h"
#include "common.h"
#include "t_mt.h"
#include "t_stat.h"
#include "t_task.h"
#include "misc.h"
#include "f_param.h"
#include "err_code.h"
#include "msg_proc.h"
#include "threads.h"
#include "mt_access.h"
#include "cb_access.h"
#include "lvc_test.h"

static int afn8a_f1(BYTE pn, BYTE *out_buf, int max_len, const BYTE *in_buf,
	int *param_len)
{
	const BYTE *in_ptr = in_buf; 
	BYTE *out_ptr = out_buf;
	WORD mtidx, meter_nbr, valid_meter_nbr = 0;
	int i, len_tmp, in_len = *param_len;

	if (in_len < 2)
		return 0;
	meter_nbr = ctos(in_ptr); in_ptr += 2;
	if (in_len < 2 * meter_nbr || max_len < 2)
		return 0;
	tmt_lock();
	*param_len = 2 + 2 * meter_nbr;
	out_ptr += 2;	//skip number of meter
	max_len -= 2;
	if (meter_nbr == 0) { // all meters
		for (i = 0; i < NBR_REAL_MT; i ++) {
			if (max_len < 22) {
				out_ptr = out_buf;
				break;
			}
			mtidx = i;
			len_tmp = tmt_get(mtidx, out_ptr, 1);
			if (len_tmp == 22) {
				out_ptr += 22;
				max_len -= 22;
				valid_meter_nbr ++;
			}
		}
	}
	else {
		for (i = 0; i < meter_nbr; i ++) {
			if (max_len < 22) {
				out_ptr = out_buf;
				break;
			}
			mtidx = METER_INDEX(ctos(in_ptr)); in_ptr += 2;
			len_tmp = tmt_get(mtidx, out_ptr, 1);
			if (len_tmp == 22) {
				out_ptr += 22;
				max_len -= 22;
				valid_meter_nbr ++;
			}
		}
	}
	stoc(out_buf, valid_meter_nbr);
	tmt_unlock();
	return out_ptr - out_buf;
}

static int afn8a_f9(BYTE pn, BYTE *out_buf, int max_len, const BYTE *in_buf,
	int *param_len)
{
	BYTE *out_ptr = out_buf;

	*param_len = 0;
	if (max_len < 16)
		return 0;
	fparam_lock();
	fparam_read(FPARAM_CON_CASCADE, out_ptr, 16); out_ptr += 16;
	fparam_unlock();
	return out_ptr - out_buf;
}

static int afn8a_f10(BYTE pn, BYTE *out_buf, int max_len, const BYTE *in_buf,
	int *param_len)
{
	BYTE *out_ptr = out_buf;

	*param_len = 0;
	if (max_len < 6)
		return 0;
	fparam_lock();
	fparam_read(FPARAM_AFN84_F10, out_ptr, 6); out_ptr += 6;	
	fparam_unlock();
	return out_ptr - out_buf;
}

static int afn8a_f11(BYTE pn, BYTE *out_buf, int max_len, const BYTE *in_buf,
	int *param_len)
{
	BYTE *out_ptr = out_buf;

	*param_len = 0;
	if (max_len < 8)
		return 0;
	fparam_lock();
	fparam_read(FPARAM_FMON_READ_TIME, out_ptr, 2); out_ptr += 2;	
	fparam_read(FPARAM_FDAY_READ_TIME, out_ptr, 1); out_ptr += 1;
	fparam_read(FPARAM_FLOAD_READ_TIME, out_ptr, 1); out_ptr += 1;
	fparam_read(FPARAM_FROZ_HOUR, out_ptr, 1); out_ptr += 1;
	fparam_read(FPARAM_BROAD_FROZ_HOUR, out_ptr, 3); out_ptr += 3;
	fparam_unlock();
	return out_ptr - out_buf;
}

static int afn8a_f12(BYTE pn, BYTE *out_buf, int max_len, const BYTE *in_buf,
	int *param_len)
{
	BYTE *out_ptr = out_buf;
	BYTE period[1 + 4 * NBR_UNREAD_PERIOD];
	int len;

	*param_len = 0;
	fparam_lock();
	fparam_read(FPARAM_UNREAD_PERIOD, period, sizeof(period));
	len = 1 + 4 * period[0];
	if (max_len >= len && period[0] <= NBR_UNREAD_PERIOD) {
		memcpy(out_ptr, period, len);
		out_ptr += len;
	}
	fparam_unlock();
	return out_ptr - out_buf;
}

static int afn8a_f13(BYTE pn, BYTE *out_buf, int max_len, const BYTE *in_buf,
	int *param_len)
{
	BYTE *out_ptr = out_buf;

	*param_len = 0;
	if (max_len < 2)
		return 0;
	fparam_lock();
	fparam_read(FPARAM_RELAY_DEPTH, out_ptr, 1); out_ptr += 1;
	fparam_read(FPARAM_RELAY_METHOD, out_ptr, 1); out_ptr += 1;
	fparam_unlock();
	return out_ptr - out_buf;
}

static int afn8a_f14(BYTE pn, BYTE *out_buf, int max_len, const BYTE *in_buf,
	int *param_len)
{
	const BYTE *in_ptr = in_buf;
	BYTE *out_ptr = out_buf;
	BYTE read_type;
	int i, meter_nbr, real_nbr = 0, order;
	int len_tmp, in_len = *param_len;

	if (in_len < 1)
		return 0;
	read_type = *in_ptr++; in_len --;
	out_ptr += 2; max_len -= 2; // skip 2 bytes for number of meter
	if (read_type == 0x00) {	 // read route table of all meter
		tstat_lock();
		for (i = 0; i < T_MT_ROWS_CNT; i ++) {
			if (tmt_is_empty(i))
				continue;
			len_tmp = tstat_get_route(i, out_ptr, max_len);
			if (len_tmp > 0) {
				out_ptr += len_tmp; max_len -= len_tmp;
				real_nbr ++;
			}
		}
		tstat_unlock();
		*param_len = 1;
		stoc(out_buf, real_nbr);
		return out_ptr - out_buf;
	}
	else {	// read route table of specify meter
		if (in_len < 2)
			return 0;
		meter_nbr = ctos(in_ptr); in_ptr += 2; in_len -= 2;
		if (in_len < meter_nbr * 2)
			return 0;
		tstat_lock();
		for (i = 0; i < meter_nbr; i ++) {
			order = ctos(in_ptr); in_ptr += 2; in_len -= 2;
			if (tmt_is_empty(METER_INDEX(order)))
				continue;
			len_tmp = tstat_get_route(METER_INDEX(order), out_ptr,
				max_len);
			if (len_tmp > 0) {
				out_ptr += len_tmp; max_len -= len_tmp;
				real_nbr ++;
			}
		}
		tstat_unlock();
		*param_len = 1 + 2 + meter_nbr * 2;
		stoc(out_buf, real_nbr);
		return out_ptr - out_buf;
	}	
}

static int afn8a_f15(BYTE pn, BYTE *out_buf, int max_len, const BYTE *in_buf,
	int *param_len)
{
	BYTE *out_ptr = out_buf;

	*param_len = 0;
	if (max_len < 16)
		return 0;
	fparam_lock();
	fparam_read(FPARAM_AFN84_F15, out_ptr, 16); out_ptr += 16;
	fparam_unlock();
	return out_ptr - out_buf;
}

static int afn8a_f20(BYTE pn, BYTE *out_buf, int max_len, const BYTE *in_buf,
	int *param_len)
{
	BYTE *out_ptr = out_buf;

	tmt_lock();
	*param_len = 0;
	out_ptr += tmt_get_all_focus_order(out_ptr, max_len);
	tmt_unlock();
	return out_ptr - out_buf;
}

static int afn8a_f21(BYTE pn, BYTE *out_buf, int max_len, const BYTE *in_buf,
	int *param_len)
{
	const BYTE *in_ptr = in_buf;
	BYTE *out_ptr = out_buf;
	BYTE type;
	int in_len = *param_len;

	if (in_len < 1 || max_len < 1)
		return 0;
	type = *in_ptr ++;
	*param_len = 1;
	tmt_lock();
	out_ptr += tmt_get_select(out_ptr, max_len, type);
	tmt_unlock();
	return out_ptr - out_buf;
}

static int afn8a_f22(BYTE pn, BYTE *out_buf, int max_len, const BYTE *in_buf,
	int *param_len)
{
	*param_len = 0;
	return 0; // TODO
}

static int afn8a_f23(BYTE pn, BYTE *out_buf, int max_len, const BYTE *in_buf,
	int *param_len)
{
	*param_len = 0;
	return 0; // TODO
}

static int afn8a_f24(BYTE pn, BYTE *out_buf, int max_len, const BYTE *in_buf,
	int *param_len)
{
	*param_len = 0;
	return 0;
}

static int afn8a_f25(BYTE pn, BYTE *out_buf, int max_len, const BYTE *in_buf,
	int *param_len)
{
	const BYTE *in_ptr = in_buf; 
	BYTE *out_ptr = out_buf;
	WORD mtidx, meter_nbr, valid_meter_nbr = 0;
	int i, in_len = *param_len;
	MTID mtid;

	if (in_len < 2)
		return 0;
	meter_nbr = ctos(in_ptr); in_ptr += 2; in_len -= 2;
	if (in_len < 2 * meter_nbr || max_len < 2)
		return 0;
	*param_len = 2 + 2 * meter_nbr;
	out_ptr += 2; max_len -= 2;	//skip number of meter
	for (i = 0; i < meter_nbr; i ++) {
		mtidx = METER_INDEX(ctos(in_ptr)); in_ptr += 2; in_len -= 2;
		if (tmt_is_empty(mtidx))
			continue;
		if (max_len < 4)
			return 0;
		tmt_get_mtid(mtidx, mtid);
		if (meter_read(mtidx, mtid, get_nbr_retry(), out_ptr + 2, 1, 0xe870)) {
			stoc(out_ptr, METER_ORDER(mtidx)); out_ptr += 2; max_len -= 2;
			out_ptr += 2; max_len -= 2;// two bytes for data
			valid_meter_nbr ++;
		}
	}
	stoc(out_buf, valid_meter_nbr);
	return out_ptr - out_buf;
}

static int afn8a_f26(BYTE pn, BYTE *out_buf, int max_len, const BYTE *in_buf,
	int *param_len)
{
	const BYTE *in_ptr = in_buf; 
	BYTE *out_ptr = out_buf;
	WORD mtidx, meter_nbr, valid_meter_nbr = 0;
	int i, in_len = *param_len;
	MTID mtid;

	if (in_len < 2)
		return 0;
	meter_nbr = ctos(in_ptr); in_ptr += 2; in_len -= 2;
	if (in_len < 2 * meter_nbr || max_len < 2)
		return 0;
	*param_len = 2 + 2 * meter_nbr;
	out_ptr += 2; max_len -= 2;	//skip number of meter
	for (i = 0; i < meter_nbr; i ++) {
		mtidx = METER_INDEX(ctos(in_ptr)); in_ptr += 2; in_len -= 2;
		if (tmt_is_empty(mtidx))
			continue;
		if (max_len < 3)
			return 0;
		tmt_get_mtid(mtidx, mtid);
		if (meter_read(mtidx, mtid, get_nbr_retry(), out_ptr + 2, 1, 0xe874)) {
			stoc(out_ptr, METER_ORDER(mtidx)); out_ptr += 2; max_len -= 2;
			out_ptr ++; max_len --;// one byte for data
			valid_meter_nbr ++;
		}
	}
	stoc(out_buf, valid_meter_nbr);
	return out_ptr - out_buf;
}

static int afn8a_f30(BYTE pn, BYTE *out_buf, int max_len, const BYTE *in_buf,
	int *param_len)
{
	int task_id = pn, ret;

	*param_len = 0;
	task_lock();
	ret = task3_get(task_id, out_buf, max_len);
	task_unlock();
	if (ret == 0)
		PRINTF("task3_get error, task_id:%d\n", task_id);
	return ret;
}

static int afn8a_f32(BYTE pn, BYTE *out_buf, int max_len, const BYTE *in_buf,
	int *param_len)
{
	int tmp;

	*param_len = 0;
	task_lock();
	tmp = task3_get_flag(pn, out_buf, max_len);
	task_unlock();
	if (tmp == 0)
		PRINTF("task3_get_flag error, task_id:%d\n", pn);
	return tmp;
}

static int afn8a_f110(BYTE pn, BYTE *out_buf, int max_len, const BYTE *in_buf,
	int *param_len)
{
	BYTE type;
	WORD di;
	int len;

	if (*param_len < 3 || max_len < 2)
		return 0;
	*param_len = 3;
	type = in_buf[0];
	di = (in_buf[1] << 8) + in_buf[2];
	fparam_lock();
	if (type == 0) {
		len = fparam_read(di, out_buf + 2, max_len - 2);
		if (len > 0) {
			out_buf[0] = len >> 8;
			out_buf[1] = len;
			len += 2;
		}
	}
	else {
		len = fparam_get_all(di, out_buf, max_len);
	}
	fparam_unlock();
	return len <= 0 ? 0 : len;
}

static int afn8a_f111(BYTE pn, BYTE *out_buf, int max_len, const BYTE *in_buf,
	int *param_len)
{
	int out_len;

	if (get_cb_data(in_buf, *param_len, out_buf, max_len, &out_len))
		return out_len;
	else
		return 0;
}

static int afn8a_f112(BYTE pn, BYTE *out_buf, int max_len, const BYTE *in_buf,
	int *param_len)
{
	*param_len = 0;
	return lvc_test_response(out_buf, max_len);
}

static int afn8a_f113(BYTE pn, BYTE *out_buf, int max_len, const BYTE *in_buf,
	int *param_len)
{
	*param_len = 0;
	return lvc_test_response(out_buf, max_len);
}

static const struct st8a_fn {
	BYTE fn;
	int (*afn8a_fn)(BYTE pn, BYTE *out_buf, int max_len, const BYTE *in_buf,
		int *param_len);
} afn8a_fn_arr[] = {
	{1,   afn8a_f1},   {9,   afn8a_f9},   {10,  afn8a_f10},  {11,  afn8a_f11},
	{12,  afn8a_f12},  {13,  afn8a_f13},  {14,  afn8a_f14},  {15,  afn8a_f15},
	{20,  afn8a_f20},  {21,  afn8a_f21},  {22,  afn8a_f22},  {23,  afn8a_f23},
	{24,  afn8a_f24},  {25,  afn8a_f25},  {26,  afn8a_f26},  {30,  afn8a_f30},
	{32,  afn8a_f32},  {110, afn8a_f110}, {111, afn8a_f111}, {112, afn8a_f112},
	{113, afn8a_f113}
};

int afn8a_get_fn(BYTE pn, BYTE fn, BYTE *out_buf, int max_len,
	const BYTE *param, int *param_len)
{
	int i;

	PRINTF("AFN=8AH, Pn=%d, Fn=%d\n", pn, fn);
	for (i = 0; i < sizeof(afn8a_fn_arr) / sizeof(struct st8a_fn); i ++) {
		if (fn == afn8a_fn_arr[i].fn)
			return (afn8a_fn_arr[i].afn8a_fn)(pn, out_buf, max_len, param,
				param_len);
	}
	return 0;
}
