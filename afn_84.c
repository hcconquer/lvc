/*****************************************************************************
*	ChangSha AMR Concentrator 
*
*	(c) Copyright 2006-2007, Shenzhen Kaifa Technology Co.,Ltd. 
*	All Rights Reserved 
*
*	read and write concentrator parameters and meter records in concentrator
*	
*	Author: jianping zhang, dajiang wan
*	Created on: 2007-02-08
*****************************************************************************/
#include "afn.h"
#include "common.h"
#include "t_mt.h"
#include "t_task.h"
#include "t_rt.h"
#include "t_stat.h"
#include "mt_access.h"
#include "mt_frzn.h"
#include "f_param.h"
#include "misc.h"
#include "err_code.h"
#include "msg_proc.h"
#include "threads.h"
#include "global.h"
#include "display.h"
#include "lvc_test.h"

// set meter record
static int afn84_f1(BYTE pn, const BYTE *in_buf, int in_len, int *used_len)
{
	const BYTE *req_buf = in_buf;
	WORD meter_nbr, i, mtidx;

	if (in_len < 2)
		return ERR_BAD_VALUE;
	meter_nbr = ctos(req_buf); req_buf += 2;
	*used_len = 2 + meter_nbr * 22;
	if (in_len < *used_len)
		return ERR_BAD_VALUE;
	tmt_lock();
	for (i = 0; i < meter_nbr; i ++) {
		mtidx = METER_INDEX(ctos(req_buf)); req_buf += 2;
		if (mtidx < T_MT_ROWS_CNT)
			tmt_set(mtidx, req_buf, 1);
		req_buf += 20;
	}
	tmt_unlock();
	meter_change();
	return ERR_OK;
}

// set address of slave concentrator in master concentrator
static int afn84_f9(BYTE pn, const BYTE *in_buf, int in_len, int *used_len)
{
	const BYTE *req_buf = in_buf;
	int param_len;
	
	*used_len = 16;
	if (in_len < 16)
		return ERR_BAD_VALUE;
	fparam_lock();
	fparam_write(FPARAM_CON_CASCADE, req_buf, 16, &param_len);
	fparam_unlock();
	return ERR_OK;
}

// set communication parameters
static int afn84_f10(BYTE pn, const BYTE *in_buf, int in_len, int *used_len)
{
	const BYTE *req_buf = in_buf;
	BYTE buf[6];
	int param_len;
	
	*used_len = 6;
	if (in_len < 6)
		return ERR_BAD_VALUE;
	fparam_lock();
	fparam_read(FPARAM_AFN84_F10, buf, 6);
	memcpy(buf + 1, req_buf + 1, 5); // keep the first byte
	fparam_write(FPARAM_AFN84_F10, buf, 6, &param_len);
	fparam_unlock();
	return ERR_OK;
}

// set time of auto read meter
static int afn84_f11(BYTE pn, const BYTE *in_buf, int in_len, int *used_len)
{
	const BYTE *req_buf = in_buf;
	int param_len;
	
	*used_len = 8;
	if (in_len < 8)
		return ERR_BAD_VALUE;
	fparam_lock();
	fparam_write(FPARAM_FMON_READ_TIME, req_buf, 2, &param_len); req_buf += param_len;
	fparam_write(FPARAM_FDAY_READ_TIME, req_buf, 1, &param_len); req_buf += param_len;
	fparam_write(FPARAM_FLOAD_READ_TIME, req_buf, 1, &param_len); req_buf += param_len;
	fparam_write(FPARAM_FROZ_HOUR, req_buf, 1, &param_len); req_buf += param_len;
	fparam_write(FPARAM_BROAD_FROZ_HOUR, req_buf, 3, &param_len); req_buf += param_len;
	fparam_unlock();
	frzn_meter_change();
	return ERR_OK;
}

// set reading-disable period
static int afn84_f12(BYTE pn, const BYTE *in_buf, int in_len, int *used_len)
{
	const BYTE *ptr = in_buf;
	int param_len;
	BYTE period[1 + 4 * NBR_UNREAD_PERIOD];
	
	if (in_len < 1)
		return ERR_BAD_VALUE;
	*used_len = 1 + ptr[0] * 4;
	if (in_len < 1 + ptr[0] * 4 || ptr[0] > NBR_UNREAD_PERIOD)
		return ERR_BAD_VALUE;
	bzero(period, sizeof(period));
	memcpy(period, ptr, *used_len);
	fparam_lock();
	fparam_write(FPARAM_UNREAD_PERIOD, period, sizeof(period), &param_len);
	fparam_unlock();
	return ERR_OK;
}

// set repeater depth and method
static int afn84_f13(BYTE pn, const BYTE *in_buf, int in_len, int *used_len)
{
	const BYTE *req_buf = in_buf;
	int param_len;
		
	if (in_len < 2)
		return ERR_BAD_VALUE;
	*used_len = 2;
	fparam_lock();
	fparam_write(FPARAM_RELAY_DEPTH, req_buf, 1, &param_len); req_buf += param_len;
	fparam_write(FPARAM_RELAY_METHOD, req_buf, 1, &param_len); req_buf += param_len;
	fparam_unlock();
	return ERR_OK;
}

// set route table
static int afn84_f14(BYTE pn, const BYTE *in_buf, int in_len, int *used_len)
{
	const BYTE *req_buf = in_buf;
	int meter_nbr, i, idx;
	BYTE depth;

	if (in_len < 2)
		return ERR_BAD_VALUE;
	meter_nbr = ctos(req_buf);
	req_buf += 2; in_len -= 2;
	for (i = 0; i < meter_nbr; i ++) {
		if (in_len < 3)
			return ERR_BAD_VALUE;
		idx = METER_INDEX(ctos(req_buf));
		req_buf += 2; in_len -= 2;
		depth = *req_buf ++; in_len --;
		if (in_len < depth * 2)
			return ERR_BAD_VALUE;
		tstat_lock();
		trt_lock();
		tstat_set_route(idx, depth, req_buf);
		trt_unlock();
		tstat_unlock();
		req_buf += depth * 2; in_len -= depth * 2;
	}
	*used_len = req_buf - in_buf;
	return ERR_OK;
}

//set concentrator event parameters
static int afn84_f15(BYTE pn, const BYTE *in_buf, int in_len, int *used_len)
{
	const BYTE *req_buf = in_buf;
	int param_len;

	if (in_len < 16)
		return ERR_BAD_VALUE;
	*used_len = 16;
	fparam_lock();
	fparam_write(FPARAM_AFN84_F15, req_buf, 16, &param_len);
	fparam_unlock();
	return ERR_OK;
}

//set meter for focus meter
static int afn84_f20(BYTE pn, const BYTE *in_buf, int in_len, int *used_len)
{
	const BYTE *req_buf = in_buf;
	BYTE focus_nbr;
	WORD focus_mt[NBR_FOCUS];
	int i, j, focus;

	if (in_len < 1)
		return ERR_BAD_VALUE;
	focus_nbr = *req_buf ++;
	*used_len = 1 + 2 * focus_nbr;
	if (in_len < 1 + 2 * focus_nbr || focus_nbr > NBR_FOCUS)
		return ERR_BAD_VALUE;
	tmt_lock();
	for (i = 0; i < focus_nbr; i ++) {
		focus_mt[i] = METER_INDEX(ctos(req_buf));
		req_buf += 2;
	}
	for (i = 0; i < T_MT_ROWS_CNT; i ++) {
		if (tmt_is_empty(i))
			continue;
		focus = 0;
		for (j = 0; j < focus_nbr; j ++) {
			if (i == focus_mt[j]) {
				focus = 1;
				break;
			}
		}
		tmt_set_meter_focus(i, focus);
	}
	tmt_unlock();
	return ERR_OK;
}

// set meter for reading or reading-disable
static int afn84_f21(BYTE pn, const BYTE *in_buf, int in_len, int *used_len)
{
	const BYTE *req_buf = in_buf;
	WORD meter_nbr, i, mtidx;
	
	if (in_len < 2)
		return ERR_BAD_VALUE;
	meter_nbr = ctos(req_buf);
	*used_len = 2 + meter_nbr * 3;
	if (in_len < 2 + meter_nbr * 3)
		return ERR_BAD_VALUE;
	req_buf += 2;	// skip number of read meter
	tmt_lock();
	for (i = 0; i < meter_nbr; i ++) {
		mtidx = METER_INDEX(ctos(req_buf));
		if (mtidx < T_MT_ROWS_CNT)
			tmt_set_select(mtidx, req_buf[2]);
		req_buf += 3;
	}
	tmt_unlock();
	return ERR_OK;
}

// set tariff table, no need at present
static int afn84_f22(BYTE pn, const BYTE *in_buf, int in_len, int *used_len)
{
	return ERR_BAD_VALUE; // TODO
}

//set tariff, no need at present
static int afn84_f23(BYTE pn, const BYTE *in_buf, int in_len, int *used_len)
{
	return ERR_BAD_VALUE; // TODO
}

// operate meter record in concentrator
static int afn84_f24(BYTE pn, const BYTE *in_buf, int in_len, int *used_len)
{
	const BYTE *req_buf = in_buf;
	BYTE operation_type;
	BYTE flag;
	int opt_idx, idx, tmp = 0;
	
	if (in_len < 3)
		return ERR_BAD_VALUE;
	tmt_lock();
	operation_type = *req_buf ++; in_len --;
	opt_idx = METER_INDEX(ctos(req_buf)); req_buf += 2; in_len -= 2;
	flag = (operation_type & 0x80) ? 1 : 0; // TODO: clear history data
	switch (operation_type & 0x07) {
	case 0:
	case 1:
		if (in_len >= 22) {
			*used_len = 22 + 3;
			idx = METER_INDEX(ctos(req_buf));
			tmp = tmt_insert_meter(idx, req_buf + 2);
		}
		break;
	case 2:
		if (in_len >= 22) {
			*used_len = 22 + 3;
			idx = METER_INDEX(ctos(req_buf));
			tmp = tmt_modify_meter(idx, req_buf + 2);
		}
		break;
	case 3:
		*used_len = 3;
		tmp = tmt_del_meter(opt_idx, flag);
		break;
	case 4:
		*used_len = 3;
		tmp = tmt_del_all_meters(flag);
		break;
	}
	tmt_unlock();
	meter_change();
	if (tmp == 0)
		return ERR_BAD_VALUE;
	else
		return ERR_OK;
}

// set meter maximum demand
static int afn84_f25(BYTE pn, const BYTE *in_buf, int in_len, int *used_len)
{
	const BYTE *ptr = in_buf, *rpt_ptr;
	BYTE method, key[4], nbr_retry;
	int i, mt_cnt, mtidx;
	MTID mtid;
		
	if (in_len < 2)
		return ERR_BAD_VALUE;
	mt_cnt = ctos(ptr); ptr += 2; in_len -= 2;
	nbr_retry = get_nbr_retry();
	for (i = 0; i < mt_cnt; i ++) {
		if (in_len < 9)
			return ERR_BAD_VALUE;
		mtidx = METER_INDEX(ctos(ptr)); ptr += 2; in_len -= 2;
		memcpy(key, ptr, 4); ptr += 4; in_len -= 4;
		method = *ptr ++; in_len --;
		if (method) { // have repeaters
			if (in_len < 2 * NBR_RPT)
				return ERR_BAD_VALUE;
			rpt_ptr = ptr; ptr += 2 * NBR_RPT; in_len -= 2 * NBR_RPT;
		}
		else {
			rpt_ptr = NULL;
		}
		if (in_len < 2)
			return ERR_BAD_VALUE;
		tmt_get_mtid(mtidx, mtid);
		if (meter_write(mtidx, mtid, nbr_retry, key, 1, 0xe870, ptr, 2)
			!= ERR_OK)
			return ERR_BAD_VALUE;
		ptr += 2; in_len -= 2;
	}
	*used_len = ptr - in_buf;
	return ERR_OK;
}

// set loadover count
static int afn84_f26(BYTE pn, const BYTE *in_buf, int in_len, int *used_len)
{
	const BYTE *ptr = in_buf, *rpt_ptr;
	BYTE method, key[4], nbr_retry;
	int i, mt_cnt, mtidx;
	MTID mtid;
		
	if (in_len < 2)
		return ERR_BAD_VALUE;
	mt_cnt = ctos(ptr); ptr += 2; in_len -= 2;
	nbr_retry = get_nbr_retry();
	for (i = 0; i < mt_cnt; i ++) {
		if (in_len < 8)
			return ERR_BAD_VALUE;
		mtidx = METER_INDEX(ctos(ptr)); ptr += 2; in_len -= 2;
		memcpy(key, ptr, 4); ptr += 4; in_len -= 4;
		method = *ptr ++; in_len --;
		if (method) { // have repeaters
			if (in_len < 2 * NBR_RPT)
				return ERR_BAD_VALUE;
			rpt_ptr = ptr; ptr += 2 * NBR_RPT; in_len -= 2 * NBR_RPT;
		}
		else {
			rpt_ptr = NULL;
		}
		if (in_len < 1)
			return ERR_BAD_VALUE;
		tmt_get_mtid(mtidx, mtid);
		if (meter_write(mtidx, mtid, nbr_retry, key, 1, 0xe874, ptr, 1)
			!= ERR_OK)
			return ERR_BAD_VALUE;
		ptr ++; in_len --;
	}
	*used_len = ptr - in_buf;
	return ERR_OK;
}

// set task parameters of class 1 data
static int afn84_f30(BYTE pn, const BYTE *in_buf, int in_len, int *used_len)
{
	const BYTE *req_buf = in_buf;
	int task_id = pn, tmp;
	
	task_lock();
	tmp = task3_set(task_id, req_buf, in_len);
	task_unlock();
	if (tmp <= 0) {
		PRINTF("task3_set fail, task_id:%d\n", task_id);
		return ERR_BAD_VALUE;
	}
	else {
		*used_len = tmp;
		return ERR_OK;
	}
}

// start or stop sending data of class 1
static int afn84_f32(BYTE pn, const BYTE *in_buf, int in_len, int *used_len)
{
	int tmp;

	task_lock();
	tmp = task3_set_flag(pn, in_buf, in_len);
	task_unlock();
	if (tmp <= 0) {
		PRINTF("task3_set_flag fail, task_id:%d\n", pn);
		return ERR_BAD_VALUE;
	}
	else {
		*used_len = tmp;
		return ERR_OK;
	}
}

// general set parameters
static int afn84_f110(BYTE pn, const BYTE *in_buf, int in_len, int *used_len)
{
	const BYTE *ptr = in_buf;
	BYTE type;
	WORD di, flag, len, offset;
	int tmp, ret;

	if (in_len < 5)
		return ERR_BAD_VALUE;
	type = *ptr ++;
	di = (ptr[0] << 8) + ptr[1]; ptr += 2;
	if (type == 0) {
		len = (ptr[0] << 8) + ptr[1]; ptr += 2;
		if (in_len < 5 + len)
			return ERR_BAD_VALUE;
		fparam_lock();
		ret = fparam_write(di, in_buf + 5, in_len - 5, &tmp);
		fparam_unlock();
		if (ret <= 0)
			return -ret;
		*used_len = 5 + tmp;
	}
	else {
		if (in_len < 8)
			return ERR_BAD_VALUE;
		flag = *ptr ++;
		offset = (ptr[0] << 8) + ptr[1]; ptr += 2;
		len = (ptr[0] << 8) + ptr[1]; ptr += 2;
		if (in_len < 8 + len)
			return ERR_BAD_VALUE;
		fparam_lock();
		ret = fparam_set_all(di, in_buf + 3, in_len - 3, &tmp);
		fparam_unlock();
		if (ret <= 0)
			return -ret;
		*used_len = 3 + tmp;
	}
	return ERR_OK;
}

static int afn84_f112(BYTE pn, const BYTE *in_buf, int in_len, int *used_len)
{
	if (in_len < 1)
		return ERR_BAD_VALUE;
	*used_len = 1;
	lvc_test_request(in_buf[0], 0, NULL);
	return ERR_OK;
}

static int afn84_f113(BYTE pn, const BYTE *in_buf, int in_len, int *used_len)
{
	if (in_len < 1 + MTID_LEN * 3)
		return ERR_BAD_VALUE;
	lvc_test_request(in_buf[0], 3, in_buf + 1);
	*used_len = 1 + MTID_LEN * 3;
	return ERR_OK;
}

static const struct st84_fn {
	BYTE fn;
	int (*afn84_fn)(BYTE pn, const BYTE *in_buf, int in_len, int *used_len);
} fn_arr[] = {
	{1,   afn84_f1},   {9,   afn84_f9},   {10,  afn84_f10},  {11,  afn84_f11},
	{12,  afn84_f12},  {13,  afn84_f13},  {14,  afn84_f14},  {15,  afn84_f15},
	{20,  afn84_f20},  {21,  afn84_f21},  {22,  afn84_f22},  {23,  afn84_f23},
	{24,  afn84_f24},  {25,  afn84_f25},  {26,  afn84_f26},  {30,  afn84_f30},
	{32,  afn84_f32},  {110, afn84_f110}, {112, afn84_f112}, {113, afn84_f113}
};

int afn84_set_fn(BYTE pn, BYTE fn, const BYTE *buf, int len, int *param_len)
{
	int i;

	PRINTF("AFN=84H, Pn=%d, Fn=%d\n", pn, fn);
	for (i = 0; i < sizeof(fn_arr) / sizeof(struct st84_fn); i ++) {
		if (fn == fn_arr[i].fn)
			return (fn_arr[i].afn84_fn)(pn, buf, len, param_len);
	}
	return ERR_BAD_VALUE;
}
