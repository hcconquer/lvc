/*
 *  C-Plan Concentrator
 *
 *  Copyright (C) 2006-2007, Shenzhen Kaifa Technology Co.,Ltd.
 *
 *  HHU thread process function
 *
 *  Author: dajiang wan
 *  Created on: 2006-09-05
 */
#include "common.h"
#include "device.h"
#include "global.h"
#include "msg_proc.h"
#include "msg_que.h"
#include "t_db.h"
#include "f_param.h"
#include "misc.h"
#include "threads.h"
#include "lvc_test.h"
#include "display.h"
#include "menu_str.h"

static int hhu_read_packet(BYTE *buf, int max_len)
{
	int len, timeout1 = 1000, timeout2 = 500;
	BYTE *ptr = buf;

	if ((len = hhu_read(buf, max_len, timeout1, timeout2)) > 0)
	{
		PRINTB("From HHU: ", buf, len);
		while (len > 0 && *ptr == 0xfe)
		{
			ptr++;
			len--;
		}
		if (len > 0 && ptr != buf)
			memmove(buf, ptr, len);
		return len;
	}
	return 0;
}

static void hhu_write_packet(const BYTE *buf, int len)
{
	PRINTB("To HHU: ", buf, len);
	hhu_write(buf, len, 500);
}

static int hhu_process(BYTE *buf, int max_len)
{
	int len, ret = 0;

	len = hhu_read_packet(buf, max_len);
	if (len > 0 && check_packet(buf, len))
	{
		lcd_update_task_info(c_info_task_exec_str[4]);
		msg_que_put(MSG_QUE_HHU_IN, buf, len, 0);
		msg_proc(MSG_QUE_HHU_IN, MSG_QUE_HHU_OUT, max_len);
		ret = 1;
		wait_delay(500);
		lcd_update_comm_info(-1);
	}
	msg_que_get(MSG_QUE_HHU_OUT, buf, max_len, &len, 0);
	if (len > 0)
	{
		lcd_update_task_info(c_info_task_exec_str[4]);
		hhu_write_packet(buf, len);
		ret = 1;
		wait_delay(500);
		lcd_update_comm_info(-1);
	}
	return ret;
}

/*手持机处理线程*/
void *hhu_thread(void *arg)
{
	long idle_time;
	BYTE buf[MAX_LDU_LEN];

	idle_time = uptime();
	while (!g_terminated)
	{
		notify_watchdog();
		check_sched();
		if (uptime() - idle_time >= 7 * 60)
		{
			self_diag(hhu_open, hhu_close);
			idle_time = uptime();
		}
		/*sizeof对数组操作,返回数组的大小*/
		if (hhu_process(buf, sizeof(buf)))
		{
			idle_time = uptime();
		}
		if (!g_terminated && get_reboot_flag(1))
		{
			msleep(500);
			g_terminated = 1; // we must reply first, then reboot
		}
	}
	return NULL;
}
