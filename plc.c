/*****************************************************************************
*	C-Plan Concentrator 
*
*	(c) Copyright 2006-2007, Shenzhen Kaifa Technology Co.,Ltd. 
*	All Rights Reserved 
*
*	The base PLC communcation routine.
*
*****************************************************************************/
#include "plc.h"
#include "fcs.h"
#include "common.h"
#include "misc.h"
#include "plc.h"
#include "global.h"
#include "device.h"
#include "msg_code.h"
#include "t_mt.h"
#include "t_stat.h"
#include "t_rt.h"
#include "err_code.h"
#include "f_param.h"
#include "threads.h"
#include "display.h"
#include "menu_str.h"

#define FRAME_CHAR			0x7e
#define DLE_CHAR			0x7d

#define OFFSET_LEN			2
#define OFFSET_ADDR			3
#define OFFSET_CTL			9
#define OFFSET_ROUTE 		10

#define PLC_MAX_APDU_LEN	(PLC_APDU_LEN * 2)
#define STUFF_SPLIT	1
#define STUFF_MERGE	0

static int version = 2, detect_done = 0;

static int get_plc_freq(void)
{
	BYTE change;
	struct tm tm;

	if (!fparam_get_value(FPARAM_CHANGE_FREQ, &change, 1) || !change)
		return 0;
	my_time(&tm);
	if ((tm.tm_hour >= 4 && tm.tm_hour < 5)
		|| (tm.tm_hour >= 22 && tm.tm_hour < 23))
		return 2;
	else if ((tm.tm_hour >= 5 && tm.tm_hour < 6)
		|| (tm.tm_hour >= 21 && tm.tm_hour < 22))
		return 1;
	else
		return 0;
}

static int calc_timeout(int baud_idx, BOOL req_resp, int nTx, int nRx, int r)
{
	#define BAUD_CB		4800	/* Baud Rate between ARM to CB */
	int total_time, head_time, cb_timeout, arm_to_cb, error, extra;
	int tzc = 25, tfwout  = 60, tappout = 1000;
	BYTE plc_tm[] = {1, 2, 4, 8}, buf[2];

	if (fparam_get_value(FPARAM_PLC_TZC, buf, 1))
		tzc = buf[0];
	if (fparam_get_value(FPARAM_PLC_TFWOUT, buf, 1))
		tfwout = buf[0];
	if (fparam_get_value(FPARAM_PLC_TAPPOUT, buf, 2))
		tappout = (buf[1] << 8) | buf[0];

	nTx += ((nTx % 2) == 0 ? 2 : 1);  //FILL BYTES
	nRx += ((nRx % 2) == 0 ? 2 : 1);  //FILL BYTES

	error = MTID_LEN * 0;	// need change 0 to zero when meter is BUG
	extra = 250; // cb process time
	if (req_resp) {
		head_time = (4 * 8 * 1000 * 2 * (r + 1)) / 200;
		cb_timeout = (nTx + nRx + r * MTID_LEN / 2 + error)
			* (r + 1) * 5 * plc_tm[baud_idx]
			+ tzc + (2 * r + 1) * (tfwout + tzc) + tappout;
		arm_to_cb = (nTx + nRx + r * MTID_LEN + 2 * 4)
			* 10 * 1000.0 / BAUD_CB + 2 * tfwout + 2 * extra;
		total_time = head_time + cb_timeout + arm_to_cb;
	}
	else {
		head_time = (4 * 8 * 1000 * (r + 1)) / 200;
		cb_timeout = (nTx + r * MTID_LEN / 2 + error)
			* (r + 1) * 5 * plc_tm[baud_idx]
			+ (r + 1) * (tfwout + tzc);
		arm_to_cb = (nTx + r * MTID_LEN + 4)
			* 10 * 1000.0 / BAUD_CB + tfwout + extra;
		total_time = head_time + cb_timeout + arm_to_cb;
	}
	//#ifdef DEBUG_TIMEOUT
	//	PRINTF("head_time:%d, cb_timeout:%d, arm_to_cb:%d, total_time:%d\n",
	//		head_time, cb_timeout, arm_to_cb, total_time);
	//#endif // DEBUG_TIMEOUT
	return total_time;
}

static int bytearr_convert(int cmd, BYTE *buffer, int len)
{
	BYTE buf[PLC_MAX_APDU_LEN];
	int i, ret = 0;

	if (cmd == STUFF_SPLIT) {
		for (i = 0; i < len; i ++) {
			if (buffer[i] == FRAME_CHAR || buffer[i] == DLE_CHAR) {
				buf[ret] = DLE_CHAR;
				buf[ret + 1] = buffer[i] ^ 0x20;
				ret += 2;
			}
			else {
				buf[ret] = buffer[i];
				ret += 1;
			}
		}
	}
	else if (cmd == STUFF_MERGE){
		for (i = 0; i < len; i ++) {
			if (buffer[i] == DLE_CHAR && i < len - 1) {
				buf[ret] = buffer[i + 1] ^ 0x20;
				i += 1;
			}
			else {
				buf[ret] = buffer[i];
			}
			ret += 1;
		}
	}
	memcpy(buffer, buf, ret);
	return ret;
}

static int fill_router(BYTE *buf, const MTID mtid, const RPT_PATH *path)
{
	int i;
	BYTE *ptr = buf;

	if (path == NULL || path->count == 0) {
		memcpy(ptr, mtid, MTID_LEN);
		ptr += MTID_LEN;
		*ptr ++ = 0; //leave for CTL
	}
	else {
		for (i = 0; i < path->count; i++) {
			memcpy(ptr, path ->rpts[i], MTID_LEN);
			ptr += MTID_LEN;
			if (i == 0)
				*ptr ++ = 0;  //leave for CTL
		}
		memcpy(ptr, mtid, MTID_LEN);
		ptr += MTID_LEN;
	}
	return ptr - buf;
}

static BYTE XOR_sum(const BYTE *buf, int len)
{
	BYTE cs = 0;

	while (len --)
		cs ^= *buf ++;
	return cs;
}

#ifdef DEBUG_TIMEOUT
static struct timeval tv1, tv2, tv3, tv4;

static long get_diff_ms(struct timeval * tv1, struct timeval * tv2)
{
	return (tv1->tv_sec - tv2->tv_sec) * 1000
		+ (tv1->tv_usec - tv2->tv_usec) / 1000;
}
#endif // DEBUG_TIMEOUT

/* PKT FORMAT: 7E, PHASE, LEN, ADDR, CTL, ROUTER, INF, CS  --- version 1 */
/* PKT FORMAT: 7E, FLAG, LEN, ADDR, CTL, ROUTER, INF, FCS16 --- version 2 */
static void plc_pack(BYTE *out, int *out_len, int baud_idx, const MTID mtid,
	BYTE phase, const RPT_PATH *path, const void *req, int len,
	int *recv_max_len)
{
	BYTE *ptr = out;
	unsigned short trialfcs;
	BYTE xor_sum, rrr, dddd, tmp;

	*ptr ++ = FRAME_CHAR;
	if (version == 1) {
		*ptr ++ = phase;
		baud_idx = 0;
	}
	else {
		*ptr ++ = phase | (baud_idx << 2) | (get_plc_freq() << 4);
	}
	ptr ++; /* Leave for LEN */
	ptr += fill_router(ptr, mtid, path);
	memcpy(ptr, req, len);
	ptr += len;
	out[OFFSET_LEN] = (ptr - out) - (OFFSET_LEN + 1);
	rrr = (path != NULL) ? path->count : 0;
	tmp = (*recv_max_len + 7) / 8;
	dddd = min(0x0f, tmp);
	*recv_max_len = tmp * 8;
	out[OFFSET_CTL] = (0x01) | (rrr << 1) | (dddd << 4);
	if (version == 1) {
		xor_sum = XOR_sum(out + 1, out[OFFSET_LEN] + 2);
		*ptr ++ = xor_sum;
	}
	else {
		trialfcs = fcs16(INITFCS16, out + 1, out[OFFSET_LEN] + 2);
		trialfcs ^= 0xffff;
		*ptr ++ = trialfcs;
		*ptr ++ = trialfcs >> 8;
	}
	*out_len = ptr - out;
}

/* PKT FORMAT: 7E, PHASE, LEN, ADDR, CTL, ROUTER, INF, CS  --- version 1 */
/* PKT FORMAT: 7E, FLAG, LEN, ADDR, CTL, ROUTER, INF, FCS16 --- version 2 */
static int plc_unpack(const BYTE *buffer, int buffer_len, const MTID mtid,
	void *buf, int max_len, int *len)
{
	const BYTE *ptr = buffer;
	int ret, total_len;
	unsigned short trialfcs;
	BYTE xor_sum;

	if (buffer_len < 1 + 1 + 1 + 6 + 1 + 1) {
		PRINTF("plc_recv: Package length error\n");
		return COMM_ERR;
	}
	if (version == 1) {
		total_len = ptr[OFFSET_LEN] + 4;
		if (buffer_len < total_len) {
			PRINTF("plc_recv: Package length error\n");
			return COMM_ERR;
		}
		xor_sum = XOR_sum(ptr + 1, total_len - 2);
		if (xor_sum != ptr[total_len - 1]) {
			PRINTF("plc_recv: CKSUM error \n");
			return COMM_ERR;
		}
		buffer_len = total_len - 1;
	}
	else {
		total_len = ptr[OFFSET_LEN] + 5;
		if (buffer_len < total_len) {
			PRINTF("plc_recv: Package length error\n");
			return COMM_ERR;
		}
		trialfcs = fcs16(INITFCS16, ptr + 1, total_len - 1);
		if (trialfcs != GOODFCS16) {
			PRINTF("plc_recv: FCS16 error \n");
			return COMM_ERR;
		}
		buffer_len = total_len - 2;
	}
	if (memcmp(ptr + OFFSET_ADDR, mtid, MTID_LEN) != 0) {
		PRINTB("plc_recv: Source addr error:", ptr + OFFSET_ADDR, MTID_LEN);
		return COMM_ERR;
	}
	if (ptr[OFFSET_CTL] & 0x01) {
		PRINTF("plc_recv: Frame type error \n");
		return COMM_ERR;
	}
	if (ptr[OFFSET_CTL] >> 1) {
		if (buffer_len - OFFSET_ROUTE != MTID_LEN) {
			PRINTF("plc_recv: Timeout frame error \n");
			return COMM_ERR;
		}
		else {
			*len = MTID_LEN;
			ret = COMM_RPT_FAIL;
		}
	}
	else {
		*len = buffer_len - OFFSET_ROUTE;
		ret = COMM_OK;
	}
	if (*len > max_len) {
		PRINTF("plc_recv: Package is too long, %d > buffer length:%d\n",
			*len, max_len);
		return COMM_ERR;
	}
	memcpy(buf, ptr + OFFSET_ROUTE, *len);
	return ret;
}

int plc_send(int baud_idx, BOOL req_resp, const MTID mtid, int phase,
	const RPT_PATH *path, const void *req, int len, int recv_max_len,
	int *timeout)
{
	#define SINGLE_PLC_MODULE

	BYTE buffer[PLC_MAX_APDU_LEN];
	int buffer_len, rrr;

	plc_pack(buffer, &buffer_len, baud_idx, mtid, phase, path, req, len,
		&recv_max_len);
	buffer_len = bytearr_convert(STUFF_SPLIT, buffer + 1, buffer_len - 1) + 1;
	rrr = (path != NULL) ? path->count : 0;
	*timeout = calc_timeout(baud_idx, req_resp, 9 + len, 9 + recv_max_len, rrr);
	#ifndef SINGLE_PLC_MODULE
	*timeout += 2500;
	#endif // SINGLE_PLC_MODULE

	PRINTB("To MT:", buffer, buffer_len);
	#ifdef DEBUG_TIMEOUT
	gettimeofday(&tv1, NULL);
	#endif // DEBUG_TIMEOUT
	plc_write(buffer, buffer_len, 200);

	#ifdef DEBUG_TIMEOUT
	gettimeofday(&tv2, NULL);
	PRINTF("TimeOutValue: %d, Send spents: %ld ms \n",
		*timeout, get_diff_ms(&tv2, &tv1)); 
	#endif // DEBUG_TIMEOUT
	return COMM_OK;
}

int plc_recv(const MTID mtid, int timeout, void *buf, int max_len, int *len)
{
	BYTE buffer[PLC_MAX_APDU_LEN], *ptr;
	int buffer_len;

	*len = 0;
	#ifdef DEBUG_TIMEOUT
	gettimeofday(&tv3, NULL);
	#endif // DEBUG_TIMEOUT
	memset(buffer, 0x00, PLC_MAX_APDU_LEN);
	buffer_len = plc_read(buffer, PLC_MAX_APDU_LEN, timeout, 200);
	#ifdef DEBUG_TIMEOUT
	gettimeofday(&tv4, NULL);
	PRINTF("recv spent: %ld ms\n", get_diff_ms(&tv4, &tv3));
	#endif // DEBUG_TIMEOUT
	if (buffer_len <= 0)
		return COMM_ERR;
	PRINTB("From MT:", buffer, buffer_len);
	if ((ptr = memchr(buffer, FRAME_CHAR, buffer_len)) == NULL) {
		PRINTF("plc_recv: No FRAME_CHAR 0x%02x\n", FRAME_CHAR);
		return COMM_ERR;
	}
	else {
		buffer_len -= (ptr - buffer);
	}
	buffer_len = bytearr_convert(STUFF_MERGE, ptr + 1, buffer_len - 1) + 1;
	return plc_unpack(ptr, buffer_len, mtid, buf, max_len, len);
}

/* PKT FORMAT: 7E, 'P', LEN, INF, CS --- version 1 */
/* PKT FORMAT: 7E, 80, LEN, INF, FCS16 --- version 2 */
int cb_send(const void *req, int len)
{
	BYTE buf[PLC_MAX_APDU_LEN], *ptr = buf;
	int buf_len;
	unsigned short trialfcs;
	BYTE xor_sum;

	*ptr ++ = FRAME_CHAR;
	if (version == 1)
		*ptr ++ = 'P';
	else
		*ptr ++ = 0x80;
	*ptr ++ = len;
	memcpy(ptr, req, len);
	ptr += len;
	if (version == 1) {
		xor_sum = XOR_sum(buf + 1, len + 2);
		*ptr ++ = xor_sum;
	}
	else {
		trialfcs = fcs16(INITFCS16, buf + 1, len + 2);
		trialfcs ^= 0xffff;
		*ptr ++ = trialfcs;
		*ptr ++ = trialfcs >> 8;
	}
	buf_len = bytearr_convert(STUFF_SPLIT, buf + 1, ptr - buf - 1) + 1;

	PRINTB("To CB:", buf, buf_len);
	plc_write(buf, buf_len, 200);
	return COMM_OK;
}

/* PKT FORMAT: 7E, 'P', LEN, INF, CS --- version 1 */
/* PKT FORMAT: 7E, 80, LEN, INF, FCS16 --- version 2 */
int cb_recv(void *buf, int max_len, int *len)
{
	BYTE buffer[PLC_MAX_APDU_LEN], *ptr = NULL;
	int len1, total_len;
	unsigned short trialfcs;
	BYTE xor_sum;

	*len = 0;
	if ((len1 = plc_read(buffer, PLC_MAX_APDU_LEN, 1000, 200)) <= 0)
		return COMM_ERR;
	PRINTB("From CB:", buffer, len1);
	if ((ptr = memchr(buffer, FRAME_CHAR, len1)) == NULL) {
		PRINTF("No FRAME_CHAR 0x%02x\n", FRAME_CHAR);
		return COMM_ERR;
	}
	else {
		len1 -= (ptr - buffer);
	}
	len1 = bytearr_convert(STUFF_MERGE, ptr + 1, len1 - 1) + 1;
	if (version == 1) {
		total_len = ptr[OFFSET_LEN] + 4;
		if (len1 < total_len) {
			PRINTF("Package length error\n");
			return COMM_ERR;
		}
		xor_sum = XOR_sum(ptr + 1, total_len - 2);
		if (xor_sum != ptr[total_len - 1]) {
			PRINTF("CKSUM error \n");
			return COMM_ERR;
		}
		*len = total_len - 4;
	}
	else {
		total_len = ptr[OFFSET_LEN] + 5;
		if (len1 < total_len) {
			PRINTF("Package length error\n");
			return COMM_ERR;
		}
		trialfcs = fcs16(INITFCS16, ptr + 1, total_len - 1);
		if (trialfcs != GOODFCS16) {
			PRINTF("FCS16 error \n");
			return COMM_ERR;
		}
		*len = total_len - 5;
	}
	if (*len < max_len) {
		memcpy(buf, ptr + 3, *len);
		return COMM_OK;
	}
	return COMM_ERR;
}

void cb_protocol_detect(void)
{
	BYTE req[PLC_APDU_LEN], resp[PLC_APDU_LEN];
	int req_len, resp_len;

	if (!detect_done) {
		version = 2;
		req_len = my_pack(req, "ccsc", CB_READ, 0, 0x0033, 2);
		cb_send(req, req_len);
		if (cb_recv(resp, sizeof(resp), &resp_len) == COMM_OK) {
			detect_done = 1;
		}
		else {
			version = 1;
			cb_send(req, req_len);
			if (cb_recv(resp, sizeof(resp), &resp_len) == COMM_OK)
				detect_done = 1;
		}
	}
}

static int get_baud_idx(int try_cnt)
{
	int baud_idx1, baud_idx2;
	BYTE val;

	if (fparam_get_value(FPARAM_CARRIER_BAUDRATE, &val, 1) >= 0) {
		baud_idx1 = (val >> 2) & 0x03;
		baud_idx2 = (val >> 0) & 0x03;
	}
	else {
		baud_idx1 = baud_idx2 = 0;
	}
	return (try_cnt == 0) ? baud_idx1 : baud_idx2;
}

static int get_max_resp_len(short mtidx, const BYTE *buf, int len)
{
	const BYTE *ptr;
	int ret, tmp;
	
	switch(buf[0]) {
	case MT_READ:
		ret = 1; len --; ptr = buf + 1;
		while (len >= 2) {
			tmp = get_mtdi_len(tmt_get_tariff(mtidx), ctos(ptr));
			if (tmp == 0) /* unknown DI */
				ret += 30; /* may be ok */
			else
				ret += tmp;
			ptr += 2; len -= 2;
		}
		break;
	default:
		ret = 2;  /* msgcode, ERR CODE */
	}
	return (ret > PLC_APDU_LEN) ? PLC_APDU_LEN : ret;
}

static int analyse_rpt_fail(BYTE *buf, int len, short mtidx, 
	const RPT_PATH *path, int *fail_mtidx)
{
	int i, ret;

	*fail_mtidx = mtidx;
	ret = ERR_NODE_UR;
	for (i = 0; i < path->count && len >= MTID_LEN; i ++) {
		if (memcmp(path->rpts[i], buf, MTID_LEN) == 0) {
			if (i < path->count - 1) { // not the last repeater
				*fail_mtidx = tmt_get_mtidx(path->rpts[i + 1]);
				ret = ERR_RPT1_UR + i + 1;
			}
			break;
		}
	}
	return ret;
}

static void path_to_path_idx(const RPT_PATH *path, RPT_PATH_IDX *path_idx)
{
	int i;

	for (i = 0; i < path->count; i ++)
		path_idx->rpts[i] = tmt_get_mtidx(path->rpts[i]);
	path_idx->count = path->count;
}

static int mt_nack_converse(BYTE err_code)
{
	return ERR_BAD_VALUE;
}

static int con_nack_converse(BYTE err_code)
{
	static const BYTE err_1[] = {
		ERR_BAD_PHASE,	ERR_BAD_CODE,	ERR_UNKNOWN_PATH,	ERR_SYNC,
		ERR_NODE_UR,	ERR_RPT1_UR,	ERR_RPT2_UR,		ERR_RPT3_UR,
		ERR_RPT4_UR,	ERR_RPT5_UR,	ERR_RPT6_UR,		ERR_RPT7_UR,
	};
	static const BYTE err_2[] = {
		0x01, 0x04, 0x01, 0x02,
		0x01, 0x01, 0x01, 0x01,
		0x01, 0x01, 0x01, 0x01,
	};
	int i;
	BYTE mfg;

	fparam_get_value(FPARAM_MFG_STATE, &mfg, 1);
	if (mfg == 0xAA) {
		return err_code;
	}
	else {
		for (i = 0; i < sizeof(err_1); i ++) {
			if (err_code == err_1[i])
				return err_2[i];
		}
		return ERR_OK;
	}
}

static char *err_code_str(BYTE err_code)
{
	switch(err_code) {
	case ERR_OK:
		return "OK";
	case ERR_BAD_VALUE:
		return "INVALID VALUE";
	/* ITEMS EXPANDED BY KAIFA */
	case ERR_BAD_PHASE:
		return "INVALID PHASE";
	case ERR_BAD_CODE:
		return "INVALID MSG CODE";
	case ERR_UNKNOWN_PATH:
		return "UNKNOWN PATH";
	case ERR_SYNC:
		return "CON NO TIME REFERENCE";
	case ERR_NODE_UR:
		return "NODE UNREACHABLE";
	case ERR_RPT1_UR:
		return "REPEATER 1 UNREACHABLE";
	case ERR_RPT2_UR:
		return "REPEATER 2 UNREACHABLE";
	case ERR_RPT3_UR:
		return "REPEATER 3 UNREACHABLE";
	case ERR_RPT4_UR:
		return "REPEATER 4 UNREACHABLE";
	case ERR_RPT5_UR:
		return "REPEATER 5 UNREACHABLE";
	case ERR_RPT6_UR:
		return "REPEATER 6 UNREACHABLE";
	case ERR_RPT7_UR:
		return "REPEATER 7 UNREACHABLE";
	default:
		return "UNKNOWN ERROR";
	}
}

static int broadcast_path(short mtidx, BYTE phase, RPT_PATH *path)
{
	if (mtidx == NO_RPT) {  /* for directly reachable meters */
		path->count = 0;
	}
	else {
		if (phase != tmt_get_phase(mtidx) || !tstat_get_path(mtidx, path))
			return 0;
		if (path->count >= NBR_RPT)
			return 0;
		tmt_get_mtid(mtidx, path->rpts[path->count ++]);
	}
	return 1;
}

int plc_sub_comm(short mtidx, const MTID mtid, BYTE phase,
	const RPT_PATH *path, BYTE nbr_retry, const void *req, int req_len,
	void *resp, int resp_max_len, int *resp_len, BITS update_log)
{
	int timeout, ret, plc_ret, fail_rpt_idx, baud_idx, plc_resp_len;
	BYTE try_cnt;
	MTID direct_mtid;
	RPT_PATH_IDX path_idx;

	memcpy(direct_mtid, path->count ? path->rpts[0] : mtid, MTID_LEN);
	plc_resp_len = get_max_resp_len(mtidx, req, req_len);
	ret = ERR_NODE_UR;
	lcd_update_task_info(c_info_task_exec_str[0]);
	for (try_cnt = 0; try_cnt <= nbr_retry; try_cnt ++) {
		baud_idx = get_baud_idx(try_cnt);
		plc_lock();
		plc_send(baud_idx, TRUE, mtid, phase, path, req, req_len, plc_resp_len,
			&timeout);
		plc_ret = plc_recv(direct_mtid, timeout, resp, resp_max_len, resp_len);
		plc_unlock();
		msleep(10);

		if (plc_ret == COMM_RPT_FAIL) {
			ret = analyse_rpt_fail(resp, *resp_len, mtidx, path, &fail_rpt_idx);
		}
		else if (plc_ret == COMM_ERR) {
			fail_rpt_idx = tmt_get_mtidx(direct_mtid);
			ret = (path->count == 0) ? ERR_NODE_UR : ERR_RPT1_UR;
		}
		else { // plc_ret == COMM_OK
			fail_rpt_idx = -1;
			if ((*((BYTE *)resp) & 0xC0) == 0xC0) /* NACK from meter */
				ret = mt_nack_converse(((BYTE *)resp)[1]);
			else
				ret = ERR_OK;
		}
		tmt_lock();
		trt_lock();
		path_to_path_idx(path, &path_idx);
		if (update_log & UPD_RT_INFO) {
			trt_update_statistic(mtidx, &path_idx, fail_rpt_idx);
		}
		if (update_log & UPD_NUN) {
			update_nun(mtidx, &path_idx, fail_rpt_idx);
		}
		trt_unlock();
		tmt_unlock();
		if (plc_ret == COMM_OK || g_terminated)
			break;
	}
	lcd_update_comm_info(-1);
	if (ret != ERR_OK)
		PRINTF("plc_sub_comm: %s (0x%02x)\n", err_code_str(ret), ret);
	return con_nack_converse(ret);
}

int plc_comm(short mtidx, const MTID mtid, BYTE nbr_retry, const void *req,
	int req_len, void *resp, int resp_max_len, int *resp_len)
{
	RPT_PATH path;
	BYTE phase;

	phase = tmt_get_phase(mtidx);
	if (!is_valid_phase(phase)) {
		PRINTF("plc_comm: phase error\n");
		return ERR_BAD_PHASE;
	}

	if (!tstat_get_path(mtidx, &path)) {
		PRINTF("plc_comm: path error\n");
		return ERR_UNKNOWN_PATH;
	}

	return plc_sub_comm(mtidx, mtid, phase, &path, nbr_retry, req,
		req_len, resp, resp_max_len, resp_len, UPD_ALL);
}

/* get current time string in bcd code ssmmhhDDMMYY */
static int broad_get_time_str(void *buf, int addi)
{
	struct timeval tv;
	time_t tt;
	struct tm tm;
	BYTE *ptr = buf;
	
	gettimeofday(&tv, NULL);
	tt = tv.tv_sec + (tv.tv_usec / 1000 + addi + 500) / 1000; // round to sec
	localtime_r(&tt, &tm);
	*ptr ++ = bin_to_bcd(tm.tm_sec);
	*ptr ++ = bin_to_bcd(tm.tm_min); 
	*ptr ++ = bin_to_bcd(tm.tm_hour); 
	*ptr ++ = bin_to_bcd(tm.tm_mday); 
	*ptr ++ = bin_to_bcd(tm.tm_mon + 1);
	*ptr ++ = bin_to_bcd(tm.tm_year % 100);
	return 6;
}

void broadcast_clock(short mtidx, BYTE nbr_retry, BYTE phase)
{
	BYTE req[PLC_APDU_LEN];
	int i, try_cnt, req_len, baud_idx, timeout;
	RPT_PATH path;

	if (!broadcast_path(mtidx, phase, &path))
		return;
	lcd_update_task_info(c_info_task_exec_str[1]);
	for (try_cnt = 0; try_cnt <= nbr_retry && !g_terminated; try_cnt ++) {
		baud_idx = get_baud_idx(try_cnt);
		timeout = calc_timeout(baud_idx, FALSE, 7 + 9, 0, path.count);
		req[0] = MT_CLK_SYNC;
		req_len = 1 + broad_get_time_str(req + 1, timeout);
		plc_lock();
		plc_send(baud_idx, FALSE, (BYTE *)BR_MTID, phase, &path, req, req_len,
			0, &timeout);
		msleep(timeout);
		plc_unlock();
		for (i = 0; i < 10; i ++) {
			if (g_terminated)
				break;
			msleep(500);
			notify_watchdog();
		}
	}
	lcd_update_comm_info(-1);
}

void broadcast_frzn_hour(short mtidx, BYTE nbr_retry, BYTE phase, BYTE hour)
{
	BYTE req[PLC_APDU_LEN];
	int try_cnt, req_len, baud_idx, timeout;
	RPT_PATH path;

	if (!broadcast_path(mtidx, phase, &path))
		return;
	lcd_update_task_info(c_info_task_exec_str[2]);
	for (try_cnt = 0; try_cnt <= nbr_retry && !g_terminated; try_cnt ++) {
		baud_idx = get_baud_idx(try_cnt);
		req[0] = MT_FROZEN_HOUR;
		req[1] = hour;
		req_len = 2;
		plc_lock();
		plc_send(baud_idx, FALSE, (BYTE *)BR_MTID, phase, &path, req, req_len,
			0, &timeout);
		msleep(timeout);
		plc_unlock();
	}
	lcd_update_comm_info(-1);
}

int cb_comm(const void *req, int req_len, void *resp, int max_len, int *resp_len)
{
	int plc_ret; 
	
	plc_lock();
	cb_protocol_detect();
	cb_send(req, req_len);
	plc_ret = cb_recv(resp, max_len, resp_len);
	plc_unlock();
	return (plc_ret == COMM_OK) ? ERR_OK : ERR_BAD_VALUE;
}
