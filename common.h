/*****************************************************************************
 *	C-Plan Concentrator 
 *
 *	(c) Copyright 2006-2007, Shenzhen Kaifa Technology Co.,Ltd. 
 *	All Rights Reserved 
 *		
 *****************************************************************************/

#ifndef _COMMON_H
#define _COMMON_H

#include "typedef.h"

unsigned char bcd_to_bin(unsigned char val);

unsigned char bin_to_bcd(unsigned char val);

WORD ctos(const BYTE *buf);

void stoc(BYTE *buf, WORD val);

DWORD ctol(const BYTE *buf);

void ltoc(BYTE *buf, DWORD val);

WORD msb_ctos(const BYTE *buf);

DWORD msb_ctol(const BYTE *buf);

void msb_stoc(BYTE *buf, WORD val);

void msb_ltoc(BYTE *buf, DWORD val);

int all_is_ff(const void *buf, int len);

int all_is_empty(const void *buf, int len);

void reverse(void *buf, int len);

int reverse_copy(void *dst, const void *src, int len);

BYTE check_sum(const void *buf, int len);

int my_pack(void *buf, const char *fmt, ...);

int my_unpack(const char *buf, const char *fmt, ...);

void wait_delay(int msec);

int wait_for_ready(int fd, int msec, int flag);

int safe_read_timeout(int fd, void *buf, int len, int timeout);

int safe_write_timeout(int fd, const void *buf, int len, int timeout);

int safe_read(int fd, void *buf, int len);

int safe_write(int fd, const void *buf, int len);

int check_file(const char *name, int size);

int create_file(const char *name, int size);

void PRINTB(const char *name, const void *data, int data_len);

void PRINTF(const char *format, ...);

void ERR_PRINTB(const char *prompt, short mtidx, const void *data, int data_len);

void ERR_PRINTF(const char *format, ...);

void LOG_PRINTF(const char *format, ...);

long uptime(void);

void close_all(int from);

int find_pid(const char *prg_name);

void msleep(int msec);

void read_rtc(void);

void set_rtc(void);

int check_rtc(void);

void adjust_date_time(int delta);

void my_time(struct tm *tm);

int is_leap_year(int year);

int escape_string(void *buf, const void *str, int size);

int read_lock_file(const char *lockfile);

int write_lock_file(const char *lockfile);

int my_gzip(const void *in_name, const void *out_name);

int my_gunzip(const void *in_name, const void *out_name);

#endif /* _COMMON_H */
