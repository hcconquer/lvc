/*****************************************************************************
*	C-Plan Concentrator 
*
*	(c) Copyright 2006-2007, Shenzhen Kaifa Technology Co.,Ltd. 
*	All Rights Reserved 
*
* 	Functions of routing procedure:
* 
* 	1. find phase & path for the phase-unknown meters.
* 	2. find path for the unreachable meters(nun > NRN) or path-unknown meters.
* 
* 	Steps:
* 
* 	1. for all booked meters, find repeater from the candidated 
*      repeaters list(at most NBR_BKUP_RPT), which stored in the t_route.dat
* 	2. for failed meters in step 1, check if it is directly reachable 
*     (only for the meters whose original path is not directly reachable)
* 	3. for each failed meters, check the meters with repeater level 0 
*     (directly reachable) one by one(the candidate repeater set should be 
*	  sorted by the location relation to the target meter), until one 
*	  could reach this meter, it will be used as the repeater, and added
*	  into the candicated repeater list of the target meter.
*	4. repeater level increase 1, repeat step 3, until level is larger 
*	  than NBR_RPT or all meters find their paths.
*
* 	NOTE: 
* 	1. This procedure should avoid building a loop path.
* 	2. Meters with same moudle no could not be as repeater each other. 
* 
*	Author: fanhua zeng
*	Created on: 2006-07-31
*****************************************************************************/
/*
 * Change logs:
 *  1. To run phase by phase, to avoid the communication phase change.
 * 		fanhua zeng, 2007-4-20
 *  2. In step1, not try all candidated repeaters, but only try those
 * 	   meters whose CSR meet a specified condition.	fanhua zeng, 2007-8-31
 *	3. In step0, if a meter's path fails, paths of meters who use it as
 *	   repeater directly or indirectly should be recomputed.
 *  4. when center manually change the repeater of meters, the repeater 
 *     will be registered in t_rt.dat
 *
 * TODO:
 *  1. the rational relation between: nbr_retry & NUN & max loop_count.
 *  2. the rational running frequency to ensure the life of the plc relay.
 */
/* CSR: Communication Success Rate */

#include "mt_route.h"
#include "common.h"
#include "global.h"
#include "t_mt.h"
#include "t_stat.h"
#include "t_rt.h"
#include "f_param.h"
#include "plc.h"
#include "msg_code.h"
#include "err_code.h"
#include "misc.h"

static int step, cur_mtidx, level, level_mtidx, cur_phase, change;
static int loop_count = 0;

/* max full routing (for all meters) count in a day */
static int full_rt_count;

static struct _mt_info {
	BYTE booked : 1;	/* if route is booked for CE */
	BYTE ok : 1;		/* if the path is ok or found */
	short rpt : 14;		/* backup repeater */
} mt_info[NBR_REAL_MT];

#ifndef DEBUG_ROUTING
/*
 * check if the meter specified by mtid is in the path.
 * there maybe have two entries in the tbl_mt for one focus meter,
 * so it is necessary to check if it is a loop path before communication.
 */
static int is_loop_path(const MTID mtid, RPT_PATH *path)
{
	int i;

	for (i = 0; i < path->count; i ++) {
		if (memcmp(mtid, path->rpts[i], MTID_LEN) == 0)
			return 1;
	}
	return 0;
}

/* output the path, so that the caller use it to update the statistic */
static int test_ce(short mtidx, int rpt_idx, int phase, RPT_PATH_IDX *path_idx)
{
	BYTE req[PLC_APDU_LEN], resp[PLC_APDU_LEN], nbr_retry;
	int req_len, resp_len, ret;
	RPT_PATH path;
	MTID mtid;

	tmt_lock();
	tmt_get_mtid(mtidx, mtid);
	tmt_unlock();

	req_len = my_pack(req, "cs", MT_READ, 0xC011);
	if (rpt_idx == NO_RPT) {
		path.count = 0;
		path_idx->count = 0;
	}
	else {
		if (!tstat_get_path_idx(rpt_idx, path_idx)
			|| !add_to_path_idx(rpt_idx, path_idx))
			return 0;
		path_idx_to_path(path_idx, &path);
		if (is_loop_path(mtid, &path))
			return 0;
	}

	nbr_retry = get_nbr_retry();
	/* !Don't udpate nun(t) here, at the end, update one time. */
	ret = plc_sub_comm(mtidx, mtid, phase, &path, nbr_retry,
		req, req_len, resp, sizeof(resp), &resp_len, UPD_RT_INFO);
	
	fparam_lock();
	set_last_proc(PROC_MT_ROUTE);
	set_last_mtidx(mtidx);
	fparam_unlock();
	if (ret == ERR_OK) {
		if (plc_sub_comm(mtidx, mtid, phase, &path, nbr_retry, req, req_len,
			resp, sizeof(resp), &resp_len, UPD_RT_INFO) == ERR_OK)
			return 2;
		return 1;
	}
	return 0;
}
#else // DEBUG_ROUTING
static char *ltrim_buf(char *str)
{
	char *ptr = str;

	while ((*ptr == ' ' || *ptr == 0x0d || *ptr == 0x0a) && *ptr != 0) {
		ptr ++;
	}
	return ptr;
}

/* for test, to simulate the CE response or not. */
static int test_reachable(int mtidx, int rpt_mtidx, int phase, int type)
{
	FILE *fp;
	char buffer[128], *ptr;
	int from, to, phase2, type2, ret = 0;
	RPT_PATH_IDX path_idx;
	int fail_rpt_idx;

	/* file nr include the not reachable meter index, one index on line */
	if ((fp = fopen("nr", "r")) != NULL) {
		while (fgets(buffer, sizeof(buffer), fp) != NULL) {
			ptr = ltrim_buf(buffer);
			if (*ptr == 0)
				continue;
			sscanf(ptr, "%d %d %d %d", &to, &from, &phase2, &type2);
			if (from == rpt_mtidx && to == mtidx && phase2 == phase && type2 >= type) {
				ret = 1;
				break;
			}
		}
		fclose(fp);
	}
//	printf("Test_reachable: phase: %d, %d -> %d, %d, %s\n", phase, rpt_mtidx, mtidx, type, ret ? "OK":"KO");
	if (rpt_mtidx != NO_RPT) {
		tstat_get_path_idx(rpt_mtidx, &path_idx);
		add_to_path_idx(rpt_mtidx, &path_idx);
	}
	else {
		path_idx.count = 0;
	}
	if (ret)
		fail_rpt_idx = -1;
	else
		fail_rpt_idx = rpt_mtidx;
	trt_update_statistic(mtidx, &path_idx, fail_rpt_idx);
	return ret;
}
#endif // DEBUG_ROUTING

static void rpt_found(short mtidx, short rpt_idx)
{
	mt_info[mtidx].ok = 1;
	tstat_lock();
	trt_lock();
	tstat_set_repeater(mtidx, rpt_idx);
	tstat_update(mtidx, 0);
	trt_unlock();
	tstat_unlock();
}

static int route_test_path(short mtidx, short rpt_idx, int phase)
{
	int ret;

#ifdef DEBUG_ROUTING
	ret = (test_reachable(mtidx, rpt_idx, phase, 1)) ? 2 : 0;
#else
	RPT_PATH_IDX path_idx;

	ret = test_ce(mtidx, rpt_idx, phase, &path_idx);
#endif
	LOG_PRINTF("route: %d %d %d->%d %d %d\n", step, level, rpt_idx, mtidx,
		phase, ret);
	if (ret > 0) {
		tmt_lock();
		tmt_set_phase(mtidx, phase);
		tmt_unlock();
		if (ret == 2) {  /* need to consider the CSR? */
			if (!mt_info[mtidx].ok) {
				rpt_found(mtidx, rpt_idx);
			}
			return 2;
		}
		else {
			/* backup the repeater idx */
			if (mt_info[mtidx].rpt == UNKNOWN_RPT) {
				mt_info[mtidx].rpt = rpt_idx;
			}
			return 1;
		}
	}
	return 0;
}

static int rpt_set[NBR_REAL_MT], nbr_rpt_set, idx_rpt_set;

/* minimun CSR for each repeater level */
static double min_csr[] = {0.8, 0.64, 0.51, 0.41, 0.33, 0.33, 0.33, 0.33};
static int build_rpt_set1(int mtidx)
{
	int i, x, lvl;
	double rpt_csr[NBR_REAL_MT];

	/* is it necessary to judge the repeater level? */
	trt_get_bkup_rpts(mtidx, rpt_set, &nbr_rpt_set, rpt_csr);

	/* filter backup repeater, level <= 3 && CSR is large enough */
	x = 0;
	for (i = 0; i < nbr_rpt_set; i ++) {
		lvl = tstat_get_level(rpt_set[i]);
		if (lvl >= 0 && lvl <= 3 && rpt_csr[i] >= min_csr[lvl]) {
			rpt_set[x ++] = rpt_set[i];
		}
	}
	nbr_rpt_set = x;
	idx_rpt_set = 0;

	return nbr_rpt_set;
}

static int next_rpt1(void)
{
	if (cur_mtidx < 0 || mt_info[cur_mtidx].ok)
		return 0;

	if (idx_rpt_set < nbr_rpt_set) {
		level_mtidx = rpt_set[idx_rpt_set];
		idx_rpt_set ++;
		return 1;
	}
	return 0;
}

static int next_idx1(void)
{
	int idx;

	for (idx = cur_mtidx + 1; idx < NBR_REAL_MT; idx ++) {
		if (mt_info[idx].booked && !mt_info[idx].ok
			&& tmt_get_phase(idx) == cur_phase) {
			cur_mtidx = idx;
			if (build_rpt_set1(idx) && next_rpt1())
				return 1;
		}
	}
	cur_mtidx = -1;
	return 0;
}

static int next_phase1(void)
{
	while ((cur_phase = next_phase(cur_phase)) > 0) {
		cur_mtidx = -1;
		if (next_idx1())
			return 1;
	}
	return 0;
}

/* if all the backuped repeaters has low CSR, it is necessary to try new
 * repeater in the next step. */
static int route_bkup_rpt(void)
{
	BYTE phase;

	if (next_rpt1() || next_idx1() || next_phase1()) {
		phase = (level_mtidx > NO_RPT) ?
			tmt_get_phase(level_mtidx) : tmt_get_phase(cur_mtidx);
		if (is_valid_phase(phase))
			route_test_path(cur_mtidx, level_mtidx, phase);
		return 0;
	}
	else {
		PRINTF("route: step1 finished.\n");
		return 1;
	}
}

static int next_idx2(void)
{
	int idx;

	for (idx = cur_mtidx + 1; idx < NBR_REAL_MT; idx ++) {
		/* do not care if the directly reachable is verified or not */
		if (mt_info[idx].booked && !mt_info[idx].ok) {
			cur_mtidx = idx;
			return 1;
		}
	}
	cur_mtidx = -1;
	return 0;
}

static int next_phase2(void)
{
	while ((cur_phase = next_phase(cur_phase)) > 0) {
		cur_mtidx = -1;
		if (next_idx2())
			return 1;
	}
	return 0;
}

/* step 2 test directly reachable */
static int route_direct(void)
{
	if (next_idx2() || next_phase2()) {
		route_test_path(cur_mtidx, NO_RPT, cur_phase);
		return 0;
	}
	else {
		PRINTF("route: step2 finished.\n");
		return 1;
	}
}

static short next_idx_for_bylevel(short mtidx)
{
	int idx;

	for (idx = mtidx + 1; idx < NBR_REAL_MT; idx ++) {
		if (mt_info[idx].booked && !mt_info[idx].ok)
			return idx;
	}
	return -1;
}

/* check if mtidx2 is in the meter boxs, which include the
 * repeater of the mtidx */
static int is_in_rpt_grp(short mtidx, short mtidx2)
{
	short idx, rpt_idx, grp_no1, grp_no2;

	grp_no1 = tmt_get_grp_no(mtidx);
	grp_no2 = tmt_get_grp_no(mtidx2);
	for (idx = 0; idx < NBR_REAL_MT; idx ++) {
		if (tmt_get_grp_no(idx) == grp_no1) {
			if ((rpt_idx = tstat_get_repeater(idx)) >= 0
				&& tmt_get_grp_no(rpt_idx) == grp_no2)
				return 1;
		}
	}
	return 0;
}

/* check if a meter is the repeater of the specified module */
static int is_rpt_of_module(short mtidx, const BYTE *ms_no)
{
	short idx;
	BYTE ms_no2[MS_NO_LEN];

	for (idx = 0; idx < NBR_REAL_MT; idx ++) {
		tmt_get_ms_no(idx, ms_no2);
		if (memcmp(ms_no2, ms_no, MS_NO_LEN) == 0
			&& tstat_get_repeater(idx) == mtidx)
			return 1;
	}
	return 0;
}

/* get the value for probability that mtidx2 could be used as the
 * repeater of mtidx. the smaller value, the larger probability */
static int get_relation_val(short mtidx, short mtidx2)
{
	BYTE la, la1, la2, la3;
	BYTE lb, lb1, lb2, lb3;
	BYTE ms_no[MS_NO_LEN];

	tmt_get_lines(mtidx, &la, &la1, &la2, &la3);
	tmt_get_lines(mtidx2, &lb, &lb1, &lb2, &lb3);

	/* different main line */
	if (la == NULL_LINE || lb == NULL_LINE || la != lb)
		return 6;
	
	/* repeaters of the meters in the same module of target meter */
	tmt_get_ms_no(mtidx, ms_no);
	if (memcmp(ms_no, NULL_MS_NO, MS_NO_LEN) && is_rpt_of_module(mtidx2, ms_no))
		return 0;

	/* meters in the repeater meter box	*/ /* if ms_no is not null, howto? */
	if (tmt_get_grp_no(mtidx) != NULL_GRP && is_in_rpt_grp(mtidx, mtidx2))
		return 0;

	/* meters on the directly parent line */
	if (la3 != NULL_LINE) {
		if (lb3 == NULL_LINE && lb2 == la2 && lb1 == la1 && lb == la)
			return 1;
	} else if (la2 != NULL_LINE) {
		if (lb2 == NULL_LINE && lb1 == la1 && lb == la)
			return 1;
	} else if (la1 != NULL_LINE) {
		if (lb1 == NULL_LINE && lb == la)
			return 1;
	}

	/* meters in the same meter box  */
	if (tmt_get_grp_no(mtidx2) == tmt_get_grp_no(mtidx))
		return 2;

	/* meters on the same branch line */
	if (la3 != NULL_LINE) {
		if (lb3 == la3 && lb2 == la2 && lb1 == la1 && lb == la)
			return 3;
	} else if (la2 != NULL_LINE) {
		if (lb2 == la2 && lb1 == la1 && lb == la)
			return 3;
	} else if (la1 != NULL_LINE) {
		if (lb1 == la1 && lb == la)
			return 3;
	} else if (la != NULL_LINE) {
		if (lb == la)
			return 3;
	}

	/* meters on the same main line */
	if (lb == la)
		return 4;

	/* others */
	return 5;
}

/*
 * All meters on the repeater level: level, arrange in this sequence:
 * 1. if ms_no(module no) is specified, the meter is a RS485 meter,
 * 	  which connect to a PLC collector.
 *    in this case, meters, which are repeaters of any one of the meters
 *    whose ms_no equal the target meter, should be firstly tried if it is
 *    suitable for the repeater of the target meter.
 *    if ms_no is not specified, meters in the meter boxs, which contains
 *    the repeaters of the other meters, whose meter box is same as that of
 *    the target meter, should be firstly tried.
 * 2. meters on the directly parent line
 * 3. if ms_no is not specified, meters on the same meter box
 * 4. other meters on the same line (not inlcude its subline)
 * 5. meters on the same main line
 * 6. meters on the same main line but different phase
 * 7. meters on the other lines.
 */

/* TODO: optimize, sort by meter box, cache the repeater sets. */
static int build_rpt_set3(short mtidx)
{
	short idx = -1, i, j, k, tmp;
	short relation[NBR_REAL_MT];
	BYTE ms_no[MS_NO_LEN], ms_no2[MS_NO_LEN];

	idx_rpt_set = 0;
	nbr_rpt_set = 0;
	tmt_get_ms_no(mtidx, ms_no);
	for (idx = 0; idx < NBR_REAL_MT; idx ++) {
		if (tmt_is_empty(idx))
			continue;
		/* path ok & on the specified level */
		if ((!mt_info[idx].booked ||(mt_info[idx].booked && mt_info[idx].ok))
			&& tmt_get_phase(idx) == cur_phase
			&& tstat_get_level(idx) == level) {
			/* meters in the same module could not be as repeater
			 * for each other.
			 */
			if (memcmp(ms_no, NULL_MS_NO, MS_NO_LEN)) {
				tmt_get_ms_no(idx, ms_no2);
				if (memcmp(ms_no, ms_no2, MS_NO_LEN) == 0)
					continue;
			}
			relation[nbr_rpt_set] = get_relation_val(mtidx, idx);
			rpt_set[nbr_rpt_set]  = idx;
			nbr_rpt_set ++;
		}
	}
	/* sort it by relation */
	for (i = 0; i < nbr_rpt_set; i ++) {
		k = i;
		for (j = i + 1; j < nbr_rpt_set; j ++) {
			if (relation[j] < relation[k]) {
				k = j;
			}
		}
		if (k != i) {
			tmp = relation[i];
			relation[i] = relation[k];
			relation[k] = tmp;
			tmp = rpt_set[i];
			rpt_set[i] = rpt_set[k];
			rpt_set[k] = tmp;
		}
	}
	return nbr_rpt_set;
}

static int next_rpt3(void)
{
	if (cur_mtidx < 0 || mt_info[cur_mtidx].ok)
		return 0;
	if (idx_rpt_set < nbr_rpt_set) {
		level_mtidx = rpt_set[idx_rpt_set];
		idx_rpt_set ++;
		return 1;
	}
	return 0;
}

static int next_idx3(void)
{
	while ((cur_mtidx = next_idx_for_bylevel(cur_mtidx)) >= 0) {
		level_mtidx = -1;
		return build_rpt_set3(cur_mtidx) && next_rpt3();
	}
	return 0;
}

static int next_phase3(void)
{
	while ((cur_phase = next_phase(cur_phase)) > 0) {
		cur_mtidx = -1;
		if (next_idx3())
			return 1;
	}
	return 0;
}

static int next_level(void)
{
	while (++ level < 3 /* NBR_RPT */) {
		cur_phase = PHASE_A;
		cur_mtidx = -1;
		if (next_idx3())
			return 1;
		else if (next_phase3())
			return 1;
	}
	return 0;
}

/* step 3 find new repeater level by level */
static int route_bylevel(void)
{
	if (next_rpt3() || next_idx3() || next_phase3() || next_level()) {
		route_test_path(cur_mtidx, level_mtidx, tmt_get_phase(level_mtidx));
		return 0;
	}
	else {
		PRINTF("route: step3 finished.\n");
		return 1;
	}
}

static void route_end(void)
{
	int idx;
	struct _mt_info *pinfo;

	for (idx = 0, pinfo = mt_info; idx < NBR_REAL_MT; idx ++, pinfo ++) {
		if (!pinfo->booked)
			continue;
		if (!pinfo->ok && pinfo->rpt != UNKNOWN_RPT)
			rpt_found(idx, pinfo->rpt);
		set_nun(idx, 0);
	}
	PRINTF("route: step4 finished.\n");
}

static int route_needed(int idx)
{
	RPT_PATH_IDX path_idx;

	return (!is_valid_phase(tmt_get_phase(idx))
		|| !tstat_get_path_idx(idx, &path_idx) /* include rpt level > 7 */
		|| get_nun(idx) >= NUN);
}

static int is_in_path(int idx, RPT_PATH_IDX *path_idx)
{
	int i;

	for (i = 0; i < path_idx->count; i ++) {
		if (path_idx->rpts[i] == idx)
			return 1;
	}
	return 0;
}

static int route_init(void)
{
	int idx, j, found = 0;
	RPT_PATH_IDX path_idx;

	/* route procedure run for: 1. phase-unknown meters;
	 * 2. path-unknown meters; 3. meters with nrn >= NUN */
	for (idx = 0; idx < NBR_REAL_MT; idx ++) {
		mt_info[idx].ok = 0;
		mt_info[idx].rpt = UNKNOWN_RPT;
		mt_info[idx].booked = 0;
		if (tmt_is_empty(idx))
			continue;
		/* for full routing */
		if (full_rt_count > 0) {
			mt_info[idx].booked = 1;
			found = 1;
			continue;
		}

		/* for partial routing */
		/* if a meter's path fails, paths of the meters who use it
		 *  as repeater directly or indirectly should be recomputed */
		if (!mt_info[idx].booked && route_needed(idx)) {
			mt_info[idx].booked = 1;
			for (j = 0; j < NBR_REAL_MT; j++) {
				if (mt_info[j].booked)
					continue;
				if (tstat_get_path_idx(j, &path_idx)
					&& is_in_path(idx, &path_idx)) {
					mt_info[j].booked = 1;
				}
			}
			found = 1;
		}
	}
	return found;
}

/*
 * Runs full routing in those days:
 * 1). 12th/28th of each month: 1 times/day, (to compute the path before
 *	   reading month energy.)
 * 2). no full routing in other time.
 * Note: no effect on the partial routing procedure.
 * 			fhzeng, 2007-10-6
 */
void sched_full_route(void)
{
	struct tm tm;

	my_time(&tm);
	if (tm.tm_mday == 12 || tm.tm_mday == 28) {
		full_rt_count = 1;
	}
	else {
		full_rt_count = 0;
	}
}

void route_event(EVENT event)
{
	trt_lock();
	if (event == EVENT_DAY_CHANGE) {
		trt_update_all();
	}
	if (event == EVENT_DAY_CHANGE || event == EVENT_DAY_BEGIN
		|| event == EVENT_POWER_ON) {
		loop_count = 0;
	}
	if (event == EVENT_DAY_CHANGE || event == EVENT_POWER_ON) {
		sched_full_route();
	}
	trt_unlock();
}

void route_meter_change(void)
{
	trt_lock();
	change = 1;
	trt_unlock();
}

int mt_route(const struct tm *tm)
{
	int need;
	BYTE type;

	trt_lock();
	fparam_lock();
	if (change) {
		step = 0;
		change = 0;
		loop_count = 0;
	}
	if (tm->tm_hour >= 0 && tm->tm_hour <= 2) {
		need = 0;
	}
	else {
		fparam_get_value(FPARAM_RELAY_METHOD, &type, 1);
		need = (type == 0);
	}
	fparam_unlock();
	trt_unlock();
	if (!need || loop_count >= 10)
		return 0;
	switch (step) {
	case 0:
		if (route_init()) {
			cur_mtidx = -1;
			level = 0;
			cur_phase = PHASE_A;
			step = 1;
		}
		break;
	case 1:
		if (route_bkup_rpt()) {
			cur_phase = PHASE_A;
			step = 2;
		}
		break;
	case 2:
		if (route_direct()) {
			level = 0;
			level_mtidx = -1;
			cur_phase = PHASE_A;
			step = 3;
		}
		break;
	case 3:
		if (route_bylevel()) {
			step = 4;
		}
		break;
	default:
		route_end();
		step = 0;
		loop_count ++;
		if (full_rt_count > 0) {
			full_rt_count --;
		}
		break;
	}
	return step;
}
