/*
 *  C-Plan Concentrator
 *
 *  Copyright (C) 2006-2007, Shenzhen Kaifa Technology Co.,Ltd.
 *
 *  Author: dajiang wan
 *  Created on: 2006-09-05
 */
#include "afn.h"
#include "msg_proc.h"
#include "common.h"
#include "misc.h"
#include "t_mt.h"
#include "f_mt.h"

#define MAX_CNT			128

typedef struct {
	BYTE cnt, day, month, year;
} ST_REC;

typedef struct {
	BYTE type, mt_cnt;
	short mt_idx[MAX_CNT];
	ST_REC mt_rec[MAX_CNT];
} ST_DMH;

static int get_dmh(const BYTE *param, int *param_len, ST_DMH *dmh, BYTE type)
{
	short tmp;
	int i, len = *param_len;
	const BYTE *ptr = param;
	ST_REC *rec;

	if (len < 1)
		return 0;
	dmh->type = type;
	dmh->mt_cnt = *ptr++; len --;
	if (dmh->mt_cnt & 0x80) { // all meters
		if ((type == 'M' && len < 3) || (type != 'M' && len < 4))
			return 0;
		rec = dmh->mt_rec;
		rec->cnt = *ptr ++; len --;
		if (type != 'M') {
			rec->day = bcd_to_bin(*ptr);
			ptr ++; len --;
		}
		rec->month = bcd_to_bin(*ptr);
		ptr ++; len --;
		rec->year = bcd_to_bin(*ptr);
		ptr ++; len --;
	}
	else {
		for (i = 0; i < dmh->mt_cnt; i ++) {
			if ((type == 'M' && len < 5) || (type != 'M' && len < 6))
				return 0;
			rec = dmh->mt_rec + i;
			tmp = ctos(ptr); ptr += 2; len -= 2;
			dmh->mt_idx[i] = METER_INDEX(tmp);
			rec->cnt = *ptr ++; len --;
			if (type != 'M') {
				rec->day = bcd_to_bin(*ptr);
				ptr ++; len --;
			}
			rec->month = bcd_to_bin(*ptr);
			ptr ++; len --;
			rec->year = bcd_to_bin(*ptr);
			ptr ++; len --;
		}
	}
	*param_len = ptr - param;
	return 1;
}

static int get_data(BYTE *buf, int len, const BYTE *param, int *param_len,
	BYTE type, int (*func)(BYTE *, int, WORD, BYTE, BYTE, BYTE, BYTE))
{
	WORD real_count;
	BYTE *ptr = buf, *cnt_ptr, *mon_ptr, *year_ptr;
	int i, tmp;
	short mtidx;
	ST_DMH dmh;
	ST_REC *rec;

	if (!get_dmh(param, param_len, &dmh, type))
		return 0;
	// save real meter count, it will be updated later
	real_count = 0; cnt_ptr = ptr; ptr += 2; len -= 2;
	mon_ptr = year_ptr = NULL;
	if (type == 'M') {
		if (len < 2)
			return 0;
		mon_ptr = ptr ++; len --;
		year_ptr = ptr ++; len --;
	}
	if (dmh.mt_cnt & 0x80) { // for all meters
		rec = dmh.mt_rec;
		for (i = 0; i < T_MT_ROWS_CNT; i ++) {
			mtidx = i;
			tmp = func(ptr, len, mtidx, rec->year, rec->month, rec->day,
				rec->cnt);
			if (tmp > 0) {
				ptr += tmp;
				len -= tmp;
				real_count ++;
			}
		}
	}
	else {
		for (i = 0; i < dmh.mt_cnt; i ++) {
			rec = dmh.mt_rec + i;
			mtidx = dmh.mt_idx[i];
			tmp = func(ptr, len, mtidx, rec->year, rec->month, rec->day,
				rec->cnt);
			if (tmp > 0) {
				ptr += tmp;
				len -= tmp;
				real_count ++;
			}
		}
	}
	if (type == 'M') {
		*mon_ptr = bin_to_bcd(dmh.mt_rec->month);
		*year_ptr = bin_to_bcd(dmh.mt_rec->year);
	}
	stoc(cnt_ptr, real_count); // update real meter count
	return ptr - buf;
}

static int fday_get(BYTE *buf, int len, WORD mtidx, BYTE year, BYTE month,
	BYTE day, BYTE cnt)
{
	BYTE *ptr = buf, *day_cnt, data[21], y1, m1, d1;
	MTID mtid;
	int i, tmp, energy_cnt;

	if (len < 4 || tmt_is_empty(mtidx))
		return 0;
	tmt_get_mtid(mtidx, mtid);
	if ((tmp = tmt_get_tariff(mtidx)) <= 1)
		tmp = 0;
	energy_cnt = tmp + 1;
	stoc(ptr, METER_ORDER(mtidx)); ptr += 2; len -= 2;
	day_cnt = ptr ++; len --; *day_cnt = 0;
	*ptr ++ = energy_cnt - 1; len --;
	for (i = 0; i < cnt && len >= 3 + 4 * 5; i ++) {
		memset(data, INVALID_VALUE, sizeof(data));
		y1 = year, m1 = month, d1 = day;
		next_day(&year, &month, &day);
		tmp = fmt_read_day(mtid, year, month, day, data);
		PRINTF("fday_get, tmp:%d, y1:%d, m1:%d, d1:%d, year:%d, month:%d, day:%d\n",
			tmp, y1, m1, d1, year, month, day);
		if (tmp == 1 + 4 * energy_cnt
			|| compare_today(2000 + year, month, day) <= -1) {
			memcpy(ptr, data + 1, 4 * energy_cnt);
			ptr += 4 * energy_cnt; len -= 4 * energy_cnt;
			*ptr ++ = bin_to_bcd(d1); len --;
			*ptr ++ = bin_to_bcd(m1); len --;
			*ptr ++ = bin_to_bcd(y1); len --;
			*day_cnt = *day_cnt + 1;
		}
	}
	if (*day_cnt == 0)
		return 0;
	return ptr - buf;
}

static int fmon_get(BYTE *buf, int len, WORD mtidx, BYTE year, BYTE month,
	BYTE day, BYTE cnt) // day is not used
{
	BYTE *ptr = buf, *real_mon, data[21], y1, m1;
	MTID mtid;
	int i, tmp;
	
	if (len < 3 || tmt_is_empty(mtidx))
		return 0;
	tmt_get_mtid(mtidx, mtid);
	stoc(ptr, METER_ORDER(mtidx)); ptr += 2; len -= 2;
	real_mon = ptr ++; len --; *real_mon = 0;
	for (i = 0; i < cnt && len >= 2 + 4; i ++) {
		memset(data, INVALID_VALUE, sizeof(data));
		y1 = year, m1 = month;
		next_month(&year, &month);
		tmp = fmt_read_mon(mtid, year, month, data);
		PRINTF("fmon_get, tmp:%d, y1:%d, m1:%d, year:%d, month:%d\n",
			tmp, y1, m1, year, month);
		if (tmp > 0 || compare_month(2000 + year, month) <= -1) {
			memcpy(ptr, data + 1, 4);
			ptr += 4; len -= 4;
			*ptr ++ = bin_to_bcd(m1); len --;
			*ptr ++ = bin_to_bcd(y1); len --;
			*real_mon = *real_mon + 1;
		}
	}
	if (*real_mon == 0)
		return 0;
	return ptr - buf;
}

static int fload_get(BYTE *buf, int len, WORD mtidx, BYTE year, BYTE month,
	BYTE day, BYTE cnt)
{
	BYTE *ptr = buf, *real_day, energy[4 * NBR_SAMPLES], *in_ptr;
	MTID mtid;
	int i, j, tmp;
	
	if (len < 3 || tmt_is_empty(mtidx))
		return 0;
	tmt_get_mtid(mtidx, mtid);
	stoc(ptr, METER_ORDER(mtidx)); ptr += 2; len -= 2;
	real_day = ptr ++; len --; *real_day = 0;
	for (i = 0; i < cnt && len >= 7 * NBR_SAMPLES; i ++) {
		memset(energy, INVALID_VALUE, sizeof(energy));
		tmp = fmt_read_load(mtid, year, month, day, energy);
		PRINTF("fload_get, tmp:%d, year:%d, month:%d, day:%d\n",
			tmp, year, month, day);
		if (tmp > 0 || compare_today(2000 + year, month, day) <= -1) {
			in_ptr = energy;
			for (j = 0; j < NBR_SAMPLES; j ++) {
				memcpy(ptr, in_ptr, 4);
				ptr += 4; len -= 4; in_ptr += 4;
				*ptr ++ = bin_to_bcd(j); len --;
				*ptr ++ = bin_to_bcd(day); len --;
				*ptr ++ = bin_to_bcd(month); len --;
			}
			*real_day = *real_day + 1;
		}
		next_day(&year, &month, &day);
	}
	if (*real_day == 0)
		return 0;
	return ptr - buf;
}

int afn8d_get_fn(BYTE pn, BYTE fn, BYTE *out_buf, int max_len,
	const BYTE *param, int *param_len)
{
	int ret;

	PRINTF("AFN=8DH, Pn=%d, Fn=%d\n", pn, fn);
	tmt_lock();
	fmt_lock();
	switch (fn) {
	case 1:	// day data in CON
		ret = get_data(out_buf, max_len, param, param_len, 'D', fday_get);
		break;
	case 2: // load data
		ret = get_data(out_buf, max_len, param, param_len, 'H', fload_get);
		break;
	case 3: // month data
		ret = get_data(out_buf, max_len, param, param_len, 'M', fmon_get);
		break;
	default:
		ret = 0;
		break;
	}
	fmt_unlock();
	tmt_unlock();
	return ret;
}
