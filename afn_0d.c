/*
 *  C-Plan Concentrator
 *
 *  Copyright (C) 2006-2007, Shenzhen Kaifa Technology Co.,Ltd.
 *
 *  Author: dajiang wan
 *  Created on: 2007-02-27
 */
#include "f_param.h"
#include "f_485.h"
#include "common.h"
#include "misc.h"

static int get_tdc(const BYTE *param, int *param_len, BYTE *year, BYTE *mon,
	BYTE *day, BYTE *hour, BYTE *min, BYTE *m, BYTE *n)
{
	if (*param_len >= 7) {
		*param_len = 7;
		*min = bcd_to_bin(param[0]);
		*hour = bcd_to_bin(param[1]);
		*day = bcd_to_bin(param[2]);
		*mon = bcd_to_bin(param[3]);
		*year = bcd_to_bin(param[4]);
		*m = param[5];
		*n = param[6];
		return 1;
	}
	return 0;
}

static int get_tdd(const BYTE *param, int *param_len, BYTE *year, BYTE *mon,
	BYTE *day)
{
	if (*param_len >= 3) {
		*param_len = 3;
		*day = bcd_to_bin(param[0]);
		*mon = bcd_to_bin(param[1]);
		*year = bcd_to_bin(param[2]);
		return 1;
	}
	return 0;
}

static int get_tdm(const BYTE *param, int *param_len, BYTE *year, BYTE *mon)
{
	if (*param_len >= 2) {
		*param_len = 2;
		*mon = bcd_to_bin(param[0]);
		*year = bcd_to_bin(param[1]);
		return 1;
	}
	return 0;
}

static time_t time_to_tt(BYTE year, BYTE mon, BYTE day, BYTE hour, BYTE min)
{
	struct tm tm;

	memset(&tm, 0, sizeof(tm));
	tm.tm_year = year + 100;
	tm.tm_mon = mon - 1;
	tm.tm_mday = day;
	tm.tm_hour = hour;
	tm.tm_min = min;
	return mktime(&tm);
}

static void tt_to_time(time_t tt, BYTE *year, BYTE *mon, BYTE *day,
	BYTE *hour, BYTE *min)
{
	struct tm tm;

	localtime_r(&tt, &tm);
	*year = tm.tm_year % 100;
	*mon = tm.tm_mon + 1;
	*day = tm.tm_mday;
	*hour = tm.tm_hour;
	*min = tm.tm_min;
}

static int mt485_m_to_sec(int m)
{
	if (m == 1)
		return 15 * 60;
	else if (m == 2)
		return 30 * 60;
	else if (m == 3)
		return 60 * 60;
	else
		return 0;
}

static int mt485_load_1(const MTID mtid, BYTE *out_buf, int max_len,
	const BYTE *param, int *param_len, int off, int len)
{
	BYTE *ptr = out_buf;
	BYTE year, mon, day, hour, min, m, n;
	F485_LOAD_REC rec;
	time_t tt;
	int add;

	if (!get_tdc(param, param_len, &year, &mon, &day, &hour, &min, &m, &n)
		|| m < 1 || m > 3 || max_len < 7 + len * n)
		return 0;
	memcpy(ptr, param, 7); ptr += 7;
	add = mt485_m_to_sec(m);
	tt = time_to_tt(year, mon, day, hour, min);
	while (n --) {
		if (f485_read_load(&rec, mtid, year, mon, day, hour, min))
			memcpy(ptr, ((BYTE *)&rec) + off, len);
		else
			memset(ptr, INVALID_VALUE, len);
		ptr += len;
		tt = tt + add;
		tt_to_time(tt, &year, &mon, &day, &hour, &min);
	}
	return ptr - out_buf;
}

static int mt485_load_2(const MTID mtid, BYTE *out_buf, int max_len,
	const BYTE *param, int *param_len, int off)
{
	BYTE *ptr = out_buf;
	BYTE year, mon, day, hour, min, m, n;
	F485_LOAD_REC rec;
	time_t tt;
	int add;

	if (!get_tdc(param, param_len, &year, &mon, &day, &hour, &min, &m, &n)
		|| m < 1 || m > 3 || max_len < 7 + 3 * n)
		return 0;
	memcpy(ptr, param, 7); ptr += 7;
	add = mt485_m_to_sec(m);
	tt = time_to_tt(year, mon, day, hour, min);
	while (n --) {
		if (f485_read_load(&rec, mtid, year, mon, day, hour, min)) {
			// 0xb640--0xb643, frm format XX.XX to XX.XXXX
			ptr[0] = 0;
			memcpy(ptr + 1, ((BYTE *)&rec) + off, 2);
		}
		else {
			memset(ptr, INVALID_VALUE, 3);
		}
		ptr += 3;
		tt = tt + add;
		tt_to_time(tt, &year, &mon, &day, &hour, &min);
	}
	return ptr - out_buf;
}

static int mt485_load_3(const MTID mtid, BYTE *out_buf, int max_len,
	const BYTE *param, int *param_len, int off)
{
	BYTE *ptr = out_buf;
	BYTE year, mon, day, hour, min, m, n;
	F485_LOAD_REC rec;
	time_t tt;
	int add, val;

	if (!get_tdc(param, param_len, &year, &mon, &day, &hour, &min, &m, &n)
		|| m < 1 || m > 3 || max_len < 7 + 2 * n)
		return 0;
	memcpy(ptr, param, 7); ptr += 7;
	add = mt485_m_to_sec(m);
	tt = time_to_tt(year, mon, day, hour, min);
	while (n --) {
		if (f485_read_load(&rec, mtid, year, mon, day, hour, min)) {
			// 0xb611--0xb613, from format 0XXX to XXX.X
			bcd_ctos(((BYTE *)&rec) + off, &val);
			val = val * 10;
			bcd_stoc(ptr, val);
		}
		else {
			memset(ptr, INVALID_VALUE, 2);
		}
		ptr += 2;
		tt = tt + add;
		tt_to_time(tt, &year, &mon, &day, &hour, &min);
	}
	return ptr - out_buf;
}

static int mt485_load_4(const MTID mtid, BYTE *out_buf, int max_len,
	const BYTE *param, int *param_len, int off)
{
	BYTE *ptr = out_buf;
	BYTE year, mon, day, hour, min, m, n;
	BYTE yy, mmm, dd, hh, mm;
	F485_LOAD_REC rec, rec2;
	time_t tt;
	int add, val, val2;

	if (!get_tdc(param, param_len, &year, &mon, &day, &hour, &min, &m, &n)
		|| m < 1 || m > 3 || max_len < 7 + 4 * n)
		return 0;
	memcpy(ptr, param, 7); ptr += 7;
	add = mt485_m_to_sec(m);
	tt = time_to_tt(year, mon, day, hour, min);
	while (n --) {
		tt_to_time(tt, &year, &mon, &day, &hour, &min);
		tt_to_time(tt + add, &yy, &mmm, &dd, &hh, &mm);
		if (f485_read_load(&rec, mtid, year, mon, day, hour, min)
			&& f485_read_load(&rec2, mtid, yy, mmm, dd, hh, mm)) {
			bcd_ctol(((BYTE *)&rec) + off, &val);
			bcd_ctol(((BYTE *)&rec2) + off, &val2);
			if (val > val2)
				memset(ptr, INVALID_VALUE, 4);
			else
				bcd_ltoc(ptr, (val2 - val) * 100);
		}
		else {
			memset(ptr, INVALID_VALUE, 4);
		}
		ptr += 4;
		tt = tt + add;
	}
	return ptr - out_buf;
}

static int mt485_load_5(const MTID mtid, BYTE *out_buf, int max_len,
	const BYTE *param, int *param_len, int off)
{
	BYTE *ptr = out_buf;
	BYTE year, mon, day, hour, min, m, n;
	F485_LOAD_REC rec;
	time_t tt;
	int add, val;

	if (!get_tdc(param, param_len, &year, &mon, &day, &hour, &min, &m, &n)
		|| m < 1 || m > 3 || max_len < 7 + 2 * n)
		return 0;
	memcpy(ptr, param, 7); ptr += 7;
	add = mt485_m_to_sec(m);
	tt = time_to_tt(year, mon, day, hour, min);
	while (n --) {
		if (f485_read_load(&rec, mtid, year, mon, day, hour, min)) {
			// 0xb650~0xb653, from format X.XXX to XXX.X
			bcd_ctos(((BYTE *)&rec) + off, &val);
			val = (int)((val / 1000.0 + 0.05) * 10.0);
			bcd_stoc(ptr, val);
		}
		else {
			memset(ptr, INVALID_VALUE, 2);
		}
		ptr += 2;
		tt = tt + add;
		tt_to_time(tt, &year, &mon, &day, &hour, &min);
	}
	return ptr - out_buf;
}

static int mt485_day_1(const MTID mtid, BYTE *out_buf, int max_len,
	const BYTE *param, int *param_len, int off1, int off2, int off3, int off4)
{
	BYTE year, mon, day, *ptr = out_buf, *ptr2;
	F485_DAY_REC rec;
	int i, size;

	if (max_len < 9 + 85 || !get_tdd(param, param_len, &year, &mon, &day)
		|| !f485_read_day(&rec, mtid, year, mon, day))
		return 0;
	memcpy(ptr, param, 3); ptr += 3;
	*ptr ++ = 0; *ptr ++ = 0; memcpy(ptr, param, 3); ptr += 3;
	*ptr ++ = 4;
	ptr2 = ((BYTE *)&rec) + off1;
	for (i = 0; i < 4 + 1; i ++) { // from format 14 to 11
		ptr[0] = 0;
		memcpy(ptr + 1, ptr2, 4);
		ptr2 += 4;
		ptr += 5;
 	}
	size = 4 * (4 + 1);
	memcpy(ptr, ((BYTE *)&rec) + off2, size); ptr += size;
	memcpy(ptr, ((BYTE *)&rec) + off3, size); ptr += size;
	memcpy(ptr, ((BYTE *)&rec) + off4, size); ptr += size;
	return ptr - out_buf;
}

static int mt485_day_2(const MTID mtid, BYTE *out_buf, int max_len,
	const BYTE *param, int *param_len, int off1, int off2, int off3, int off4)
{
	BYTE year, mon, day, *ptr = out_buf;
	F485_DAY_REC rec;

	if (max_len < 9 + 70 || !get_tdd(param, param_len, &year, &mon, &day)
		|| !f485_read_day(&rec, mtid, year, mon, day))
		return 0;
	memcpy(ptr, param, 3); ptr += 3;
	*ptr ++ = 0; *ptr ++ = 0; memcpy(ptr, param, 3); ptr += 3;
	*ptr ++ = 4;
	memcpy(ptr, ((BYTE *)&rec) + off1, 15); ptr += 15;
	memcpy(ptr, ((BYTE *)&rec) + off2, 20); ptr += 20;
	memcpy(ptr, ((BYTE *)&rec) + off3, 15); ptr += 15;
	memcpy(ptr, ((BYTE *)&rec) + off4, 20); ptr += 20;
	return ptr - out_buf;
}

static int mt485_day_3(const MTID mtid, BYTE *out_buf, int max_len,
	const BYTE *param, int *param_len, int off)
{
	BYTE *ptr = out_buf, *ptr1, *ptr2;
	BYTE y1, m1, d1, y2, m2, d2;
	F485_DAY_REC rec1, rec2;
	int i, val1, val2;

	if (max_len < 4 + 20 || !get_tdd(param, param_len, &y1, &m1, &d1))
		return 0;
	y2 = y1, m2 = m1, d2 = d1;
	next_day(&y2, &m2, &d2);
	if (!f485_read_day(&rec1, mtid, y1, m1, d1)
		|| !f485_read_day(&rec2, mtid, y2, m2, d2))
		return 0;
	memcpy(ptr, param, 3); ptr += 3;
	*ptr ++ = 4;
	ptr1 = ((BYTE *)&rec1) + off;
	ptr2 = ((BYTE *)&rec2) + off;
	for (i = 0; i < 5; i ++) {
		if (!bcd_ctol(ptr1, &val1) || !bcd_ctol(ptr2, &val2) || val1 > val2)
			memset(ptr, INVALID_VALUE, 4);
		else
			bcd_ltoc(ptr, (val2 - val1) * 100);
		ptr += 4, ptr1 += 4, ptr2 += 4;
	}
	return ptr - out_buf;
}

static int mt485_mon_1(const MTID mtid, BYTE *out_buf, int max_len,
	const BYTE *param, int *param_len, int off1, int off2, int off3, int off4)
{
	BYTE year, mon, *ptr = out_buf, *ptr2;
	F485_MON_REC rec;
	int i, size;

	if (max_len < 8 + 85 || !get_tdm(param, param_len, &year, &mon)
		|| !f485_read_mon(&rec, mtid, year, mon))
		return 0;
	memcpy(ptr, param, 2); ptr += 2;
	*ptr ++ = 0; *ptr ++ = 0; *ptr ++ = 1; memcpy(ptr, param, 2); ptr += 2;
	*ptr ++ = 4;
	ptr2 = ((BYTE *)&rec) + off1;
	for (i = 0; i < 4 + 1; i ++) { // from format 14 to 11
		ptr[0] = 0;
		memcpy(ptr + 1, ptr2, 4);
		ptr2 += 4;
		ptr += 5;
 	}
	size = 4 * (4 + 1);
	memcpy(ptr, ((BYTE *)&rec) + off2, size); ptr += size;
	memcpy(ptr, ((BYTE *)&rec) + off3, size); ptr += size;
	memcpy(ptr, ((BYTE *)&rec) + off4, size); ptr += size;
	return ptr - out_buf;
}

static int mt485_mon_2(const MTID mtid, BYTE *out_buf, int max_len,
	const BYTE *param, int *param_len, int off1, int off2, int off3, int off4)
{
	BYTE year, mon, *ptr = out_buf;
	F485_MON_REC rec;

	if (max_len < 8 + 70 || !get_tdm(param, param_len, &year, &mon)
		|| !f485_read_mon(&rec, mtid, year, mon))
		return 0;
	memcpy(ptr, param, 2); ptr += 2;
	*ptr ++ = 0; *ptr ++ = 0; *ptr ++ = 1; memcpy(ptr, param, 2); ptr += 2;
	*ptr ++ = 4;
	memcpy(ptr, ((BYTE *)&rec) + off1, 15); ptr += 15;
	memcpy(ptr, ((BYTE *)&rec) + off2, 20); ptr += 20;
	memcpy(ptr, ((BYTE *)&rec) + off3, 15); ptr += 15;
	memcpy(ptr, ((BYTE *)&rec) + off4, 20); ptr += 20;
	return ptr - out_buf;
}

static int mt485_mon_3(const MTID mtid, BYTE *out_buf, int max_len,
	const BYTE *param, int *param_len, int off)
{
	BYTE *ptr = out_buf, *ptr1, *ptr2;
	BYTE y1, m1, y2, m2;
	F485_MON_REC rec1, rec2;
	int i, val1, val2;

	if (max_len < 3 + 20 || !get_tdm(param, param_len, &y1, &m1))
		return 0;
	y2 = y1, m2 = m1;
	next_month(&y2, &m2);
	if (!f485_read_mon(&rec1, mtid, y1, m1)
		|| !f485_read_mon(&rec2, mtid, y2, m2))
		return 0;
	memcpy(ptr, param, 2); ptr += 2;
	*ptr ++ = 4;
	ptr1 = ((BYTE *)&rec1) + off;
	ptr2 = ((BYTE *)&rec2) + off;
	for (i = 0; i < 5; i ++) {
		if (!bcd_ctol(ptr1, &val1) || !bcd_ctol(ptr2, &val2) || val1 > val2)
			memset(ptr, INVALID_VALUE, 4);
		else
			bcd_ltoc(ptr, (val2 - val1) * 100);
		ptr += 4, ptr1 += 4, ptr2 += 4;
	}
	return ptr - out_buf;
}

int afn0d_get_fn(BYTE pn, BYTE fn, BYTE *out_buf, int max_len,
	const BYTE *param, int *param_len)
{
	MTID mtid;

	PRINTF("AFN=0DH, Pn=%d, Fn=%d\n", pn, fn);
	t485_get_mtid(pn - 1, mtid);
	switch (fn) {
	case 1:
		return mt485_day_1(mtid, out_buf, max_len, param, param_len,
			offsetof(F485_DAY_REC, di_901f),
			offsetof(F485_DAY_REC, di_911f),
			offsetof(F485_DAY_REC, di_913f),
			offsetof(F485_DAY_REC, di_914f));
	case 2:
		return mt485_day_1(mtid, out_buf, max_len, param, param_len,
			offsetof(F485_DAY_REC, di_902f),
			offsetof(F485_DAY_REC, di_912f),
			offsetof(F485_DAY_REC, di_915f),
			offsetof(F485_DAY_REC, di_916f));
	case 3:
		return mt485_day_2(mtid, out_buf, max_len, param, param_len,
			offsetof(F485_DAY_REC, di_a01f),
			offsetof(F485_DAY_REC, di_b01f),
			offsetof(F485_DAY_REC, di_a11f),
			offsetof(F485_DAY_REC, di_b11f));
	case 4:
		return mt485_day_2(mtid, out_buf, max_len, param, param_len,
			offsetof(F485_DAY_REC, di_a02f),
			offsetof(F485_DAY_REC, di_b02f),
			offsetof(F485_DAY_REC, di_a12f),
			offsetof(F485_DAY_REC, di_b12f));
	case 5:
		return mt485_day_3(mtid, out_buf, max_len, param, param_len,
			offsetof(F485_DAY_REC, di_901f));
	case 6:
		return mt485_day_3(mtid, out_buf, max_len, param, param_len,
			offsetof(F485_DAY_REC, di_911f));
	case 7:
		return mt485_day_3(mtid, out_buf, max_len, param, param_len,
			offsetof(F485_DAY_REC, di_902f));
	case 8:
		return mt485_day_3(mtid, out_buf, max_len, param, param_len,
			offsetof(F485_DAY_REC, di_912f));
	case 9:
		return mt485_day_1(mtid, out_buf, max_len, param, param_len,
			offsetof(F485_DAY_REC, di_901f),
			offsetof(F485_DAY_REC, di_911f),
			offsetof(F485_DAY_REC, di_913f),
			offsetof(F485_DAY_REC, di_914f));
	case 10:
		return mt485_day_1(mtid, out_buf, max_len, param, param_len,
			offsetof(F485_DAY_REC, di_902f),
			offsetof(F485_DAY_REC, di_912f),
			offsetof(F485_DAY_REC, di_915f),
			offsetof(F485_DAY_REC, di_916f));
	case 11:
		return mt485_day_2(mtid, out_buf, max_len, param, param_len,
			offsetof(F485_DAY_REC, di_a01f),
			offsetof(F485_DAY_REC, di_b01f),
			offsetof(F485_DAY_REC, di_a11f),
			offsetof(F485_DAY_REC, di_b11f));
	case 12:
		return mt485_day_2(mtid, out_buf, max_len, param, param_len,
			offsetof(F485_DAY_REC, di_a02f),
			offsetof(F485_DAY_REC, di_b02f),
			offsetof(F485_DAY_REC, di_a12f),
			offsetof(F485_DAY_REC, di_b12f));
	case 17:
		return mt485_mon_1(mtid, out_buf, max_len, param, param_len,
			offsetof(F485_MON_REC, di_901f),
			offsetof(F485_MON_REC, di_911f),
			offsetof(F485_MON_REC, di_913f),
			offsetof(F485_MON_REC, di_914f));
	case 18:
		return mt485_mon_1(mtid, out_buf, max_len, param, param_len,
			offsetof(F485_MON_REC, di_902f),
			offsetof(F485_MON_REC, di_912f),
			offsetof(F485_MON_REC, di_915f),
			offsetof(F485_MON_REC, di_916f));
	case 19:
		return mt485_mon_2(mtid, out_buf, max_len, param, param_len,
			offsetof(F485_MON_REC, di_a01f),
			offsetof(F485_MON_REC, di_b01f),
			offsetof(F485_MON_REC, di_a11f),
			offsetof(F485_MON_REC, di_b11f));
	case 20:
		return mt485_mon_2(mtid, out_buf, max_len, param, param_len,
			offsetof(F485_MON_REC, di_a02f),
			offsetof(F485_MON_REC, di_b02f),
			offsetof(F485_MON_REC, di_a12f),
			offsetof(F485_MON_REC, di_b12f));
	case 21:
		return mt485_mon_3(mtid, out_buf, max_len, param, param_len,
			offsetof(F485_MON_REC, di_901f));
	case 22:
		return mt485_mon_3(mtid, out_buf, max_len, param, param_len,
			offsetof(F485_MON_REC, di_911f));
	case 23:
		return mt485_mon_3(mtid, out_buf, max_len, param, param_len,
			offsetof(F485_MON_REC, di_902f));
	case 24:
		return mt485_mon_3(mtid, out_buf, max_len, param, param_len,
			offsetof(F485_MON_REC, di_912f));
	case 25: // TODO
	case 26: // TODO
	case 27: // TODO
	case 28: // TODO
	case 29: // TODO
	case 30: // TODO
	case 31: // TODO
		return 0;
	case 33: // TODO
	case 34: // TODO
	case 35: // TODO
	case 36: // TODO
	case 37: // TODO
	case 38: // TODO
		return 0;
	case 41: // TODO
	case 42: // TODO
	case 43: // TODO
	case 44: // TODO
		return 0;
	case 49:
	case 50:
	case 51:
	case 52:
		return 0;
	case 57:
	case 58:
	case 59:
	case 60:
	case 61:
	case 62:
		return 0;
	case 65:
	case 66:
		return 0;
	case 73:
	case 74:
	case 75:
	case 76:
		return 0;
	case 81:
		return mt485_load_1(mtid, out_buf, max_len, param, param_len,
			offsetof(F485_LOAD_REC, di_b630), 3);
	case 82:
		return mt485_load_1(mtid, out_buf, max_len, param, param_len,
			offsetof(F485_LOAD_REC, di_b631), 3);
	case 83:
		return mt485_load_1(mtid, out_buf, max_len, param, param_len,
			offsetof(F485_LOAD_REC, di_b632), 3);
	case 84:
		return mt485_load_1(mtid, out_buf, max_len, param, param_len,
			offsetof(F485_LOAD_REC, di_b633), 3);
	case 85:
		return mt485_load_2(mtid, out_buf, max_len, param, param_len,
			offsetof(F485_LOAD_REC, di_b640));
	case 86:
		return mt485_load_2(mtid, out_buf, max_len, param, param_len,
			offsetof(F485_LOAD_REC, di_b641));
	case 87:
		return mt485_load_2(mtid, out_buf, max_len, param, param_len,
			offsetof(F485_LOAD_REC, di_b642));
	case 88:
		return mt485_load_2(mtid, out_buf, max_len, param, param_len,
			offsetof(F485_LOAD_REC, di_b643));
	case 89:
		return mt485_load_3(mtid, out_buf, max_len, param, param_len,
			offsetof(F485_LOAD_REC, di_b611));
	case 90:
		return mt485_load_3(mtid, out_buf, max_len, param, param_len,
			offsetof(F485_LOAD_REC, di_b612));
	case 91:
		return mt485_load_3(mtid, out_buf, max_len, param, param_len,
			offsetof(F485_LOAD_REC, di_b613));
	case 92:
		return mt485_load_1(mtid, out_buf, max_len, param, param_len,
			offsetof(F485_LOAD_REC, di_b621), 2);
	case 93:
		return mt485_load_1(mtid, out_buf, max_len, param, param_len,
			offsetof(F485_LOAD_REC, di_b622), 2);
	case 94:
		return mt485_load_1(mtid, out_buf, max_len, param, param_len,
			offsetof(F485_LOAD_REC, di_b623), 2);
	case 95: // TODO
		return 0;
	case 97:
		return mt485_load_4(mtid, out_buf, max_len, param, param_len,
			offsetof(F485_LOAD_REC, di_9010));
	case 98:
		return mt485_load_4(mtid, out_buf, max_len, param, param_len,
			offsetof(F485_LOAD_REC, di_9110));
	case 99:
		return mt485_load_4(mtid, out_buf, max_len, param, param_len,
			offsetof(F485_LOAD_REC, di_9020));
	case 100:
		return mt485_load_4(mtid, out_buf, max_len, param, param_len,
			offsetof(F485_LOAD_REC, di_9120));
	case 101:
		return mt485_load_1(mtid, out_buf, max_len, param, param_len,
			offsetof(F485_LOAD_REC, di_9010), 4);
	case 102:
		return mt485_load_1(mtid, out_buf, max_len, param, param_len,
			offsetof(F485_LOAD_REC, di_9110), 4);
	case 103:
		return mt485_load_1(mtid, out_buf, max_len, param, param_len,
			offsetof(F485_LOAD_REC, di_9020), 4);
	case 104:
		return mt485_load_1(mtid, out_buf, max_len, param, param_len,
			offsetof(F485_LOAD_REC, di_9120), 4);
	case 105:
		return mt485_load_5(mtid, out_buf, max_len, param, param_len,
			offsetof(F485_LOAD_REC, di_b650));
	case 106:
		return mt485_load_5(mtid, out_buf, max_len, param, param_len,
			offsetof(F485_LOAD_REC, di_b651));
	case 107:
		return mt485_load_5(mtid, out_buf, max_len, param, param_len,
			offsetof(F485_LOAD_REC, di_b652));
	case 108:
		return mt485_load_5(mtid, out_buf, max_len, param, param_len,
			offsetof(F485_LOAD_REC, di_b653));
	case 113: // TODO
	case 114: // TODO
	case 115: // TODO
	case 116: // TODO
	case 117: // TODO
	case 118: // TODO
		return 0;
	case 121: // TODO
	case 122: // TODO
	case 123: // TODO
		return 0;
	case 129:
	case 130:
		return 0;
	case 138:
		return 0;	
	default:
		return 0;
	}
}
