/*
 *  C-Plan Concentrator
 *
 *  Copyright (C) 2006-2007, Shenzhen Kaifa Technology Co.,Ltd.
 *
 *  Author: dajiang wan
 *  Created on: 2006-09-05
 */
#include "common.h"
#include "device.h"
#include "global.h"
#include "t_db.h"
#include "t_stat.h"
#include "t_rt.h"
#include "msg_que.h"
#include "afn.h"
#include "dwl_con.h"
#include "mt_test.h"
#include "threads.h"
#include "f_param.h"
#include "f_mt.h"
#include "misc.h"
#include "lvc_test.h"
#include "lcd.h"
#include "menu_str.h"

static void print_version(void)
{
	BYTE ver[20];

	software_version_str(ver);
	printf("KaiFa C-PLAN Concentrator Software Version: %s\n", ver);
}

static void print_quit(void)
{
	char prog[PATH_MAX];

	get_prog_name(prog, sizeof(prog));
	printf("CON program \"%s\" exit.\n", prog);
}

static void process_opt_addr(const char *buf)
{
	const char *ptr;
	char addr[16];
	int len, port;

	if ((ptr = strchr(buf, ':')) != NULL) {
		len = ptr - buf;
		if (len <= sizeof(addr)) {
			memcpy(addr, buf, len);
			addr[len] = 0x0;
			port = atoi(ptr + 1);
			set_opt_addr(addr, port);
		}
	}
}

static void usage(const char *prog)
{
	printf("Usage:\n\t%s [OPTIONS]\n", prog);
	printf("\t-h,\ttake no param, to display the usage\n");
	printf("\t-v,\ttake no param, to display the software version\n");
	printf("\t-r,\ttake no param, print the path, route, day, month info\n");
	printf("\t-s,\ttake no param, to disable print debug information\n");
	printf("\t-u,\ttake no param, to use UDP instead of TCP\n");
	printf("\t-c,\ttake one param, to verride the center address\n");
	printf("\t\t(format is something like 10.0.0.1:6666)\n");
	printf("\t-n,\ttake one param, to special the lcd type\n");
	printf("\t\t(0 for st7529 default, 1 for ra8835)\n");
}

static void signal_term(int sig)
{
	g_terminated = 1;
}

static void signal_chld(int sig)
{
	int status;

	while (waitpid(-1, &status, WNOHANG) > 0)
		;
}

static void daemon_init(void)
{
	if (fork() != 0)
		exit(0);
	setsid();
	signal(SIGHUP, SIG_IGN);
	signal(SIGPIPE, SIG_IGN);
	if (fork() != 0)
		exit(0);
	close_all(3);
}

int main(int argc, char **argv)
{
	int opt, route_view;
	char prog[PATH_MAX];

	route_view = 0;
	realpath(argv[0], prog);
	set_prog_name(prog);
	while ((opt = getopt(argc, argv, "hvrtsuc:m:n:")) > 0) {
		switch (opt) {
		case 'v':
			print_version();
			return 1;
		case 'r':
			route_view = 1;
			break;
		case 's':
			g_silent = 1;
			break;
		case 'u':
			g_udp = 1;
			break;
		case 'c':
			process_opt_addr(optarg);
			break;
		case 'n':
			g_lcd_type = atoi(optarg);
			break;
		case 'h':
		default:
			usage(argv[0]);
			return 1;
		}
	}
	if (route_view) {
		open_tables();
		list_paths();
		list_route_info();
		list_day_data();
		list_month_data();
		list_load_data();
		close_tables();
		return 0;
	}
	daemon_init();
	signal(SIGINT, signal_term);
	signal(SIGTERM, signal_term);
	signal(SIGCHLD, signal_chld);
	open_tables();
	open_devices();
	msg_que_init();
	lcd_show_lines();
	lcd_update_head_info();
	lcd_update_info(c_info_sys_init_str);
	init_proc();
	msleep(2000);  /* wait for WATCHDOG ready */
	init_watchdog();
	system_status(1);
	threads_create();
	handler_power_fail();
	threads_join();
	device_status_led(0);
	dwl_check();
	system_status(0);
	msg_que_destroy();
	close_devices();
	close_tables();
	clean_check();
	print_quit();

	if (get_reboot_flag(0)) {
		kill_watchdog();
		system_reboot();
	}
	return 0;
}
