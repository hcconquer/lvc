/*****************************************************************************
 *	C-Plan Concentrator
 *
 *	(c) Copyright 2006-2007, Shenzhen Kaifa Technology Co.,Ltd.
 *	All Rights Reserved
 *
 *****************************************************************************/

#ifndef _T_RT_H
#define _T_RT_H

#include "typedef.h"

/*
 * This table is used to collect the candidated repeaters information
 * for all meters, so that the routing procedure find new paths quickly.
 *
 * In order to protect the flash disk, usually reading or writing to this
 * file is happened in the memory, does not on the disk. We load
 * the content of this file into memory when power up, and only save the
 * changes to the disk once a day(at the begining of the day) or before
 * exiting the application.
 *
 * If power failure happen, we do not save this file for no enough time,
 * the new changes of today will be lost. This is not a problem, because
 * after power up, the routing procedure could learn this information again.
 */

/* max number of successive not_reachable, if nrn larger than NRN,
 * run ROUTING procedure find new path for this meter,
 * TODO: this parameter decides the running freqency of the ROUTING.
 * so it is better to make it be configurable.
 */
#define NUN		5

#define NBR_BKUP_RPT 	10	/* number of candidate repeaters for a meter */
#define NBR_SMPL		80
#define NBR_SMPL_BYTES	((NBR_SMPL + 7) / 8)

#define T_RT_ROWS_CNT	NBR_REAL_MT

typedef struct {
	short mtidx;
	BYTE smpl[NBR_SMPL_BYTES]; /* samples, a cyclical queue */
	BYTE smpl_idx;
	BYTE smpl_cnt;
} RPT_STATS;

typedef struct {
	BYTE nbr_rpt; /* real number of candidated repeaters */
	BYTE idx; /* index of current repeater in the info array */
	RPT_STATS rpts[NBR_BKUP_RPT];
} T_RT;

void trt_lock(void);

void trt_unlock(void);

void trt_init_row(short rowno);

void trt_get_bkup_rpts(int mtidx, int *rpts, int *n, double *csr);

void trt_add_bkup_rpt(int mtidx, int rpt_idx);

void
		trt_update_statistic(short mtidx, RPT_PATH_IDX *path_idx,
				int fail_rpt_idx);

double trt_get_csr(short mtidx);

void trt_open(void);

void trt_update(short mtidx, int flush_flag);

void trt_update_all(void);

void trt_flush(void);

void trt_close(void);

void set_nun(int mtidx, int n);

int get_nun(int mtidx);

int trt_get_rpt_csr(short mtidx, short *rpt_idx, double *csr);

void update_nun(short mtidx, RPT_PATH_IDX *path_idx, int fail_rpt_idx);

void list_route_info(void);

#endif /* _T_RT_H */
