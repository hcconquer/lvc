/*
 *  C-Plan Concentrator
 *
 *  Copyright (C) 2006-2007, Shenzhen Kaifa Technology Co.,Ltd.
 *
 *  Author: dajiang wan
 *  Created on: 2006-09-05
 */
#include "afn.h"
#include "msg_proc.h"
#include "common.h"
#include "mt_access.h"
#include "msg_code.h"
#include "t_mt.h"
#include "t_stat.h"
#include "f_mt.h"
#include "f_param.h"
#include "misc.h"
#include "plc.h"
#include "err_code.h"

static void time_mt_to_1(const BYTE *in_buf, int in_len, BYTE *out_buf,
		int max_len)
{
	memcpy(out_buf, in_buf, 3);
	out_buf[3] = in_buf[4];
	out_buf[4] = (in_buf[3] << 5) | in_buf[5];
	out_buf[5] = in_buf[6];
}

static int get_param(const BYTE *param, int *param_len, BYTE *method,
		WORD *mt_arr, int mt_max_cnt, int *mt_cnt, BYTE *mt_rpt)
{
	const BYTE *ptr = param;
	int i, len;
	WORD tmp;

	len = *param_len;
	if (len < 1)
		return 0;
	*mt_cnt = 0;
	*method = *ptr++;
	len--;
	if (*method != 3)
	{
		if (len < 2)
			return 0;
		tmp = ctos(ptr);
		ptr += 2;
		len -= 2;
		if (tmp > mt_max_cnt)
			return 0;
		*mt_cnt = tmp;
		for (i = 0; i < *mt_cnt; i++)
		{
			if (len < 2)
				return 0;
			tmp = ctos(ptr);
			ptr += 2;
			len -= 2;
			mt_arr[i] = METER_INDEX(tmp);
			if (*method == 1)
			{ // has repeaters
				if (len < 2 * NBR_RPT)
					return 0;
				memcpy(mt_rpt, ptr, 2 * NBR_RPT);
				ptr += 2 * NBR_RPT;
				len -= 2 * NBR_RPT;
			}
			mt_rpt += 2 * NBR_RPT;
		}
	}
	*param_len = ptr - param;
	return *param_len;
}

static int get_con_param(const BYTE *param, int *param_len)
{
	WORD mt_arr[NBR_REAL_MT];
	BYTE method, mt_rpt[NBR_REAL_MT * 2 * NBR_RPT];
	int mt_cnt, tmp;

	tmp = get_param(param, param_len, &method, mt_arr, sizeof(mt_arr)
			/ sizeof(WORD), &mt_cnt, mt_rpt);
	if (tmp == 0 || method != 3)
		return 0;
	return 1;
}

static int mt_read_1(BYTE *buf, int len, const BYTE *param, int *param_len,
		WORD di, int di_len, int add_tariff)
{
	WORD mt_arr[NBR_REAL_MT];
	BYTE method, mt_rpt[NBR_REAL_MT * 2 * NBR_RPT];
	BYTE resp[PLC_APDU_LEN], nbr_retry, *data;
	int mt_cnt, real_cnt, tmp, i, data_len, resp_len, data_cnt;
	MTID mtid;

	tmp = get_param(param, param_len, &method, mt_arr, sizeof(mt_arr)
			/ sizeof(WORD), &mt_cnt, mt_rpt);
	if (tmp == 0 || method == 3 || mt_cnt == 0 || len < 1)
		return 0;
	plc_set_idle(0);
	data = buf + 1;
	data_len = len - 1;
	real_cnt = 0;
	nbr_retry = get_nbr_retry();
	for (i = 0; i < mt_cnt; i++)
	{
		bzero(resp, sizeof(resp));
		tmt_get_mtid(mt_arr[i], mtid);
		resp_len = meter_read(mt_arr[i], mtid, nbr_retry, resp, 1, di);
		if (resp_len > 0)
		{
			if (data_len < resp_len + 3)
				break;
			stoc(data, METER_ORDER(mt_arr[i]));
			data += 2;
			data_len -= 2;
			data_cnt = resp_len / di_len;
			if (add_tariff)
			{
				*data = data_cnt - 1;
				data++;
				data_len--; // tariff count
			}
			tmp = di_len * data_cnt;
			memcpy(data, resp, tmp);
			data += tmp;
			data_len -= tmp;
			real_cnt++;
		}
	}
	buf[0] = real_cnt;
	plc_set_idle(1);
	return data - buf;
}

static int mt_read_2(BYTE *buf, int len, const BYTE *param, int *param_len,
		WORD *id_arr, int id_cnt)
{
	WORD mt_arr[NBR_REAL_MT];
	BYTE method, mt_rpt[NBR_REAL_MT * 2 * NBR_RPT];
	BYTE resp[PLC_APDU_LEN], *data, nbr_retry;
	int mt_cnt, real_cnt, tmp, i, data_len, resp_len;
	MTID mtid;

	tmp = get_param(param, param_len, &method, mt_arr, sizeof(mt_arr)
			/ sizeof(WORD), &mt_cnt, mt_rpt);
	if (tmp == 0 || method == 3 || mt_cnt == 0 || len < 1)
		return 0;
	plc_set_idle(0);
	data = buf + 1;
	data_len = len - 1;
	real_cnt = 0;
	nbr_retry = get_nbr_retry();
	for (i = 0; i < mt_cnt; i++)
	{
		bzero(resp, sizeof(resp));
		tmt_get_mtid(mt_arr[i], mtid);
		resp_len = meter_read_array(mt_arr[i], mtid, nbr_retry, resp, id_arr,
				id_cnt);
		if (resp_len > 0)
		{
			if (data_len < resp_len + 2)
				break;
			stoc(data, METER_ORDER(mt_arr[i]));
			data += 2;
			data_len -= 2;
			memcpy(data, resp, resp_len);
			data += resp_len;
			data_len -= resp_len;
			real_cnt++;
		}
	}
	buf[0] = real_cnt;
	plc_set_idle(1);
	return data - buf;
}

static int mt_read_3(BYTE *buf, int len, const BYTE *param, int *param_len)
{ // ID:0xb630..0xb633,0xb636,0xb651..0xb653,0xb611..0xb613,0xb621..0xb623
	WORD mt_arr[NBR_REAL_MT];
	BYTE method, mt_rpt[NBR_REAL_MT * 2 * NBR_RPT];
	BYTE resp[PLC_APDU_LEN], *data, nbr_retry;
	int mt_cnt, real_cnt, tmp, i, data_len, resp_len;
	MTID mtid;

	tmp = get_param(param, param_len, &method, mt_arr, sizeof(mt_arr)
			/ sizeof(WORD), &mt_cnt, mt_rpt);
	if (tmp == 0 || method == 3 || mt_cnt == 0 || len < 1)
		return 0;
	plc_set_idle(0);
	data = buf + 1;
	data_len = len - 1;
	real_cnt = 0;
	nbr_retry = get_nbr_retry();
	for (i = 0; i < mt_cnt; i++)
	{
		bzero(resp, sizeof(resp));
		tmt_get_mtid(mt_arr[i], mtid);
		resp_len = meter_read(mt_arr[i], mtid, nbr_retry, resp, 14, 0xb630,
				0xb631, 0xb632, 0xb633, 0xb636, 0xb651, 0xb652, 0xb653, 0xb611,
				0xb612, 0xb613, 0xb621, 0xb622, 0xb623);
		if (resp_len > 3)
		{
			if (data_len < resp_len + 2 + 5)
				break;
			stoc(data, METER_ORDER(mt_arr[i]));
			data += 2;
			data_len -= 2;
			tt_to_fmt_15(time(NULL), data);
			data += 5;
			data_len -= 5;
			memcpy(data, resp, resp_len);
			data += resp_len;
			data_len -= resp_len;
			real_cnt++;
		}
	}
	buf[0] = real_cnt;
	plc_set_idle(1);
	return data - buf;
}

static int mt_read_4(BYTE *buf, int len, const BYTE *param, int *param_len)
{ // read time and date, 0xC011, 0xC010
	WORD mt_arr[NBR_REAL_MT];
	BYTE method, mt_rpt[NBR_REAL_MT * 2 * NBR_RPT];
	BYTE resp[PLC_APDU_LEN], *data, nbr_retry;
	int mt_cnt, real_cnt, tmp, i, data_len, resp_len;
	MTID mtid;

	tmp = get_param(param, param_len, &method, mt_arr, sizeof(mt_arr)
			/ sizeof(WORD), &mt_cnt, mt_rpt);
	if (tmp == 0 || method == 3 || mt_cnt == 0 || len < 1)
		return 0;
	plc_set_idle(0);
	data = buf + 1;
	data_len = len - 1;
	real_cnt = 0;
	nbr_retry = get_nbr_retry();
	for (i = 0; i < mt_cnt; i++)
	{
		bzero(resp, sizeof(resp));
		tmt_get_mtid(mt_arr[i], mtid);
		resp_len = meter_read(mt_arr[i], mtid, nbr_retry, resp, 2, 0xC011,
				0xC010);
		if (resp_len >= 7)
		{
			if (data_len < 2 + 6)
				break;
			stoc(data, METER_ORDER(mt_arr[i]));
			data += 2;
			data_len -= 2;
			time_mt_to_1(resp, resp_len, data, data_len);
			data += 6;
			data_len -= 6;
			real_cnt++;
		}
	}
	buf[0] = real_cnt;
	plc_set_idle(1);
	return data - buf;
}

static int mt_read_5(BYTE *buf, int len, const BYTE *param, int *param_len,
		WORD *id_arr, int id_cnt)
{
	WORD mt_arr[NBR_REAL_MT];
	BYTE method, mt_rpt[NBR_REAL_MT * 2 * NBR_RPT];
	BYTE resp[PLC_APDU_LEN], *data, nbr_retry;
	int mt_cnt, real_cnt, tmp, i, data_len, resp_len;
	MTID mtid;

	tmp = get_param(param, param_len, &method, mt_arr, sizeof(mt_arr)
			/ sizeof(WORD), &mt_cnt, mt_rpt);
	if (tmp == 0 || method == 3 || mt_cnt == 0 || len < 1)
		return 0;
	plc_set_idle(0);
	data = buf + 1;
	data_len = len - 1;
	real_cnt = 0;
	nbr_retry = get_nbr_retry();
	for (i = 0; i < mt_cnt; i++)
	{
		bzero(resp, sizeof(resp));
		tmt_get_mtid(mt_arr[i], mtid);
		resp_len = meter_read_array(mt_arr[i], mtid, nbr_retry, resp, id_arr,
				id_cnt);
		if (resp_len > 0)
		{
			if (data_len < resp_len + 5 || resp_len < 17)
				break;
			stoc(data, METER_ORDER(mt_arr[i]));
			data += 2;
			data_len -= 2;
			*data++ = 0;
			data_len--; // seconds
			memcpy(data, resp, 5);
			data += 5;
			data_len -= 5;
			*data++ = 0;
			data_len--; // seconds
			memcpy(data, resp + 5, 5);
			data += 5;
			data_len -= 5;
			*data++ = 0;
			data_len--; // seconds
			memcpy(data, resp + 10, 5);
			data += 5;
			data_len -= 5;
			memcpy(data, resp + 15, 2);
			data += 2;
			data_len -= 2;
			real_cnt++;
		}
	}
	buf[0] = real_cnt;
	plc_set_idle(1);
	return data - buf;
}

static int afn8c_f1(BYTE *buf, int len, const BYTE *param, int *param_len)
{
	int tmp;

	if (!get_con_param(param, param_len))
		return 0;
	if ((tmp = get_date_time(buf, len)) == 0)
		return 0;
	return tmp;
}

static int afn8c_f2(BYTE *buf, int len, const BYTE *param, int *param_len)
{
	BYTE *ptr = buf;

	if (!get_con_param(param, param_len) || len < 4)
		return 0;
	fparam_lock();
	fparam_read(FPARAM_FDAY_FINISH_TIME, ptr, 3);
	ptr += 3;
	fparam_read(FPARAM_FDAY_FINISH_FLAG, ptr, 1);
	ptr++;
	fparam_unlock();

	return ptr - buf;
}

static int afn8c_f9(BYTE *buf, int len, const BYTE *param, int *param_len)
{
	if (!get_con_param(param, param_len))
		return 0;
	if (len < 16)
		return 0;
	get_software_version(buf, 8);
	get_hardware_version(buf + 8, 8);
	return 16;
}

static int afn8c_f10(BYTE *buf, int len, const BYTE *param, int *param_len)
{
	if (!get_con_param(param, param_len))
		return 0;
	if (len < 6)
		return 0;
	get_protocol_version(buf, 6);
	return 6;
}

static int afn8c_f17(BYTE *buf, int len, const BYTE *param, int *param_len)
{
	if (!get_con_param(param, param_len))
		return 0;
	if (len < 2)
		return 0;
	tmt_lock();
	stoc(buf, tmt_get_read_nbr());
	tmt_unlock();
	return 2;
}

static int afn8c_f18(BYTE *buf, int len, const BYTE *param, int *param_len)
{
	if (!get_con_param(param, param_len))
		return 0;
	if (len < 2)
		return 0;
	fmt_lock();
	stoc(buf, fmt_get_read_nbr());
	fmt_unlock();
	return 2;
}

static int afn8c_f19(BYTE *buf, int len, const BYTE *param, int *param_len)
{
	int tmp;

	if (!get_con_param(param, param_len))
		return 0;
	tmt_lock();
	tmp = tmt_get_sort_nbr(buf, len);
	tmt_unlock();
	if (tmp == 0)
		return 0;
	return tmp;
}

static int afn8c_f26(BYTE *buf, int len, const BYTE *param, int *param_len)
{ // TODO, cannot find the ID in the concentrator document
	BYTE *ptr = buf;

	if (!get_con_param(param, param_len))
		return 0;
	return ptr - buf;
}

static int afn8c_f27(BYTE *buf, int len, const BYTE *param, int *param_len)
{ // TODO, cannot find the ID in the concentrator document
	BYTE *ptr = buf;

	if (!get_con_param(param, param_len))
		return 0;
	return ptr - buf;
}

static int afn8c_f56(BYTE *buf, int len, const BYTE *param, int *param_len)
{ // read reverse energy, 0x9020
	return mt_read_1(buf, len, param, param_len, 0x9020, 4, 0);
}

static int afn8c_f57(BYTE *buf, int len, const BYTE *param, int *param_len)
{ // read current energy, 0x9010
	return mt_read_1(buf, len, param, param_len, 0x9010, 4, 0);
}

static int afn8c_f58(BYTE *buf, int len, const BYTE *param, int *param_len)
{ // read current energy block, 0x901f
	return mt_read_1(buf, len, param, param_len, 0x901f, 4, 1);
}

static int afn8c_f59(BYTE *buf, int len, const BYTE *param, int *param_len)
{ // read previous month energy block, 0x941f
	return mt_read_1(buf, len, param, param_len, 0x941f, 4, 1);
}

static int afn8c_f60(BYTE *buf, int len, const BYTE *param, int *param_len)
{ // read previous day energy, 0x9a1f
	return mt_read_1(buf, len, param, param_len, 0x9a1f, 4, 1);
}

static int afn8c_f61(BYTE *buf, int len, const BYTE *param, int *param_len)
{ // read previous day used energy
	// TODO, cannot find the ID in the concentrator document
	BYTE *ptr = buf;

	return ptr - buf;
}

static int afn8c_f62(BYTE *buf, int len, const BYTE *param, int *param_len)
{ // read remain energy, 0xb215
	WORD di_arr[] =
	{ 0xb215 };

	return mt_read_2(buf, len, param, param_len, di_arr, 1);
}

static int afn8c_f63(BYTE *buf, int len, const BYTE *param, int *param_len)
{ // read instance value
	// ID: 0xb630..0xb633, 0xb636, 0xb651..0xb653, 0xb611..0xb613, 0xb621..0xb623
	// TODO, cannot find the ID in the concentrator document
	return mt_read_3(buf, len, param, param_len);
}

static int afn8c_f65(BYTE *buf, int len, const BYTE *param, int *param_len)
{ // read breaker control, 0xe872,0xe873,0xe871
	WORD di_arr[] =
	{ 0xe872, 0xe873, 0xe871 };

	return mt_read_5(buf, len, param, param_len, di_arr, 3);
}

static int afn8c_f66(BYTE *buf, int len, const BYTE *param, int *param_len)
{ // read program count and program time, 0xb212, 0xb210
	WORD di_arr[] =
	{ 0xb212, 0xb210 };

	return mt_read_2(buf, len, param, param_len, di_arr, 2);
}

static int afn8c_f67(BYTE *buf, int len, const BYTE *param, int *param_len)
{ // read time and date, 0xc011, 0xc010
	return mt_read_4(buf, len, param, param_len);
}

static int afn8c_f73(BYTE *buf, int len, const BYTE *param, int *param_len)
{ // read lose voltage count, lose current count
	// ID: 0xe133,0xe136,0xe139, 0xe122,0xe124,0x2126
	WORD di_arr[] =
	{ 0xe133, 0xe136, 0xe139, 0xe122, 0xe124, 0xe126 };

	return mt_read_2(buf, len, param, param_len, di_arr, 6);
}

static int afn8c_f74(BYTE *buf, int len, const BYTE *param, int *param_len)
{ // read meter status word, 0xc020
	WORD di_arr[] =
	{ 0xc020 };

	return mt_read_2(buf, len, param, param_len, di_arr, 1);
}

static int afn8c_f81(BYTE *buf, int len, const BYTE *param, int *param_len)
{
	WORD mt_arr[NBR_REAL_MT];
	BYTE method, mt_rpt[NBR_REAL_MT * 2 * NBR_RPT], nbr;
	BYTE *ptr = buf;
	time_t time_nr, time_reach;
	int i, tmp, mt_cnt, mtidx, real_cnt;

	tmp = get_param(param, param_len, &method, mt_arr, sizeof(mt_arr)
			/ sizeof(WORD), &mt_cnt, mt_rpt);
	if (tmp == 0 || mt_cnt == 0 || len < 2 + 11 * mt_cnt)
		return 0;
	real_cnt = 0;
	ptr += 2;
	tstat_lock();
	for (i = 0; i < mt_cnt; i++)
	{
		mtidx = mt_arr[i];
		nbr = tstat_get_nbr_nr(mtidx);
		time_nr = tstat_get_time_nr(mtidx);
		time_reach = tstat_get_time_reach(mtidx);
		stoc(ptr, METER_ORDER(mtidx));
		ptr += 2;
		*ptr++ = nbr;
		tt_to_fmt_17(time_nr, ptr);
		ptr += 4;
		tt_to_fmt_17(time_reach, ptr);
		ptr += 4;
		real_cnt++;
	}
	tstat_unlock();
	stoc(buf, real_cnt);
	return ptr - buf;
}

static int afn8c_f89(BYTE *buf, int len, const BYTE *param, int *param_len)
{ // read previous 1 day frozen energy, 0x9a2f
	return mt_read_1(buf, len, param, param_len, 0x9a2f, 6, 1);
}

static int afn8c_f90(BYTE *buf, int len, const BYTE *param, int *param_len)
{ // read previous 2 day frozen energy, 0x9a3f
	return mt_read_1(buf, len, param, param_len, 0x9a3f, 6, 1);
}

static int afn8c_f91(BYTE *buf, int len, const BYTE *param, int *param_len)
{ // read previous 3 day frozen energy, 0x9a4f
	return mt_read_1(buf, len, param, param_len, 0x9a4f, 6, 1);
}

static int afn8c_f92(BYTE *buf, int len, const BYTE *param, int *param_len)
{ // read previous 4 day frozen energy, 0x9a5f
	return mt_read_1(buf, len, param, param_len, 0x9a5f, 6, 1);
}

static int afn8c_f93(BYTE *buf, int len, const BYTE *param, int *param_len)
{ // read previous 5 day frozen energy, 0x9a6f
	return mt_read_1(buf, len, param, param_len, 0x9a6f, 6, 1);
}

static int afn8c_f94(BYTE *buf, int len, const BYTE *param, int *param_len)
{ // read previous 6 day frozen energy, 0x9a7f
	return mt_read_1(buf, len, param, param_len, 0x9a7f, 6, 1);
}

static int afn8c_f95(BYTE *buf, int len, const BYTE *param, int *param_len)
{ // read previous 7 day frozen energy, 0x9a8f
	return mt_read_1(buf, len, param, param_len, 0x9a8f, 6, 1);
}

static int afn8c_f97(BYTE *buf, int len, const BYTE *param, int *param_len)
{ // read previous 1 month frozen energy, 0x941f
	return mt_read_1(buf, len, param, param_len, 0x941f, 4, 1);
}

static int afn8c_f98(BYTE *buf, int len, const BYTE *param, int *param_len)
{ // read previous 2 month frozen energy, 0x981f
	return mt_read_1(buf, len, param, param_len, 0x981f, 4, 1);
}

static int afn8c_f99(BYTE *buf, int len, const BYTE *param, int *param_len)
{ // read previous 3 month frozen energy, 0x9b1f
	return mt_read_1(buf, len, param, param_len, 0x9b1f, 4, 1);
}

static int afn8c_f110(BYTE *buf, int len, const BYTE *param, int *param_len)
{
	int mtidx, ret, req_len, resp_len;
	BYTE req[PLC_APDU_LEN], resp[PLC_APDU_LEN];
	WORD order;
	MTID mtid;

	if (*param_len < 3 || (*param_len - 2) > sizeof(req))
		return 0;
	order = ctos(param);
	mtidx = METER_INDEX(order);
	tmt_get_mtid(mtidx, mtid);
	memcpy(req, param + 2, *param_len - 2);
	req_len = *param_len - 2;
	ret = plc_comm(mtidx, mtid, get_nbr_retry(), req, req_len, resp,
			sizeof(resp), &resp_len);
	if (ret == ERR_OK && resp_len > 0 && len >= 2 + resp_len)
	{
		stoc(buf, order);
		memcpy(buf + 2, resp, resp_len);
		return 2 + resp_len;
	}
	return 0;
}

static struct st_fn
{
	BYTE fn;
	int (*get_fn)(BYTE *buf, int len, const BYTE *param, int *param_len);
} fn_arr[] =
{
{ 1, afn8c_f1 },
{ 2, afn8c_f2 },
{ 9, afn8c_f9 },
{ 10, afn8c_f10 },
{ 17, afn8c_f17 },
{ 18, afn8c_f18 },
{ 19, afn8c_f19 },
{ 26, afn8c_f26 },
{ 27, afn8c_f27 },
{ 56, afn8c_f56 },
{ 57, afn8c_f57 },
{ 58, afn8c_f58 },
{ 59, afn8c_f59 },
{ 60, afn8c_f60 },
{ 61, afn8c_f61 },
{ 62, afn8c_f62 },
{ 63, afn8c_f63 },
{ 65, afn8c_f65 },
{ 66, afn8c_f66 },
{ 67, afn8c_f67 },
{ 73, afn8c_f73 },
{ 74, afn8c_f74 },
{ 81, afn8c_f81 },
{ 89, afn8c_f89 },
{ 90, afn8c_f90 },
{ 91, afn8c_f91 },
{ 92, afn8c_f92 },
{ 93, afn8c_f93 },
{ 94, afn8c_f94 },
{ 95, afn8c_f95 },
{ 97, afn8c_f97 },
{ 98, afn8c_f98 },
{ 99, afn8c_f99 },
{ 110, afn8c_f110 }, };

/*
 * 读取Fn,然后将数据转到相应的处理函数
 * */
int afn8c_get_fn(BYTE pn, BYTE fn, BYTE *out_buf, int max_len,
		const BYTE *param, int *param_len)
{
	int i;

	PRINTF("AFN=8CH, Pn=%d, Fn=%d\n", pn, fn);
	for (i = 0; i < sizeof(fn_arr) / sizeof(struct st_fn); i++)
	{
		if (fn == fn_arr[i].fn)
			return (fn_arr[i].get_fn)(out_buf, max_len, param, param_len);
	}
	return 0;
}
