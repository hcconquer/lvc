#ifndef _INPUT_H
#define _INPUT_H

#include "typedef.h"
#include "lcd.h"

#define KEY_NONE	0
#define KEY_UP		5
#define KEY_DOWN	24
#define KEY_LEFT	19
#define KEY_RIGHT	4
#define KEY_ENTER	13
#define KEY_ESC		27

typedef enum {
	_dir_up = 0,
	_dir_down,
	_dir_left,
	_dir_right,
} direction_t;

typedef struct {
	int idx;
	int up_idx;
	int down_idx;
	int left_idx;
	int right_idx;
	int row;
	int col;
	int str_len;
} KEY_ATTR;

BYTE key_getch(void);

BYTE getch_timeout();

int get_input(keyboard_t type, int maxlen, char *buf, BYTE ispasswd);

#endif /* _INPUT_H */
