/*****************************************************************************
 *	C-Plan Concentrator 
 *
 *	(c) Copyright 2006-2007, Shenzhen Kaifa Technology Co.,Ltd. 
 *	All Rights Reserved 
 *		
 *****************************************************************************/

#ifndef _MSG_QUE_H
#define _MSG_QUE_H

#include "typedef.h"

#define MSG_QUE_MASTER_IN		0
#define MSG_QUE_MASTER_OUT		1
#define MSG_QUE_SLAVE_IN		2
#define MSG_QUE_SLAVE_OUT		3
#define MSG_QUE_HHU_IN			4
#define MSG_QUE_HHU_OUT			5
#define MSG_QUE_PLC_REQ			6
#define MSG_QUE_PLC_RESP		7

#define MSG_QUE_MAX				8

void msg_que_init(void);

void msg_que_destroy(void);

int msg_que_is_empty(int idx, int stamp);

int msg_que_get(int idx, void *buf, int max_len, int *len, int stamp);

int msg_que_put(int idx, const void *buf, int len, int stamp);

#endif /* _MSG_QUE_H */
