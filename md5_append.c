#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <time.h>
#include "md5.h"

static unsigned char key[16] = "\xFE\xB0\xFE\xB1\xFE\xAB\x32\x00\x21\x13\xEB\xCC\xAF\xFE\x43\x90";

static void fill_stamp(unsigned char *time_stamp)
{
	struct tm tm;
	time_t tt;
	int year;

	tt = time(NULL) + 10 * 24 * 60 * 60; // valid in 10 days
	localtime_r(&tt, &tm);
	year = tm.tm_year + 1900;
	time_stamp[0] = year / 256;
	time_stamp[1] = year % 256;
	time_stamp[2] = tm.tm_mon + 1;
	time_stamp[3] = tm.tm_mday;
	time_stamp[4] = tm.tm_hour;
	time_stamp[5] = tm.tm_min;
	time_stamp[6] = tm.tm_sec;
	time_stamp[7] = 0;
}

static void md5_append(const char *name)
{
	MD5_CTX md5;
	unsigned char hash[16], buf[1024], stamp[8], *ptr;
	int fd, size, tmpsize, tmp;

	if ((fd = open(name, O_RDWR | O_EXCL)) >= 0) {
		fill_stamp(stamp);
		MD5Init(&md5);
		size = lseek(fd, 0, SEEK_END);
		lseek(fd, 0, SEEK_SET);
		tmpsize = size;
		while (tmpsize > 0) {
			tmp = read(fd, buf, sizeof(buf));
			if (tmp < 0 && errno == EINTR)
				continue;
			if (tmp <= 0)
				break;
			MD5Update(&md5, buf, tmp);
			tmpsize -= tmp;
		}
		MD5Update(&md5, stamp, 8);
		MD5Update(&md5, key, sizeof(key));
		MD5Final(hash, &md5);
		memcpy(hash, stamp, 8);
		tmpsize = 16; ptr = hash;
		while (tmpsize > 0) {
			tmp = write(fd, ptr, tmpsize);
			if (tmp < 0 && errno == EINTR)
				continue;
			if (tmp <= 0)
				break;
			tmpsize -= tmp; ptr += tmp;
		}
		close(fd);
	}
}
	
int main(int argc, char **argv)
{
	if (argc != 2) {
		printf("This program append MD5 digest to a file\n");
		printf("Usage: %s <filename>\n", argv[0]);
		return 0;
	}
	md5_append(argv[1]);
	return 0;
}
