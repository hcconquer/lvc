#include "common.h"
#include "threads.h"
#include "global.h"
#include "device.h"

unsigned char bcd_to_bin(unsigned char val)
{
	return (val >> 4) * 10 + (val & 0x0f);
}

unsigned char bin_to_bcd(unsigned char val)
{
	return ((val / 10) << 4) + (val % 10);
}

WORD ctos(const BYTE *buf)
{
	return buf[0] + (buf[1] << 8);
}

void stoc(BYTE *buf, WORD val)
{
	buf[0] = val;
	buf[1] = val >> 8;
}

DWORD ctol(const BYTE *buf)
{
	return buf[0] + (buf[1] << 8) + (buf[2] << 16) + (buf[3] << 24);
}

void ltoc(BYTE *buf, DWORD val)
{
	buf[0] = val;
	buf[1] = val >> 8;
	buf[2] = val >> 16;
	buf[3] = val >> 24;
}

WORD msb_ctos(const BYTE *buf)
{
	return buf[1] + (buf[0] << 8);
}

DWORD msb_ctol(const BYTE *buf)
{
	return buf[3] + (buf[2] << 8) + (buf[1] << 16) + (buf[0] << 24);
}

void msb_stoc(BYTE *buf, WORD val)
{
	buf[0] = val >> 8;
	buf[1] = val;
}

void msb_ltoc(BYTE *buf, DWORD val)
{
	buf[0] = val >> 24;
	buf[1] = val >> 16;
	buf[2] = val >> 8;
	buf[3] = val;
}

int all_is_ff(const void *buf, int len)
{
	const unsigned char *ptr = buf;

	while (len--)
	{
		if (*ptr != 0xff)
			return 0;
		ptr++;
	}
	return 1;
}

int all_is_empty(const void *buf, int len)
{
	const unsigned char *ptr = buf;

	while (len--)
	{
		if (*ptr != 0x0)
			return 0;
		ptr++;
	}
	return 1;
}

void reverse(void *buf, int len)
{
	BYTE *data = buf, ch;
	int i, j, k;

	j = 0;
	k = len - 1;
	for (i = 0; i < len / 2; i++)
	{
		ch = data[j];
		data[j] = data[k];
		data[k] = ch;
		j++;
		k--;
	}
}

int reverse_copy(void *dst, const void *src, int len)
{
	char *ptr1 = dst;
	const char *ptr2 = src + (len - 1);
	int ret = len;

	while (ret--)
		*ptr1++ = *ptr2--;
	return len;
}

BYTE check_sum(const void *buf, int len)
{
	const BYTE *ptr = buf;
	BYTE sum = 0;

	while (len-- > 0)
		sum += *ptr++;
	return sum;
}

/* general pack function, multiple bytes number(low byte transfer first) */
int my_pack(void *buf, const char *fmt, ...)
{
	va_list args;
	const char *p;
	char *bp, *str;
	unsigned int val, len;

	bp = buf;
	va_start(args, fmt);
	for (p = fmt; *p != '\0'; p++)
	{
		switch (*p)
		{
		case 'c':
			*bp++ = va_arg(args, int);
			break;
		case 's':
			val = va_arg(args, int);
			*bp++ = val & 0xff;
			*bp++ = (val >> 8);
			break;
		case 'd':
			val = va_arg(args, int);
			*bp++ = val & 0xff;
			*bp++ = val >> 8;
			*bp++ = val >> 16;
			*bp++ = val >> 24;
			break;
		case 'v':
			len = va_arg(args, int);
			str = va_arg(args, char *);
			memmove(bp, str, len);
			bp += len;
			break;
		default:
			va_end(args);
			return -1;
		}
	}
	va_end(args);
	return bp - (char *) buf;
}

int my_unpack(const char *buf, const char *fmt, ...)
{
	va_list args;
	char *pc;
	unsigned short *ps;
	const char *bp, *p;
	unsigned int *pl, len;

	bp = buf;
	va_start(args, fmt);
	for (p = fmt; *p != '\0'; p++)
	{
		switch (*p)
		{
		case 'c':
			pc = va_arg(args, char *);
			*pc = *bp++;
			break;
		case 's':
			ps = va_arg(args, unsigned short *);
			*ps = ctos((unsigned char *) bp);
			bp += 2;
			break;
		case 'd':
			pl = va_arg(args, unsigned int *);
			*pl = ctol((unsigned char *) bp);
			bp += 4;
			break;
		case 'v':
			len = va_arg(args, uint);
			pc = va_arg(args, char *);
			memmove(pc, bp, len);
			bp += len;
			break;
		default:
			va_end(args);
			return -1;
		}
	}
	va_end(args);
	return bp - buf;
}

void wait_delay(int msec)
{
	int tmp;

	while (!g_terminated && msec > 0)
	{
		tmp = (msec > 1000) ? 1000 : msec;
		msleep(tmp);
		msec -= tmp;
		notify_watchdog();
	}
}

int wait_for_ready(int fd, int msec, int flag)
{
	int ret;
	fd_set fds;
	struct timeval tv;

	while (!g_terminated)
	{
		tv.tv_sec = 0;
		tv.tv_usec = msec * 1000;
		if (fd < 0)
		{
			ret = select(0, NULL, NULL, NULL, &tv);
		}
		else
		{
			FD_ZERO(&fds);
			FD_SET(fd, &fds);
			if (!flag)
				ret = select(fd + 1, &fds, NULL, NULL, &tv);
			else
				ret = select(fd + 1, NULL, &fds, NULL, &tv);
		}
		if (ret < 0 && errno == EINTR)
			continue;
		return ret;
	}
	return 0;
}

int safe_read_timeout(int fd, void *buf, int len, int timeout)
{
	int ret;
	void *ptr = buf;

	while (len > 0 && wait_for_ready(fd, timeout, 0) > 0)
	{
		notify_watchdog();
		ret = read(fd, ptr, len);
		if (ret < 0 && errno == EINTR)
			continue;
		if (ret < 0)
			return ret;
		if (ret == 0)
			break;
		ptr += ret;
		len -= ret;
	}
	return ptr - buf;
}

int safe_write_timeout(int fd, const void *buf, int len, int timeout)
{
	int ret;
	const void *ptr = buf;

	while (len > 0 && wait_for_ready(fd, timeout, 1) > 0)
	{
		notify_watchdog();
		ret = write(fd, ptr, len);
		if (ret < 0 && errno == EINTR)
			continue;
		if (ret < 0)
			return ret;
		if (ret == 0)
			break;
		ptr += ret;
		len -= ret;
	}
	return ptr - buf;
}

int safe_read(int fd, void *buf, int len)
{
	int ret;
	void *ptr = buf;

	while (len > 0)
	{
		ret = read(fd, ptr, len);
		if (ret < 0 && errno == EINTR)
			continue;
		if (ret < 0)
			return ret;
		if (ret == 0)
			break;
		ptr += ret;
		len -= ret;
	}
	return ptr - buf;
}

int safe_write(int fd, const void *buf, int len)
{
	int ret;
	const void *ptr = buf;

	while (len > 0)
	{
		ret = write(fd, ptr, len);
		if (ret < 0 && errno == EINTR)
			continue;
		if (ret < 0)
			return ret;
		if (ret == 0)
			break;
		ptr += ret;
		len -= ret;
	}
	return ptr - buf;
}

int check_file(const char *name, int size)
{
	struct stat buf;

	return stat(name, &buf) == 0 && buf.st_size == size;
}

int create_file(const char *name, int size)
{
	int fd, buf_size;
	char buf[1024 * 100];

	PRINTF("File %s is created, size:%d\n", name, size);
	if ((fd = open(name, O_CREAT | O_RDWR | O_TRUNC, 0600)) >= 0)
	{
		buf_size = sizeof(buf);
		memset(buf, 0, buf_size);
		while (size > 0)
		{
			safe_write(fd, buf, min(size, buf_size));
			size -= buf_size;
		}
		fdatasync(fd);
		close(fd);
	}
	return fd >= 0;
}

static void print_head(FILE *fp)
{
	struct tm tm;
	struct timeval tv;
	char buf[1024];
	int len;

	gettimeofday(&tv, NULL);
	localtime_r(&tv.tv_sec, &tm);
	len = snprintf(buf, sizeof(buf), "%02d-%02d %02d:%02d:%02d.%03ld\t",
			tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec,
			tv.tv_usec / 1000);
	if (len > 0)
	{
		buf[len] = 0x0;
		fprintf(fp, "%s", buf);
	}
}

static int stdout_error(void)
{
	static int stdout_err = 0;
	int ret, fd, usec;
	fd_set fds;
	struct timeval tv;

	if (stdout_err == 0)
	{
		fd = 1;
		usec = 1000 * 1000 * 60 * 4; // timeout is 4 min, watchdog is 5 min.
		do
		{
			tv.tv_sec = 0;
			tv.tv_usec = usec;
			FD_ZERO(&fds);
			FD_SET(fd, &fds);
			ret = select(fd + 1, NULL, &fds, NULL, &tv);
		} while (ret < 0 && errno == EINTR);
		if (ret <= 0)
			stdout_err = 1;
	}
	return stdout_err;
}

static void do_truncate(const char *name, int delta)
{
	int fd, size, new_size;
	char *buf;

	if ((fd = open(name, O_RDWR, 0600)) >= 0)
	{
		lockf(fd, F_LOCK, 0L);
		size = lseek(fd, 0L, SEEK_END) - delta;
		if (size > 0 && (buf = malloc(size)) != NULL)
		{
			lseek(fd, delta, SEEK_SET);
			new_size = safe_read(fd, buf, size);
			if (new_size > 0)
			{
				lseek(fd, 0L, SEEK_SET);
				safe_write(fd, buf, new_size);
				ftruncate(fd, new_size);
			}
			free(buf);
		}
		lseek(fd, 0L, SEEK_SET);
		lockf(fd, F_ULOCK, 0L);
		close(fd);
	}
}

static void do_append(const char *name, const char *data, int data_len)
{
	int fd, buf_len;
	char buf[256];
	struct tm tm;
	struct timeval tv;

	if ((fd = open(name, O_RDWR | O_CREAT, 0600)) >= 0)
	{
		gettimeofday(&tv, NULL);
		localtime_r(&tv.tv_sec, &tm);
		buf_len = snprintf(buf, sizeof(buf), "%02d-%02d %02d:%02d\t", tm.tm_mon
				+ 1, tm.tm_mday, tm.tm_hour, tm.tm_min);
		lockf(fd, F_LOCK, 0L);
		lseek(fd, 0L, SEEK_END);
		write(fd, buf, buf_len);
		write(fd, data, data_len);
		lockf(fd, F_ULOCK, 0L);
		close(fd);
	}
}

static void do_log(const char *log_file, int log_max, int log_delta,
		const char *data, int data_len)
{
	struct stat st;
	int prompt = 24;

	if (stat(log_file, &st) == 0 && st.st_size + prompt + data_len > log_max)
	{
		log_delta = st.st_size + prompt + data_len - (log_max - log_delta);
		do_truncate(log_file, log_delta);
	}
	do_append(log_file, data, data_len);
}

void PRINTB(const char *name, const void *data, int data_len)
{
	int i, j;
	const unsigned char *ptr = data;

	if (g_silent || data_len <= 0 || stdout_error())
		return;
	print_head(stdout);
	printf("%s\n\t", name);
	j = 0;
	for (i = 0; i < data_len; i++)
	{
		printf("%02x ", ptr[i]);
		j++;
		if (j == 16 && i < data_len - 1)
		{
			printf("\n\t");
			notify_watchdog();
			j = 0;
		}
	}
	printf("\n");
}

void PRINTF(const char *format, ...)
{
	va_list ap;

	if (g_silent || stdout_error())
		return;
	print_head(stdout);
	va_start(ap, format);
	vprintf(format, ap);
	va_end(ap);
}

void ERR_PRINTB(const char *prompt, short mtidx, const void *data, int data_len)
{
	char buf[4 * 1024], *ptr;
	const char *tmp;
	int i, len;
	BYTE val;

	if (data_len > 0)
	{
		if (data_len > 1024)
			data_len = 1024;
		len = snprintf(buf, sizeof(buf), "%s, mtidx:%d, data:", prompt, mtidx);
		ptr = buf + len;
		tmp = data;
		for (i = 0; i < data_len; i++)
		{
			val = *tmp++;
			snprintf(ptr, 4, "%02x ", val);
			ptr += 3;
		}
		*ptr++ = '\n';
		*ptr = 0x0;
		len = ptr - buf;
		do_log(ERR_NAME, 40 * 1024, 1 * 1024, buf, len);
	}
}

void ERR_PRINTF(const char *format, ...)
{
	va_list ap;
	char buf[1024];
	int len;

	va_start(ap, format);
	len = vsnprintf(buf, sizeof(buf), format, ap);
	do_log(ERR_NAME, 40 * 1024, 1 * 1024, buf, len);
	va_end(ap);
	PRINTF("%s", buf);
}

void LOG_PRINTF(const char *format, ...)
{
	va_list ap;
	char buf[1024];
	int len;

	va_start(ap, format);
	len = vsnprintf(buf, sizeof(buf), format, ap);
	do_log(LOG_NAME, 1024 * 512, 10 * 1024, buf, len);
	sync();
	if (!g_silent && !stdout_error())
	{
		print_head(stdout);
		vprintf(format, ap);
	}
	va_end(ap);
}

long uptime(void)
{
	struct sysinfo info;

	sysinfo(&info);
	return info.uptime;
}

void close_all(int from)
{
	int i;

	for (i = from; i < 1024; i++)
		close(i);
}

int find_pid(const char *prg_name)
{
	DIR *dir;
	struct dirent *entry;
	char status[32], buf[1024], *name;
	int fd, len, pid, pid_found;

	pid_found = 0;
	dir = opendir("/proc");
	while ((entry = readdir(dir)) != NULL)
	{
		name = entry->d_name;
		if (!(*name >= '0' && *name <= '9'))
			continue;
		pid = atoi(name);
		sprintf(status, "/proc/%d/stat", pid);
		if ((fd = open(status, O_RDONLY)) < 0)
			continue;
		len = safe_read(fd, buf, sizeof(buf) - 1);
		close(fd);
		if (len <= 0)
			continue;
		buf[len] = 0x0;
		name = strrchr(buf, ')');
		if (name == NULL || name[1] != ' ')
			continue;
		*name = 0;
		name = strrchr(buf, '(');
		if (name == NULL)
			continue;
		if (strncmp(name + 1, prg_name, 16 - 1) == 0)
		{
			pid_found = pid;
			break;
		}
	}
	closedir(dir);
	return pid_found;
}

void msleep(int msec)
{
	int ret;
	struct timeval tv;

	tv.tv_sec = 0;
	tv.tv_usec = msec * 1000;
	do
	{
		ret = select(0, NULL, NULL, NULL, &tv);
	} while (ret < 0 && errno == EINTR);
}

int my_gzip(const void *in_name, const void *out_name)
{
	FILE *fp1;
	gzFile *fp2;
	char buf[1024];
	int len, ret = 0;

	snprintf(buf, sizeof(buf), "gzip -c -9 %s >%s", (const char *) in_name,
			(const char *) out_name);
	PRINTF("Execute: %s\n", buf);
	fp1 = fopen(in_name, "rb");
	fp2 = gzopen(out_name, "wb9");
	if (fp1 != NULL && fp2 != NULL)
	{
		while (1)
		{
			len = fread(buf, 1, sizeof(buf), fp1);
			if (len > 0)
			{
				if (gzwrite(fp2, buf, len) == 0)
					break; // Fail
			}
			if (len != sizeof(buf))
			{ // Eof or Fail
				if (ferror(fp1) == 0)
					ret = 1; // Eof
				break;
			}
		}
	}
	if (fp1 != NULL)
		fclose(fp1);
	if (fp2 != NULL)
		gzclose(fp2);
	if (ret == 0)
		unlink(out_name);
	return ret;
}

int my_gunzip(const void *in_name, const void *out_name)
{
	gzFile *fp1;
	FILE *fp2;
	char buf[1024];
	int len, ret = 0;

	snprintf(buf, sizeof(buf), "gunzip -c %s >%s", (const char *) in_name,
			(const char *) out_name);
	PRINTF("Execute: %s\n", buf);
	fp1 = gzopen(in_name, "rb");
	fp2 = fopen(out_name, "wb");
	if (fp1 != NULL && fp2 != NULL)
	{
		while (1)
		{
			len = gzread(fp1, buf, sizeof(buf));
			if (len < 0)
				break; // Fail
			if (len == 0)
			{ // Eof
				ret = 1;
				break;
			}
			if (fwrite(buf, 1, len, fp2) != len)
				break; // Fail
		}
	}
	if (fp1 != NULL)
		gzclose(fp1);
	if (fp2 != NULL)
		fclose(fp2);
	if (ret == 0)
		unlink(out_name);
	return ret;
}

static const char *rtc_name = "/dev/rtc";

int check_rtc(void)
{
	int rtc, ret = 0;
	struct tm tm;

	rtc_lock();
	if ((rtc = open(rtc_name, O_RDONLY)) >= 0)
	{
		if (ioctl(rtc, RTC_RD_TIME, &tm) >= 0)
			ret = 1;
		close(rtc);
	}
	rtc_unlock();
	return ret;
}

void read_rtc(void)
{
	// Set the system time from the hardware clock
	struct timeval tv =
	{ 0, 0 };
	const struct timezone tz =
	{ timezone / 60 - 60 * daylight, 0 };
	struct tm tm;
	char *oldtz = 0;
	time_t t = 0;
	int pid, rtc, ret;

	// we must fork a child process to set time, because TZ cannot be changed
	if ((pid = fork()) < 0)
		return;
	if (pid == 0)
	{ // child process
		close_all(3);
		memset(&tm, 0, sizeof(struct tm));
		if ((rtc = open(rtc_name, O_RDONLY)) >= 0)
		{
			ret = ioctl(rtc, RTC_RD_TIME, &tm);
			close(rtc);
			if (ret >= 0)
			{
				tm.tm_isdst = -1; /* not known */
				oldtz = getenv("TZ");
				setenv("TZ", "UTC 0", 1);
				tzset();
				t = mktime(&tm);
				if (oldtz)
					setenv("TZ", oldtz, 1);
				else
					unsetenv("TZ");
				tzset();
				tv.tv_sec = t;
				settimeofday(&tv, &tz);
			}
		}
		exit(0);
	}
	// parent proces need to do nothing because we have caught the signal
}

void set_rtc(void)
{
	// Set the hardware clock to the current system time
	struct timeval tv =
	{ 0, 0 };
	struct timezone tz =
	{ 0, 0 };
	struct tm tm;
	int rtc;

	if ((rtc = open(rtc_name, O_WRONLY)) >= 0)
	{
		if (gettimeofday(&tv, &tz) == 0)
		{
			gmtime_r(&tv.tv_sec, &tm);
			tm.tm_isdst = 0;
			ioctl(rtc, RTC_SET_TIME, &tm);
		}
		close(rtc);
	}
}

void adjust_date_time(int delta)
{
	struct timeval tv;

	gettimeofday(&tv, NULL);
	tv.tv_sec += delta;
	settimeofday(&tv, NULL);
	set_rtc();
}

void my_time(struct tm *tm)
{
	time_t tt;

	time(&tt);
	localtime_r(&tt, tm);
}

int is_leap_year(int year)
{
	return ((year % 4) == 0 && (year % 100) != 0) || (year % 400) == 0;
}

int escape_string(void *buf, const void *str, int size)
{
	int i, ch;
	char *ptr1 = buf;
	const char *ptr2 = str;

	*ptr1++ = '\"';
	for (i = 0; i < size && *ptr2 != 0; i++)
	{
		ch = *ptr2++;
		if ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z') || (ch >= '0'
				&& ch <= '9'))
		{
			*ptr1++ = ch;
		}
		else
		{
			*ptr1++ = '\\';
			*ptr1++ = ((ch >> 6) & 7) + '0';
			*ptr1++ = ((ch >> 3) & 7) + '0';
			*ptr1++ = (ch & 7) + '0';
		}
	}
	*ptr1++ = '\"';
	*ptr1 = 0x0;
	return ptr1 - (char *) buf;
}

int read_lock_file(const char *name)
{
	int fd, len, pid = -1;
	char apid[20];

	if ((fd = open(name, O_RDONLY)) == -1)
		return -1;
	len = safe_read(fd, apid, sizeof(apid) - 1);
	close(fd);
	if (len <= 0)
		return -1;
	apid[len] = 0;
	sscanf(apid, "%d", &pid);
	return pid;
}

int write_lock_file(const char *lockfile)
{
	int fd, pid, lock_pid, len;
	char buf[PATH_MAX + 1], apid[16];

	snprintf(buf, sizeof(buf), "/var/lock/%s", "TM.XXXXXX");
	if ((fd = mkstemp(buf)) < 0)
		return 0;
	chmod(buf, 0644);
	pid = getpid();
	len = snprintf(apid, sizeof(apid), "%10d\n", pid);
	safe_write(fd, apid, len);
	close(fd);
	while (link(buf, lockfile) == -1)
	{
		if (errno == EEXIST)
		{
			lock_pid = read_lock_file(lockfile);
			if (lock_pid == pid)
				break;
			if (lock_pid < 0)
				continue;
			if (lock_pid == 0 || kill(lock_pid, 0) == -1)
			{
				unlink(lockfile);
				continue;
			}
		}
		unlink(buf);
		return 0;
	}
	unlink(buf);
	return 1;
}
