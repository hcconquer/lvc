/*****************************************************************************
 *	C-Plan Concentrator 
 *
 *	(c) Copyright 2006-2007, Shenzhen Kaifa Technology Co.,Ltd. 
 *	All Rights Reserved 
 *		
 *****************************************************************************/

#ifndef _T_DB_H
#define _T_DB_H

#include "typedef.h"

typedef struct {
	BYTE flag;
	MTID mtid;
	time_t tt;
} FMT_IDX;

void fmt_idx_init(FMT_IDX *fmt, int idx, const MTID mtid, const BYTE *time,
	int time_len);

int fmt_idx_read(FMT_IDX *fmt, int cnt, const MTID mtid, const BYTE *time,
	int time_len);

int fmt_idx_write(FMT_IDX *fmt, int cnt, const MTID mtid, const BYTE *time,
	int time_len);

void open_tables(void);

void flush_tables(void);

void close_tables(void);

#endif /* _T_DB_H */
