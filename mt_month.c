/*****************************************************************************
*	C-Plan Concentrator 
*
*	(c) Copyright 2006-2007, Shenzhen Kaifa Technology Co.,Ltd. 
*	All Rights Reserved 
*
*	to read the meters month frozen data.	
*
*	Author: fanhua zeng, djwan
*	Created on: 2006-07-31
*****************************************************************************/
#include "common.h"
#include "t_mt.h"
#include "t_stat.h"
#include "msg_code.h"
#include "mt_access.h"
#include "mt_month.h"
#include "f_mt.h"
#include "f_param.h"
#include "misc.h"
#include "err_code.h"

static void begin_mt_month(void)
{
	BYTE buf[128];
	int param_len;
	
	fparam_get_value(FPARAM_FMON_FINISH_FLAG, buf, 1); 
	if (buf[0] != 0x55) {
		buf[0] = 0x55;
		fparam_set_value(FPARAM_FMON_FINISH_FLAG, buf, 1, &param_len);
	}
}

static void end_mt_month(void)
{
	BYTE buf[128];
	int param_len;
	struct tm tm;

	fparam_get_value(FPARAM_FMON_FINISH_FLAG, buf, 1); 
	if (buf[0] != 0x00) {
		buf[0] = 0x00;
		my_time(&tm);
		buf[1] = bin_to_bcd(tm.tm_min);
		buf[2] = bin_to_bcd(tm.tm_hour);
		buf[3] = bin_to_bcd(tm.tm_mday);
		fparam_set_value(FPARAM_FMON_FINISH_FLAG, buf, 1, &param_len);
		fparam_set_value(FPARAM_FMON_FINISH_TIME, buf + 1, 3, &param_len);
	}
}

static void month_read(const struct tm *tm, short mtidx, const MTID mtid)
{
	#define MONTH_ENERGY_ID		0x9410
	BYTE resp[PLC_APDU_LEN], data[5], nbr_retry;
	//STATEWD statewd;
	int resp_len;

	/* to read stateword, time/date, frozen energy */
	nbr_retry = get_nbr_retry();
	bzero(resp, sizeof(resp));
	if (read_frozen_energy(mtidx, mtid, nbr_retry, MONTH_ENERGY_ID, resp,
		&resp_len)) {
		fmt_lock();
		memcpy(data, resp + 7, 5);
		fmt_write_mon(mtid, tm->tm_year % 100, tm->tm_mon + 1, data, 5);
		fmt_unlock();
	}	
	fparam_lock();
	set_last_proc(PROC_MT_DAY);
	set_last_mtidx(mtidx);
	fparam_unlock();
}

static int month_next_mtidx(short *mtidx, MTID mtid, BYTE year, BYTE mon)
{
	while ((*mtidx = tmt_next(*mtidx)) >= 0) {
		if (!tmt_is_selected(*mtidx))
			continue;
		tmt_get_mtid(*mtidx, mtid);
		if (!fmt_exist_mon(mtid, year, mon))
			return 1;
	}
	return 0;
}

static int month_all_read(BYTE year, BYTE mon)
{
	short mtidx;
	MTID mtid;

	for (mtidx = 0; mtidx < NBR_REAL_MT; mtidx ++) {
		if (!tmt_is_empty(mtidx) && tmt_is_selected(mtidx)) {
			tmt_get_mtid(mtidx, mtid);
			if (!fmt_exist_mon(mtid, year, mon))
				return 0;
		}
	}
	return 1;
}

static int month_cond(const struct tm *tm)
{
	int ret = 0;
	BYTE buf[2];  // DDHH
	
	if (!meter_read_disable(tm)) {
		fparam_get_value(FPARAM_FMON_READ_TIME, buf, 2);
		if (buf[0] == 0xff || buf[1] == 0xff || after_ddhh(bcd_to_bin(buf[0]),
			bcd_to_bin(buf[1]), tm))
			ret = 1;
	}
	return ret;
}

void month_event(EVENT event)
{
}	

int mt_month(const struct tm *tm)
{
	static short mtidx = -1;
	static MTID mtid;
	BYTE year, mon;
	short need;

	tmt_lock();
	fparam_lock();
	fmt_lock();
	year = tm->tm_year % 100;
	mon = tm->tm_mon + 1;
	if (!month_cond(tm))
		need = 0;
	else
		need = month_next_mtidx(&mtidx, mtid, year, mon);
	fmt_unlock();
	fparam_unlock();
	tmt_unlock();
	if (need) {
		LOG_PRINTF("month: %d\n", mtidx);
		month_read(tm, mtidx, mtid);
	}
	tmt_lock();
	fparam_lock();
	fmt_lock();
	if (month_all_read(year, mon))
		end_mt_month();
	else if (need)
		begin_mt_month();
	fmt_unlock();
	fparam_unlock();
	tmt_unlock();
	return need;
}
