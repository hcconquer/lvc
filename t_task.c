#include "t_task.h"
#include "t_mt.h"
#include "common.h"
#include "afn.h"
#include "msg_que.h"
#include "msg_proc.h"
#include "f_param.h"

static TASK t_task;
static TASK *p_t_task = &t_task;
static int fd_t_task;
static sem_t sem_t_task;

void task_lock(void)
{
	sem_wait(&sem_t_task);
}

void task_unlock(void)
{
	sem_post(&sem_t_task);
}

static void task_mt_write(TASK_MT *task)
{
	int off;

	off = (char *)task - (char *)p_t_task;
	lseek(fd_t_task, off, SEEK_SET);
	safe_write(fd_t_task, task, sizeof(TASK_MT));
	task_flush();
}

static void task_485_write(TASK_485 *task)
{
	int off;

	off = (char *)task - (char *)p_t_task;
	lseek(fd_t_task, off, SEEK_SET);
	safe_write(fd_t_task, task, sizeof(TASK_485));
	task_flush();
}

static int task_set(TASK_485 *task, const BYTE *buf, int len)
{
	const BYTE *ptr = buf;

	if (len >= 9 && buf[8] <= TASK_DI_COUNT && len >= 9 + buf[8] * 4) {
		task->sent = 0;
		task->cycle = *ptr ++;
		memcpy(task->clock, ptr, 6); ptr += 6;
		task->multi = *ptr ++;
		task->di_cnt = *ptr ++;
		memcpy(task->di_buf, ptr, 4 * task->di_cnt); ptr += 4 * task->di_cnt;
		task_485_write(task);
	}
	return ptr - buf;
}

static int task_set_flag(TASK_485 *task, const BYTE *buf, int len)
{
	const BYTE *ptr = buf;

	if (len >= 1) {
		task->flag = *ptr ++;
		task_485_write(task);
	}
	return ptr - buf;
}

static int task_get(const TASK_485 *task, BYTE *out_buf, int max_len)
{
	BYTE *ptr = out_buf;

	if (max_len >= 9 && max_len >= 9 + task->di_cnt * 4) {
		*ptr ++ = task->cycle;
		memcpy(ptr, task->clock, 6); ptr += 6;
		*ptr ++ = task->multi;
		*ptr ++ = task->di_cnt;
		memcpy(ptr, task->di_buf, 4 * task->di_cnt); ptr += 4 * task->di_cnt;
	}
	return ptr - out_buf;
}

static int task_get_flag(const TASK_485 *task, BYTE *out_buf, int max_len)
{
	BYTE *ptr = out_buf;

	if (max_len >= 1)
		*ptr ++ = task->flag;
	return ptr - out_buf;
}

int task1_set(int task_id, const BYTE *buf, int len)
{
	if (task_id < 1 || task_id > TASK_COUNT)
		return 0;
	return task_set(p_t_task->task1 + task_id - 1, buf, len);
}

int task2_set(int task_id, const BYTE *buf, int len)
{
	if (task_id < 1 || task_id > TASK_COUNT)
		return 0;
	return task_set(p_t_task->task2 + task_id - 1, buf, len);
}

int task3_set(int task_id, const BYTE *buf, int len)
{
	const BYTE *ptr = buf;
	TASK_MT task, *task_ptr;
	int tmp;

	if (task_id < 1 || task_id > TASK_COUNT || len < 8
		|| buf[7] > TASK_DI_COUNT || len < 8 + 4 * buf[7])
		return 0;
	task.cycle = *ptr ++; len --;
	memcpy(task.clock, ptr, 6); ptr += 6; len -= 6;
	task.di_cnt = *ptr ++; len --;
	tmp = 4 * task.di_cnt;
	memcpy(task.di_buf, ptr, tmp); ptr += tmp; len -= tmp;
	tmp = TASK_METER_COUNT;
	if (len < 1 || ptr[0] > tmp || len < 2 * ptr[0])
		return 0;
	task.mt_cnt = *ptr ++; len --;
	tmp = 2 * task.mt_cnt;
	memcpy(task.mt_buf, ptr, tmp); ptr += tmp; len -= tmp;
	task_ptr = p_t_task->task3 + task_id - 1;
	*task_ptr = task;
	task_mt_write(task_ptr);
	return ptr - buf;
}

int task1_set_flag(int task_id, const BYTE *buf, int len)
{
	if (task_id < 1 || task_id > TASK_COUNT)
		return 0;
	return task_set_flag(p_t_task->task1 + task_id - 1, buf, len);
}

int task2_set_flag(int task_id, const BYTE *buf, int len)
{
	if (task_id < 1 || task_id > TASK_COUNT)
		return 0;
	return task_set_flag(p_t_task->task2 + task_id - 1, buf, len);
}

int task3_set_flag(int task_id, const BYTE *buf, int len)
{
	TASK_MT *task;

	if (task_id < 1 || task_id > TASK_COUNT || len < 1)
		return 0;
	task = p_t_task->task3 + task_id - 1;
	task->flag = buf[0];
	task_mt_write(task);
	return 1;
}

int task1_get(int task_id, BYTE *out_buf, int max_len)
{
	if (task_id < 1 || task_id > TASK_COUNT)
		return 0;
	return task_get(p_t_task->task1 + task_id - 1, out_buf, max_len);
}

int task2_get(int task_id, BYTE *out_buf, int max_len)
{
	if (task_id < 1 || task_id > TASK_COUNT)
		return 0;
	return task_get(p_t_task->task2 + task_id - 1, out_buf, max_len);
}

int task3_get(int task_id, BYTE *out_buf, int max_len)
{
	BYTE *ptr = out_buf;
	TASK_MT *task;
	int tmp;

	if (task_id < 1 || task_id > TASK_COUNT)
		return 0;
	task = p_t_task->task3 + task_id - 1;
	if (max_len < 8 + 4 * task->di_cnt + 1 + 2 * task->mt_cnt)
		return 0;
	*ptr ++ = task->cycle; max_len --;
	memcpy(ptr, task->clock, 6); ptr += 6; max_len -= 6;
	*ptr ++ = task->di_cnt; max_len --;
	tmp = 4 * task->di_cnt;
	memcpy(ptr, task->di_buf, tmp); ptr += tmp; max_len -= tmp;
	*ptr ++ = task->mt_cnt; max_len --;
	tmp = 2 * task->mt_cnt;
	memcpy(ptr, task->mt_buf, tmp); ptr += tmp; max_len -= tmp;
	return ptr - out_buf;
}

int task1_get_flag(int task_id, BYTE *out_buf, int max_len)
{
	if (task_id < 1 || task_id > TASK_COUNT)
		return 0;
	return task_get_flag(p_t_task->task1 + task_id - 1, out_buf, max_len);
}

int task2_get_flag(int task_id, BYTE *out_buf, int max_len)
{
	if (task_id < 1 || task_id > TASK_COUNT)
		return 0;
	return task_get_flag(p_t_task->task2 + task_id - 1, out_buf, max_len);
}

int task3_get_flag(int task_id, BYTE *out_buf, int max_len)
{
	TASK_MT *task;

	if (task_id < 1 || task_id > TASK_COUNT || max_len < 1)
		return 0;
	task = p_t_task->task3 + task_id - 1;
	out_buf[0] = task->flag;
	return 1;
}

static void task1_send_data(int que_idx, BYTE *buf, int max_len, int di_cnt,
	BYTE *di_arr)
{
	BYTE data[MAX_APDU_LEN], pn[8], fn[8], pn_cnt, fn_cnt, *di_ptr;
	int i, j, len, data_len, param_len = 0;

	PRINTF("task1_send_data\n");
	di_ptr = di_arr;
	for (i = 0; i < di_cnt; i ++) {
		pn_cnt = da_to_pn(di_ptr, pn); di_ptr += 2;
		fn_cnt = dt_to_fn(di_ptr, fn); di_ptr += 2;
		if (!check_pn(pn, pn_cnt) || !check_fn(fn, fn_cnt))
			continue;
		for (j = 0; j < fn_cnt; j ++) {
			pn_to_da(pn[0], data); fn_to_dt(fn[j], data + 2);
			data_len = afn0c_get_fn(pn[0], fn[j], data + 4, sizeof(data) - 4,
				NULL, &param_len);
			if (data_len > 0) {
				len = packet_any(buf, max_len, 4, 0x0c, 0, data, data_len);
				msg_que_put(que_idx, buf, len, 0);
			}
		}
	}
}

static void task2_send_data(int que_idx, BYTE *buf, int max_len, time_t end,
	int high, int interval, int multi, int di_cnt, BYTE *di_arr)
{
	BYTE data[MAX_APDU_LEN], pn[8], fn[8], pn_cnt, fn_cnt, *di_ptr, param[7];
	int i, j, len, data_len, param_len;
	time_t start;
	struct tm tm;

	PRINTF("task2_send_data\n");
	di_ptr = di_arr;
	for (i = 0; i < di_cnt; i ++) {
		pn_cnt = da_to_pn(di_ptr, pn); di_ptr += 2;
		fn_cnt = dt_to_fn(di_ptr, fn); di_ptr += 2;
		if (!check_pn(pn, pn_cnt) || !check_fn(fn, fn_cnt))
			continue;
		for (j = 0; j < fn_cnt; j ++) {
			pn_to_da(pn[0], data); fn_to_dt(fn[j], data + 2);
			start = end - interval;
			localtime_r(&start, &tm);
			if (high == 0 || high == 1) { // td_c
				param[0] = bcd_to_bin(tm.tm_min);
				param[1] = bin_to_bcd(tm.tm_hour);
				param[2] = bin_to_bcd(tm.tm_mday);
				param[3] = bin_to_bcd(tm.tm_mon + 1);
				param[4] = bin_to_bcd(tm.tm_year % 100);
				if (multi == 1)
					param[5] = 1;
				else if (multi == 2)
					param[5] = 2;
				else if (multi == 4)
					param[5] = 3;
				else
					param[5] = 1;
				param[6] = interval / (multi * 15 * 60);
				param_len = 7;
			}
			else if (high == 2) { // td_d
				param[0] = bin_to_bcd(tm.tm_mday);
				param[1] = bin_to_bcd(tm.tm_mon + 1);
				param[2] = bin_to_bcd(tm.tm_year % 100);
				param_len = 3;
			}
			else { // td_m
				param[0] = bin_to_bcd(tm.tm_mon + 1);
				param[1] = bin_to_bcd(tm.tm_year % 100);
				param_len = 2;
			}
			data_len = afn0d_get_fn(pn[0], fn[j], data + 4,
				sizeof(data) - 4, param, &param_len);
			if (data_len > 0) {
				len = packet_any(buf, max_len, 4, 0x0d, 0, data, data_len);
				msg_que_put(que_idx, buf, len, 0);
			}
		}
	}
}

static void task3_send_data(int que_idx, BYTE *buf, int max_len, int di_cnt,
	BYTE *di_arr, int mt_cnt, BYTE *mt_arr)
{
	BYTE data[MAX_APDU_LEN], pn[8], fn[8], pn_cnt, fn_cnt, *di_ptr;
	BYTE param[MAX_APDU_LEN];
	int i, j, len, data_len, param_len;

	PRINTF("task3_send_data\n");
	param[0] = 0x00;
	stoc(param + 1, mt_cnt);
	memcpy(param + 3, mt_arr, mt_cnt * 2);
	param_len = 3 + mt_cnt * 2;
	di_ptr = di_arr;
	for (i = 0; i < di_cnt; i ++) {
		pn_cnt = da_to_pn(di_ptr, pn); di_ptr += 2;
		fn_cnt = dt_to_fn(di_ptr, fn); di_ptr += 2;
		if (!check_pn(pn, pn_cnt) || !check_fn(fn, fn_cnt))
			continue;
		for (j = 0; j < fn_cnt; j ++) {
			pn_to_da(pn[0], data); fn_to_dt(fn[j], data + 2);
			data_len = afn8c_get_fn(pn[0], fn[j], data + 4, sizeof(data) - 4,
				param, &param_len);
			if (data_len > 0) {
				len = packet_any(buf, max_len, 4, 0x8c, 0, data, data_len);
				msg_que_put(que_idx, buf, len, 0);
			}
		}
	}
}

static time_t task_get_time(time_t now, time_t sent, const BYTE *base,
	int high, int low, int *interval)
{
	struct tm tm;
	time_t old_tmp, new_tmp, end;

	memset(&tm, 0, sizeof(tm));
	tm.tm_sec = bcd_to_bin(base[0]);
	tm.tm_min = bcd_to_bin(base[1]);
	tm.tm_hour = bcd_to_bin(base[2]);
	tm.tm_mday = bcd_to_bin(base[3]);
	tm.tm_mon = bcd_to_bin(base[4]) - 1;
	tm.tm_year = bcd_to_bin(base[5]) + 100;
	old_tmp = new_tmp = mktime(&tm);
	end = 0;
	*interval = new_tmp - old_tmp;
	while (new_tmp <= now + 60) {
		if (abs(new_tmp - now) <= 60) {
			if (sent != new_tmp)
				end = new_tmp;
			break;
		}
		if (high == 0)
			tm.tm_min += low;
		else if (high == 1)
			tm.tm_hour += low;
		else if (high == 2)
			tm.tm_mday += low;
		else
			tm.tm_mon += low;
		new_tmp = mktime(&tm);
		*interval = new_tmp - old_tmp;
		old_tmp = new_tmp;
	}
	return end;
}

static void task1_fill(int que_idx, BYTE *buf, int max_len, TASK_485 *task)
{
	int i, high, low, interval;
	time_t now, end;

	time(&now);
	for (i = 0; i < TASK_COUNT; i ++) {
		high = (task->cycle >> 6) & 0x03;
		low = task->cycle & 0x3f;
		if (task->flag == 0x55 && low != 0) {
			if ((end = task_get_time(now, task->sent, task->clock, high, low,
				&interval)) > 0) {
				task1_send_data(que_idx, buf, max_len, task->di_cnt,
					task->di_buf);
				task->sent = end;
				task_485_write(task);
			}
		}
		task ++;
	}
}

static void task2_fill(int que_idx, BYTE *buf, int max_len, TASK_485 *task)
{
	int i, high, low, interval;
	time_t now, end;

	time(&now);
	for (i = 0; i < TASK_COUNT; i ++) {
		high = (task->cycle >> 6) & 0x03;
		low = task->cycle & 0x3f;
		if (task->flag == 0x55 && low != 0) {
			if ((end = task_get_time(now, task->sent, task->clock, high, low,
				&interval)) > 0) {
				task2_send_data(que_idx, buf, max_len, end, high, interval,
					task->multi, task->di_cnt, task->di_buf);
				task->sent = end;
				task_485_write(task);
			}
		}
		task ++;
	}
}

static void task3_fill(int que_idx, BYTE *buf, int max_len, TASK_MT *task)
{
	int i, high, low, interval;
	time_t now, end;

	time(&now);
	for (i = 0; i < TASK_COUNT; i ++) {
		high = (task->cycle >> 6) & 0x03;
		low = task->cycle & 0x3f;
		if (task->flag == 0x55 && low != 0) {
			if ((end = task_get_time(now, task->sent, task->clock, high, low,
				&interval)) > 0) {
				task3_send_data(que_idx, buf, max_len, task->di_cnt,
					task->di_buf, task->mt_cnt, task->mt_buf);
				task->sent = end;
				task_mt_write(task);
			}
		}
		task ++;
	}
}

void task_fill(int que_idx, BYTE *buf, int max_len)
{
	task1_fill(que_idx, buf, max_len, p_t_task->task1);
	task2_fill(que_idx, buf, max_len, p_t_task->task2);
	task3_fill(que_idx, buf, max_len, p_t_task->task3);
}

void task_open(void)
{
    int size = sizeof(t_task);
	const char *name = T_TASK_NAME;

	sem_init(&sem_t_task, 0, 1);
	if (!check_file(name, size)) {
		/* File doesnot exist or file length is invalid. */
		PRINTF("File %s is created, size:%d\n", name, size);
		bzero(p_t_task, sizeof(TASK));
		fd_t_task = open(name, O_CREAT | O_RDWR | O_TRUNC, 0600);
		safe_write(fd_t_task, p_t_task, size);
		fdatasync(fd_t_task);
		close(fd_t_task);
	}
	fd_t_task = open(name, O_RDWR);
	safe_read(fd_t_task, p_t_task, size);
}

void task_flush(void)
{
	fdatasync(fd_t_task);
}

void task_close(void)
{
	fdatasync(fd_t_task);
	close(fd_t_task);
	sem_destroy(&sem_t_task);
}
