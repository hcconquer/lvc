/*****************************************************************************
 *	C-Plan Concentrator 
 *
 *	(c) Copyright 2006-2007, Shenzhen Kaifa Technology Co.,Ltd. 
 *	All Rights Reserved 
 *		
 *****************************************************************************/

#ifndef _MT_SYNC_H
#define _MT_SYNC_H

#include "typedef.h"

void sync_event(EVENT event);

int mt_sync(const struct tm *tm);

#endif // _MT_SYNC_H
