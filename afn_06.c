/*
 *  C-Plan Concentrator
 *
 *  Copyright (C) 2006-2007, Shenzhen Kaifa Technology Co.,Ltd.
 *
 *  Author: dajiang wan
 *  Created on: 2007-02-27
 */
#include "afn.h"
#include "msg_proc.h"
#include "common.h"

int afn06_get_fn(BYTE pn, BYTE fn, BYTE *out_buf, int max_len,
	const BYTE *param, int *param_len)
{
	PRINTF("AFN=06H, Pn=%d, Fn=%d\n", pn, fn);
	switch (fn) {
	case 1:	// TODO 0
		return 0;
	default:
		return 0;
	}
}
