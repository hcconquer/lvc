/*****************************************************************************
 *	C-Plan Concentrator 
 *
 *	(c) Copyright 2006-2007, Shenzhen Kaifa Technology Co.,Ltd. 
 *	All Rights Reserved 
 *		
 *****************************************************************************/

#ifndef _MT_DAY_H
#define _MT_DAY_H

#include "typedef.h"

void day_event(EVENT event);

int mt_day(const struct tm *tm);

#endif // _MT_DAY_H
