/*
 *  C-Plan Concentrator
 *
 *  Copyright (C) 2006-2007, Shenzhen Kaifa Technology Co.,Ltd.
 *
 *  Provide function to open/close tables and semaphores. 
 *
 *  Author: dajiang wan
 *  Created on: 2008-01-10
 */
#include "t_485.h"
#include "t_db.h"
#include "f_485.h"
#include "common.h"

static sem_t sem_f_485;
static int fd_f_485;
static FMT_IDX idx_load[F485_LOAD_CNT];
static FMT_IDX idx_day[F485_DAY_CNT];
static FMT_IDX idx_mon[F485_MON_CNT];

void f485_lock(void)
{
	sem_wait(&sem_f_485);
}

void f485_unlock(void)
{
	sem_post(&sem_f_485);
}

#define F485_MOVE(di) memcpy(rec->di_##di, data->di_##di, sizeof(rec->di_##di))

static void f485_convert_load(F485_LOAD_REC *rec, const T485_DATA *data)
{
	memcpy(rec->di_9010, data->di_901f, sizeof(rec->di_9010));
	memcpy(rec->di_9020, data->di_902f, sizeof(rec->di_9020));
	memcpy(rec->di_9110, data->di_911f, sizeof(rec->di_9110));
	memcpy(rec->di_9120, data->di_912f, sizeof(rec->di_9120));

	F485_MOVE(b611); F485_MOVE(b612); F485_MOVE(b613);

	F485_MOVE(b621); F485_MOVE(b622); F485_MOVE(b623); 
	F485_MOVE(b630); F485_MOVE(b631); F485_MOVE(b632); F485_MOVE(b633);

	F485_MOVE(b640); F485_MOVE(b641); F485_MOVE(b642); F485_MOVE(b643);

	F485_MOVE(b650); F485_MOVE(b651); F485_MOVE(b652); F485_MOVE(b653);
}

static void f485_convert_day(F485_DAY_REC *rec, const T485_DATA *data)
{
	F485_MOVE(901f); F485_MOVE(902f); F485_MOVE(911f); F485_MOVE(912f);
	F485_MOVE(913f); F485_MOVE(914f); F485_MOVE(915f); F485_MOVE(916f);

	F485_MOVE(a01f); F485_MOVE(a02f); F485_MOVE(a11f); F485_MOVE(a12f);
	F485_MOVE(a13f); F485_MOVE(a14f); F485_MOVE(a15f); F485_MOVE(a16f);

	F485_MOVE(b01f); F485_MOVE(b02f); F485_MOVE(b11f); F485_MOVE(b12f);
	F485_MOVE(b13f); F485_MOVE(b14f); F485_MOVE(b15f); F485_MOVE(b16f);

	F485_MOVE(b310); F485_MOVE(b311); F485_MOVE(b312); F485_MOVE(b313);

	F485_MOVE(b320); F485_MOVE(b321); F485_MOVE(b322); F485_MOVE(b323);

	F485_MOVE(b330); F485_MOVE(b331); F485_MOVE(b332); F485_MOVE(b333);

	F485_MOVE(b340); F485_MOVE(b341); F485_MOVE(b342); F485_MOVE(b343);
}

static void f485_convert_mon(F485_MON_REC *rec, const T485_DATA *data)
{
	F485_MOVE(901f); F485_MOVE(902f); F485_MOVE(911f); F485_MOVE(912f);
	F485_MOVE(913f); F485_MOVE(914f); F485_MOVE(915f); F485_MOVE(916f);

	F485_MOVE(a01f); F485_MOVE(a02f); F485_MOVE(a11f); F485_MOVE(a12f);
	F485_MOVE(a13f); F485_MOVE(a14f); F485_MOVE(a15f); F485_MOVE(a16f);

	F485_MOVE(b01f); F485_MOVE(b02f); F485_MOVE(b11f); F485_MOVE(b12f);
	F485_MOVE(b13f); F485_MOVE(b14f); F485_MOVE(b15f); F485_MOVE(b16f);
}

static void f485_load_init(void)
{
	int idx;
	F485_LOAD f485_rec[F485_LOAD_CNT];

	lseek(fd_f_485, offsetof(F_485, load), SEEK_SET);
	safe_read(fd_f_485, f485_rec, sizeof(f485_rec));
	memset(idx_load, 0, sizeof(idx_load));
	for (idx = 0; idx < F485_LOAD_CNT; idx ++) {
		if (f485_rec[idx].flag) {
			fmt_idx_init(idx_load, idx, f485_rec[idx].mtid, f485_rec[idx].time,
				sizeof(f485_rec[idx].time));
		}
	}
}

static void f485_day_init(void)
{
	int idx;
	F485_DAY f485_rec[F485_DAY_CNT];

	lseek(fd_f_485, offsetof(F_485, day), SEEK_SET);
	safe_read(fd_f_485, f485_rec, sizeof(f485_rec));
	memset(idx_day, 0, sizeof(idx_day));
	for (idx = 0; idx < F485_DAY_CNT; idx ++) {
		if (f485_rec[idx].flag) {
			fmt_idx_init(idx_day, idx, f485_rec[idx].mtid, f485_rec[idx].time,
				sizeof(f485_rec[idx].time));
		}
	}
}

static void f485_mon_init(void)
{
	int idx;
	F485_MON f485_rec[F485_MON_CNT];

	lseek(fd_f_485, offsetof(F_485, mon), SEEK_SET);
	safe_read(fd_f_485, f485_rec, sizeof(f485_rec));
	memset(idx_mon, 0, sizeof(idx_mon));
	for (idx = 0; idx < F485_MON_CNT; idx ++) {
		if (f485_rec[idx].flag) {
			fmt_idx_init(idx_mon, idx, f485_rec[idx].mtid, f485_rec[idx].time,
				sizeof(f485_rec[idx].time));
		}
	}
}

static void f485_add_load(const MTID mtid, const BYTE *time,
	const T485_DATA *data)
{
	int idx, size = sizeof(F485_LOAD);
	F485_LOAD f485_rec;

	f485_rec.flag = 1;
	memcpy(f485_rec.time, time, sizeof(f485_rec.time));
	memcpy(f485_rec.mtid, mtid, MTID_LEN);
	f485_convert_load(&f485_rec.rec, data);

	idx = fmt_idx_write(idx_load, F485_LOAD_CNT, mtid, f485_rec.time,
		sizeof(f485_rec.time));
	lseek(fd_f_485, offsetof(F_485, load) + idx * size, SEEK_SET);
	safe_write(fd_f_485, &f485_rec, size);
	PRINTF("f485_add_load, idx:%d, size:%d\n", idx, size);
}

static void f485_add_day(const MTID mtid, const BYTE *time,
	const T485_DATA *data)
{
	int idx, size = sizeof(F485_DAY);
	F485_DAY f485_rec;

	f485_rec.flag = 1;
	memcpy(f485_rec.time, time, sizeof(f485_rec.time));
	memcpy(f485_rec.mtid, mtid, MTID_LEN);
	f485_convert_day(&f485_rec.rec, data);

	idx = fmt_idx_write(idx_day, F485_DAY_CNT, mtid, f485_rec.time,
		sizeof(f485_rec.time));
	lseek(fd_f_485, offsetof(F_485, day) + idx * size, SEEK_SET);
	safe_write(fd_f_485, &f485_rec, size);
	PRINTF("f485_add_day, idx:%d, size:%d\n", idx, size);
}

static void f485_add_mon(const MTID mtid, const BYTE *time,
	const T485_DATA *data)
{
	int idx, size = sizeof(F485_MON);
	F485_MON f485_rec;

	f485_rec.flag = 1;
	memcpy(f485_rec.time, time, sizeof(f485_rec.time));
	memcpy(f485_rec.mtid, mtid, MTID_LEN);
	f485_convert_mon(&f485_rec.rec, data);

	idx = fmt_idx_write(idx_mon, F485_MON_CNT, mtid, f485_rec.time,
		sizeof(f485_rec.time));
	lseek(fd_f_485, offsetof(F_485, mon) + idx * size, SEEK_SET);
	safe_write(fd_f_485, &f485_rec, size);
	PRINTF("f485_add_mon, idx:%d, size:%d\n", idx, size);
}

void f485_set_data(const struct tm *tm, const MTID mtid, const T485_DATA *data)
{
	BYTE time[5];

	if (tm->tm_min % 15 == 0) {
		time[0] = tm->tm_year % 100;
		time[1] = tm->tm_mon + 1;
		time[2] = tm->tm_mday;
		time[3] = tm->tm_hour;
		time[4] = tm->tm_min;

		f485_add_load(mtid, time, data);
		if (tm->tm_hour == 0 && tm->tm_min == 0)
			f485_add_day(time, mtid, data);
		if (tm->tm_mday == 1 && tm->tm_hour == 0 && tm->tm_min == 0)
			f485_add_mon(time, mtid, data);
		PRINTF("f485_set_data, tm:%02d-%02d-%02d %02d:%02d\n",
			tm->tm_year % 100, tm->tm_mon + 1, tm->tm_mday,
			tm->tm_hour, tm->tm_min);
		f485_flush();
	}
}

int f485_read_load(F485_LOAD_REC *load_rec, const MTID mtid,
	BYTE year, BYTE mon, BYTE day, BYTE hour, BYTE min)
{
	int idx, size = sizeof(F485_LOAD);
	BYTE time[] = {year, mon, day, hour, min};
	F485_LOAD f485_rec;

	idx = fmt_idx_read(idx_load, F485_LOAD_CNT, mtid, time, sizeof(time));
	if (idx >= 0) {
		lseek(fd_f_485, offsetof(F_485, load) + idx * size, SEEK_SET);
		safe_read(fd_f_485, &f485_rec, size);
		*load_rec = f485_rec.rec;
		PRINTF("f485_read_load, idx:%d, size:%d\n", idx, size);
		return 1;
	}
	return 0;
}

int f485_read_day(F485_DAY_REC *day_rec, const MTID mtid,
	BYTE year, BYTE mon, BYTE day)
{
	int idx, size = sizeof(F485_DAY);
	BYTE time[] = {year, mon, day};
	F485_DAY f485_rec;

	idx = fmt_idx_read(idx_day, F485_DAY_CNT, mtid, time, sizeof(time));
	if (idx >= 0) {
		lseek(fd_f_485, offsetof(F_485, day) + idx * size, SEEK_SET);
		safe_read(fd_f_485, &f485_rec, size);
		*day_rec = f485_rec.rec;
		PRINTF("f485_read_day, idx:%d, size:%d\n", idx, size);
		return 1;
	}
	return 0;
}

int f485_read_mon(F485_MON_REC *mon_rec, const MTID mtid,
	BYTE year, BYTE mon)
{
	int idx, size = sizeof(F485_MON);
	BYTE time[] = {year, mon};
	F485_MON f485_rec;

	idx = fmt_idx_read(idx_mon, F485_MON_CNT, mtid, time, sizeof(time));
	if (idx >= 0) {
		lseek(fd_f_485, offsetof(F_485, mon) + idx * size, SEEK_SET);
		safe_read(fd_f_485, &f485_rec, size);
		*mon_rec = f485_rec.rec;
		PRINTF("f485_read_mon, idx:%d, size:%d\n", idx, size);
		return 1;
	}
	return 0;
}

void f485_open(void)
{
	int size = sizeof(F_485);
	const char *name = F_485_NAME;

	sem_init(&sem_f_485, 0, 1);
	if (!check_file(name, size))
		create_file(name, size);
	fd_f_485 = open(name, O_RDWR);
	f485_load_init();
	f485_day_init();
	f485_mon_init();
}

void f485_flush(void)
{
	fdatasync(fd_f_485);
}

void f485_close(void)
{
	fdatasync(fd_f_485);
	close(fd_f_485);
	sem_destroy(&sem_f_485);
}
