/*
 *  C-Plan Concentrator
 *
 *  Copyright (C) 2006-2007, Shenzhen Kaifa Technology Co.,Ltd.
 *
 *  Provide some functions to work with serial or socket etc.
 *
 *  Author: dajiang wan
 *  Created on: 2006-09-05
 */
#include "device.h"
#include "common.h"
#include "f_param.h"
#include "misc.h"
#include "global.h"
#include "lcd.h"
#include "t_db.h"
#include "threads.h"
#include "display.h"

#define GPIOCTL_INP_STATE	_IOR('G', 0x01, unsigned int)
#define GPIOCTL_OUTP_STATE	_IOR('G', 0x02, unsigned int)
#define GPIOCTL_DIR_STATE	_IOR('G', 0x03, unsigned int)
#define GPIOCTL_SDINP_STATE	_IOR('G', 0x04, unsigned int)
#define GPIOCTL_MUX_STATE	_IOR('G', 0x05, unsigned int)

#define GPIOCTL_OUTP_SET	_IOW('G', 0x01, unsigned int)
#define GPIOCTL_OUTP_CLR	_IOW('G', 0x02, unsigned int)
#define GPIOCTL_DIR_SET		_IOW('G', 0x03, unsigned int)
#define GPIOCTL_DIR_CLR		_IOW('G', 0x04, unsigned int)
#define GPIOCTL_SDOUTP_SET	_IOW('G', 0x05, unsigned int)
#define GPIOCTL_SDOUTP_CLR	_IOW('G', 0x06, unsigned int)
#define GPIOCTL_MUX_SET		_IOW('G', 0x07, unsigned int)
#define GPIOCTL_MUX_CLR		_IOW('G', 0x08, unsigned int)

#define BIT_WATCHDOG		(1 << 5)
#define BIT_EB_RELAY		(1 << 6)
#define BIT_LED_NORMAL		(1 << 10)
#define BIT_LCD_LIGHT		(1 << 14)
#define BIT_LED_ALARM		(1 << 15)
#define BIT_MODEM_ON		(1 << 16)
#define BIT_MODEM_OFF		(1 << 17)
#define BIT_MODEM_RELAY		(1 << 18)
#define BIT_LCD_XRESET		(1 << 19)

static sem_t sem_gpio;
static sem_t sem_hhu;
static sem_t sem_rs485;
static sem_t sem_cas;
static sem_t sem_plc;
static sem_t sem_rtc;

static int gpio_fd = -1;
static int hhu_fd = -1;
static int rs485_fd = -1;
static int cas_fd = -1;
static int plc_fd = -1;

const char *gpio_device = "/dev/gpio";
const char *cas_device = "/dev/ttyS0";
const char *rs485_device = "/dev/ttyS1";
const char *hhu_device = "/dev/ttyS3";

#ifdef DEBUG_X86
const char *plc_device = "/dev/ttyS1";
const char *modem_device = "/dev/ttyS3";
const char *modem_lockname = "/var/lock/LCK..ttyS3";
#else //DEBUG_X86
const char *plc_device = "/dev/ttyPSHS0";
const char *modem_device = "/dev/ttyPSHS2";
const char *modem_lockname = "/var/lock/LCK..ttyPSHS2";
#endif // DEBUG_X86

static int get_baud(WORD di)
{
	static const int baud1[] = {1200, 2400, 4800, 9600, 19200, 38400,
		57600, 115200};
	static const int baud2[] = {B1200, B2400, B4800, B9600, B19200, B38400,
		B57600, B115200};
	int i, baud;
	BYTE buf[4];

	fparam_lock();
	fparam_get_value(di, buf, 4);
	fparam_unlock();
	baud = ctol(buf);
	for (i = 0; i < sizeof(baud1) / sizeof(int); i ++) {
		if (baud == baud1[i])
			return baud2[i];
	}
	return B9600;
}

void gpio_open(void)
{
	gpio_fd = open(gpio_device, O_RDWR);
}

void gpio_close(void)
{
	if (gpio_fd >= 0)
		close(gpio_fd);
}

void cas_open(void)
{
	cas_fd = open_serial(cas_device, get_baud(FPARAM_485_BAUD1), 8, PARENB);
}

void cas_close(void)
{
	close_serial(cas_fd);
}

void rs485_open(void)
{
	rs485_fd = open_serial(rs485_device, get_baud(FPARAM_485_BAUD2), 8, PARENB);
}

void rs485_close(void)
{
	close_serial(rs485_fd);
}

void hhu_open(void)
{
	hhu_fd = open_serial(hhu_device, get_baud(FPARAM_HHU_BAUD), 8, PARENB);
}

void hhu_close(void)
{
	close_serial(hhu_fd);
}

void plc_open(void)
{
	plc_fd = open_serial(plc_device, B4800, 8, 0);
}

void plc_close(void)
{
	close_serial(plc_fd);
}

void gpio_lock(void)
{
	sem_wait(&sem_gpio);
}

void gpio_unlock(void)
{
	sem_post(&sem_gpio);
}

void cas_lock(void)
{
	sem_wait(&sem_cas);
}

void cas_unlock(void)
{
	sem_post(&sem_cas);
}

void rs485_lock(void)
{
	sem_wait(&sem_rs485);
}

void rs485_unlock(void)
{
	sem_post(&sem_rs485);
}

void hhu_lock(void)
{
	sem_wait(&sem_hhu);
}

void hhu_unlock(void)
{
	sem_post(&sem_hhu);
}

void plc_lock(void)
{
	sem_wait(&sem_plc);
}

void plc_unlock(void)
{
	sem_post(&sem_plc);
}

void rtc_lock(void)
{
	sem_wait(&sem_rtc);
}

void rtc_unlock(void)
{
	sem_post(&sem_rtc);
}

int cas_read(void *buf, int len, int timeout1, int timeout2)
{
	int ret;

	if ((ret = wait_for_ready(cas_fd, timeout1, 0)) > 0)
		return read_serial(cas_fd, buf, len, timeout2);
	return ret;
}

void cas_write(const void *buf, int len, int timeout)
{
	write_serial(cas_fd, buf, len, timeout);
}

int rs485_read(void *buf, int len, int timeout1, int timeout2)
{
	int ret;

	if ((ret = wait_for_ready(rs485_fd, timeout1, 0)) > 0)
		return read_serial(rs485_fd, buf, len, timeout2);
	return ret;
}

void rs485_write(const void *buf, int len, int timeout)
{
	write_serial(rs485_fd, buf, len, timeout);
}

int hhu_read(void *buf, int len, int timeout1, int timeout2)
{
	int ret;

	if ((ret = wait_for_ready(hhu_fd, timeout1, 0)) > 0)
		return read_serial(hhu_fd, buf, len, timeout2);
	return ret;
}

void hhu_write(const void *buf, int len, int timeout)
{
	write_serial(hhu_fd, buf, len, timeout);
}

int plc_read(void *buf, int len, int timeout1, int timeout2)
{
	int ret;

	if ((ret = wait_for_ready(plc_fd, timeout1, 0)) > 0)
		return read_serial(plc_fd, buf, len, timeout2);
	return ret;
}

void plc_write(const void *buf, int len, int timeout)
{
	write_serial(plc_fd, buf, len, timeout);
}

void set_nonblocking(int fd, int which)
{
	int flags;

	if (fd >= 0) {
		flags = fcntl(fd, F_GETFL, 0);
		if (which)
			flags |= O_NONBLOCK;
		else
			flags &= (~O_NONBLOCK);
		fcntl(fd, F_SETFL, flags);
	}
}

int open_socket(const char *addr, int port, int timeout)
{
	int fd, error, tmp;
	unsigned int len;
	struct sockaddr_in sa_in;
	struct timeval tv;
	fd_set fds;

	if ((fd = socket(PF_INET, SOCK_STREAM, 0)) < 0)
		return -1;
	bzero(&sa_in, sizeof(sa_in));
	sa_in.sin_family = AF_INET;
	sa_in.sin_port = htons(port);
	sa_in.sin_addr.s_addr = inet_addr(addr);
	set_nonblocking(fd, 1);
	PRINTF("Connect to %s:%d, timeout is %d ms\n", addr, port, timeout);
	tmp = connect(fd, (struct sockaddr *)&sa_in, sizeof(sa_in));
	if (tmp < 0) {
		error = errno;
		if (error != EINTR && error != EINPROGRESS) {
			close(fd);
			PRINTF("Connect fail, error code:%d, reason:%s\n",
				error, strerror(error));
			return -1;
		}
	}
	while (!g_terminated && timeout > 0) {
		tv.tv_sec = 0;
		tv.tv_usec = 0;
		FD_ZERO(&fds);
		FD_SET(fd, &fds);
		tmp = select(fd + 1, NULL, &fds, NULL, &tv);
		if (tmp > 0) {
			len = sizeof(error);
			if (getsockopt(fd, SOL_SOCKET, SO_ERROR, &error, &len) < 0
				|| error) {
				close(fd);
				PRINTF("Connect fail, error code:%d, reason:%s\n",
					error, strerror(error));
				return -1;
			}
			else {
				set_nonblocking(fd, 0);
				PRINTF("Connect ok, fd is %d\n", fd);
				return fd;
			}
		}
		else if (tmp < 0) {
			error = errno;
			if (error != EINTR && error != EINPROGRESS) {
				close(fd);
				PRINTF("Connect fail, error code:%d, reason:%s\n",
					error, strerror(error));
				return -1;
			}
		}
		msleep(100);
		timeout -= 100;
	}
	close(fd);
	PRINTF("Connect timeout\n");
	return -1;
}

int open_udp(const char *addr, int port)
{
	int fd;
	struct sockaddr_in sa_in;

	PRINTF("Open udp to %s:%d\n", addr, port);
	if ((fd = socket(PF_INET, SOCK_DGRAM, 0)) < 0)
		return -1;
	memset((char *)&sa_in, 0, sizeof(sa_in));
	sa_in.sin_family = AF_INET;
	sa_in.sin_port = htons(port);
	sa_in.sin_addr.s_addr = INADDR_ANY;
	bind(fd, (struct sockaddr *)&sa_in, sizeof(sa_in));
	memset((char *)&sa_in, 0, sizeof(sa_in));
	sa_in.sin_family = AF_INET;
	sa_in.sin_port = htons(port);
	sa_in.sin_addr.s_addr = inet_addr(addr);
	if (connect(fd, (struct sockaddr *)&sa_in, sizeof(sa_in)) < 0) {
		close(fd);
		return -1;
	}
	return fd;
}

void close_socket(int fd)
{
	close(fd);
	PRINTF("Disconnect, fd is %d\n", fd);
}

int open_serial(const char *serial, int baud, int char_size, int parity)
{
	int fd;
	struct termios newtios;

	if ((fd = open(serial, O_RDWR | O_NDELAY | O_NOCTTY)) >= 0) {
		fcntl(fd, F_SETFL, O_RDWR);
		PRINTF("Open %s, fd: %d, baud: %d, data bits: %d.\n",
			serial, fd, baud, char_size);
		tcgetattr(fd, &newtios);
		cfmakeraw(&newtios);
		if (char_size == 7)
			newtios.c_cflag = (newtios.c_cflag & ~CSIZE) | (CS7);
		newtios.c_cflag |= parity;
		tcsetattr(fd, TCSANOW, &newtios);
		tcflush(fd, TCIOFLUSH);
		set_baudrate(fd, baud);
	}
	else {
		PRINTF("Can not open %s, baud: %d, data bits: %d.\n",
			serial, baud, char_size);
	}
	return fd;
}

void close_serial(int fd)
{
	if (fd >= 0) {
		PRINTF("Close the serial port, whose fd is %d\n", fd);
		tcflush(fd, TCIOFLUSH);
		close(fd);
	}
}

void set_baudrate(int fd, int baud)
{
	struct termios newtios;

	if (fd >= 0) {
		tcgetattr(fd, &newtios);
		cfsetospeed(&newtios, baud);
		cfsetispeed(&newtios, baud);
		tcsetattr(fd, TCSADRAIN, &newtios);
		PRINTF("Change baud to %d, fd is %d\n", baud, fd);
	}
}

int write_serial(int fd, const void *buf, int len, int timeout)
{
	int ret;

	ret = safe_write_timeout(fd, buf, len, timeout);
	tcdrain(fd);
	return ret;
}

int read_serial(int fd, void *buf, int len, int timeout)
{
	return safe_read_timeout(fd, buf, len, timeout);
}

int at_cmd(int fd, const char *send, char *recv, int max_len,
	int timeout1, int timeout2)
{
	char temp[1024];
	int len;

	if ((len = strlen(send)) > 0) {
		PRINTF("To modem: %s\n", send);
		write_serial(fd, send, len, timeout1);
	}
	if (wait_for_ready(fd, timeout1, 0) <= 0 || g_terminated)
		return 0;
	if ((len = read_serial(fd, recv, max_len, timeout2)) > 0) {
		recv[len] = 0x0;
		/* discard data when we have not enough memory */
		while (read_serial(fd, temp, sizeof(temp), timeout2) > 0)
			;
		if (strlen(recv) > 0)
			PRINTF("From modem: %s\n", recv);
		wait_delay(500);
		return len;
	}
	return 0;
}

int at_cmd_full(int fd, const char *send, char **recv,
	int *recv_len, int timeout1, int timeout2)
{
	char temp[1024], *buf = NULL, *ptr = NULL;
	int len, block_size = 65536, total_size, free_size;

	if ((buf = malloc(block_size)) != NULL) {
		ptr = buf; total_size = free_size = block_size;
		if ((len = strlen(send)) > 0) {
			PRINTF("To modem: %s\n", send);
			write_serial(fd, send, len, timeout1);
		}
		if (wait_for_ready(fd, timeout1, 0) <= 0 || g_terminated) {
			free(buf); *recv = NULL; *recv_len = 0;
			return 0;
		}
		while ((len=read_serial(fd,temp,sizeof(temp),timeout2))>0) {
			if (free_size < len) {
				total_size += block_size;
				if (realloc(buf, total_size) == NULL) {
					free(buf); *recv = NULL; *recv_len = 0;
					return 0;
				}
				free_size += block_size;
			}
			memcpy(ptr, temp, len); ptr += len; free_size -= len;
		}
		*ptr = 0x0; *recv = buf; *recv_len = ptr - buf;
		if (strlen(*recv) > 0)
			PRINTF("From modem: %s\n", *recv);
		return *recv_len;
	}
	*recv = NULL; *recv_len = 0;
	return 0;
}

static BYTE modem_net_type = 0, modem_signal_intensity = 99;

int get_net_type_signal(BYTE *type, BYTE *value, BYTE *level)
{
	*type = modem_net_type;
	if (modem_signal_intensity == 99)
		return 0;
	*value = modem_signal_intensity;
	if (modem_signal_intensity <= 7)
		*level = 1;
	else if (modem_signal_intensity <= 14)
		*level = 2;
	else if (modem_signal_intensity <= 21)
		*level = 3;
	else
		*level = 4;
	return 1;
}

static void gpio_ioctl(int cmd, unsigned int *val)
{
	if (gpio_fd >= 0) {
		gpio_lock();
		ioctl(gpio_fd, cmd, val);
		gpio_unlock();
	}
}

static void modem_reset(void)
{
	unsigned int val;

	val = BIT_MODEM_OFF;
	gpio_ioctl(GPIOCTL_OUTP_SET, &val);
	wait_delay(3500);
	val = BIT_MODEM_OFF | BIT_MODEM_ON;
	gpio_ioctl(GPIOCTL_OUTP_CLR, &val);
	wait_delay(2000);
	val = BIT_MODEM_ON;
	gpio_ioctl(GPIOCTL_OUTP_SET, &val);
	wait_delay(120);
	val = BIT_MODEM_ON;
	gpio_ioctl(GPIOCTL_OUTP_CLR, &val);
}

static void modem_reboot(void)
{
	unsigned int val;

	val = BIT_MODEM_RELAY;
	gpio_ioctl(GPIOCTL_OUTP_SET, &val);
	wait_delay(2000);
	gpio_ioctl(GPIOCTL_OUTP_CLR, &val);
}

static void eb_reboot(void)
{
	unsigned int val;

	val = BIT_EB_RELAY;
	gpio_ioctl(GPIOCTL_OUTP_SET, &val);
	wait_delay(2000);
	gpio_ioctl(GPIOCTL_OUTP_CLR, &val);
}

void eb_power_off(void)
{
	unsigned int val;

	val = BIT_EB_RELAY;
	gpio_ioctl(GPIOCTL_OUTP_SET, &val);
}

void eb_power_on(void)
{
	unsigned int val;

	val = BIT_EB_RELAY;
	gpio_ioctl(GPIOCTL_OUTP_CLR, &val);
}

void device_lcd_light(int on)
{
	unsigned int val;

	val = BIT_LCD_LIGHT;
	if (on)
		gpio_ioctl(GPIOCTL_OUTP_SET, &val);
	else
		gpio_ioctl(GPIOCTL_OUTP_CLR, &val);
}

void device_lcd_reset(void)
{
	unsigned int val;

	val = BIT_LCD_XRESET;
	gpio_ioctl(GPIOCTL_OUTP_CLR, &val);
	wait_delay(100);
	gpio_ioctl(GPIOCTL_OUTP_SET, &val);
	wait_delay(100);
}

// Set status led, bit 0 for green(normal), bit 1 for red(alarm)
void device_status_led(int on)
{
	unsigned int val1, val2;

	val1 = val2 = 0;
	if (on & 0x01)
		val1 |= BIT_LED_NORMAL;
	else
		val2 |= BIT_LED_NORMAL;
	if (on & 0x02)
		val1 |= BIT_LED_ALARM;
	else
		val2 |= BIT_LED_ALARM;
	gpio_ioctl(GPIOCTL_OUTP_SET, &val1);
	gpio_ioctl(GPIOCTL_OUTP_CLR, &val2);
}

unsigned int device_signal_input(void)
{
	unsigned int tmp = 0;

	gpio_ioctl(GPIOCTL_INP_STATE, &tmp);
	return (tmp >> 10) & 0x1f;
}

static int eb_reset_flag, modem_reset_flag;

void set_eb_reboot(void)
{
	gpio_lock();
	eb_reset_flag = 1;
	gpio_unlock();
}

void set_modem_reboot(void)
{
	gpio_lock();
	modem_reset_flag = 1;
	gpio_unlock();
}

void check_eb_reboot(void)
{
	int flag = 0;

	gpio_lock();
	if (eb_reset_flag) {
		flag = eb_reset_flag;
		eb_reset_flag = 0;
	}
	gpio_unlock();
	if (flag) {
		eb_reboot();
		ERR_PRINTF("eb_hard_reset\n");
		wait_delay(3000); /* wait for 3 seconds */
	}
}

void check_modem_reboot(void)
{
	int flag = 0;

	gpio_lock();
	if (modem_reset_flag) {
		flag = modem_reset_flag;
		modem_reset_flag = 0;
	}
	gpio_unlock();
	if (flag) {
		modem_reboot();
		ERR_PRINTF("modem reboot\n");
		wait_delay(6000); /* wait modem to work properly */
	}
}

static int modem_exist(const char *device_name, const char *lock_name)
{
	const char *str = "AT\r";
	int ret = 0, fd, msr, len;
	char buf[128];

	if ((fd = open_serial(device_name, B115200, 8, 0)) >= 0) {
		ioctl(fd, TIOCMGET, &msr);
		if ((ret = msr & TIOCM_DSR) == 0) {
			write_serial(fd, str, strlen(str), 200);
			if ((len = read_serial(fd, buf, sizeof(buf), 2000)) > 0) {
				buf[len] = 0x00;
				if (strstr(buf, "OK") != NULL)
					ret = 1;
			}
		}
		close_serial(fd);
	}
	PRINTF("modem_exist, return:%d\n", ret);
	return ret;
}

static int do_modem_check(const char *device_name, const char *lock_name,
	int baudrate)
{
	int ret = 0, fd, lock_pid, val, param_len;
	char resp[1024], *ptr;

	lcd_update_head_enable(0);
	modem_signal_intensity = 99;
	if (!write_lock_file(lock_name)) {
		lock_pid = read_lock_file(lock_name);
		PRINTF("%s is locked by %d\n", device_name, lock_pid);
		lcd_update_head_enable(1);
		return 0;
	}
	if (!modem_exist(device_name, lock_name)) {
		remove(lock_name);
		PRINTF("modem %s\n", ret ? "exists" : "doesnot exist");
		lcd_update_comm_info(0);
		lcd_update_head_enable(1);
		return 0;
	}
	if ((fd = open_serial(device_name, baudrate, 8, 0)) < 0) {
		remove(lock_name);
		PRINTF("modem test %s\n", ret ? "ok" : "fail");
		lcd_update_head_enable(1);
		return 0;
	}
	at_cmd(fd, "ATZ\r", resp, sizeof(resp), 1000, 500);
	wait_delay(3000);
	if (!at_cmd(fd, "ATE0&C1&D2&S0\r", resp, sizeof(resp), 1000, 500)) {
		lcd_update_comm_info(1);
		ret = 0;
	}
	else {
		if(!at_cmd(fd, "AT+CXXCID\r", resp, sizeof(resp), 1000, 500)) {
			lcd_update_comm_info(1);
			ret = 0;
		}
		else if (strstr(resp, "OK") != NULL) {
			modem_net_type = 0;
			if (at_cmd(fd, "AT+GMM\r", resp, sizeof(resp), 1000, 500)) {
				if (strstr(resp, "+GMM:") != NULL)
					modem_net_type = 1;
			}
			fparam_lock();
			fparam_set_value(FPARAM_LAST_NET_TYPE, &modem_net_type, 1, &param_len);
			fparam_unlock();
			lcd_update_comm_info(4);
			if (at_cmd(fd, "AT+CSQ\r", resp, sizeof(resp), 1000, 500)) {
				if ((ptr = strstr(resp, "+CSQ:")) != NULL) {
					ptr += 5;
					sscanf(ptr, "%d", &val);
					modem_signal_intensity = val;
					lcd_update_head_enable(1);
					ret = 1;
					if (modem_signal_intensity != 99 && modem_signal_intensity > 10)
						ret = 3;
				}
			}
			lcd_update_comm_info(6);
			wait_delay(2000);
		}
		else {
			lcd_update_comm_info(2);
			ret = 2; // SIM card is not exist
		}
	}
	close_serial(fd);
	remove(lock_name);
	lcd_update_head_enable(1);
	PRINTF("modem test %s\n", ret > 1 ? "ok" : "fail");
	return ret;
}

void set_net_icon(BYTE type, BYTE signal)
{
	if (type == 0 || type == 1)
		modem_net_type = type;
	if (signal <= 31)
		modem_signal_intensity = signal;
}

int get_ppp_addr(char *addr, char *dstaddr)
{
	int i, fd, ret = 0;
	char buf[1024];
	struct ifconf ifconf;
	struct ifreq *ifreq;
	struct sockaddr_in *in_addr;

	fd = socket(AF_INET, SOCK_DGRAM, 0);
	ifconf.ifc_buf = buf;
	ifconf.ifc_len = sizeof(buf);
	if (ioctl(fd, SIOCGIFCONF, &ifconf) == 0) {
		ifreq = ifconf.ifc_req;
		for (i = 0; i < ifconf.ifc_len / sizeof(struct ifreq); i ++) {
			if (!strncmp(ifreq->ifr_name, "ppp", 3)) {
				if (ioctl(fd, SIOCGIFFLAGS, ifreq) == 0
					&& (ifreq->ifr_flags & IFF_UP)) {
					in_addr = (struct sockaddr_in *)&ifreq->ifr_addr;
					if (ioctl(fd, SIOCGIFDSTADDR, ifreq) == 0)
						inet_ntop(AF_INET, &in_addr->sin_addr, dstaddr, 16);
					if (ioctl(fd, SIOCGIFADDR, ifreq) == 0) {
						inet_ntop(AF_INET, &in_addr->sin_addr, addr, 16);
						ret = 1;
					}
					PRINTF("addr:%s, dstaddr:%s\n", addr, dstaddr);
				}
				break;
			}
			ifreq ++;
		}
	}
	close(fd);
	return ret;
}

/*
 * return 0: fail
 * return 1: OK, but SIM card is not exist
 * return 2: OK, SIM card is OK
 */
static int test_baudrate = B115200;
int test_modem(const char *device_name, const char *lock_name)
{
	int ret = 0, i;
	int baudrate[] = {B115200, B9600, B38400, B4800, B1200};

	for (i = 0; i < sizeof(baudrate) / sizeof(int); i ++) {
		ret = do_modem_check(device_name, lock_name, baudrate[i]);
		if (ret) {
			test_baudrate = baudrate[i];
			break;
		}
	}
	return ret;
}

int check_modem_relay(const char *device_name, const char *lock_name)
{
	int exist;
	unsigned int val;

	val = BIT_MODEM_OFF;
	gpio_ioctl(GPIOCTL_OUTP_SET, &val);
	wait_delay(3500);
	val = BIT_MODEM_ON;
	gpio_ioctl(GPIOCTL_OUTP_SET, &val);
	wait_delay(500);

	val = BIT_MODEM_RELAY;
	gpio_ioctl(GPIOCTL_OUTP_SET, &val);

	PRINTF("MODEM RELAY off.\n");
	PRINTF("Waiting 5s ...\n");
	wait_delay(5000);
	exist = modem_exist(device_name, lock_name);

	val = BIT_MODEM_RELAY;
	gpio_ioctl(GPIOCTL_OUTP_CLR, &val);

	PRINTF("MODEM RELAY on.\n");
	PRINTF("Waiting 5s ...\n");
	wait_delay(5000);

	val = BIT_MODEM_ON;
	gpio_ioctl(GPIOCTL_OUTP_CLR, &val);
	wait_delay(20);
	val = BIT_MODEM_ON;
	gpio_ioctl(GPIOCTL_OUTP_SET, &val);
	wait_delay(120);
	val = BIT_MODEM_ON;
	gpio_ioctl(GPIOCTL_OUTP_CLR, &val);
	wait_delay(2000);
	val = BIT_MODEM_OFF;
	gpio_ioctl(GPIOCTL_OUTP_CLR, &val);

	remove(lock_name);
	return exist ? 0 : 1;
}

int modem_check(const char *device_name, const char *lock_name, int baudrate)
{
	static unsigned int interval, last_result;
	static long test_uptime;
	const int max_interval = 30;
	const int min_interval = 10;
	int ret;

	PRINTF("modem_check, interval:%ld, uptime:%ld, test_uptime:%ld\n",
		interval, uptime(), test_uptime);
	if (test_uptime == 0 || uptime() < test_uptime) {
		// for first time or time is error
		ret = do_modem_check(device_name, lock_name, baudrate);
		interval = min_interval;
		test_uptime = uptime();
		last_result = ret;
	}
	else if (uptime() - test_uptime <= interval) {
		ret = 0;
	}
	else { // need to test modem again
		ret = do_modem_check(device_name, lock_name, baudrate);
		if (ret == 0 && last_result == 0) {
			modem_reset();
			wait_delay(20 * 1000); /* wait modem to work properly */
			ret = do_modem_check(device_name, lock_name, baudrate);
			if (ret == 0) set_modem_reboot();
		}
		if (ret || interval >= max_interval)
			interval = min_interval;
		else
			interval = interval + 5;
		test_uptime = uptime();
		last_result = ret;
	}
	return ret > 2 ? 1 : 0;
}

static int power_fail_flag = 0;

// When power fail, this function is called
static void power_fail(int sig)
{
	time_t tt;
	int param_len;

	if (power_fail_flag == 0) {
		power_fail_flag = 1;
		g_terminated = 1;	// tell all threads to exit
		tt = time(NULL);
		fparam_set_value(FPARAM_POWERFAIL_TIME, &tt, sizeof(tt), &param_len);
		flush_tables();
		sync();
		set_reboot_flag(0);
		LOG_PRINTF("Power fail\n");
	}
}

int power_is_fail(void)
{
	return power_fail_flag;
}

void handler_power_fail(void)
{
	int flags;

	if (gpio_fd < 0) {
		PRINTF("Can't open /dev/gpio\n");
	}
	else {
		signal(SIGIO, power_fail);
		fcntl(gpio_fd, F_SETOWN, getpid());
		flags = fcntl(gpio_fd, F_GETFL);
		fcntl(gpio_fd, F_SETFL, flags | FASYNC);
	}
}

void self_diag(void (*dev_open)(void), void (*dev_close)(void))
{
	(*dev_close)();
	(*dev_open)();
}

void open_devices(void)
{
	BYTE font_size;

	sem_init(&sem_rtc, 0, 1);
	sem_init(&sem_plc, 0, 1);
	sem_init(&sem_cas, 0, 1);
	sem_init(&sem_rs485, 0, 1);
	sem_init(&sem_hhu, 0, 1);
	sem_init(&sem_gpio, 0, 1);

	gpio_open();
	cas_open();
	rs485_open();
	hhu_open();
	plc_open();
	fparam_get_value(FPARAM_FONT_SIZE, &font_size, 1);
	lcd_open(g_lcd_type, font_size);
}

void close_devices(void)
{
	lcd_close();
	plc_close();
	hhu_close();
	rs485_close();
	cas_close();
	gpio_close();

	sem_destroy(&sem_gpio);
	sem_destroy(&sem_hhu);
	sem_destroy(&sem_rs485);
	sem_destroy(&sem_cas);
	sem_destroy(&sem_plc);
	sem_destroy(&sem_rtc);
}
