/*****************************************************************************
*	C-Plan Concentrator 
*
*	(c) Copyright 2006-2007, Shenzhen Kaifa Technology Co.,Ltd. 
*	All Rights Reserved 
*
* 	To read/write Communication Board 
*
*	Author: fanhua zeng, dajiang wan
*	Created on: 2006-07-31
*****************************************************************************/

#include "common.h"
#include "msg_code.h"
#include "err_code.h"
#include "plc.h"

/* to read extension board software version */
int get_cb_softver(BYTE *buf)
{
	BYTE req[PLC_APDU_LEN], resp[PLC_APDU_LEN];
	int req_len, resp_len;

	req_len = my_pack(req, "ccsc", CB_READ, 0, 0x0033, 2);
	if (cb_comm(req, req_len, resp, sizeof(resp), &resp_len) == ERR_OK
		&& resp_len == 4 && resp[1] == 0) {
		memcpy(buf, resp + 2, 2);
		return 1;
	}
	return 0;
}

/* to reset the state of CommBoard to 0x00, after reboot the state is 0xff */
int reset_cb_state(void)
{
	BYTE req[PLC_APDU_LEN], resp[PLC_APDU_LEN];
	int req_len, resp_len, ret;

	req_len = my_pack(req, "ccscc", CB_WRITE, 1, 0x0028, 1, 0x00);
	ret = cb_comm(req, req_len, resp, sizeof(resp), &resp_len);
	return ret == ERR_OK && resp_len == 2;
}

/* get the state of CB */
int get_cb_state(BYTE *buf)
{
	BYTE req[PLC_APDU_LEN], resp[PLC_APDU_LEN];
	int req_len, resp_len, ret;

	req_len = my_pack(req, "ccsc", CB_READ, 1, 0x0028, 1);
	ret = cb_comm(req, req_len, resp, sizeof(resp), &resp_len);
	if (ret == ERR_OK && resp_len == 3 && resp[1] == 0) {
		buf[0] = resp[2];
		return 1;
	}
	return 0;
}

int get_cb_data(const BYTE *in_buf, int in_len, BYTE *out_buf, int max_out,
	int *out_len)
{
	int ret;

	ret = cb_comm(in_buf, in_len, out_buf, max_out, out_len);
	if (ret == ERR_OK)
		return 1;
	return 0;
}
