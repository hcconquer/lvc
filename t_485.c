/*
 *  C-Plan Concentrator
 *
 *  Copyright (C) 2006-2007, Shenzhen Kaifa Technology Co.,Ltd.
 *
 *  Provide function to open/close tables and semaphores. 
 *
 *  Author: dajiang wan
 *  Created on: 2008-01-07
 */
#include "t_485.h"
#include "f_param.h"
#include "f_alm.h"
#include "common.h"
#include "misc.h"
#include "global.h"
#include "mt_485.h"
#include "threads.h"

#define BIT6	(1 << 6)
#define BIT7	(1 << 7)

static T_485 t_485[T_485_ROWS_CNT];
static T_485 *p_t_485 = t_485;
static sem_t sem_t_485;
static int fd_t_485;

void t485_lock(void)
{
	sem_wait(&sem_t_485);
}

void t485_unlock(void)
{
	sem_post(&sem_t_485);
}

static void t485_init_row(int rowno) // initial one line meter record
{
	T_485 *p_485 = p_t_485 + rowno - 1;

	memset(p_485, 0, sizeof(T_485));
	memcpy(p_485->mtid, INVALID_MTID, MTID_LEN);
}

int t485_all_is_empty(void)
{
	int i;

	for (i = 0; i < T_485_ROWS_CNT; i ++) {
		if (memcmp(p_t_485[i].mtid, INVALID_MTID, MTID_LEN))
			return 0;
	}
	return 1;
}

int t485_is_empty(short mtidx)
{
	return memcmp(p_t_485[mtidx].mtid, INVALID_MTID, MTID_LEN) == 0;
}

void t485_get_mtid(short mtidx, MTID mtid)
{
	memcpy(mtid, p_t_485[mtidx].mtid, MTID_LEN);
}

void t485_get_para(short mtidx, T485_PARA *data)
{
	*data = p_t_485[mtidx].para;
}

void t485_get_current(short mtidx, T485_DATA *data)
{
	*data = p_t_485[mtidx].current;
}

void t485_get_day_data(short mtidx, T485_DATA *data)
{
	*data = p_t_485[mtidx].day_data;
}

int t485_get_meters_type(void *out_buf, int max_len)
{
	BYTE *ptr = out_buf;
	int i, cnt;

	if (max_len >= 4) {
		cnt = 0;
		for (i = 0; i < T_485_ROWS_CNT; i ++) {
			if (memcmp(p_t_485[i].mtid, INVALID_MTID, MTID_LEN) != 0)
				cnt ++;
		}
		*ptr ++ = cnt;
		*ptr ++ = 0;
		*ptr ++ = 0;
		*ptr ++ = 0;
	}
	return ptr - (BYTE *)out_buf;
}

int t485_find_frzn(short mtidx, BYTE fn)
{
	BYTE frzn_cnt, *frzn;
	int idx;

	frzn_cnt = p_t_485[mtidx].frzn_cnt;
	frzn = p_t_485[mtidx].frzn;
	for (idx = 0; idx < frzn_cnt; idx ++) {
		if (frzn[0] == fn)
			return frzn[1];
		frzn += 2;
	}
	return 1; // return 1 default
}

int t485_fill_frzn_data(short mtidx, const BYTE *time_stamp, int di_len,
	int offset, BYTE *buf)
{
	T485_DATA *data;
	int i;

	data = p_t_485[mtidx].data;
	for (i = 0; i < 8; i ++) {
		if (data->flag && memcmp(data->time, time_stamp, 5) == 0) {
			memcpy(buf, ((BYTE *)data) + offset, di_len);
			return di_len; 
		}
		data ++;
	}
	return 0;
}

void t485_set_default(int mtidx, const MTID mtid)
{
	const BYTE base[8] = {0x01, 0x00, 0x01, 0x00, 0x00, 0x22, 0x60, 2};
	const BYTE limit[27] = {0x30, 0x23, 0x90, 0x20, 0x10, 0x17, 0x20, 0x24,
		0x80, 0x19, 0x80, 0x01, 0x50, 0x01, 0x00, 0x05, 0x00, 0x00, 0x05,
		0x00, 0x00, 0x03, 0x00, 0x05, 0x00, 0x05, 0x58};
	const BYTE pfl[4] = {0x00, 0x80, 0x00, 0x90};
	T_485 *p_485 = p_t_485 + mtidx;

	if (memcmp(p_485->mtid, INVALID_MTID, MTID_LEN) == 0) {
		p_485->collect = mtidx + 1;
		p_485->comm = 2;
		p_485->prot = 1;
		memcpy(p_485->mtid, mtid, MTID_LEN);
		memset(p_485->pass, 0, 6);
		p_485->flag = 0x49;
		memcpy(p_485->base, base, 8);
		memcpy(p_485->limit, limit, 27);
		p_485->frzn_cnt = 0;
		memcpy(p_485->pfl, pfl, 4);
	}
	else {
		memcpy(p_485->mtid, mtid, MTID_LEN);
	}
	t485_update(mtidx, 1);
}

static int t485_check_read(const MTID mtid, BYTE *buf, int len, WORD di)
{
	check_sched();
	if (!g_terminated) {
		memset(buf, INVALID_VALUE, len);
		mt485_read(mtid, buf, len, di);
		return 1;
	}
	return 0;
}

#define READ_DI(x)		t485_check_read(mtid, data->di_##x, \
	sizeof(data->di_##x), 0x##x)

int t485_read_real_para(const MTID mtid, BYTE *time_str, T485_PARA *data)
{
	if (!t485_check_read(mtid, time_str, 3, 0xc011)
		|| !t485_check_read(mtid, time_str + 3, 4, 0xc010))
		return 0;

	if (!READ_DI(c030) || !READ_DI(c031) || !READ_DI(c033) || !READ_DI(c034))
		return 0;

	if (!READ_DI(c111) || !READ_DI(c112) || !READ_DI(c113) || !READ_DI(c114))
		return 0;

	if (!READ_DI(c115) || !READ_DI(c116) || !READ_DI(c117) || !READ_DI(c118)
		|| !READ_DI(c119) || !READ_DI(c11a))
		return 0;

	if (!READ_DI(c211))
		return 0;

	return 1;
}

int t485_read_real_data(const MTID mtid, T485_DATA *data)
{
	if (!READ_DI(901f) || !READ_DI(902f) || !READ_DI(911f) || !READ_DI(912f)
		|| !READ_DI(913f) || !READ_DI(914f) || !READ_DI(915f) || !READ_DI(916f))
		return 0;

	if (!READ_DI(a01f) || !READ_DI(a02f) || !READ_DI(a11f) || !READ_DI(a12f)
		|| !READ_DI(a13f) || !READ_DI(a14f) || !READ_DI(a15f) || !READ_DI(a16f))
		return 0;

	if (!READ_DI(b01f) || !READ_DI(b02f) || !READ_DI(b11f) || !READ_DI(b12f)
		|| !READ_DI(b13f) || !READ_DI(b14f) || !READ_DI(b15f) || !READ_DI(b16f))
		return 0;

	if (!READ_DI(b210) || !READ_DI(b211) || !READ_DI(b212) || !READ_DI(b213)
		|| !READ_DI(b214))
		return 0;

	if (!READ_DI(b310) || !READ_DI(b311) || !READ_DI(b312) || !READ_DI(b313))
		return 0;

	if (!READ_DI(b320) || !READ_DI(b321) || !READ_DI(b322) || !READ_DI(b323))
		return 0;

	if (!READ_DI(b330) || !READ_DI(b331) || !READ_DI(b332) || !READ_DI(b333))
		return 0;

	if (!READ_DI(b340) || !READ_DI(b341) || !READ_DI(b342) || !READ_DI(b343))
		return 0;

	if (!READ_DI(b611) || !READ_DI(b612) || !READ_DI(b613))
		return 0;

	if (!READ_DI(b621) || !READ_DI(b622) || !READ_DI(b623))
		return 0;

	if (!READ_DI(b630) || !READ_DI(b631) || !READ_DI(b632) || !READ_DI(b633)
		|| !READ_DI(b634) || !READ_DI(b635))
		return 0;

	if (!READ_DI(b640) || !READ_DI(b641) || !READ_DI(b642) || !READ_DI(b643))
		return 0;

	if (!READ_DI(b650) || !READ_DI(b651) || !READ_DI(b652) || !READ_DI(b653))
		return 0;

	if (!READ_DI(c020) || !READ_DI(c021))
		return 0;

	return 1;
}

void t485_set_para(const struct tm *tm, short mtidx, const MTID mtid,
	const T485_PARA *data)
{
	T485_PARA *dst;
	MTID tmp_mtid;

	t485_get_mtid(mtidx, tmp_mtid);
	if (memcmp(tmp_mtid, mtid, MTID_LEN) == 0) {
		dst = &p_t_485[mtidx].para;
		memcpy(dst, data, sizeof(T485_PARA));
		dst->time[0] = bin_to_bcd(tm->tm_year % 100);
		dst->time[1] = bin_to_bcd(tm->tm_mon + 1);
		dst->time[2] = bin_to_bcd(tm->tm_mday);
		dst->time[3] = bin_to_bcd(tm->tm_hour);
		dst->time[4] = bin_to_bcd(tm->tm_min);
		dst->flag = 1;
		t485_update(mtidx, 1);
	}
}

static void t485_check_status(const struct tm *tm, short mtidx,
	const MTID mtid, const T485_PARA *para, const T485_DATA *data)
{
	#define PARA_ALM_CMP(di) memcmp(para->di, old_para->di, sizeof(para->di))
	#define DATA_ALM_CMP(di) memcmp(data->di, old_data->di, sizeof(data->di))
	const T485_PARA *old_para;
	const T485_DATA *old_data;
	BYTE alm8 = 0, alm13 = 0, alm_buf[2], data_index;

	data_index = p_t_485[mtidx].data_index;
	data_index = (data_index == 0) ? 7 : (data_index - 1);
	old_para = &p_t_485[mtidx].para;
	old_data = p_t_485[mtidx].data + data_index;
	if (old_para->flag) {
		if (PARA_ALM_CMP(di_c117))
			alm8 |= (1 << 2);
		if (PARA_ALM_CMP(di_c030) || PARA_ALM_CMP(di_c031))
			alm8 |= (1 << 3);
	}
	if (old_data->flag) {
		if (DATA_ALM_CMP(di_b210))
			alm8 |= (1 << 1);
		if (DATA_ALM_CMP(di_b213))
			alm8 |= (1 << 5);

		if (DATA_ALM_CMP(di_b212) || DATA_ALM_CMP(di_b213))
			alm13 |= (1 << 0);
		if (DATA_ALM_CMP(di_b310) || DATA_ALM_CMP(di_b311)
			|| DATA_ALM_CMP(di_b312) || DATA_ALM_CMP(di_b313))
			alm13 |= (1 << 1);
	}
	if (data->di_c020[0] & (1 << 2))
		alm13 |= (1 << 4);
	if (alm8) {
		alm_buf[0] = mtidx; alm_buf[1] = alm8;
		falm_add_485(mtidx, 8, alm_buf, 2);
	}
	if (alm13) {
		alm_buf[0] = mtidx; alm_buf[1] = alm13;
		falm_add_485(mtidx, 13, alm_buf, 2);
	}
}

void t485_check_clock(const struct tm *tm, short mtidx, const BYTE *limit,
	const BYTE *time_str)
{
	time_t tt1, tt2;
	struct tm tm1, tm2;
	BYTE alm_buf[1], save;
	T_485 *t_485;

	save = 0;
	if (time_str[0] != INVALID_VALUE && time_str[6] != INVALID_VALUE) {
		t_485 = p_t_485 + mtidx;
		tm1 = *tm;
		memset(&tm2, 0, sizeof(tm2));
		tm2.tm_sec = bcd_to_bin(time_str[0]);
		tm2.tm_min = bcd_to_bin(time_str[1]);
		tm2.tm_hour = bcd_to_bin(time_str[2]);
		tm2.tm_mday = bcd_to_bin(time_str[4]);
		tm2.tm_mon = bcd_to_bin(time_str[5]) - 1;
		tm2.tm_year = bcd_to_bin(time_str[6]) + 100;
		tt1 = mktime(&tm1);
		tt2 = mktime(&tm2);
		if (check_time_t(tt1, tt2, limit[3] * 60)) { // clock is ok
			if (t_485->ex_time_flag) {
				t_485->ex_time_flag = 0;
				alm_buf[0] = mtidx;
				falm_add_485(mtidx, 12, alm_buf, 1);
				save = 1;
			}
		}
		else { // clock is bad
			if (!t_485->ex_time_flag) {
				t_485->ex_time_flag = 1;
				alm_buf[0] = (1 << 7) | mtidx;
				falm_add_485(mtidx, 12, alm_buf, 1);
				save = 1;
			}
		}
	}
	if (save)
		t485_update(mtidx, 0);
}

static int convert_1(const BYTE *buf, int *val)
{
	int i;
	BYTE tmp, high, low;

	for (i = 0; i < 2; i ++) {
		tmp = buf[i];
		high = (tmp >> 4) & 0x0f;
		low = tmp & 0x0f;
		if (low >= 10 || high >= 10) {
			*val = 0;
			return 0;
		}
	}
	*val = bcd_to_bin(buf[0]) + bcd_to_bin(buf[1]) * 100;
	return 1;
}

static int convert_2(const BYTE *buf, int *val)
{
	BYTE tmp, high, low, val1, val2;

	tmp = buf[0];
	high = (tmp >> 4) & 0x0f;
	low = tmp & 0x0f;
	if (low >= 10 || high >= 10) {
		*val = 0;
		return 0;
	}
	val1 = bcd_to_bin(tmp);
	tmp = buf[1];
	high = (tmp >> 4) & 0x0f;
	low = tmp & 0x0f;
	if (low >= 10 || (high & 0x07) >= 10) {
		*val = 0;
		return 0;
	}
	val2 = bcd_to_bin(tmp & 0x7f);
	*val = val1 + val2 * 100;
	if (tmp & 0x80)
		*val = (-1) * (*val);
	return 1;
}

static void convert_3(BYTE *dst, int val)
{
	dst[0] = bin_to_bcd(val % 100);
	dst[1] = bin_to_bcd(val / 100);
}

static void convert_4(BYTE *dst, const BYTE *src)
{
	dst[0] = 0;
	memcpy(dst + 1, src, 4);
}

static int convert_5(const BYTE *buf, int *val)
{
	BYTE tmp, high, low;

	tmp = buf[0];
	high = (tmp >> 4) & 0x0f;
	low = tmp & 0x0f;
	if (low >= 10 || high >= 10) {
		*val = 0;
		return 0;
	}
	*val = bcd_to_bin(buf[0]);
	return 1;
}

static void t485_check_voltage(const struct tm *tm, short mtidx,
	const MTID mtid, const BYTE *limit, const T485_DATA *data)
{
	BYTE start_stop[2], alm_flag[2], alm_buf[8];
	int i, ret[3], val[3], val1, val2, save;
	T_485 *t_485;

	if (!convert_1(limit + 6, &val1) || !convert_1(limit + 8, &val2))
		return;
	save = 0;
	t_485 = p_t_485 + mtidx;
	val1 = val1 / 10; val2 = val2 / 10;
	ret[0] = convert_1(data->di_b611, &val[0]);
	ret[1] = convert_1(data->di_b612, &val[1]);
	ret[2] = convert_1(data->di_b613, &val[2]);
	for (i = 0; i < 3; i++) {
		if (!ret[i])
			continue;
		start_stop[0] = start_stop[1] = 0;
		alm_flag[0] = alm_flag[1] = 0;
		if (val[i] > val1) { // too high
			if ((t_485->ex_vol_flag[i] & BIT7) != 0) {
				start_stop[0] = mtidx; // stop flag
				alm_flag[0] = BIT7 | (1 << i);
				t_485->ex_vol_flag[i] &= ~BIT7;
				save = 1;
			}
			if ((t_485->ex_vol_flag[i] & BIT6) == 0) {
				start_stop[1] = (1 << 7) | mtidx; // start flag
				alm_flag[1] = BIT6 | (1 << i);
				t_485->ex_vol_flag[i] |= BIT6;
				save = 1;
			}
		}
		else if (val[i] < val2) { // too low
			if ((t_485->ex_vol_flag[i] & BIT6) != 0) {
				start_stop[0] = mtidx; // stop flag
				alm_flag[0] = BIT6 | (1 << i);
				t_485->ex_vol_flag[i] &= ~BIT6;
				save = 1;
			}
			if ((t_485->ex_vol_flag[i] & BIT7) == 0) {
				start_stop[1] = (1 << 7) | mtidx; // start flag
				alm_flag[1] = BIT7 | (1 << i);
				t_485->ex_vol_flag[i] |= BIT7;
				save = 1;
			}
		}
		else { // normal
			if ((t_485->ex_vol_flag[i] & BIT6) != 0) {
				start_stop[0] = mtidx; // stop flag
				alm_flag[0] = BIT6 | (1 << i);
				t_485->ex_vol_flag[i] &= ~BIT6;
				save = 1;
			}
			if ((t_485->ex_vol_flag[i] & BIT7) != 0) {
				start_stop[1] = mtidx; // stop flag
				alm_flag[1] = BIT7 | (1 << i);
				t_485->ex_vol_flag[i] &= ~BIT7;
				save = 1;
			}
		}
		if (alm_flag[0]) {
			alm_buf[0] = start_stop[0];
			alm_buf[1] = alm_flag[0];
			convert_3(alm_buf + 2, val[0] * 10);
			convert_3(alm_buf + 4, val[1] * 10);
			convert_3(alm_buf + 6, val[2] * 10);
			falm_add_485(mtidx, 24, alm_buf, 8);
		}
		if (alm_flag[1]) {
			alm_buf[0] = start_stop[1];
			alm_buf[1] = alm_flag[1];
			convert_3(alm_buf + 2, val[0] * 10);
			convert_3(alm_buf + 4, val[1] * 10);
			convert_3(alm_buf + 6, val[2] * 10);
			falm_add_485(mtidx, 24, alm_buf, 8);
		}
	}
	if (save)
		t485_update(mtidx, 0);
}

static void t485_check_current(const struct tm *tm, short mtidx,
	const MTID mtid, const BYTE *limit, const T485_DATA *data)
{
	BYTE start_stop[2], alm_flag[2], alm_buf[8];
	int i, ret[3], val[3], val1, val2, save;
	T_485 *t_485;

	if (!convert_2(limit + 10, &val1) || !convert_2(limit + 12, &val2))
		return;
	save = 0;
	t_485 = p_t_485 + mtidx;
	ret[0] = convert_1(data->di_b621, &val[0]);
	ret[1] = convert_1(data->di_b622, &val[1]);
	ret[2] = convert_1(data->di_b623, &val[2]);
	for (i = 0; i < 3; i++) {
		if (!ret[i])
			continue;
		start_stop[0] = start_stop[1] = 0;
		alm_flag[0] = alm_flag[1] = 0;
		if (val[i] > val1) { // too high
			if ((t_485->ex_cur_flag[i] & BIT7) != 0) {
				start_stop[0] = mtidx; // stop flag
				alm_flag[0] = BIT7 | (1 << i);
				t_485->ex_cur_flag[i] &= ~BIT7;
				save = 1;
			}
			if ((t_485->ex_cur_flag[i] & BIT6) == 0) {
				start_stop[1] = (1 << 7) | mtidx; // start flag
				alm_flag[1] = BIT6 | (1 << i);
				t_485->ex_cur_flag[i] |= BIT6;
				save = 1;
			}
		}
		else if (val[i] > val2) { // high
			if ((t_485->ex_cur_flag[i] & BIT6) != 0) {
				start_stop[0] = mtidx; // stop flag
				alm_flag[0] = BIT6 | (1 << i);
				t_485->ex_cur_flag[i] &= ~BIT6;
				save = 1;
			}
			if ((t_485->ex_cur_flag[i] & BIT7) == 0) {
				start_stop[1] = (1 << 7) | mtidx; // start flag
				alm_flag[1] = BIT7 | (1 << i);
				t_485->ex_cur_flag[i] |= BIT7;
				save = 1;
			}
		}
		else { // normal
			if ((t_485->ex_cur_flag[i] & BIT6) != 0) {
				start_stop[0] = mtidx; // stop flag
				alm_flag[0] = BIT6 | (1 << i);
				t_485->ex_cur_flag[i] &= ~BIT6;
				save = 1;
			}
			if ((t_485->ex_cur_flag[i] & BIT7) != 0) {
				start_stop[1] = mtidx; // stop flag
				alm_flag[1] = BIT7 | (1 << i);
				t_485->ex_cur_flag[i] &= ~BIT7;
				save = 1;
			}
		}
		if (alm_flag[0]) {
			alm_buf[0] = start_stop[0];
			alm_buf[1] = alm_flag[0];
			convert_3(alm_buf + 2, val[0]);
			convert_3(alm_buf + 4, val[1]);
			convert_3(alm_buf + 6, val[2]);
			falm_add_485(mtidx, 25, alm_buf, 8);
		}
		if (alm_flag[1]) {
			alm_buf[0] = start_stop[1];
			alm_buf[1] = alm_flag[1];
			convert_3(alm_buf + 2, val[0]);
			convert_3(alm_buf + 4, val[1]);
			convert_3(alm_buf + 6, val[2]);
			falm_add_485(mtidx, 25, alm_buf, 8);
		}
	}
	if (save)
		t485_update(mtidx, 0);
}

static void t485_check_energy(const struct tm *tm, short mtidx,
	const MTID mtid, const BYTE *limit, const T485_DATA *data)
{
	const T485_DATA *old_data;
	BYTE data_index, alm_buf[128];
	int ret1, ret2, val1, val2, valid1, valid2, valid3, update_flag;
	struct tm tmp;
	time_t tt;

	valid1 = convert_5(limit + 0, &val1);
	valid2 = convert_5(limit + 1, &val1);
	valid3 = (limit[2] != 0xff);
	data_index = p_t_485[mtidx].data_index;
	data_index = (data_index == 0) ? 7 : (data_index - 1);
	old_data = p_t_485[mtidx].data + data_index;
	if (old_data->flag && p_t_485[mtidx].tt_9010) {
		tmp = *tm; tt = mktime(&tmp);
		ret1 = energy_convert(p_t_485[mtidx].di_9010, &val1);
		ret2 = energy_convert(data->di_901f, &val2);
		if (ret1 && ret2) {
			if (val2 - val1 < 0) {
				alm_buf[0] = mtidx;
				convert_4(alm_buf + 1, p_t_485[mtidx].di_9010);
				convert_4(alm_buf + 6, data->di_901f);
				falm_add_485(mtidx, 27, alm_buf, 11);
			}
			if (valid1 && val2 - val1 > bcd_to_bin(limit[0]) * 10) {
				alm_buf[0] = mtidx;
				convert_4(alm_buf + 1, p_t_485[mtidx].di_9010);
				convert_4(alm_buf + 6, data->di_901f);
				alm_buf[11] = limit[0];
				falm_add_485(mtidx, 28, alm_buf, 12);
			}
			update_flag = 0;
			if (valid2 && val2 - val1 > bcd_to_bin(limit[1]) * 10) {
				alm_buf[0] = mtidx;
				convert_4(alm_buf + 1, p_t_485[mtidx].di_9010);
				convert_4(alm_buf + 6, data->di_901f);
				alm_buf[11] = bcd_to_bin(limit[1]); // TODO, To be confirmed
				falm_add_485(mtidx, 29, alm_buf, 12);
				update_flag = 1;
			}
			if (valid3 && val2 - val1 == 0
				&& tt - p_t_485[mtidx].tt_9010 > limit[2] * 15 * 60) {
				alm_buf[0] = mtidx;
				convert_4(alm_buf + 1, data->di_901f);
				alm_buf[6] = limit[2];
				falm_add_485(mtidx, 30, alm_buf, 7);
				update_flag = 1;
			}
			if (update_flag) {
				memcpy(p_t_485[mtidx].di_9010, data->di_901f, 4);
				p_t_485[mtidx].tt_9010 = tt;
			}
		}
	}
}

void t485_check_para_data(const struct tm *tm, short mtidx, const MTID mtid,
	const BYTE *time_str, const T485_PARA *para, const T485_DATA *data)
{
	BYTE buf1[27], buf2[4];
	MTID tmp_mtid;

	t485_get_mtid(mtidx, tmp_mtid);
	if (memcmp(tmp_mtid, mtid, MTID_LEN) == 0) {
		memcpy(buf1, p_t_485[mtidx].limit, 27);
		fparam_get_value(FPARAM_AFN04_F59, buf2, 4);
		t485_check_status(tm, mtidx, mtid, para, data);
		t485_check_clock(tm, mtidx, buf2, time_str);
		t485_check_voltage(tm, mtidx, mtid, buf1, data);
		t485_check_current(tm, mtidx, mtid, buf1, data);
		t485_check_energy(tm, mtidx, mtid, buf2, data);
	}
}

void t485_set_data(const struct tm *tm, short mtidx, const MTID mtid,
	const T485_DATA *data)
{
	int idx;
	T485_DATA *current;
	MTID tmp_mtid;
	struct tm tmp;

	t485_get_mtid(mtidx, tmp_mtid);
	if (memcmp(tmp_mtid, mtid, MTID_LEN) == 0) {
		current = &p_t_485[mtidx].current;
		memcpy(current, data, sizeof(T485_DATA));
		current->time[0] = bin_to_bcd(tm->tm_year % 100);
		current->time[1] = bin_to_bcd(tm->tm_mon + 1);
		current->time[2] = bin_to_bcd(tm->tm_mday);
		current->time[3] = bin_to_bcd(tm->tm_hour);
		current->time[4] = bin_to_bcd(tm->tm_min);
		current->flag = 1;
		if (tm->tm_min % 15 == 0) {
			idx = p_t_485[mtidx].data_index;
			p_t_485[mtidx].data[idx] = *current;
			p_t_485[mtidx].data_index = (idx + 1) % 8;
			if (tm->tm_hour == 0 && tm->tm_min == 0)
				p_t_485[mtidx].day_data = *current;
		}
		if (memcmp(p_t_485[mtidx].di_9010, data->di_901f, 4)) {
			memcpy(p_t_485[mtidx].di_9010, data->di_901f, 4);
			tmp = *tm;
			p_t_485[mtidx].tt_9010 = mktime(&tmp);
		}
		PRINTF("t485_set_data, tm:%02d-%02d-%02d %02d:%02d\n",
			tm->tm_year % 100, tm->tm_mon + 1, tm->tm_mday, tm->tm_hour,
			tm->tm_min);
		t485_update(mtidx, 1);
	}
}

int t485_read_all_meters(BYTE *out_buf, int max_len)
{
	BYTE *ptr = out_buf;
	int i, cnt;

	cnt = 0;
	for (i = 0; i < T_485_ROWS_CNT; i ++) {
		if (memcmp(p_t_485[i].mtid, INVALID_MTID, MTID_LEN))
			cnt ++;
	}
	if (max_len >= 17 * cnt + 1) {
		*ptr ++ = cnt;
		for (i = 0; i < T_485_ROWS_CNT; i ++) {
			if (memcmp(p_t_485[i].mtid, INVALID_MTID, MTID_LEN)) {
				*ptr ++ = i + 1;
				t485_read(i + 1, 10, ptr, 16);
				ptr += 16;
			}
		}
	}
	return ptr - out_buf;
}

int t485_read(BYTE pn, BYTE fn, void *out_buf, int max_len)
{
	const T_485 *p_485;
	BYTE *ptr = out_buf;

	if (pn >= 1 && pn <= T_485_ROWS_CNT) {
		p_485 = p_t_485 + (pn - 1);
		switch (fn) {
		case 10:
			if (max_len >= 16) {
				*ptr ++ = p_485->collect;
				*ptr ++ = p_485->comm;
				*ptr ++ = p_485->prot;
				memcpy(ptr, p_485->mtid, MTID_LEN); ptr += MTID_LEN;
				memcpy(ptr, p_485->pass, 6); ptr += 6;
				*ptr ++ = p_485->flag;
			}
			break;
		case 25:
			if (max_len >= sizeof(p_485->base)) {
				memcpy(ptr, p_485->base, sizeof(p_485->base));
				ptr += sizeof(p_485->base);
			}
			break;
		case 26:
			if (max_len >= sizeof(p_485->limit)) {
				memcpy(ptr, p_485->limit, sizeof(p_485->limit));
				ptr += sizeof(p_485->limit);
			}
			break;
		case 27:
			if (max_len >= p_485->frzn_cnt * 2 + 1) {
				*ptr ++ = p_485->frzn_cnt;
				memcpy(ptr, p_485->frzn, 2 * p_485->frzn_cnt);
				ptr += 2 * p_485->frzn_cnt;
			}
			break;
		case 28:
			if (max_len >= sizeof(p_485->pfl)) {
				memcpy(ptr, p_485->pfl, sizeof(p_485->pfl));
				ptr += sizeof(p_485->pfl);
			}
			break;
		}
	}
	return ptr - (BYTE *)out_buf;
}

int t485_write(BYTE pn, BYTE fn, const void *buf, int len, int *param_len)
{
	T_485 *p_485;
	const BYTE *ptr = buf;

	if (pn >= 1 && pn <= T_485_ROWS_CNT) {
		p_485 = p_t_485 + (pn - 1);
		switch (fn) {
		case 10:
			memset(p_485, 0, sizeof(T_485));
			p_485->collect = *ptr ++;
			p_485->comm = *ptr ++;
			p_485->prot = *ptr ++;
			memcpy(p_485->mtid, ptr, MTID_LEN); ptr += MTID_LEN;
			memcpy(p_485->pass, ptr, 6); ptr += 6;
			p_485->flag = *ptr ++;
			break;
		case 25:
			if (len >= sizeof(p_485->base)) {
				if (memcmp(p_485->base, ptr, 4)) {
					BYTE alm_buf[2];

					fparam_lock();
					falm_lock();
					alm_buf[0] = pn - 1;
					alm_buf[1] = 1 << 4;
					falm_add_485(pn - 1, 8, alm_buf, 2);
					falm_unlock();
					fparam_unlock();
				}
				memcpy(p_485->base, ptr, sizeof(p_485->base));
				ptr += sizeof(p_485->base);
			}
			break;
		case 26:
			if (len >= sizeof(p_485->limit)) {
				memcpy(p_485->limit, ptr, sizeof(p_485->limit));
				ptr += sizeof(p_485->limit);
			}
			break;
		case 27:
			if (len >= ptr[0] * 2 + 1) {
				p_485->frzn_cnt = *ptr ++;
				memcpy(p_485->frzn, ptr, 2 * p_485->frzn_cnt);
				ptr += 2 * p_485->frzn_cnt;
			}
			break;
		case 28:
			if (len >= sizeof(p_485->pfl)) {
				memcpy(p_485->pfl, ptr, sizeof(p_485->pfl));
				ptr += sizeof(p_485->pfl);
			}
			break;
		}
		t485_update(pn - 1, 1);
	}
	*param_len = ptr - (const BYTE *)buf;
	return *param_len;
}

void t485_open(void)
{
	int mtidx, size;
	const char *name = T_485_NAME;

	size = sizeof(T_485) * T_485_ROWS_CNT;
	sem_init(&sem_t_485, 0, 1);
	if (!check_file(name, size)) {
		/* File doesnot exist or file length is invalid. */
		PRINTF("File %s is created, size:%d\n", name, size);
		fd_t_485 = open(name, O_CREAT | O_RDWR | O_TRUNC, 0600);
		for (mtidx = 0; mtidx < T_485_ROWS_CNT; mtidx ++)
			t485_init_row(mtidx + 1);
		safe_write(fd_t_485, p_t_485, size);
		fdatasync(fd_t_485);
		close(fd_t_485);
	}
	fd_t_485 = open(name, O_RDWR);
	safe_read(fd_t_485, p_t_485, size);
}

void t485_update(short mtidx, int flush_flag)
{
	lseek(fd_t_485, mtidx * sizeof(T_485), SEEK_SET);
	safe_write(fd_t_485, p_t_485 + mtidx, sizeof(T_485));
	if (flush_flag) t485_flush();
}

void t485_flush(void)
{
	fdatasync(fd_t_485);
}

void t485_close(void)
{
	fdatasync(fd_t_485);
	close(fd_t_485);
	sem_destroy(&sem_t_485);
}
