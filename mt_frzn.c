/*****************************************************************************
*	ChangSha AMR Concentrator 
*
*	(c) Copyright 2006-2007, Shenzhen Kaifa Technology Co.,Ltd. 
*	All Rights Reserved 
*
*	broadcast day energy frozen hour
* 
*	Author: jianping zhang
*	Created on: 2007-03-08
*****************************************************************************/
#include "mt_frzn.h"
#include "mt_access.h"
#include "common.h"
#include "t_mt.h"
#include "t_stat.h"
#include "f_param.h"
#include "f_alm.h"
#include "misc.h"
#include "msg_code.h"
#include "plc.h"
#include "global.h"
#include "device.h"
#include "err_code.h"

static int step, change;
static short mt_arr[NBR_REAL_MT], mt_cnt, mt_idx;
static ST_BRMT brmt;
static BYTE frzn_hour;

static void frzn_check(short mtidx, const MTID mtid, BYTE nbr_retry,
	BYTE phase, BYTE hour)
{
	BYTE resp[PLC_APDU_LEN], alm_buf[1];
	int resp_len;

	resp_len = meter_read(mtidx, mtid, nbr_retry, resp, 1, 0xC117);
	if (resp_len > 0) {
		if (resp[0] != hour) { // broadcast not received
			broadcast_frzn_hour(mtidx, nbr_retry, phase, hour); // again */
		}
		else {
			fparam_lock();
			falm_lock();
			alm_buf[0] = 4;
			falm_add_meter(mtidx, 4, 1, alm_buf, 1);
			falm_unlock();
			fparam_unlock();
		}
	}
}

static int frzn_cond(const struct tm *tm, BYTE *frzn_hour)
{
	BYTE time[3];

	fparam_get_value(FPARAM_BROAD_FROZ_HOUR, time, 3);
	if (time[0] == 0xff || time[1] == 0xff || time[2] == 0xff)
		return 0;
	time[0] = bcd_to_bin(time[0]);
	time[1] = bcd_to_bin(time[1]);
	time[2] = bcd_to_bin(time[2]);
	if (tm->tm_mday == time[2] && (tm->tm_hour > time[1]
		|| (tm->tm_hour == time[1] && tm->tm_min >= time[0]))) {
		fparam_get_value(FPARAM_FROZ_HOUR, frzn_hour, 1);
		return 1;
	}
	return 0;
}

void frzn_event(EVENT event)
{
	fparam_lock();
	if (event == EVENT_POWER_ON || event == EVENT_DAY_CHANGE)
		step = 0;
	fparam_unlock();
}

void frzn_meter_change(void)
{
	fparam_lock();
	change = 1;
	fparam_unlock();
}

int mt_frzn(const struct tm *tm)
{
	BYTE phase, nbr_retry;
	short mtidx, need;
	MTID mtid;

	tmt_lock();
	fparam_lock();
	if (change) {
		step = change = 0;
	}
	need = 0;
	if (step == 0 && frzn_cond(tm, &frzn_hour)) {
		brmt_init(&brmt);
		step = 1;
	}
	if (step == 1) {
		if (brmt_next(&brmt, &mtidx, &phase)) {
			need = 1;
		}
		else {
			mtidx = -1;
			mt_cnt = 0;
			while ((mtidx = tmt_next2(mtidx)) >= 0)
				mt_arr[mt_cnt ++] = mtidx;
			mt_idx = 0;
			step = 2;
		}
	}
	if (step == 2) {
		if (mt_idx >= mt_cnt) {
			step = 3;
		}
		else {
			mtidx = mt_arr[mt_idx ++];
			phase = tmt_get_phase(mtidx);
			tmt_get_mtid(mtidx, mtid);
			need = 2;
		}
	}
	fparam_unlock();
	tmt_unlock();
	nbr_retry = get_nbr_retry();
	if (need == 1) {
		LOG_PRINTF("frzn1: %d %d %d\n", mtidx, phase, frzn_hour);
		broadcast_frzn_hour(mtidx, nbr_retry, phase, frzn_hour);
	}
	else if (need == 2) {
		LOG_PRINTF("frzn2: %d %d %d\n", mtidx, phase, frzn_hour);
		frzn_check(mtidx, mtid, nbr_retry, phase, frzn_hour);
	}
	if (need) {
		fparam_lock();
		set_last_proc(PROC_MT_FRZN);
		set_last_mtidx(mtidx);
		fparam_unlock();
	}
	return need;
}
