/*
 *  C-Plan Concentrator
 *
 *  Copyright (C) 2006-2007, Shenzhen Kaifa Technology Co.,Ltd.
 *
 *  Author: dajiang wan
 *  Created on: 2007-02-27
 */
#include "common.h"
#include "f_alm.h"

int afn0e_get_fn(BYTE pn, BYTE fn, BYTE *out_buf, int max_len,
	const BYTE *param, int *param_len)
{
	int ret = 0;

	PRINTF("AFN=0EH, Pn=%d, Fn=%d\n", pn, fn);
	switch (fn) {
	case 1:	// important event
	case 2: // normal event
		if (*param_len >= 2) {
			*param_len = 2;
			falm_lock();
			ret = falm_get(out_buf, max_len, 0x0e, fn, param[0], param[1]);
			falm_unlock();
		}
		break;
	default:
		break;
	}
	return ret;
}
