/*****************************************************************************
 *	C-Plan Concentrator 
 *
 *	(c) Copyright 2006-2007, Shenzhen Kaifa Technology Co.,Ltd. 
 *	All Rights Reserved 
 *		
 *****************************************************************************/

#ifndef _ERR_CODE_H
#define _ERR_CODE_H

/* ERROR CODE FOR ABNORMAL RESPONSE TO CENTRE */

/* ITEMS FROM THE FORMAL PROTOCOL */
#define ERR_OK				0x00	/* 00H NO ERROR */
#define ERR_BAD_VALUE		0x01	/* 01H INVALID VALUE */

/* ITEMS EXPANDED BY KAIFA */
#define ERR_BAD_PHASE		0x51	/* INVALID PHASE */
#define ERR_BAD_CODE		0x52	/* INVALID MSG CODE */
#define ERR_UNKNOWN_PATH	0x53	/* UNKNOWN PATH */
#define ERR_SYNC			0x54	/* CON NO TIME REFERENCE */
#define ERR_NODE_UR			0x55	/* NODE UNREACHABLE */
#define ERR_RPT1_UR			0x60	/* REPEATER 1 UNREACHABLE */
#define ERR_RPT2_UR			0x61	/* REPEATER 2 UNREACHABLE */
#define ERR_RPT3_UR			0x62	/* REPEATER 3 UNREACHABLE */
#define ERR_RPT4_UR			0x63	/* REPEATER 4 UNREACHABLE */
#define ERR_RPT5_UR			0x64	/* REPEATER 5 UNREACHABLE */
#define ERR_RPT6_UR			0x65	/* REPEATER 6 UNREACHABLE */
#define ERR_RPT7_UR			0x66	/* REPEATER 7 UNREACHABLE */

/* ERROR CODE FOR ABNORMAL RESPONSE FROM METER */
#define ET_DATA				0x01	/* INVALID DATA */ 
#define ET_BAD_DI			0x02	/* ERROR DATA IDEN */
#define ET_PSW				0x04	/* ERROR PASSWORD */
#define ET_PERIOD 			0x10	/* ERROR DAY PERIOD FOR TARIFF */
#define ET_TARIFF			0x20	/* ERROR TARIFF TYPE */

#endif /* _ERR_CODE_H */
