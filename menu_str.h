#ifndef _MENU_STR_H
#define _MENU_STR_H

#include "typedef.h"

#define MAX_FORMAT_LEN	40

extern const char *PROTOCOL_TYPE_INVALID_STR;
extern const char *PROTOCOL_TYPE_645_STR;
extern const char *PROTOCOL_TYPE_AC_SAMPLE_STR;
extern const char *METER_TYPE_RS485_STR;
extern const char *METER_TYPE_PLC_STR;
extern const char *METER_TYPE_SIMPLE_STR;
extern const char *METER_TYPE_MULTI_STR;
extern const char *METER_TYPE_REPEATER_STR;
extern const char *PREPAY_TYPE_ENERGY_STR;
extern const char *PREPAY_TYPE_FEE_STR;
extern const char *PREPAY_TYPE_NONO_STR;
extern const char *CONNECT_TYPE_DIRECT_STR;
extern const char *CONNECT_TYPE_NON_DIRECT_STR;
extern const char *SPMT_STR;
extern const char *PPMT_STR;
extern const char *NO_PHASE_STR;
extern const char *PHASE_STR;
extern const char *PHASE_A_STR;
extern const char *PHASE_B_STR;
extern const char *PHASE_C_STR;
extern const char *NORMAL_USER_STR;
extern const char *FOCUS_USER_STR;
extern const char *UNSELECT_STR;
extern const char *SELECT_STR;
extern const char *NO_RELAY_STR;
extern const char *RELAY_STR;
extern const char *ENABLE_POWEROFF_STR;
extern const char *DISABLE_POWEROFF_STR;
extern const char *UNKNOWN_STR;
extern const char *REBOOT_STR;

extern const char *c_lvc_test_str1[];
extern const char *c_lvc_test_str2[];
extern const char *c_pls_input_key_str[];
extern const char *c_pls_input_yx_str[];
extern const char *c_lvc_test_cb_ok_str;
extern const char *c_lvc_test_cb_ko_str;

extern const char *c_lvc_test_prompt_info_str[];

extern const char *menu_name1;
extern const char *menu_name1_1;
extern const char *menu_name1_2[];
extern const char *menu_name1_2_0[];
extern const char *menu_name2;
extern const char *menu_name2_1;
extern const char *menu_name2_2;
extern const char *menu_name2_3;
extern const char *menu_name2_4;
extern const char *menu_name2_5;
extern const char *menu_name2_6;
extern const char *menu_name2_7;
extern const char *menu_name2_8;
extern const char *menu_name2_8_1;
extern const char *menu_name2_8_2;
extern const char *menu_name2_8_3;
extern const char *menu_name2_8_4;
extern const char *menu_name2_8_5;
extern const char *menu_name3;
extern const char *menu_name3_1;
extern const char *menu_name3_2;
extern const char *menu_name3_2_1;
extern const char *menu_name3_2_2;
extern const char *menu_name3_2_3;
extern const char *menu_name3_2_4;
extern const char *menu_name3_2_5;
extern const char *menu_name3_3;
extern const char *menu_name3_4;
extern const char *menu_name3_5;
extern const char *menu_name3_6;
extern const char *menu_name3_8;
extern const char *menu_name3_9;
extern const char *menu_name3_9_1;
extern const char *menu_name3_9_2;
extern const char *menu_name3_9_3;

extern const char *c_numeric_list[];
extern const char *c_caps_numeric[];
extern const char *c_sample_di_str[];
extern const char *c_terminal_info_str[];
extern const char *c_calibrate_clock_str[];
extern const char *c_menu_password_str[];
extern const char *c_up_channel_type_str[];
extern const char *c_ip_address_str;
extern const char *c_port_str;
extern const char *c_username_str;
extern const char *c_password_str;
extern const char *c_host_phone_str;
extern const char *c_sms_center_phone_str;
extern const char *c_cascade_parameter_str[];
extern const char *c_sample_doc_str[];
extern const char *c_usermt_doc_str[];
extern const char *c_sample_basic_str[];
extern const char *c_sample_limit_str[];
extern const char *c_con_event_stat_str[];
extern const char *c_no_str;
extern const char *c_no_this_event_str;
extern const char *c_view_erc_info_str[];
extern const char *c_con_erc_record_str[];
extern const char *c_meter_erc_record_str[];
extern const char *c_con_malfunction_str[];
extern const char *c_read_usermt_status_str[];
extern const char *c_usermt_history_str[];
extern const char *c_usermt_realtime_str[];
extern const char *c_process_status[];
extern const char *c_arrow_str[];
extern const char *c_numeric_keyboard_str[];
extern const char *c_lower_letter_keyboard_str[];
extern const char *c_upper_letter_keyboard_str[];

extern const char *con_address_str;
extern const char *c_info_sys_init_str;
extern const char *c_info_sys_ready_str;
extern const char *c_info_main_menu_str;
extern const char *c_info_comm_str[];
extern const char *c_info_task_exec_str[];

struct scroll_screen {
	unsigned short screen;
	unsigned short di;
	unsigned short data_off;
	unsigned short data_len;
};

int format_di_str(void *buf, int len, int di, const void *data, int data_len);

int get_scroll_screen(int idx, struct scroll_screen *screen, int max_cnt);

#endif /* _MENU_STR_H */
