#ifndef _LCD_H
#define _LCD_H

#include "typedef.h"

#define MAX_SCREEN_ROW			lcd_screen_row()
#define MAX_SCREEN_COL			lcd_screen_col()
#define STATUS_BAR_LINE			1
#define INFO_BAR_LINE			1
#define MAX_WORKSPACE_LINE		(MAX_SCREEN_ROW-STATUS_BAR_LINE-INFO_BAR_LINE)

#define MAX_MENU_LINE			100
#define MAX_INPUT_GROUP_NUM		100
#define MAX_INPUT_DIGIT_NUM		16
#define MAX_INPUT_STRING_LEN	16
#define MAX_LIST_NUM			32

/* basic menu */
typedef struct {
	int row;
	int col;
	int vrow_num;
	int vcol_num;
	int line_num;
	int start_line;
	const char *str[MAX_MENU_LINE];
} MENU;

/* used in ITEMS_MENU, flag: 0: view, 1: set */
typedef void (*ITEM_FUNC)(BYTE flag, void *para, const char *info);

/* used to select menu, from main menu to submenu */
typedef struct {
	MENU menu;
	int cur_line;
	ITEM_FUNC func[MAX_MENU_LINE];
	void *para[MAX_MENU_LINE];
	BYTE flag[MAX_MENU_LINE]; /* 0: can't set the item; 1: view and set. */
} ITEMS_MENU;

/* used in PARAM_LIST */
typedef struct {
	int row;
	int col;
	WORD digit_num;
	BYTE value_idx[MAX_INPUT_DIGIT_NUM];
	WORD list_num;
	const char *const_list[MAX_LIST_NUM];
	WORD max_value;
} INPUT_LIST;

/* used to list information of the concentrator */
typedef struct {
	MENU menu;
	WORD group_num;
	int group_idx;
	int digit_idx;
	INPUT_LIST list[MAX_INPUT_GROUP_NUM];
	/* flag, 1: several digits are a union, such as "645",
			 0: several digits are alone */
	BYTE flag[MAX_INPUT_GROUP_NUM];
} PARAM_LIST;

/* used to list document information */
typedef struct {
	int row;
	WORD idx_num;
	int start_idx;
	int cur_idx;
	MENU menu;
} DOC_LIST;

/* used in PARAM_SET */
typedef struct {
	int row;
	int col;
	int len;
	char str[MAX_INPUT_STRING_LEN];
	int list_idx;
	int list_num;
	const char *const_list[MAX_LIST_NUM];
	int maxlen;
} INPUT_STRING;

typedef enum {
	_num_keyboard = 0,
	_lower_letter_keyboard,
	_upper_letter_keyboard,
	_symbol_keyboard,
	_none_keyboard,
} keyboard_t;

/* used to set parameters of concentrator */
typedef struct {
	MENU menu;
	WORD group_num;
	int group_idx;
	INPUT_STRING input[MAX_INPUT_GROUP_NUM];
	keyboard_t keyboard_type[MAX_INPUT_GROUP_NUM];
} PARAM_SET;

void lcd_lock(void);

void lcd_unlock(void);

void lcd_update_lock(void);

void lcd_update_unlock(void);

int lcd_is_ok(void);

void lcd_open(int lcd_type, int font_size);

void lcd_close(void);

void lcd_show_cursor(int row, int col, BYTE mode);

void lcd_show_underline(int row, int col1, int col2, int pixel);

void lcd_show_string(int row, int col, int len, const void *buf);

void lcd_reverse_string(int row, int col1, int col2);

int lcd_save_window(int row1, int col1, int row2, int col2, void **buf,
	int *len);

int lcd_restore_window(int row1, int col1, int row2, int col2, void *buf,
	int len);

int lcd_screen_row(void);

int lcd_screen_col(void);

void lcd_clear_screen(void);

void lcd_show_screen(BYTE *buf);

void lcd_show_lines(void);

void lcd_update_head_info(void);

void lcd_update_info(const char *buf);

int lcd_key_in(void);

void lcd_clean_workspace(void);

void lcd_show_arrow(int up, int down, int left, int right);

#endif /* _LCD_H */
