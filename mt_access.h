/*****************************************************************************
 *	C-Plan Concentrator 
 *
 *	(c) Copyright 2006-2007, Shenzhen Kaifa Technology Co.,Ltd. 
 *	All Rights Reserved 
 *		
 *****************************************************************************/

#ifndef _MT_ACCESS_H
#define _MT_ACCESS_H

#include "typedef.h"

typedef struct {
	short rpt_arr[NBR_REAL_MT + 3];
	short rpt_cnt, rpt_idx;
} ST_BRMT;

int meter_read_disable(const struct tm *tm);

int check_meter_clock_state(short mtidx, const BYTE *buf, time_t tt);

void brmt_init(ST_BRMT *brmt);

int brmt_next(ST_BRMT *brmt, short *mtidx, BYTE *phase);

void event_change(EVENT event);

int dlc_comm_set(const BYTE *req, int req_len, short mt_idx, const BYTE *rpt);

int dlc_comm_get(const BYTE *req, int req_len, BYTE *resp, int resp_len,
	BYTE method, short mt_idx, const BYTE *rpt);

int meter_read(short mtidx, const MTID mtid, BYTE nbr_retry, void *buf,
	int id_cnt, ...);

int meter_write(short mtidx, const MTID mtid, BYTE nbr_retry, const void *key,
	int id_cnt, ...);

int meter_read_array(short mtidx, const MTID mtid, BYTE nbr_retry, void *buf,
	WORD *id_arr, int id_cnt);

int read_frozen_energy(short mtidx, const MTID mtid, BYTE nbr_retry, short di,
	BYTE *resp, int *resp_len);

#endif // _MT_ACCESS_H
