#ifndef _FONT_H
#define _FONT_H

int font_init(int size);

void font_destroy(void);

int text_out(int x, int y, const void *buf, int len,
	void (*pixel_out)(int x, int y, int pixel));

#endif /* _FONT_H */
