/*
 *  C-Plan Concentrator
 *
 *  Copyright (C) 2008, Shenzhen Kaifa Technology Co.,Ltd.
 *
 *  Author: jianping zhang
 *  Created on: 2008-01-14
 */

#include "lvc_test.h"
#include "common.h"
#include "device.h"
#include "cb_access.h"
#include "mt_access.h"
#include "misc.h"
#include "t_mt.h"
#include "lcd.h"
#include "input.h"
#include "menu_str.h"
#include "t_db.h"
#include "msg_que.h"
#include "global.h"
#include "threads.h"
#include "plc.h"
#include "display.h"
#include "f_param.h"

#define TEST_YX_EN			1
#define TEST_LED_EN			1
#define TEST_RTC_EN			1
#define TEST_PLC_EN			1
#define TEST_RS485_EN		1
#define TEST_HHU_EN			1
#define TEST_MODEM_EN		1
#define TEST_MODEM_RELAY_EN	1
#define TEST_PLC_RELAY_EN	1
#define TEST_KEY_EN			1
#define TEST_LCD_EN			1

typedef enum {
	st_result_fail = 0,
	st_result_ok,
	st_result_err = -1,
} test_result_t;

static BYTE new_seq = 0xff, cur_seq = 0xff, new_mt_cnt = 0, test_step = 1;
static pthread_t th_lvc_test, th_led;
static MTID test_mtid[3] = {
	{0xff, 0xff, 0xff, 0xff, 0xff, 0xff,},
	{0xff, 0xff, 0xff, 0xff, 0xff, 0xff,},
	{0xff, 0xff, 0xff, 0xff, 0xff, 0xff,},
};
static test_result_t test_result[TEST_MAX_NUM];
static int plc_cb_result[4]; // 0..2 for 'A'..'C', 3 for CB
static int lcd_ok, lvc_test_cnt;

static void show_test_result(int idx, int test_end)
{
	char buf[MAX_SCREEN_COL + 1], *ptr = buf;
	int i, spaces_nbr;
	
	if (!lcd_ok)
		return;
	sprintf(ptr, "%s", c_lvc_test_str1[idx]);
	ptr += strlen(ptr);
	if (test_end == 1) {
		if (idx == TEST_PLC) {
			if (new_mt_cnt == 3) {
				if (!(plc_cb_result[0] || plc_cb_result[1] || plc_cb_result[2]))
					sprintf(ptr, "%s", c_lvc_test_str2[1]);
				else
					sprintf(ptr, "%s%s%s%s%s", plc_cb_result[0] ? "A" : "",
						plc_cb_result[1] ? "B" : "",plc_cb_result[2] ? "C" : "",
						PHASE_STR, c_lvc_test_str2[0]);
			}
			else {
				if (plc_cb_result[3])
					sprintf(ptr, "%s", c_lvc_test_cb_ok_str);
				else
					sprintf(ptr, "%s", c_lvc_test_cb_ko_str);
			}
		}
		else {
			if (test_result[idx] == st_result_ok)
				sprintf(ptr, "%s", c_lvc_test_str2[0]);
			else
				sprintf(ptr, "%s", c_lvc_test_str2[1]);
		}
	}
	else
		sprintf(ptr, "%s", "...");
	ptr += strlen(ptr);
	spaces_nbr = sizeof(buf) - strlen(buf) - 1;
	for (i = 0; i < spaces_nbr; i ++)
		*(ptr + i) = ' ';
	if (idx > MAX_SCREEN_ROW)
		idx = idx - MAX_SCREEN_ROW;
	lcd_show_string(idx, 1, sizeof(buf), buf);
	wait_delay(1000);
}

static void lvc_test_lcd(void)
{
#if TEST_LCD_EN
	void *save_ptr;
	int save_len;
	BYTE buf[20];
	
	PRINTF("LCD test start ...\n");
	lcd_save_window(1, 1, MAX_SCREEN_ROW, MAX_SCREEN_COL,
			&save_ptr, &save_len);
	memset(buf, 0, sizeof(buf));
	lcd_show_screen(buf);
	wait_delay(1000);
	memset(buf, 0xff, sizeof(buf));
	lcd_show_screen(buf);
	wait_delay(1000);
	memset(buf, 0xaa, sizeof(buf));
	lcd_show_screen(buf);
	test_result[TEST_LCD] = st_result_ok;
	PRINTF("LCD test OK.\n");
	lcd_restore_window(1, 1, MAX_SCREEN_ROW, MAX_SCREEN_COL,
			save_ptr, save_len);
	PRINTF("LCD test finish.\n");
#else
	return;
#endif
}

static void lvc_test_led(void)
{
#if TEST_LED_EN
	const char *buf1 = "\x80\x80\x80\x80\x80\x80\x80\x80";
	const char *buf2 = "The strings are used to test led\n";
	int i, status = 0x01;
	BYTE cb_state;
	
	PRINTF("LED test start ...\n");
	show_test_result(TEST_LED, 0);
	PRINTF("LED test down-stream channel ...\n");
	for (i = 0; !g_terminated && i < 10; i ++) {
		plc_write(buf1, strlen(buf1), 200);
		get_cb_state(&cb_state);
	}
	PRINTF("LED test cascade channel ...\n");
	for (i = 0; !g_terminated && i < 10; i ++)
		cas_write(buf2, strlen(buf2), 200);
	device_status_led(status);
	status = (~status) & 0x03;
	test_result[TEST_LED] = st_result_ok;
	show_test_result(TEST_LED, 1);
	device_status_led(0);
	PRINTF("LED test finish.\n");
#else
	return;
#endif
}

static void lvc_test_rtc(void)
{
#if TEST_RTC_EN
	static int cmd_exec;
	
	PRINTF("RTC test start ...\n");
	if (cmd_exec == 0) {
		system("hwclock -w");
		cmd_exec = 1;
	}
	if (check_rtc()) {
		test_result[TEST_RTC] = st_result_ok;
		PRINTF("RTC test OK\n");
	}
	else
		PRINTF("RTC test fail\n");
	show_test_result(TEST_RTC, 1);
	PRINTF("RTC test finish\n");
#else
	return;
#endif
}

static void lvc_test_plc(MTID *mtid)
{
#if TEST_PLC_EN
	const MTID default_mtid = {0x01, 0x00, 0x00, 0x00, 0x00, 0x00};
	BYTE cb_state, req[PLC_APDU_LEN], resp[PLC_APDU_LEN];
	BYTE phase[3] = {PHASE_A, PHASE_B, PHASE_C};
	RPT_PATH path;
	int i, broad, timeout, req_len, resp_len, ret = COMM_ERR;
	
	PRINTF("PLC test start ...\n");
	show_test_result(TEST_PLC, 0);
	if (new_mt_cnt == 0) {
		if (!get_cb_state(&cb_state)) {
			test_result[TEST_PLC] = st_result_fail;
			plc_cb_result[3] = 0;
		}
		else {
			test_result[TEST_PLC] = st_result_ok;
			plc_cb_result[3] = 1;
		}
	}
	else {
		req_len = my_pack(req, "css", 0x01, 0xC010, 0xC011);
		for (i = 0; i < new_mt_cnt; i ++) {
			if (memcmp(mtid[i], INVALID_MTID, MTID_LEN) == 0)
				memcpy(mtid[i], default_mtid, MTID_LEN);
			if (memcmp(mtid, BR_MTID, MTID_LEN) == 0)
				broad = 1;
			else
				broad = 0;
			path.count = 0;
			plc_lock();
			plc_send(0, !broad, mtid[i], phase[i], &path,
					req, req_len, PLC_APDU_LEN, &timeout);
			if (broad)
				msleep(timeout);
			else
				ret = plc_recv(mtid[i], timeout, resp, PLC_APDU_LEN, &resp_len);
			plc_unlock();
			if (ret == COMM_OK)
				plc_cb_result[i] = 1;
			else
				plc_cb_result[i] = 0;
		}
		if (plc_cb_result[0] == 1 && plc_cb_result[1] == 1 && plc_cb_result[2] == 1)
			test_result[TEST_PLC] = st_result_ok;
		else
			test_result[TEST_PLC] = st_result_fail;
	}
	if (test_result[TEST_PLC] == st_result_ok)
		PRINTF("PLC test OK.\n");
	else
		PRINTF("PLC test fail.\n");
	show_test_result(TEST_PLC, 1);
	PRINTF("PLC test finish.\n");
#else
	return;
#endif
}

static void lvc_test_rs485(void)
{
#if TEST_RS485_EN
	const char *buf1 = "The string from cascade channel to general meter \
		channel is used for test rs485\n";
	const char *buf2 = "The string from general meter channel to cascade \
		channel is used for test rs485\n";
	int outlen1 = strlen(buf1);
	int outlen2 = strlen(buf2);
	BYTE receive[255], state = 0;

	PRINTF("RS485 test start ...\n");
	show_test_result(TEST_RS485, 0);
	cas_lock();
	rs485_lock();
	while (cas_read(receive, sizeof(receive), 500, 500) != 0)
		;
	while (rs485_read(receive, sizeof(receive), 500, 500) != 0)
		;
	cas_write(buf1, outlen1, 200);
	memset(receive, 0, sizeof(receive));
	if (!(rs485_read(receive, outlen1, 500, 500) == outlen1
		&& memcmp(receive, buf1, outlen1) == 0)) {
		state = 1; // from cascade channel to general meter channel fail.
	}
	rs485_write(buf2, outlen2, 200);
	memset(receive, 0, sizeof(receive));
	if (cas_read(receive, outlen2, 500, 500) == outlen2
		&& memcmp(receive, buf2, outlen2) == 0) {
		if (state == 0)
			test_result[TEST_RS485] = st_result_ok;
	}
	else {
		if (state == 1)
			state = 3; // RS485-1 and RS485-2 are both fail.
		else
			state = 2; // from general meter channel to cascade channel fail.
	}
	cas_unlock();
	rs485_unlock();
	if (test_result[TEST_RS485] == st_result_ok) {
		PRINTF("RS485 test OK.\n");
	}
	else {
		switch (state) {
		case 1:
			PRINTF("RS485 test fail. \
					(RS485 (2 -> 1) is OK, RS485 (1 -> 2) is fail)\n");
			break;
		case 2:
			PRINTF("RS485 test fail. \
					(RS485 (1 -> 2) is OK, RS485 (2 -> 1) is fail)\n");
			break;
		case 3:
			PRINTF("RS485 test fail. (RS485 (1 and 2) are both fail)\n");
			break;
		default:
			PRINTF("RS485 test error.\n");
			break;
		}
	}
	show_test_result(TEST_RS485, 1);
	while (cas_read(receive, sizeof(receive), 500, 500) != 0)
		;
	while (rs485_read(receive, sizeof(receive), 500, 500) != 0)
		;
	PRINTF("RS485 test finish.\n");
#else
	return;
#endif
}

static void lvc_test_hhu(void)
{
#if TEST_HHU_EN
	PRINTF("HHU test start ...\n");
	show_test_result(TEST_HHU, 0);
	test_result[TEST_HHU] = st_result_ok;
	if (test_result[TEST_HHU] == st_result_ok)
		PRINTF("HHU test OK.\n");
	else
		PRINTF("HHU test fail.\n");
	show_test_result(TEST_HHU, 1);
	PRINTF("HHU test finish.\n");
#else
	return;
#endif
}

static void lvc_test_modem(void)
{
#if TEST_MODEM_EN
	PRINTF("MODEM test start ...\n");
	show_test_result(TEST_MODEM, 0);
	switch (test_modem(modem_device, modem_lockname)) {
	case 0:
		test_result[TEST_MODEM] = st_result_fail;
		PRINTF("MODEM test fail.\n");
		break;
	case 1:
		test_result[TEST_MODEM] = st_result_ok;
		PRINTF("MODEM test OK, but SIM card is not exist.\n");
		break;
	case 2:
		test_result[TEST_MODEM] = st_result_ok;
		PRINTF("MODEM test OK, and SIM card is OK.\n");
		break;
	default:
		PRINTF("MODEM test error.\n");
		break;
	}
	show_test_result(TEST_MODEM, 1);
	PRINTF("MODEM test finish.\n");
#else
	return;
#endif
}

static void lvc_test_modem_relay(void)
{
#if TEST_MODEM_RELAY_EN
	PRINTF("MODEM RELAY test start ...\n");
	show_test_result(TEST_MODEM_RELAY, 0);
	if (check_modem_relay(modem_device, modem_lockname)) {
		test_result[TEST_MODEM_RELAY] = st_result_ok;
		PRINTF("MODEM RELAY test OK\n");
	}
	else {
		test_result[TEST_MODEM_RELAY] = st_result_fail;
		PRINTF("MODEM RELAY test fail\n");
	}
	show_test_result(TEST_MODEM_RELAY, 1);
	PRINTF("MODEM RELAY test finish.\n");
#else
	return;
#endif
}

static void lvc_test_plc_relay(void)
{
#if TEST_PLC_RELAY_EN
	int ret;
	BYTE cb_state;

	PRINTF("PLC RELAY test start ...\n");
	show_test_result(TEST_PLC_RELAY, 0);
	eb_power_off();
	PRINTF("EB power off\n");
	PRINTF("Waiting for 2s ...\n");
	wait_delay(2000);
	ret = get_cb_state(&cb_state);
	eb_power_on();
	PRINTF("EB power on\n");
	PRINTF("Waiting for 2s ...\n");
	wait_delay(2000);
	if (ret == 0) {
		test_result[TEST_PLC_RELAY] = st_result_ok;
		PRINTF("PLC RELAY test OK\n");
	}
	else {
		test_result[TEST_PLC_RELAY] = st_result_fail;
		PRINTF("PLC RELAY test fail\n");
	}
	show_test_result(TEST_PLC_RELAY, 1);
	PRINTF("PLC RELAY test finish.\n");
#else
	return;
#endif
}

static void lvc_test_key(void)
{
#if TEST_KEY_EN
	int step = 0;
	BYTE key, c_key[] = {
		KEY_LEFT, KEY_RIGHT, KEY_UP, KEY_DOWN, KEY_ESC, KEY_ENTER
	};
	void *save_ptr;
	int save_len;
	
	PRINTF("KEY test start ...\n");
	show_test_result(TEST_KEY, 0);
	while (key_getch() != KEY_NONE)
		;
	lcd_save_window(1, 1, MAX_SCREEN_ROW, MAX_SCREEN_COL,
			&save_ptr, &save_len);
	while (!g_terminated) {
		lcd_show_string(step + 2, 3, strlen(c_pls_input_key_str[step]),
				c_pls_input_key_str[step]);
		key = getch_timeout();
		if (key == KEY_NONE)
			break;
		if (key == c_key[step]) {
			lcd_show_string(step + 2, 18, 2, "OK");
			step ++;
		}
		if (step == sizeof(c_key) / sizeof(BYTE))
			break;
	}
	lcd_restore_window(1, 1, MAX_SCREEN_ROW, MAX_SCREEN_COL,
			save_ptr, save_len);
	if (step == sizeof(c_key) / sizeof(BYTE))
		test_result[TEST_KEY] = st_result_ok;
	else
		test_result[TEST_KEY] = st_result_fail;
	show_test_result(TEST_KEY, 1);
	PRINTF("KEY test finish.\n");
#else
	return;
#endif
}

#if TEST_YX_EN
static int get_signal_input(int i)
{
	BYTE val;
	long idle_time;
	
	while (key_getch() != KEY_NONE)
		;
	idle_time = uptime();
	while (!g_terminated && uptime() - idle_time < 60) {
		if (key_getch() != KEY_NONE)
			return 0;
		val = device_signal_input();
		if ((val >> i) & 1)
			return 1;
		wait_delay(50);
	}
	return 0;
}
#endif

static void lvc_test_yx(void)
{
#if TEST_YX_EN
	int i;
	void *save_ptr;
	int save_len;
	
	PRINTF("YX test start ...\n");
	show_test_result(TEST_YX, 0);
	lcd_save_window(1, 1, MAX_SCREEN_ROW, MAX_SCREEN_COL,
			&save_ptr, &save_len);
	for (i = 0; i < 5; i ++) {
		lcd_show_string(i + 2, 2, strlen(c_pls_input_yx_str[i]), c_pls_input_yx_str[i]);
		if (get_signal_input(i))
			lcd_show_string(i + 2, 18, 2, "OK");
		else
			break;
	}
	lcd_restore_window(1, 1, MAX_SCREEN_ROW, MAX_SCREEN_COL,
			save_ptr, save_len);
	if (i == 5) {
		test_result[TEST_YX] = st_result_ok;
		PRINTF("YX test OK\n");
	}
	else {
		test_result[TEST_YX] = st_result_fail;
		PRINTF("YX test fail\n");
	}
	show_test_result(TEST_YX, 1);
	PRINTF("YX test finish\n");
#else
	return;
#endif
}

static void *lvc_test_thread(void *arg)
{
	long idle_time, relay_uptime;
	
	lvc_test_cnt = 1;
	test_step = 2;
	relay_uptime = 0;
	while (!g_terminated && lvc_test_cnt > 0) {
		memset(test_result, st_result_fail, sizeof(test_result));
		memset(plc_cb_result, 0, sizeof(plc_cb_result));
		cur_seq = new_seq;
		if (lcd_is_ok()) {
			lcd_ok = 1;
			lvc_test_lcd();
		}
		else {
			lcd_ok = 0;
		}
		if (!g_terminated && new_seq == cur_seq) {
			lvc_test_modem_relay();
			relay_uptime = uptime();
		}
		if (!g_terminated && new_seq == cur_seq)
			lvc_test_plc_relay();
		if (!g_terminated && new_seq == cur_seq)
			lvc_test_rtc();
		if (!g_terminated && new_seq == cur_seq)
			lvc_test_plc(test_mtid);
		if (!g_terminated && new_seq == cur_seq)
			lvc_test_rs485();
		if (!g_terminated && new_seq == cur_seq)
			lvc_test_hhu(); /* need 30s */
		if (!g_terminated && new_seq == cur_seq)
			lvc_test_led(); /* need 10s */
		while (!g_terminated && new_seq == cur_seq
			&& uptime() - relay_uptime <= 30)
			msleep(1000);
		if (!g_terminated && new_seq == cur_seq)
			lvc_test_modem();
		while (!g_terminated && key_getch() != KEY_NONE)
			;
		if (!g_terminated && new_seq == cur_seq)
			lvc_test_yx();
		if (!g_terminated && new_seq == cur_seq)
			lvc_test_key(); /* max 60s */
		idle_time = uptime();
		while (!g_terminated && uptime() - idle_time <= 5) {
			if (key_getch() == KEY_ESC)
				break;
			msleep(200);
		}
		lcd_clear_screen();
		lvc_test_cnt --;
	}
	test_step = 0;
	return NULL;
}

static void *poll_led_thread(void *arg)
{
	int status = 0x01;
	
	while (!g_terminated && lvc_test_cnt > 0) {
		device_status_led(status);
		wait_delay(500);
		status = ~status;
	}
	device_status_led(0x01);
	return NULL;
}

void lvc_test_request(BYTE seq, int mt_cnt, const void *mt_arr)
{
	const char *ptr = mt_arr;

	new_seq = seq;
	new_mt_cnt = mt_cnt;
	if (mt_cnt > 0) {
		memcpy(test_mtid[0], ptr, MTID_LEN); ptr += MTID_LEN;
		memcpy(test_mtid[1], ptr, MTID_LEN); ptr += MTID_LEN;
		memcpy(test_mtid[2], ptr, MTID_LEN); ptr += MTID_LEN;
	}
	lcd_mode_set(3);
}

int lvc_test_response(BYTE *out_buf, int max_len)
{
	BYTE *ptr = out_buf;
	int i;
	
	if (max_len < TEST_MAX_NUM + 2)
		return 0;
	*ptr ++ = cur_seq;
	if (test_step == 0) {
		*ptr ++ = TEST_MAX_NUM;
		for (i = 0; i < TEST_MAX_NUM; i ++)
			*ptr ++ = test_result[i];
	}
	else {
		*ptr ++ = 1;
		*ptr ++ = test_step;
	}
	return ptr - out_buf;
}

void lvc_test(void)
{
	lcd_clear_screen();
	pthread_create(&th_lvc_test, NULL, lvc_test_thread, NULL);
	pthread_create(&th_led, NULL, poll_led_thread, NULL);
	pthread_join(th_led, NULL);
	pthread_join(th_lvc_test, NULL);
	lcd_update_head_enable(1);
}
