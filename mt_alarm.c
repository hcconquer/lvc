/*****************************************************************************
*	C-Plan Concentrator
*
*	(c) Copyright 2006-2007, Shenzhen Kaifa Technology Co.,Ltd.
*	All Rights Reserved
*
*	To read the meters alarm data.
*
*	Author: djwan
*	Created on: 2008-06-04
*****************************************************************************/
#include "common.h"
#include "t_mt.h"
#include "t_stat.h"
#include "msg_code.h"
#include "mt_access.h"
#include "mt_alarm.h"
#include "f_mt.h"
#include "f_alm.h"
#include "f_param.h"
#include "misc.h"
#include "err_code.h"

static void alarm_read(short mtidx, const MTID mtid)
{
	BYTE resp[PLC_APDU_LEN], data[128], alm[128], tmp, nbr_retry;
	int resp_len, data_len;
	time_t tt;

	nbr_retry = get_nbr_retry();
	bzero(resp, sizeof(resp));
	resp_len = meter_read(mtidx, mtid, nbr_retry, resp, 7, 0xC011, 0xC010,
		0xC020, 0xe870, 0xe871, 0xe872, 0xe873);
	tt = time(NULL);
	tstat_lock();
	fparam_lock();
	falm_lock();
	if (resp_len >= 27) {
		check_meter_clock_state(mtidx, resp, tt);
		if ((data_len = tstat_get_data(mtidx, data, sizeof(data))) == 19) {
			if (memcmp(data, resp + 8, 2)) { // data of ID_e870
				memcpy(data, resp + 8, 2);
			}
			if (memcmp(data + 2, resp + 10, 7)) { // data of ID_e871
				memcpy(data + 2, resp + 10, 7);
				memcpy(alm, resp + 10, 5);
				alm[5] = 0x01;
				memcpy(alm + 6, resp + 15, 2);
				memset(alm + 8, INVALID_VALUE, 4);
				falm_add_meter(mtidx, 2, 0, alm, 12);
			}
			if (memcmp(data + 9, resp + 17, 5)) { // data of ID_e872
				memcpy(data + 9, resp + 17, 5);
				memcpy(alm, resp + 17, 5);
				alm[5] = 0x01;
				memset(alm + 6, INVALID_VALUE, 4);
				falm_add_meter(mtidx, 1, 0, alm, 10);
			}
			if (memcmp(data + 14, resp + 22, 5)) { // data of ID_e873
				memcpy(data + 14, resp + 22, 5);
			}
			tstat_set_data(mtidx, data, data_len);
		}
		tstat_set_time_reach(mtidx, tt);
		tstat_set_nbr_nr(mtidx, 0);
	}
	else {
		tstat_set_time_nr(mtidx, tt);
		tmp = tstat_get_nbr_nr(mtidx);
		if (tmp < 255)
			tstat_set_nbr_nr(mtidx, tmp + 1);
	}
	tstat_update(mtidx, 0);
	set_last_proc(PROC_MT_DAY);
	set_last_mtidx(mtidx);

	falm_unlock();
	fparam_unlock();
	tstat_unlock();
}

static void alarm_notify(void)
{
	int idx, interval = 60 * 60 * 24; // 1 day
	time_t tt;

	tstat_lock();
	fparam_lock();
	falm_lock();
	tt = time(NULL);
	idx = -1;
	while ((idx = tmt_next(idx)) >= 0) {
		if (tt - tstat_get_time_reach(idx) > interval) {
			if (tstat_allow_alarm(idx, 0))
				falm_add_meter_alm7(idx, 7);
		}
	}
	falm_unlock();
	fparam_unlock();
	tstat_unlock();
}

static int alarm_next_mtidx(short *mtidx, MTID mtid)
{
	while ((*mtidx = tmt_next(*mtidx)) >= 0) {
		if (!tmt_is_selected(*mtidx))
			continue;
		tmt_get_mtid(*mtidx, mtid);
		return 1;
	}
	return 0;
}

void alarm_event(EVENT event)
{
	// nothing todo
}

int mt_alarm(const struct tm *tm)
{
	static short mtidx = -1;
	static MTID mtid;
	static long last_idle_time = -10 * 60; /* 10 minutes */
	int need;

	if (uptime() - last_idle_time < 10 * 60)
		return 0;
	tmt_lock();
	need = alarm_next_mtidx(&mtidx, mtid);
	tmt_unlock();
	if (need) {
		alarm_read(mtidx, mtid);
	}
	else {
		alarm_notify();
		last_idle_time = uptime();
	}
	return need;
}
